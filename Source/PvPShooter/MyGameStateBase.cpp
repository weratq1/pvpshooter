// Fill out your copyright notice in the Description page of Project Settings.


#include "MyGameStateBase.h"
#include "UnrealNetwork.h"

void AMyGameStateBase::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AMyGameStateBase, GameScore);

}

TArray<FPlayerGameScore> AMyGameStateBase::GetSortedGameScore()
{
	GameScore.Sort([](const FPlayerGameScore& A, const FPlayerGameScore& B)
	{
		return A.Kills > B.Kills;
	});
	return GameScore;
}
