// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "Cpp_MyPlayerState.generated.h"

/**
 * 
 */



UCLASS()
class PVPSHOOTER_API ACpp_MyPlayerState : public APlayerState
{
	GENERATED_BODY()

		
public:
//ACpp_MyPlayerState();
		UPROPERTY(VisibleAnywhere, Replicated, BlueprintReadWrite)
		int Kills;
		UPROPERTY(VisibleAnywhere, Replicated, BlueprintReadWrite)
			int Death;

		UFUNCTION(BlueprintCallable)
			void SetPlayerNameFromBP(FString NewName);
		UFUNCTION(BlueprintCallable)
			TArray<ACpp_MyPlayerState*> SortArrayByScore(TArray<ACpp_MyPlayerState*> Players);
};
