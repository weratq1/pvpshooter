// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "MyGameStateBase.generated.h"

/**
 * 
 */

USTRUCT(BlueprintType)
struct FPlayerGameScore
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString PlayerName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Kills;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Death;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool TeamBlue;

};

UCLASS()
class PVPSHOOTER_API AMyGameStateBase : public AGameStateBase
{
	GENERATED_BODY()
public:
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite)
		TArray<FPlayerGameScore> GameScore;
	UFUNCTION(BlueprintCallable)
		TArray<FPlayerGameScore> GetSortedGameScore();

	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > &OutLifetimeProps) const;
};
