// Fill out your copyright notice in the Description page of Project Settings.
#include "MyGameModeBase.h"
#include "Runtime/JsonUtilities/Public/JsonObjectConverter.h"
#include "Runtime/Json/Public/Dom/JsonObject.h"
#include "Runtime/Core/Public/Misc/FileHelper.h"




FGameSettings AMyGameModeBase::ReadSettingsFomFile(int Gameid, FString GameDir)
{
	FString LoadJsonData;
	FGameSettings Info;
	FString FileDirectory = GameDir + "\\" + "ServerSettings" + FString::FromInt(Gameid) + ".txt";
	
	
	if (FFileHelper::LoadFileToString(LoadJsonData, *FileDirectory))
	{
		FJsonObjectConverter::JsonObjectStringToUStruct<FGameSettings>(LoadJsonData, &Info, 0, 0);
	}

	UE_LOG(LogTemp, Warning, TEXT("%s"), *LoadJsonData);
	CurrGameSettings = Info;
	return Info;

}
