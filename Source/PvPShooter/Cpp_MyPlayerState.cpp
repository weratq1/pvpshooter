// Fill out your copyright notice in the Description page of Project Settings.


#include "Cpp_MyPlayerState.h"
#include "UnrealNetwork.h"



void ACpp_MyPlayerState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ACpp_MyPlayerState, Kills);
	DOREPLIFETIME(ACpp_MyPlayerState, Death);
}

void ACpp_MyPlayerState::SetPlayerNameFromBP(FString NewName)
{
	SetPlayerName(NewName);
}

TArray<ACpp_MyPlayerState*> ACpp_MyPlayerState::SortArrayByScore(TArray<ACpp_MyPlayerState*> Players)
{
	TArray<ACpp_MyPlayerState*> Sorted;
	Sorted = Players;
	Sorted.Sort([](const ACpp_MyPlayerState& A, const ACpp_MyPlayerState& B)
	{
		return A.Kills > B.Kills;
	});
	return Sorted;
}

