// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MyGameModeBase.generated.h"

/**
 * 
 */

USTRUCT(BlueprintType)
struct FGameSettings
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		uint8 Rifle : 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		uint8 Shotgun :1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		uint8 Sniper :1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		uint8 Rocket : 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		uint8 Bomb : 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		uint8 Command : 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int RoundTimeMin;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int RoundCount;
};


UCLASS()
class PVPSHOOTER_API AMyGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
public:
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FGameSettings CurrGameSettings;
		UFUNCTION(BlueprintCallable)
		FGameSettings ReadSettingsFomFile(int Gameid, FString GameDir);
};
