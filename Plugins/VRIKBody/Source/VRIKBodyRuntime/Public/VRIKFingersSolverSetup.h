// Copyright Yuri N K, 2018. All Rights Reserved.
// Support: ykasczc@gmail.com

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Animation/AnimTypes.h"
#include "Components/SkeletalMeshComponent.h"
#include "VRIKFingersSolverSetup.generated.h"

/**
* Humanoid hand side, left or right
*/
UENUM(BlueprintType)
enum class EVRIK_VRHand : uint8
{
	VRH_Left				UMETA(DisplayName = "Left"),
	VRH_Right				UMETA(DisplayName = "Right"),
	VRH_MAX					UMETA(Hidden)
};

/**
* Arm visibility
*/
UENUM(BlueprintType)
enum class EVRIK_HandStyle : uint8
{
	HS_FullArm		UMETA(DisplayName = "Full Arm"),
	HS_Lowerarm		UMETA(DisplayName = "Lowerarm"),
	HS_HandOnly		UMETA(DisplayName = "Hand Only"),
	HS_MAX			UMETA(Hidden)
};

/**
* Bone axis value
*/
UENUM(BlueprintType)
enum class EVRIK_BoneOrientationAxis : uint8
{
	X				UMETA(DisplayName = "X+"),
	Y				UMETA(DisplayName = "Y+"),
	Z				UMETA(DisplayName = "Z+"),
	X_Neg			UMETA(DisplayName = "X-"),
	Y_Neg			UMETA(DisplayName = "Y-"),
	Z_Neg			UMETA(DisplayName = "Z-"),
	BOA_MAX			UMETA(Hidden)
};

/**
* Humanoid fingers
*/
UENUM(BlueprintType)
enum class EVRIKFingerName : uint8
{
	FN_Thumb		UMETA(DisplayName = "Thumb Finger"),
	FN_Index		UMETA(DisplayName = "Index Finger"),
	FN_Middle		UMETA(DisplayName = "Middle Finger"),
	FN_Ring			UMETA(DisplayName = "Ring Finger"),
	FN_Pinky		UMETA(DisplayName = "Pinky Finger"),
	EFingerName_MAX UMETA(Hidden)
};

/**
* Finger 3DOF rotation
*/
USTRUCT(BlueprintType)
struct VRIKBODYRUNTIME_API FVRIK_FingerRotation
{
	GENERATED_USTRUCT_BODY()

	/** One unit is 90 degrees */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Finger Rotation")
	float CurlValue;

	/** One unit is 20 degrees */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Finger Rotation")
	float SpreadValue;

	/** One unit is 20 degrees */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Finger Rotation")
	float RollValue;
};

/**
Transform struct with support of orientation transformations
*/
struct VRIKBODYRUNTIME_API FVRIK_OrientTransform : FTransform
{
	EVRIK_BoneOrientationAxis ForwardAxis;
	EVRIK_BoneOrientationAxis RightAxis;
	EVRIK_BoneOrientationAxis UpAxis;
	float RightAxisMultiplier;

private:
	FORCEINLINE FVector GetAxisVector(EVRIK_BoneOrientationAxis Axis) const
	{
		return GetAxisVector(*this, Axis);
	}

public:

	static FVector GetAxisVector(const FTransform& InTransform, EVRIK_BoneOrientationAxis Axis)
	{
		float DirMul = 1.f;
		uint8 convAxis = (uint8)Axis;

		if (Axis >= EVRIK_BoneOrientationAxis::X_Neg)
		{
			DirMul = -1.f;
			convAxis -= (uint8)EVRIK_BoneOrientationAxis::X_Neg;
		}
		return DirMul * InTransform.GetScaledAxis((EAxis::Type)(convAxis + 1));
	}

	static FVector GetAxisVector(const FRotator& InRotator, EVRIK_BoneOrientationAxis Axis)
	{
		switch (Axis)
		{
		case EVRIK_BoneOrientationAxis::X:
			return InRotator.Quaternion().GetForwardVector();
		case EVRIK_BoneOrientationAxis::Y:
			return InRotator.Quaternion().GetRightVector();
		case EVRIK_BoneOrientationAxis::Z:
			return InRotator.Quaternion().GetUpVector();
		case EVRIK_BoneOrientationAxis::X_Neg:
			return InRotator.Quaternion().GetForwardVector() * -1.f;
		case EVRIK_BoneOrientationAxis::Y_Neg:
			return InRotator.Quaternion().GetRightVector() * -1.f;
		case EVRIK_BoneOrientationAxis::Z_Neg:
			return InRotator.Quaternion().GetUpVector() * -1.f;
		}
		return FVector();
	}

	FVRIK_OrientTransform()
	{
		FTransform();
		ForwardAxis = EVRIK_BoneOrientationAxis::X;
		RightAxis = EVRIK_BoneOrientationAxis::Y;
		UpAxis = EVRIK_BoneOrientationAxis::Z;
		RightAxisMultiplier = 1.f;
	}

	FVRIK_OrientTransform(const FVRIK_OrientTransform& OrientationBase, const FTransform& InTransform,
		float InRightAxisMultiplier = 1.f,
		EVRIK_BoneOrientationAxis InForwardAxis = EVRIK_BoneOrientationAxis::X, EVRIK_BoneOrientationAxis InUpAxis = EVRIK_BoneOrientationAxis::Z)
	{
		*this = InTransform;

		ForwardAxis = OrientationBase.ForwardAxis; RightAxis = OrientationBase.RightAxis; UpAxis = OrientationBase.UpAxis;
		RightAxisMultiplier = InRightAxisMultiplier;

		const FVector V = GetAxisVector(InTransform, InForwardAxis);
		const FVector U = GetAxisVector(InTransform, InUpAxis);
		const FRotator ConvertedRot = MakeRotFromTwoAxis(ForwardAxis, V, UpAxis, U);
		SetRotation(ConvertedRot.Quaternion());
	}

	void SetTransform(const FTransform& InTransform)
	{
		CopyTranslation(InTransform);
		CopyRotation(InTransform);
		CopyScale3D(InTransform);
	}

	static FVRIK_OrientTransform MakeOrientTransform(const FVRIK_OrientTransform& OrientationBase, const FTransform& InTransform)
	{
		FVRIK_OrientTransform ret;
		ret.CopyTranslation(InTransform);
		ret.CopyRotation(InTransform);
		ret.CopyScale3D(InTransform);
		ret.ForwardAxis = OrientationBase.ForwardAxis; ret.RightAxis = OrientationBase.RightAxis; ret.UpAxis = OrientationBase.UpAxis;
		ret.RightAxisMultiplier = OrientationBase.RightAxisMultiplier;

		return ret;
	}

	static FRotator ConvertRotator(const FVRIK_OrientTransform& SourceRot, const FVRIK_OrientTransform& TargetRot)
	{
		return MakeRotFromTwoAxis(TargetRot.ForwardAxis, SourceRot.GetConvertedForwardVector(), TargetRot.RightAxis, SourceRot.GetConvertedRightVector());
	}

	FVector GetBoneForwardVector() const { return GetScaledAxis(EAxis::X); }
	FVector GetBoneRightVector() const { return GetScaledAxis(EAxis::Y); }
	FVector GetBoneUpVector() const { return GetScaledAxis(EAxis::Z); }

	FVector GetConvertedForwardVector() const { return GetAxisVector(ForwardAxis); }
	FVector GetConvertedRightVector() const { return GetAxisVector(RightAxis) * RightAxisMultiplier; }
	FVector GetConvertedUpVector() const { return GetAxisVector(UpAxis); }

	FVector GetConvertedForwardVectorUnscaled() const { return GetAxisVector(ForwardAxis); }
	FVector GetConvertedRightVectorUnscaled() const { return GetAxisVector(RightAxis); }
	FVector GetConvertedUpVectorUnscaled() const { return GetAxisVector(UpAxis); }

	static FRotator MakeRotFromTwoAxis(EVRIK_BoneOrientationAxis Axis1, const FVector& Vector1, EVRIK_BoneOrientationAxis Axis2, const FVector& Vector2)
	{
		FRotator r;
		FVector V1, V2;

		if (Axis1 == EVRIK_BoneOrientationAxis::X_Neg) {
			Axis1 = EVRIK_BoneOrientationAxis::X; V1 = -Vector1;
		}
		else if (Axis1 == EVRIK_BoneOrientationAxis::Y_Neg) {
			Axis1 = EVRIK_BoneOrientationAxis::Y; V1 = -Vector1;
		}
		else if (Axis1 == EVRIK_BoneOrientationAxis::Z_Neg) {
			Axis1 = EVRIK_BoneOrientationAxis::Z; V1 = -Vector1;
		}
		else V1 = Vector1;

		if (Axis2 == EVRIK_BoneOrientationAxis::X_Neg) {
			Axis2 = EVRIK_BoneOrientationAxis::X; V2 = -Vector2;
		}
		else if (Axis2 == EVRIK_BoneOrientationAxis::Y_Neg) {
			Axis2 = EVRIK_BoneOrientationAxis::Y; V2 = -Vector2;
		}
		else if (Axis2 == EVRIK_BoneOrientationAxis::Z_Neg) {
			Axis2 = EVRIK_BoneOrientationAxis::Z; V2 = -Vector2;
		}
		else V2 = Vector2;

		if (Axis1 == EVRIK_BoneOrientationAxis::X) {
			if (Axis2 == EVRIK_BoneOrientationAxis::Y)
				r = FRotationMatrix::MakeFromXY(V1, V2).Rotator();
			else if (Axis2 == EVRIK_BoneOrientationAxis::Z)
				r = FRotationMatrix::MakeFromXZ(V1, V2).Rotator();
		}
		else if (Axis1 == EVRIK_BoneOrientationAxis::Y) {
			if (Axis2 == EVRIK_BoneOrientationAxis::X)
				r = FRotationMatrix::MakeFromYX(V1, V2).Rotator();
			else if (Axis2 == EVRIK_BoneOrientationAxis::Z)
				r = FRotationMatrix::MakeFromYZ(V1, V2).Rotator();
		}
		else if (Axis1 == EVRIK_BoneOrientationAxis::Z) {
			if (Axis2 == EVRIK_BoneOrientationAxis::X)
				r = FRotationMatrix::MakeFromZX(V1, V2).Rotator();
			else if (Axis2 == EVRIK_BoneOrientationAxis::Y)
				r = FRotationMatrix::MakeFromZY(V1, V2).Rotator();
		}

		return r;
	}

	FORCEINLINE FVRIK_OrientTransform operator*(const FTransform& Other) const
	{
		FVRIK_OrientTransform ret;
		ret = (FTransform)*this * Other;
		ret.ForwardAxis = ForwardAxis; ret.RightAxis = RightAxis; ret.UpAxis = UpAxis; ret.RightAxisMultiplier = RightAxisMultiplier;
		return ret;
	}
	FORCEINLINE FVRIK_OrientTransform& operator=(const FTransform& Other)
	{
		CopyTranslation(Other);
		CopyRotation(Other);
		CopyScale3D(Other);
		return *this;
	}
	FORCEINLINE FVRIK_OrientTransform& operator=(const FVRIK_OrientTransform& Other)
	{
		CopyTranslation(Other);
		CopyRotation(Other);
		CopyScale3D(Other);
		ForwardAxis = Other.ForwardAxis; RightAxis = Other.RightAxis; UpAxis = Other.UpAxis; RightAxisMultiplier = Other.RightAxisMultiplier;
		return *this;
	}

	/** Helper. Find axis in transform rotation co-directed with vector */
	static EVRIK_BoneOrientationAxis FindCoDirection(const FTransform& BoneRotator, const FVector Direction)
	{
		EVRIK_BoneOrientationAxis RetAxis;
		const FVector dir = Direction.GetSafeNormal();
		const FVector v1 = FVRIK_OrientTransform::GetAxisVector(BoneRotator, EVRIK_BoneOrientationAxis::X);
		const FVector v2 = FVRIK_OrientTransform::GetAxisVector(BoneRotator, EVRIK_BoneOrientationAxis::Y);
		const FVector v3 = FVRIK_OrientTransform::GetAxisVector(BoneRotator, EVRIK_BoneOrientationAxis::Z);
		const float dp1 = FVector::DotProduct(v1, dir);
		const float dp2 = FVector::DotProduct(v2, dir);
		const float dp3 = FVector::DotProduct(v3, dir);

		if (FMath::Abs(dp1) > FMath::Abs(dp2) && FMath::Abs(dp1) > FMath::Abs(dp3))
		{
			RetAxis = dp1 > 0.f ? EVRIK_BoneOrientationAxis::X : EVRIK_BoneOrientationAxis::X_Neg;
		}
		else if (FMath::Abs(dp2) > FMath::Abs(dp1) && FMath::Abs(dp2) > FMath::Abs(dp3))
		{
			RetAxis = dp2 > 0.f ? EVRIK_BoneOrientationAxis::Y : EVRIK_BoneOrientationAxis::Y_Neg;
		}
		else
		{
			RetAxis = dp3 > 0.f ? EVRIK_BoneOrientationAxis::Z : EVRIK_BoneOrientationAxis::Z_Neg;
		}

		return RetAxis;
	}

	void DebugDraw(UWorld* World, bool bContinuous) const;
};

/**
* Description of finger knuckle. Initialized in UFingerFKIKSolver::Initialize.
*/
USTRUCT(BlueprintType)
struct VRIKBODYRUNTIME_API FVRIK_Knuckle
{
	GENERATED_USTRUCT_BODY()

	/** Name of skeleton bone */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Knuckle")
	FName BoneName;

	/** Index of bone in skeleton */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Knuckle")
	int32 BoneIndex;

	/** Knuckle radius */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Knuckle")
	float Radius;

	/** Distance to the next knuckle (or finger end) */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Knuckle")
	float Length;

	/** Instantaneous transform in world space */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Knuckle")
	FTransform WorldTransform;

	/** Current relative transform */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Knuckle")
	FTransform RelativeTransform;

	/** Relative transform from input animation skeleton */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Knuckle")
	FTransform RefPoseRelativeTransform;

	/** Relative transform from input animation pose */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Knuckle")
	FTransform TraceRefPoseRelativeTransform;

	/** Relative transform for vr input */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Knuckle")
	FTransform InputRefPoseRelativeTransform;
};

/**
* Figer description. Used both for input and output
*/
USTRUCT(BlueprintType)
struct VRIKBODYRUNTIME_API FVRIK_FingerSolver
{
	GENERATED_USTRUCT_BODY()

	/** Name of last knuckle bone */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Finger Solver")
	FName TipBoneName;

	/** Number of knuckles */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Finger Solver")
	int32 KnucklesNum;

	/** Radius of tip bone (smallest) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Finger Solver")
	float TipRadius;

	/** Radius of first bone */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Finger Solver")
	float RootRadius;

	/** Transform convertatation for finger */
	FVRIK_OrientTransform FingerOrientation;

	/** Outward local axis of the finger (normal to fingers plane) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Finger Solver")
	EVRIK_BoneOrientationAxis OutwardAxis;

	/** Should solver process this finger? */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Finger Solver")
	bool bEnabled;

	/** Current alpha to blend between input finger pose and calculated finger pose */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Finger Solver")
	float Alpha;

	/** Calculated: array of knuckles */
	UPROPERTY()
	TArray<FVRIK_Knuckle> Knuckles;

	/** Calculated: hand bone name */
	UPROPERTY()
	FName RootBoneName;

	FVRIK_FingerSolver()
	{
		KnucklesNum = 3; TipRadius = 0.4f; RootRadius = 0.8f; Alpha = 1.f; bEnabled = true;
		OutwardAxis = EVRIK_BoneOrientationAxis::Y_Neg;
	};
};

/** Struct to get knuckle transform by bone index */
USTRUCT()
struct VRIKBODYRUNTIME_API FVRIK_FingerKnucklePointer
{
	GENERATED_USTRUCT_BODY()

	/** Name of finger in UHandSkeletalMeshComponent::Fingers map */
	UPROPERTY()
	EVRIKFingerName FingerId;

	/** Index in FVRIK_FingerSolver::Knuckles array */
	UPROPERTY()
	int32 KnuckleId;
};

/** Struct to get twist rotation by bone index */
USTRUCT()
struct VRIKBODYRUNTIME_API FVRIK_TwistData
{
	GENERATED_USTRUCT_BODY()

	/** Pointer to twist variable */
	float* TwistSource;

	/** Twist multiplier from UHandSkeletalMeshComponent::UpperarmTwists or UHandSkeletalMeshComponent::LowerarmTwists */
	UPROPERTY()
	float Mulitplier;
};

/** Fingers pose description: rotation of all fingers */
USTRUCT(BlueprintType)
struct VRIKBODYRUNTIME_API FVRIK_FingersPosePreset
{
	GENERATED_USTRUCT_BODY()

	/** Thumb rotation. All axes usually require adjustment. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Finger Solver")
	FVRIK_FingerRotation ThumbRotation;

	/** Index finger rotation. Set curl value and others if necessary. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Finger Solver")
	FVRIK_FingerRotation IndexRotation;

	/** Middle finger rotation. Set curl value and others if necessary. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Finger Solver")
	FVRIK_FingerRotation MiddleRotation;

	/** Ring finger rotation. Set curl value and others if necessary. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Finger Solver")
	FVRIK_FingerRotation RingRotation;

	/** Pinky finger rotation. Set curl value and others if necessary. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Finger Solver")
	FVRIK_FingerRotation PinkyRotation;
};

/**
 * Global fingers settings. Create separate objects for all hands.
 */
UCLASS(BlueprintType)
class VRIKBODYRUNTIME_API UVRIKFingersSolverSetup : public UDataAsset
{
	GENERATED_BODY()
	
public:
	UVRIKFingersSolverSetup(const FObjectInitializer& ObjectInitializer);

	/**
	* Hand side associated with this asset
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setup")
	EVRIK_VRHand Hand;

	/**
	* Trace distance from knuckle to inside and outside
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setup")
	float TraceHalfDistance;

	/**
	* Trace channel to probe: Visible, Camera etc
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setup")
	TEnumAsByte<ECollisionChannel> TraceChannel;

	/**
	* Should trace by trace channel (false) or object type (true)
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setup")
	bool bTraceByObjectType;

	/**
	* Lower border for VR input (in degrees). VR input values (0..1) are interpolated to (InputMinRotation, InputMaxRotation)
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setup")
	float InputMinRotation;

	/**
	* Upper border for VR input (in degrees). VR input values (0..1) are interpolated to (InputMinRotation, InputMaxRotation)
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setup")
	float InputMaxRotation;

	/**
	* Interpolatoin speed for processing poses applied by SetFingersPose function
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setup")
	float PosesInterpolationSpeed;

	/**
	* Fingers settings.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Setup")
	TMap<EVRIKFingerName, FVRIK_FingerSolver> Fingers;
};
