// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VRIKBodyRuntime/Public/VRIKSkeletalMeshTranslator.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVRIKSkeletalMeshTranslator() {}
// Cross Module References
	VRIKBODYRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FSkeletalMeshSetup();
	UPackage* Z_Construct_UPackage__Script_VRIKBodyRuntime();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	VRIKBODYRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FBoneRotatorSetup();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	VRIKBODYRUNTIME_API UClass* Z_Construct_UClass_UVRIKSkeletalMeshTranslator_NoRegister();
	VRIKBODYRUNTIME_API UClass* Z_Construct_UClass_UVRIKSkeletalMeshTranslator();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalcTorsoIK();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugGetYawRotation();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugShowBonesOrientation();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DrawAxes();
	ENGINE_API UClass* Z_Construct_UClass_UWorld_NoRegister();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_ForceSetComponentAsCalibrated();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetLastPose();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FPoseSnapshot();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetMeshWorldTransform();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetSkeletalMeshSetup();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_Initialize();
	VRIKBODYRUNTIME_API UClass* Z_Construct_UClass_UVRIKBody_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USkeletalMeshComponent_NoRegister();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_IsInitialized();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_MakeRotByTwoAxes();
	COREUOBJECT_API UEnum* Z_Construct_UEnum_CoreUObject_EAxis();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_PrintBoneInfo();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RefreshComponentPosition();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RestoreSkeletalMeshSetup();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_UpdateSkeleton();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_UpdateSkeletonBonesSetup();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_VRIKBody_OnCalibrationComplete();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FQuat();
	VRIKBODYRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FIKBodyData();
// End Cross Module References
class UScriptStruct* FSkeletalMeshSetup::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern VRIKBODYRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FSkeletalMeshSetup_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSkeletalMeshSetup, Z_Construct_UPackage__Script_VRIKBodyRuntime(), TEXT("SkeletalMeshSetup"), sizeof(FSkeletalMeshSetup), Get_Z_Construct_UScriptStruct_FSkeletalMeshSetup_Hash());
	}
	return Singleton;
}
template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<FSkeletalMeshSetup>()
{
	return FSkeletalMeshSetup::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSkeletalMeshSetup(FSkeletalMeshSetup::StaticStruct, TEXT("/Script/VRIKBodyRuntime"), TEXT("SkeletalMeshSetup"), false, nullptr, nullptr);
static struct FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFSkeletalMeshSetup
{
	FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFSkeletalMeshSetup()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("SkeletalMeshSetup")),new UScriptStruct::TCppStructOps<FSkeletalMeshSetup>);
	}
} ScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFSkeletalMeshSetup;
	struct Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FirstPassBones_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FirstPassBones;
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_FirstPassBones_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RootBoneRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RootBoneRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FootLeftRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FootLeftRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FootRightRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FootRightRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PalmLeftRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PalmLeftRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PalmRightRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PalmRightRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LeftLegRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LeftLegRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RightLegRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RightLegRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LeftHandRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LeftHandRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RightHandRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RightHandRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LeftShoulderRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LeftShoulderRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RightShoulderRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RightShoulderRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HeadRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HeadRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RibcageRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RibcageRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PelvisRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PelvisRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MeshScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NeckBoneNames_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_NeckBoneNames;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_NeckBoneNames_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpineBoneNames_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SpineBoneNames;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_SpineBoneNames_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NeckBones_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_NeckBones;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_NeckBones_Key_KeyProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NeckBones_ValueProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpineBones_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_SpineBones;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_SpineBones_Key_KeyProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SpineBones_ValueProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RelativeRibcageOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RelativeRibcageOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FootToGroundLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FootToGroundLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FootToGroundRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FootToGroundRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RibcageToSpine_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RibcageToSpine;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HeadToNeck_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HeadToNeck;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UpperarmToShoulderLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UpperarmToShoulderLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UpperarmToShoulderRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UpperarmToShoulderRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShoulderToRibcageLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ShoulderToRibcageLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShoulderToRibcageRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ShoulderToRibcageRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_fForearmLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_fForearmLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_fUpperarmLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_fUpperarmLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshHeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MeshHeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshHandsFullLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MeshHandsFullLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshForearmLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MeshForearmLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshUpperarmLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MeshUpperarmLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SkeletalMeshName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_SkeletalMeshName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Custom-to-universal skeleton bone orientation translation" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSkeletalMeshSetup>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_FirstPassBones_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Bones need to be evaluated in the first pass" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_FirstPassBones = { "FirstPassBones", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, FirstPassBones), METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_FirstPassBones_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_FirstPassBones_MetaData)) };
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_FirstPassBones_Inner = { "FirstPassBones", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RootBoneRotation_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Rotation of root bone relative in component space" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RootBoneRotation = { "RootBoneRotation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, RootBoneRotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RootBoneRotation_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RootBoneRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_FootLeftRotation_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Left Hand Orientation" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_FootLeftRotation = { "FootLeftRotation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, FootLeftRotation), Z_Construct_UScriptStruct_FBoneRotatorSetup, METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_FootLeftRotation_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_FootLeftRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_FootRightRotation_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Right Hand Orientation" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_FootRightRotation = { "FootRightRotation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, FootRightRotation), Z_Construct_UScriptStruct_FBoneRotatorSetup, METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_FootRightRotation_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_FootRightRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_PalmLeftRotation_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Left Hand Orientation" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_PalmLeftRotation = { "PalmLeftRotation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, PalmLeftRotation), Z_Construct_UScriptStruct_FBoneRotatorSetup, METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_PalmLeftRotation_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_PalmLeftRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_PalmRightRotation_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Right Hand Orientation" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_PalmRightRotation = { "PalmRightRotation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, PalmRightRotation), Z_Construct_UScriptStruct_FBoneRotatorSetup, METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_PalmRightRotation_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_PalmRightRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_LeftLegRotation_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Left Leg Orientation" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_LeftLegRotation = { "LeftLegRotation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, LeftLegRotation), Z_Construct_UScriptStruct_FBoneRotatorSetup, METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_LeftLegRotation_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_LeftLegRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RightLegRotation_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Right Leg Orientation" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RightLegRotation = { "RightLegRotation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, RightLegRotation), Z_Construct_UScriptStruct_FBoneRotatorSetup, METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RightLegRotation_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RightLegRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_LeftHandRotation_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Left Arm Orientation" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_LeftHandRotation = { "LeftHandRotation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, LeftHandRotation), Z_Construct_UScriptStruct_FBoneRotatorSetup, METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_LeftHandRotation_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_LeftHandRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RightHandRotation_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Right Arm Orientation" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RightHandRotation = { "RightHandRotation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, RightHandRotation), Z_Construct_UScriptStruct_FBoneRotatorSetup, METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RightHandRotation_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RightHandRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_LeftShoulderRotation_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Left Shoulder Orientation" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_LeftShoulderRotation = { "LeftShoulderRotation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, LeftShoulderRotation), Z_Construct_UScriptStruct_FBoneRotatorSetup, METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_LeftShoulderRotation_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_LeftShoulderRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RightShoulderRotation_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Right Shoulder Orientation" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RightShoulderRotation = { "RightShoulderRotation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, RightShoulderRotation), Z_Construct_UScriptStruct_FBoneRotatorSetup, METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RightShoulderRotation_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RightShoulderRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_HeadRotation_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Head Orientation" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_HeadRotation = { "HeadRotation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, HeadRotation), Z_Construct_UScriptStruct_FBoneRotatorSetup, METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_HeadRotation_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_HeadRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RibcageRotation_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Ribcage Orientation" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RibcageRotation = { "RibcageRotation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, RibcageRotation), Z_Construct_UScriptStruct_FBoneRotatorSetup, METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RibcageRotation_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RibcageRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_PelvisRotation_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Pelvis Orientation" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_PelvisRotation = { "PelvisRotation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, PelvisRotation), Z_Construct_UScriptStruct_FBoneRotatorSetup, METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_PelvisRotation_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_PelvisRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_MeshScale_MetaData[] = {
		{ "Category", "Skeletal Mesh Setup" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Scale of Mesh Component in local space" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_MeshScale = { "MeshScale", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, MeshScale), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_MeshScale_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_MeshScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_NeckBoneNames_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Reversed list of intermediate neck bones names: (neck_03, neck_02, neck_01, ...)" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_NeckBoneNames = { "NeckBoneNames", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, NeckBoneNames), METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_NeckBoneNames_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_NeckBoneNames_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_NeckBoneNames_Inner = { "NeckBoneNames", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_SpineBoneNames_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Reversed list of intermediate spine bones names (spine_02, spine_01, ...)" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_SpineBoneNames = { "SpineBoneNames", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, SpineBoneNames), METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_SpineBoneNames_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_SpineBoneNames_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_SpineBoneNames_Inner = { "SpineBoneNames", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_NeckBones_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Neck bones relative transforms" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_NeckBones = { "NeckBones", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, NeckBones), METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_NeckBones_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_NeckBones_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_NeckBones_Key_KeyProp = { "NeckBones_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_NeckBones_ValueProp = { "NeckBones", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_SpineBones_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Spine bones relative transforms" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_SpineBones = { "SpineBones", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, SpineBones), METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_SpineBones_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_SpineBones_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_SpineBones_Key_KeyProp = { "SpineBones_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_SpineBones_ValueProp = { "SpineBones", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RelativeRibcageOffset_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RelativeRibcageOffset = { "RelativeRibcageOffset", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, RelativeRibcageOffset), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RelativeRibcageOffset_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RelativeRibcageOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_FootToGroundLeft_MetaData[] = {
		{ "Category", "Skeletal Mesh Setup" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Left foot relative to ground and forward" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_FootToGroundLeft = { "FootToGroundLeft", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, FootToGroundLeft), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_FootToGroundLeft_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_FootToGroundLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_FootToGroundRight_MetaData[] = {
		{ "Category", "Skeletal Mesh Setup" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Right foot relative to ground and forward" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_FootToGroundRight = { "FootToGroundRight", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, FootToGroundRight), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_FootToGroundRight_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_FootToGroundRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RibcageToSpine_MetaData[] = {
		{ "Category", "Skeletal Mesh Setup" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Local Transform of Ribcage Bone" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RibcageToSpine = { "RibcageToSpine", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, RibcageToSpine), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RibcageToSpine_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RibcageToSpine_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_HeadToNeck_MetaData[] = {
		{ "Category", "Skeletal Mesh Setup" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Local Transform of Head Bone" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_HeadToNeck = { "HeadToNeck", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, HeadToNeck), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_HeadToNeck_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_HeadToNeck_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_UpperarmToShoulderLeft_MetaData[] = {
		{ "Category", "Skeletal Mesh Setup" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Local Transform of Left Upperarm" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_UpperarmToShoulderLeft = { "UpperarmToShoulderLeft", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, UpperarmToShoulderLeft), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_UpperarmToShoulderLeft_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_UpperarmToShoulderLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_UpperarmToShoulderRight_MetaData[] = {
		{ "Category", "Skeletal Mesh Setup" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Local Transform of Right Upperarm" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_UpperarmToShoulderRight = { "UpperarmToShoulderRight", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, UpperarmToShoulderRight), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_UpperarmToShoulderRight_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_UpperarmToShoulderRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_ShoulderToRibcageLeft_MetaData[] = {
		{ "Category", "Skeletal Mesh Setup" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Local Transform of Left Collerbone" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_ShoulderToRibcageLeft = { "ShoulderToRibcageLeft", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, ShoulderToRibcageLeft), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_ShoulderToRibcageLeft_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_ShoulderToRibcageLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_ShoulderToRibcageRight_MetaData[] = {
		{ "Category", "Skeletal Mesh Setup" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Local Transform of Right Collerbone" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_ShoulderToRibcageRight = { "ShoulderToRibcageRight", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, ShoulderToRibcageRight), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_ShoulderToRibcageRight_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_ShoulderToRibcageRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_fForearmLength_MetaData[] = {
		{ "Category", "Skeletal Mesh Setup" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Lowerarm Length scaled" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_fForearmLength = { "fForearmLength", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, fForearmLength), METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_fForearmLength_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_fForearmLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_fUpperarmLength_MetaData[] = {
		{ "Category", "Skeletal Mesh Setup" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Upperarm Length scaled" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_fUpperarmLength = { "fUpperarmLength", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, fUpperarmLength), METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_fUpperarmLength_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_fUpperarmLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_MeshHeight_MetaData[] = {
		{ "Category", "Skeletal Mesh Setup" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Distance from ground to head bone" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_MeshHeight = { "MeshHeight", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, MeshHeight), METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_MeshHeight_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_MeshHeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_MeshHandsFullLength_MetaData[] = {
		{ "Category", "Skeletal Mesh Setup" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Distance from one palm to another when hands are horisontal in T Pose" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_MeshHandsFullLength = { "MeshHandsFullLength", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, MeshHandsFullLength), METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_MeshHandsFullLength_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_MeshHandsFullLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_MeshForearmLength_MetaData[] = {
		{ "Category", "Skeletal Mesh Setup" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Lowerarm Length not scaled" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_MeshForearmLength = { "MeshForearmLength", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, MeshForearmLength), METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_MeshForearmLength_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_MeshForearmLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_MeshUpperarmLength_MetaData[] = {
		{ "Category", "Skeletal Mesh Setup" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Upperarm Length not scaled" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_MeshUpperarmLength = { "MeshUpperarmLength", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, MeshUpperarmLength), METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_MeshUpperarmLength_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_MeshUpperarmLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_SkeletalMeshName_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Name of Skeletal Mesh Object" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_SkeletalMeshName = { "SkeletalMeshName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshSetup, SkeletalMeshName), METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_SkeletalMeshName_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_SkeletalMeshName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_FirstPassBones,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_FirstPassBones_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RootBoneRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_FootLeftRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_FootRightRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_PalmLeftRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_PalmRightRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_LeftLegRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RightLegRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_LeftHandRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RightHandRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_LeftShoulderRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RightShoulderRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_HeadRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RibcageRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_PelvisRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_MeshScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_NeckBoneNames,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_NeckBoneNames_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_SpineBoneNames,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_SpineBoneNames_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_NeckBones,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_NeckBones_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_NeckBones_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_SpineBones,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_SpineBones_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_SpineBones_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RelativeRibcageOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_FootToGroundLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_FootToGroundRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_RibcageToSpine,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_HeadToNeck,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_UpperarmToShoulderLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_UpperarmToShoulderRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_ShoulderToRibcageLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_ShoulderToRibcageRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_fForearmLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_fUpperarmLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_MeshHeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_MeshHandsFullLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_MeshForearmLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_MeshUpperarmLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::NewProp_SkeletalMeshName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_VRIKBodyRuntime,
		nullptr,
		&NewStructOps,
		"SkeletalMeshSetup",
		sizeof(FSkeletalMeshSetup),
		alignof(FSkeletalMeshSetup),
		Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSkeletalMeshSetup()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSkeletalMeshSetup_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_VRIKBodyRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SkeletalMeshSetup"), sizeof(FSkeletalMeshSetup), Get_Z_Construct_UScriptStruct_FSkeletalMeshSetup_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSkeletalMeshSetup_Hash() { return 3853117521U; }
class UScriptStruct* FBoneRotatorSetup::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern VRIKBODYRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FBoneRotatorSetup_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FBoneRotatorSetup, Z_Construct_UPackage__Script_VRIKBodyRuntime(), TEXT("BoneRotatorSetup"), sizeof(FBoneRotatorSetup), Get_Z_Construct_UScriptStruct_FBoneRotatorSetup_Hash());
	}
	return Singleton;
}
template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<FBoneRotatorSetup>()
{
	return FBoneRotatorSetup::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FBoneRotatorSetup(FBoneRotatorSetup::StaticStruct, TEXT("/Script/VRIKBodyRuntime"), TEXT("BoneRotatorSetup"), false, nullptr, nullptr);
static struct FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFBoneRotatorSetup
{
	FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFBoneRotatorSetup()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("BoneRotatorSetup")),new UScriptStruct::TCppStructOps<FBoneRotatorSetup>);
	}
} ScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFBoneRotatorSetup;
	struct Z_Construct_UScriptStruct_FBoneRotatorSetup_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBoneRotatorSetup_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Custom-to-universal skeleton bone orientation translation" },
	};
#endif
	void* Z_Construct_UScriptStruct_FBoneRotatorSetup_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FBoneRotatorSetup>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FBoneRotatorSetup_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_VRIKBodyRuntime,
		nullptr,
		&NewStructOps,
		"BoneRotatorSetup",
		sizeof(FBoneRotatorSetup),
		alignof(FBoneRotatorSetup),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FBoneRotatorSetup_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FBoneRotatorSetup_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FBoneRotatorSetup()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FBoneRotatorSetup_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_VRIKBodyRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("BoneRotatorSetup"), sizeof(FBoneRotatorSetup), Get_Z_Construct_UScriptStruct_FBoneRotatorSetup_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FBoneRotatorSetup_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FBoneRotatorSetup_Hash() { return 3934023305U; }
	void UVRIKSkeletalMeshTranslator::StaticRegisterNativesUVRIKSkeletalMeshTranslator()
	{
		UClass* Class = UVRIKSkeletalMeshTranslator::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CalcTorsoIK", &UVRIKSkeletalMeshTranslator::execCalcTorsoIK },
			{ "CalculateSpine", &UVRIKSkeletalMeshTranslator::execCalculateSpine },
			{ "CalculateTwoBoneIK", &UVRIKSkeletalMeshTranslator::execCalculateTwoBoneIK },
			{ "DebugGetYawRotation", &UVRIKSkeletalMeshTranslator::execDebugGetYawRotation },
			{ "DebugShowBonesOrientation", &UVRIKSkeletalMeshTranslator::execDebugShowBonesOrientation },
			{ "DrawAxes", &UVRIKSkeletalMeshTranslator::execDrawAxes },
			{ "ForceSetComponentAsCalibrated", &UVRIKSkeletalMeshTranslator::execForceSetComponentAsCalibrated },
			{ "GetLastPose", &UVRIKSkeletalMeshTranslator::execGetLastPose },
			{ "GetMeshWorldTransform", &UVRIKSkeletalMeshTranslator::execGetMeshWorldTransform },
			{ "GetSkeletalMeshSetup", &UVRIKSkeletalMeshTranslator::execGetSkeletalMeshSetup },
			{ "Initialize", &UVRIKSkeletalMeshTranslator::execInitialize },
			{ "IsInitialized", &UVRIKSkeletalMeshTranslator::execIsInitialized },
			{ "MakeRotByTwoAxes", &UVRIKSkeletalMeshTranslator::execMakeRotByTwoAxes },
			{ "PrintBoneInfo", &UVRIKSkeletalMeshTranslator::execPrintBoneInfo },
			{ "RefreshComponentPosition", &UVRIKSkeletalMeshTranslator::execRefreshComponentPosition },
			{ "RestoreSkeletalMeshSetup", &UVRIKSkeletalMeshTranslator::execRestoreSkeletalMeshSetup },
			{ "UpdateSkeleton", &UVRIKSkeletalMeshTranslator::execUpdateSkeleton },
			{ "UpdateSkeletonBonesSetup", &UVRIKSkeletalMeshTranslator::execUpdateSkeletonBonesSetup },
			{ "VRIKBody_OnCalibrationComplete", &UVRIKSkeletalMeshTranslator::execVRIKBody_OnCalibrationComplete },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalcTorsoIK_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalcTorsoIK_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "---------------------------------- FUNCTIONS ----------------------------------" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalcTorsoIK_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKSkeletalMeshTranslator, nullptr, "CalcTorsoIK", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalcTorsoIK_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalcTorsoIK_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalcTorsoIK()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalcTorsoIK_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics
	{
		struct VRIKSkeletalMeshTranslator_eventCalculateSpine_Parms
		{
			FTransform Start;
			FTransform End;
			TArray<FName> RelativeBoneNames;
			TMap<FName,FTransform> RelativeTransforms;
			TArray<FTransform> ResultTransforms;
			bool bDrawDebugGeometry;
		};
		static void NewProp_bDrawDebugGeometry_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDrawDebugGeometry;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ResultTransforms;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ResultTransforms_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RelativeTransforms_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_RelativeTransforms;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_RelativeTransforms_Key_KeyProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RelativeTransforms_ValueProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RelativeBoneNames_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_RelativeBoneNames;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_RelativeBoneNames_Inner;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_End;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Start_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Start;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::NewProp_bDrawDebugGeometry_SetBit(void* Obj)
	{
		((VRIKSkeletalMeshTranslator_eventCalculateSpine_Parms*)Obj)->bDrawDebugGeometry = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::NewProp_bDrawDebugGeometry = { "bDrawDebugGeometry", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VRIKSkeletalMeshTranslator_eventCalculateSpine_Parms), &Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::NewProp_bDrawDebugGeometry_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::NewProp_ResultTransforms = { "ResultTransforms", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventCalculateSpine_Parms, ResultTransforms), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::NewProp_ResultTransforms_Inner = { "ResultTransforms", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::NewProp_RelativeTransforms_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::NewProp_RelativeTransforms = { "RelativeTransforms", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventCalculateSpine_Parms, RelativeTransforms), METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::NewProp_RelativeTransforms_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::NewProp_RelativeTransforms_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::NewProp_RelativeTransforms_Key_KeyProp = { "RelativeTransforms_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::NewProp_RelativeTransforms_ValueProp = { "RelativeTransforms", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::NewProp_RelativeBoneNames_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::NewProp_RelativeBoneNames = { "RelativeBoneNames", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventCalculateSpine_Parms, RelativeBoneNames), METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::NewProp_RelativeBoneNames_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::NewProp_RelativeBoneNames_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::NewProp_RelativeBoneNames_Inner = { "RelativeBoneNames", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::NewProp_End = { "End", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventCalculateSpine_Parms, End), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::NewProp_Start_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::NewProp_Start = { "Start", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventCalculateSpine_Parms, Start), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::NewProp_Start_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::NewProp_Start_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::NewProp_bDrawDebugGeometry,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::NewProp_ResultTransforms,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::NewProp_ResultTransforms_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::NewProp_RelativeTransforms,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::NewProp_RelativeTransforms_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::NewProp_RelativeTransforms_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::NewProp_RelativeBoneNames,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::NewProp_RelativeBoneNames_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::NewProp_End,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::NewProp_Start,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKSkeletalMeshTranslator, nullptr, "CalculateSpine", sizeof(VRIKSkeletalMeshTranslator_eventCalculateSpine_Parms), Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00C80401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics
	{
		struct VRIKSkeletalMeshTranslator_eventCalculateTwoBoneIK_Parms
		{
			FVector ChainStart;
			FVector ChainEnd;
			FVector JointTarget;
			float UpperBoneSize;
			float LowerBoneSize;
			FTransform UpperBone;
			FTransform LowerBone;
			FBoneRotatorSetup LimbSpace;
			bool bInvertBending;
		};
		static void NewProp_bInvertBending_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInvertBending;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LimbSpace;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LowerBone;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UpperBone;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LowerBoneSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LowerBoneSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UpperBoneSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_UpperBoneSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_JointTarget_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_JointTarget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChainEnd_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ChainEnd;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChainStart_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ChainStart;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_bInvertBending_SetBit(void* Obj)
	{
		((VRIKSkeletalMeshTranslator_eventCalculateTwoBoneIK_Parms*)Obj)->bInvertBending = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_bInvertBending = { "bInvertBending", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VRIKSkeletalMeshTranslator_eventCalculateTwoBoneIK_Parms), &Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_bInvertBending_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_LimbSpace = { "LimbSpace", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventCalculateTwoBoneIK_Parms, LimbSpace), Z_Construct_UScriptStruct_FBoneRotatorSetup, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_LowerBone = { "LowerBone", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventCalculateTwoBoneIK_Parms, LowerBone), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_UpperBone = { "UpperBone", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventCalculateTwoBoneIK_Parms, UpperBone), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_LowerBoneSize_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_LowerBoneSize = { "LowerBoneSize", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventCalculateTwoBoneIK_Parms, LowerBoneSize), METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_LowerBoneSize_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_LowerBoneSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_UpperBoneSize_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_UpperBoneSize = { "UpperBoneSize", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventCalculateTwoBoneIK_Parms, UpperBoneSize), METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_UpperBoneSize_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_UpperBoneSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_JointTarget_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_JointTarget = { "JointTarget", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventCalculateTwoBoneIK_Parms, JointTarget), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_JointTarget_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_JointTarget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_ChainEnd_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_ChainEnd = { "ChainEnd", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventCalculateTwoBoneIK_Parms, ChainEnd), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_ChainEnd_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_ChainEnd_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_ChainStart_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_ChainStart = { "ChainStart", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventCalculateTwoBoneIK_Parms, ChainStart), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_ChainStart_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_ChainStart_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_bInvertBending,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_LimbSpace,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_LowerBone,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_UpperBone,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_LowerBoneSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_UpperBoneSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_JointTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_ChainEnd,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::NewProp_ChainStart,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Math. Calculate IK transforms for two-bone chain.\nNeed to be refactored to consider skeleton bones orientation!" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKSkeletalMeshTranslator, nullptr, "CalculateTwoBoneIK", sizeof(VRIKSkeletalMeshTranslator_eventCalculateTwoBoneIK_Parms), Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00C80401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugGetYawRotation_Statics
	{
		struct VRIKSkeletalMeshTranslator_eventDebugGetYawRotation_Parms
		{
			FVector ForwardVector;
			FVector UpVector;
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UpVector_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UpVector;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ForwardVector_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ForwardVector;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugGetYawRotation_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventDebugGetYawRotation_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugGetYawRotation_Statics::NewProp_UpVector_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugGetYawRotation_Statics::NewProp_UpVector = { "UpVector", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventDebugGetYawRotation_Parms, UpVector), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugGetYawRotation_Statics::NewProp_UpVector_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugGetYawRotation_Statics::NewProp_UpVector_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugGetYawRotation_Statics::NewProp_ForwardVector_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugGetYawRotation_Statics::NewProp_ForwardVector = { "ForwardVector", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventDebugGetYawRotation_Parms, ForwardVector), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugGetYawRotation_Statics::NewProp_ForwardVector_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugGetYawRotation_Statics::NewProp_ForwardVector_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugGetYawRotation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugGetYawRotation_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugGetYawRotation_Statics::NewProp_UpVector,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugGetYawRotation_Statics::NewProp_ForwardVector,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugGetYawRotation_Statics::Function_MetaDataParams[] = {
		{ "Category", "VR IK Skeletal Mesh Translator" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Debug function. Returns calculated component yaw from pelvis forward and up vectors." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugGetYawRotation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKSkeletalMeshTranslator, nullptr, "DebugGetYawRotation", sizeof(VRIKSkeletalMeshTranslator_eventDebugGetYawRotation_Parms), Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugGetYawRotation_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugGetYawRotation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugGetYawRotation_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugGetYawRotation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugGetYawRotation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugGetYawRotation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugShowBonesOrientation_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugShowBonesOrientation_Statics::Function_MetaDataParams[] = {
		{ "Category", "VR IK Skeletal Mesh Translator" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Debug function. Draws orientation axes for all calculated bones of the skeletal mesh." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugShowBonesOrientation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKSkeletalMeshTranslator, nullptr, "DebugShowBonesOrientation", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugShowBonesOrientation_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugShowBonesOrientation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugShowBonesOrientation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugShowBonesOrientation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DrawAxes_Statics
	{
		struct VRIKSkeletalMeshTranslator_eventDrawAxes_Parms
		{
			UWorld* World;
			FVector Location;
			FBoneRotatorSetup RotSetup;
			FRotator Rot;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Rot;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RotSetup;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Location_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Location;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_World;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DrawAxes_Statics::NewProp_Rot = { "Rot", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventDrawAxes_Parms, Rot), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DrawAxes_Statics::NewProp_RotSetup = { "RotSetup", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventDrawAxes_Parms, RotSetup), Z_Construct_UScriptStruct_FBoneRotatorSetup, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DrawAxes_Statics::NewProp_Location_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DrawAxes_Statics::NewProp_Location = { "Location", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventDrawAxes_Parms, Location), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DrawAxes_Statics::NewProp_Location_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DrawAxes_Statics::NewProp_Location_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DrawAxes_Statics::NewProp_World = { "World", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventDrawAxes_Parms, World), Z_Construct_UClass_UWorld_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DrawAxes_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DrawAxes_Statics::NewProp_Rot,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DrawAxes_Statics::NewProp_RotSetup,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DrawAxes_Statics::NewProp_Location,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DrawAxes_Statics::NewProp_World,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DrawAxes_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Helper" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DrawAxes_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKSkeletalMeshTranslator, nullptr, "DrawAxes", sizeof(VRIKSkeletalMeshTranslator_eventDrawAxes_Parms), Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DrawAxes_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DrawAxes_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00C80401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DrawAxes_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DrawAxes_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DrawAxes()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DrawAxes_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_ForceSetComponentAsCalibrated_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_ForceSetComponentAsCalibrated_Statics::Function_MetaDataParams[] = {
		{ "Category", "VR IK Skeletal Mesh Translator" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Function mark body as calibrated and allow to receive input from VRIKBody component.\nNote: function isn't replicated, call it on all clients manually." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_ForceSetComponentAsCalibrated_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKSkeletalMeshTranslator, nullptr, "ForceSetComponentAsCalibrated", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_ForceSetComponentAsCalibrated_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_ForceSetComponentAsCalibrated_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_ForceSetComponentAsCalibrated()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_ForceSetComponentAsCalibrated_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetLastPose_Statics
	{
		struct VRIKSkeletalMeshTranslator_eventGetLastPose_Parms
		{
			FPoseSnapshot ReturnPoseValue;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnPoseValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetLastPose_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VRIKSkeletalMeshTranslator_eventGetLastPose_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetLastPose_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VRIKSkeletalMeshTranslator_eventGetLastPose_Parms), &Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetLastPose_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetLastPose_Statics::NewProp_ReturnPoseValue = { "ReturnPoseValue", nullptr, (EPropertyFlags)0x0010000008000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventGetLastPose_Parms, ReturnPoseValue), Z_Construct_UScriptStruct_FPoseSnapshot, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetLastPose_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetLastPose_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetLastPose_Statics::NewProp_ReturnPoseValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetLastPose_Statics::Function_MetaDataParams[] = {
		{ "Category", "VR IK Skeletal Mesh Translator" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Get current pose from VRIKBody component adjusted for controlled skeletal mesh\n@return Pose Snapshot object to use in Animation Blueprint" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetLastPose_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKSkeletalMeshTranslator, nullptr, "GetLastPose", sizeof(VRIKSkeletalMeshTranslator_eventGetLastPose_Parms), Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetLastPose_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetLastPose_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetLastPose_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetLastPose_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetLastPose()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetLastPose_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetMeshWorldTransform_Statics
	{
		struct VRIKSkeletalMeshTranslator_eventGetMeshWorldTransform_Parms
		{
			FVector Location;
			FRotator Rotation;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Rotation;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Location;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetMeshWorldTransform_Statics::NewProp_Rotation = { "Rotation", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventGetMeshWorldTransform_Parms, Rotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetMeshWorldTransform_Statics::NewProp_Location = { "Location", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventGetMeshWorldTransform_Parms, Location), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetMeshWorldTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetMeshWorldTransform_Statics::NewProp_Rotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetMeshWorldTransform_Statics::NewProp_Location,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetMeshWorldTransform_Statics::Function_MetaDataParams[] = {
		{ "Category", "VR IK Skeletal Mesh Translator" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "@return current Location and Rotation of Skeletal Mesh Component in World Space in Root Motion is enabled\nor predicted character position if Root Motion is disabled" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetMeshWorldTransform_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKSkeletalMeshTranslator, nullptr, "GetMeshWorldTransform", sizeof(VRIKSkeletalMeshTranslator_eventGetMeshWorldTransform_Parms), Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetMeshWorldTransform_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetMeshWorldTransform_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetMeshWorldTransform_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetMeshWorldTransform_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetMeshWorldTransform()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetMeshWorldTransform_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetSkeletalMeshSetup_Statics
	{
		struct VRIKSkeletalMeshTranslator_eventGetSkeletalMeshSetup_Parms
		{
			FSkeletalMeshSetup ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetSkeletalMeshSetup_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventGetSkeletalMeshSetup_Parms, ReturnValue), Z_Construct_UScriptStruct_FSkeletalMeshSetup, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetSkeletalMeshSetup_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetSkeletalMeshSetup_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetSkeletalMeshSetup_Statics::Function_MetaDataParams[] = {
		{ "Category", "VR IK Skeletal Mesh Translator" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Get current skeletal mesh bones setup. Use this function to save/restore mesh data without reinitialization." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetSkeletalMeshSetup_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKSkeletalMeshTranslator, nullptr, "GetSkeletalMeshSetup", sizeof(VRIKSkeletalMeshTranslator_eventGetSkeletalMeshSetup_Parms), Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetSkeletalMeshSetup_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetSkeletalMeshSetup_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetSkeletalMeshSetup_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetSkeletalMeshSetup_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetSkeletalMeshSetup()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetSkeletalMeshSetup_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_Initialize_Statics
	{
		struct VRIKSkeletalMeshTranslator_eventInitialize_Parms
		{
			USkeletalMeshComponent* ControlledSkeletalMesh;
			UVRIKBody* VRIKBodyComponent;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VRIKBodyComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_VRIKBodyComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ControlledSkeletalMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ControlledSkeletalMesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_Initialize_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VRIKSkeletalMeshTranslator_eventInitialize_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_Initialize_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VRIKSkeletalMeshTranslator_eventInitialize_Parms), &Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_Initialize_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_Initialize_Statics::NewProp_VRIKBodyComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_Initialize_Statics::NewProp_VRIKBodyComponent = { "VRIKBodyComponent", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventInitialize_Parms, VRIKBodyComponent), Z_Construct_UClass_UVRIKBody_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_Initialize_Statics::NewProp_VRIKBodyComponent_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_Initialize_Statics::NewProp_VRIKBodyComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_Initialize_Statics::NewProp_ControlledSkeletalMesh_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_Initialize_Statics::NewProp_ControlledSkeletalMesh = { "ControlledSkeletalMesh", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventInitialize_Parms, ControlledSkeletalMesh), Z_Construct_UClass_USkeletalMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_Initialize_Statics::NewProp_ControlledSkeletalMesh_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_Initialize_Statics::NewProp_ControlledSkeletalMesh_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_Initialize_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_Initialize_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_Initialize_Statics::NewProp_VRIKBodyComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_Initialize_Statics::NewProp_ControlledSkeletalMesh,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_Initialize_Statics::Function_MetaDataParams[] = {
		{ "Category", "VR IK Skeletal Mesh Translator" },
		{ "CPP_Default_ControlledSkeletalMesh", "None" },
		{ "CPP_Default_VRIKBodyComponent", "None" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Initialize references and build information about controlled skeletal mesh. References can be automatically extracted from the owner actor\nif it has VRIKBody component and a single Skeletal Mesh Component\nSkeletal Mesh Component must contain a Skeletal Mesh object in T Pose" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_Initialize_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKSkeletalMeshTranslator, nullptr, "Initialize", sizeof(VRIKSkeletalMeshTranslator_eventInitialize_Parms), Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_Initialize_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_Initialize_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_Initialize_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_Initialize_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_Initialize()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_Initialize_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_IsInitialized_Statics
	{
		struct VRIKSkeletalMeshTranslator_eventIsInitialized_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_IsInitialized_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VRIKSkeletalMeshTranslator_eventIsInitialized_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_IsInitialized_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VRIKSkeletalMeshTranslator_eventIsInitialized_Parms), &Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_IsInitialized_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_IsInitialized_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_IsInitialized_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_IsInitialized_Statics::Function_MetaDataParams[] = {
		{ "Category", "VR IK Skeletal Mesh Translator" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Is component initialized or not?" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_IsInitialized_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKSkeletalMeshTranslator, nullptr, "IsInitialized", sizeof(VRIKSkeletalMeshTranslator_eventIsInitialized_Parms), Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_IsInitialized_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_IsInitialized_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_IsInitialized_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_IsInitialized_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_IsInitialized()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_IsInitialized_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_MakeRotByTwoAxes_Statics
	{
		struct VRIKSkeletalMeshTranslator_eventMakeRotByTwoAxes_Parms
		{
			TEnumAsByte<EAxis::Type> MainAxis;
			FVector MainAxisDirection;
			TEnumAsByte<EAxis::Type> SecondaryAxis;
			FVector SecondaryAxisDirection;
			FRotator ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SecondaryAxisDirection;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SecondaryAxis;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MainAxisDirection;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_MainAxis;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_MakeRotByTwoAxes_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventMakeRotByTwoAxes_Parms, ReturnValue), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_MakeRotByTwoAxes_Statics::NewProp_SecondaryAxisDirection = { "SecondaryAxisDirection", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventMakeRotByTwoAxes_Parms, SecondaryAxisDirection), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_MakeRotByTwoAxes_Statics::NewProp_SecondaryAxis = { "SecondaryAxis", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventMakeRotByTwoAxes_Parms, SecondaryAxis), Z_Construct_UEnum_CoreUObject_EAxis, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_MakeRotByTwoAxes_Statics::NewProp_MainAxisDirection = { "MainAxisDirection", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventMakeRotByTwoAxes_Parms, MainAxisDirection), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_MakeRotByTwoAxes_Statics::NewProp_MainAxis = { "MainAxis", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventMakeRotByTwoAxes_Parms, MainAxis), Z_Construct_UEnum_CoreUObject_EAxis, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_MakeRotByTwoAxes_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_MakeRotByTwoAxes_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_MakeRotByTwoAxes_Statics::NewProp_SecondaryAxisDirection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_MakeRotByTwoAxes_Statics::NewProp_SecondaryAxis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_MakeRotByTwoAxes_Statics::NewProp_MainAxisDirection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_MakeRotByTwoAxes_Statics::NewProp_MainAxis,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_MakeRotByTwoAxes_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Helper" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_MakeRotByTwoAxes_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKSkeletalMeshTranslator, nullptr, "MakeRotByTwoAxes", sizeof(VRIKSkeletalMeshTranslator_eventMakeRotByTwoAxes_Parms), Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_MakeRotByTwoAxes_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_MakeRotByTwoAxes_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00880401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_MakeRotByTwoAxes_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_MakeRotByTwoAxes_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_MakeRotByTwoAxes()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_MakeRotByTwoAxes_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_PrintBoneInfo_Statics
	{
		struct VRIKSkeletalMeshTranslator_eventPrintBoneInfo_Parms
		{
			FBoneRotatorSetup BoneSetup;
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoneSetup_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BoneSetup;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_PrintBoneInfo_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventPrintBoneInfo_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_PrintBoneInfo_Statics::NewProp_BoneSetup_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_PrintBoneInfo_Statics::NewProp_BoneSetup = { "BoneSetup", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventPrintBoneInfo_Parms, BoneSetup), Z_Construct_UScriptStruct_FBoneRotatorSetup, METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_PrintBoneInfo_Statics::NewProp_BoneSetup_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_PrintBoneInfo_Statics::NewProp_BoneSetup_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_PrintBoneInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_PrintBoneInfo_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_PrintBoneInfo_Statics::NewProp_BoneSetup,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_PrintBoneInfo_Statics::Function_MetaDataParams[] = {
		{ "Category", "VR IK Skeletal Mesh Translator" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Debug Function. Returns text information describing bone orientation." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_PrintBoneInfo_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKSkeletalMeshTranslator, nullptr, "PrintBoneInfo", sizeof(VRIKSkeletalMeshTranslator_eventPrintBoneInfo_Parms), Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_PrintBoneInfo_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_PrintBoneInfo_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_PrintBoneInfo_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_PrintBoneInfo_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_PrintBoneInfo()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_PrintBoneInfo_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RefreshComponentPosition_Statics
	{
		struct VRIKSkeletalMeshTranslator_eventRefreshComponentPosition_Parms
		{
			bool bUpdateCapturedBones;
		};
		static void NewProp_bUpdateCapturedBones_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUpdateCapturedBones;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RefreshComponentPosition_Statics::NewProp_bUpdateCapturedBones_SetBit(void* Obj)
	{
		((VRIKSkeletalMeshTranslator_eventRefreshComponentPosition_Parms*)Obj)->bUpdateCapturedBones = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RefreshComponentPosition_Statics::NewProp_bUpdateCapturedBones = { "bUpdateCapturedBones", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VRIKSkeletalMeshTranslator_eventRefreshComponentPosition_Parms), &Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RefreshComponentPosition_Statics::NewProp_bUpdateCapturedBones_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RefreshComponentPosition_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RefreshComponentPosition_Statics::NewProp_bUpdateCapturedBones,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RefreshComponentPosition_Statics::Function_MetaDataParams[] = {
		{ "Category", "VR IK Skeletal Mesh Translator" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Update data for quickly moving actor. Call before GetLastPose." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RefreshComponentPosition_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKSkeletalMeshTranslator, nullptr, "RefreshComponentPosition", sizeof(VRIKSkeletalMeshTranslator_eventRefreshComponentPosition_Parms), Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RefreshComponentPosition_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RefreshComponentPosition_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RefreshComponentPosition_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RefreshComponentPosition_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RefreshComponentPosition()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RefreshComponentPosition_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RestoreSkeletalMeshSetup_Statics
	{
		struct VRIKSkeletalMeshTranslator_eventRestoreSkeletalMeshSetup_Parms
		{
			FSkeletalMeshSetup SkeletalMeshSetup;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SkeletalMeshSetup_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SkeletalMeshSetup;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RestoreSkeletalMeshSetup_Statics::NewProp_SkeletalMeshSetup_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RestoreSkeletalMeshSetup_Statics::NewProp_SkeletalMeshSetup = { "SkeletalMeshSetup", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKSkeletalMeshTranslator_eventRestoreSkeletalMeshSetup_Parms, SkeletalMeshSetup), Z_Construct_UScriptStruct_FSkeletalMeshSetup, METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RestoreSkeletalMeshSetup_Statics::NewProp_SkeletalMeshSetup_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RestoreSkeletalMeshSetup_Statics::NewProp_SkeletalMeshSetup_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RestoreSkeletalMeshSetup_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RestoreSkeletalMeshSetup_Statics::NewProp_SkeletalMeshSetup,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RestoreSkeletalMeshSetup_Statics::Function_MetaDataParams[] = {
		{ "Category", "VR IK Skeletal Mesh Translator" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Load skeletal mesh bones setup to component. Use this function to save/restore mesh data without reinitialization." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RestoreSkeletalMeshSetup_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKSkeletalMeshTranslator, nullptr, "RestoreSkeletalMeshSetup", sizeof(VRIKSkeletalMeshTranslator_eventRestoreSkeletalMeshSetup_Parms), Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RestoreSkeletalMeshSetup_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RestoreSkeletalMeshSetup_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RestoreSkeletalMeshSetup_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RestoreSkeletalMeshSetup_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RestoreSkeletalMeshSetup()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RestoreSkeletalMeshSetup_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_UpdateSkeleton_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_UpdateSkeleton_Statics::Function_MetaDataParams[] = {
		{ "Category", "VR IK Skeletal Mesh Translator" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Call after VRIKBody::CalculateFrame to load calculated data and update skeletal mesh position if Root Motion is enabled" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_UpdateSkeleton_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKSkeletalMeshTranslator, nullptr, "UpdateSkeleton", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_UpdateSkeleton_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_UpdateSkeleton_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_UpdateSkeleton()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_UpdateSkeleton_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_UpdateSkeletonBonesSetup_Statics
	{
		struct VRIKSkeletalMeshTranslator_eventUpdateSkeletonBonesSetup_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_UpdateSkeletonBonesSetup_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VRIKSkeletalMeshTranslator_eventUpdateSkeletonBonesSetup_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_UpdateSkeletonBonesSetup_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VRIKSkeletalMeshTranslator_eventUpdateSkeletonBonesSetup_Parms), &Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_UpdateSkeletonBonesSetup_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_UpdateSkeletonBonesSetup_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_UpdateSkeletonBonesSetup_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_UpdateSkeletonBonesSetup_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Save Skeletal mesh bones orientation" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_UpdateSkeletonBonesSetup_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKSkeletalMeshTranslator, nullptr, "UpdateSkeletonBonesSetup", sizeof(VRIKSkeletalMeshTranslator_eventUpdateSkeletonBonesSetup_Parms), Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_UpdateSkeletonBonesSetup_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_UpdateSkeletonBonesSetup_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_UpdateSkeletonBonesSetup_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_UpdateSkeletonBonesSetup_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_UpdateSkeletonBonesSetup()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_UpdateSkeletonBonesSetup_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_VRIKBody_OnCalibrationComplete_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_VRIKBody_OnCalibrationComplete_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_VRIKBody_OnCalibrationComplete_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKSkeletalMeshTranslator, nullptr, "VRIKBody_OnCalibrationComplete", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_VRIKBody_OnCalibrationComplete_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_VRIKBody_OnCalibrationComplete_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_VRIKBody_OnCalibrationComplete()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_VRIKBody_OnCalibrationComplete_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UVRIKSkeletalMeshTranslator_NoRegister()
	{
		return UVRIKSkeletalMeshTranslator::StaticClass();
	}
	struct Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bComponentWasMovedInTick_MetaData[];
#endif
		static void NewProp_bComponentWasMovedInTick_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bComponentWasMovedInTick;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LowerarmTwistLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LowerarmTwistLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LowerarmTwistRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LowerarmTwistRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedBody_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_CapturedBody;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_CapturedBody_Key_KeyProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CapturedBody_ValueProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastRotationOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LastRotationOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedSkeletonL_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_CapturedSkeletonL;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CapturedSkeletonL_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedSkeletonR_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_CapturedSkeletonR;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CapturedSkeletonR_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedBodyRaw_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CapturedBodyRaw;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BodySetup_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BodySetup;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ComponentWorldTransform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ComponentWorldTransform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BodyMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BodyMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VRIKSolver_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_VRIKSolver;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TrialYear_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_TrialYear;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TrialMonth_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_TrialMonth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bHasRootBone_MetaData[];
#endif
		static void NewProp_bHasRootBone_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bHasRootBone;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bTrial_MetaData[];
#endif
		static void NewProp_bTrial_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bTrial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsCalibrated_MetaData[];
#endif
		static void NewProp_bIsCalibrated_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsCalibrated;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsInitialized_MetaData[];
#endif
		static void NewProp_bIsInitialized_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsInitialized;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDisableFlexibleTorso_MetaData[];
#endif
		static void NewProp_bDisableFlexibleTorso_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDisableFlexibleTorso;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HandTwistLimit_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HandTwistLimit;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshHeadHalfHeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MeshHeadHalfHeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpineVerticalOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SpineVerticalOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bHandsIKLateUpdate_MetaData[];
#endif
		static void NewProp_bHandsIKLateUpdate_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bHandsIKLateUpdate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInvertElbows_MetaData[];
#endif
		static void NewProp_bInvertElbows_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInvertElbows;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bLockRootBoneRotation_MetaData[];
#endif
		static void NewProp_bLockRootBoneRotation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bLockRootBoneRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRootMotion_MetaData[];
#endif
		static void NewProp_bRootMotion_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRootMotion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScaleMultiplierHorizontal_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScaleMultiplierHorizontal;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScaleMultiplierVertical_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScaleMultiplierVertical;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRescaleMesh_MetaData[];
#endif
		static void NewProp_bRescaleMesh_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRescaleMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LowerarmTwistsLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_LowerarmTwistsLeft;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_LowerarmTwistsLeft_Key_KeyProp;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LowerarmTwistsLeft_ValueProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LowerarmTwistsRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_LowerarmTwistsRight;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_LowerarmTwistsRight_Key_KeyProp;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LowerarmTwistsRight_ValueProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FootLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_FootLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CalfLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_CalfLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ThighLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ThighLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FootRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_FootRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CalfRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_CalfRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ThighRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ThighRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PalmLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PalmLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LowerarmLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_LowerarmLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UpperarmLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_UpperarmLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShoulderLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ShoulderLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PalmRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PalmRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LowerarmRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_LowerarmRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UpperarmRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_UpperarmRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShoulderRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ShoulderRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Head_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Head;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Ribcage_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Ribcage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Pelvis_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Pelvis;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RootBone_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_RootBone;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_VRIKBodyRuntime,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalcTorsoIK, "CalcTorsoIK" }, // 3581739463
		{ &Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateSpine, "CalculateSpine" }, // 1319338872
		{ &Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_CalculateTwoBoneIK, "CalculateTwoBoneIK" }, // 768111618
		{ &Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugGetYawRotation, "DebugGetYawRotation" }, // 401750984
		{ &Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DebugShowBonesOrientation, "DebugShowBonesOrientation" }, // 1219060441
		{ &Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_DrawAxes, "DrawAxes" }, // 3368115688
		{ &Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_ForceSetComponentAsCalibrated, "ForceSetComponentAsCalibrated" }, // 1190080477
		{ &Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetLastPose, "GetLastPose" }, // 459861176
		{ &Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetMeshWorldTransform, "GetMeshWorldTransform" }, // 3723494689
		{ &Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_GetSkeletalMeshSetup, "GetSkeletalMeshSetup" }, // 680122073
		{ &Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_Initialize, "Initialize" }, // 995114664
		{ &Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_IsInitialized, "IsInitialized" }, // 2221920904
		{ &Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_MakeRotByTwoAxes, "MakeRotByTwoAxes" }, // 312260056
		{ &Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_PrintBoneInfo, "PrintBoneInfo" }, // 316462134
		{ &Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RefreshComponentPosition, "RefreshComponentPosition" }, // 1557487859
		{ &Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_RestoreSkeletalMeshSetup, "RestoreSkeletalMeshSetup" }, // 2002152451
		{ &Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_UpdateSkeleton, "UpdateSkeleton" }, // 55403346
		{ &Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_UpdateSkeletonBonesSetup, "UpdateSkeletonBonesSetup" }, // 892761339
		{ &Z_Construct_UFunction_UVRIKSkeletalMeshTranslator_VRIKBody_OnCalibrationComplete, "VRIKBody_OnCalibrationComplete" }, // 2016896623
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "IKBody" },
		{ "IncludePath", "VRIKSkeletalMeshTranslator.h" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "This component transforms internal skeleton data from VRIKBody component to PoseSnapshot for any custom skeletal mesh" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bComponentWasMovedInTick_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
	};
#endif
	void Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bComponentWasMovedInTick_SetBit(void* Obj)
	{
		((UVRIKSkeletalMeshTranslator*)Obj)->bComponentWasMovedInTick = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bComponentWasMovedInTick = { "bComponentWasMovedInTick", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKSkeletalMeshTranslator), &Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bComponentWasMovedInTick_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bComponentWasMovedInTick_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bComponentWasMovedInTick_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmTwistLeft_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Captured Body: calculated forearm twist" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmTwistLeft = { "LowerarmTwistLeft", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, LowerarmTwistLeft), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmTwistLeft_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmTwistLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmTwistRight_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Captured Body: calculated forearm twist" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmTwistRight = { "LowerarmTwistRight", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, LowerarmTwistRight), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmTwistRight_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmTwistRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CapturedBody_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Map of current bone transforms in world space" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CapturedBody = { "CapturedBody", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, CapturedBody), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CapturedBody_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CapturedBody_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CapturedBody_Key_KeyProp = { "CapturedBody_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CapturedBody_ValueProp = { "CapturedBody", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LastRotationOffset_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LastRotationOffset = { "LastRotationOffset", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, LastRotationOffset), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LastRotationOffset_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LastRotationOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CapturedSkeletonL_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CapturedSkeletonL = { "CapturedSkeletonL", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, CapturedSkeletonL), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CapturedSkeletonL_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CapturedSkeletonL_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CapturedSkeletonL_Inner = { "CapturedSkeletonL", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CapturedSkeletonR_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Map of current bone transforms in world space" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CapturedSkeletonR = { "CapturedSkeletonR", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, CapturedSkeletonR), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CapturedSkeletonR_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CapturedSkeletonR_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CapturedSkeletonR_Inner = { "CapturedSkeletonR", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CapturedBodyRaw_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Map of current bone transforms in world space" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CapturedBodyRaw = { "CapturedBodyRaw", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, CapturedBodyRaw), Z_Construct_UScriptStruct_FIKBodyData, METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CapturedBodyRaw_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CapturedBodyRaw_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_BodySetup_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Current Skeletal Mesh Setup" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_BodySetup = { "BodySetup", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, BodySetup), Z_Construct_UScriptStruct_FSkeletalMeshSetup, METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_BodySetup_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_BodySetup_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ComponentWorldTransform_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Current Root Bone Transform" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ComponentWorldTransform = { "ComponentWorldTransform", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, ComponentWorldTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ComponentWorldTransform_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ComponentWorldTransform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_BodyMesh_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Reference to controlled skeletal mesh" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_BodyMesh = { "BodyMesh", nullptr, (EPropertyFlags)0x0020080000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, BodyMesh), Z_Construct_UClass_USkeletalMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_BodyMesh_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_BodyMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_VRIKSolver_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Reference to player's VRIKBody Component" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_VRIKSolver = { "VRIKSolver", nullptr, (EPropertyFlags)0x0020080000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, VRIKSolver), Z_Construct_UClass_UVRIKBody_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_VRIKSolver_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_VRIKSolver_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_TrialYear_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_TrialYear = { "TrialYear", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, TrialYear), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_TrialYear_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_TrialYear_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_TrialMonth_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_TrialMonth = { "TrialMonth", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, TrialMonth), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_TrialMonth_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_TrialMonth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bHasRootBone_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
	};
#endif
	void Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bHasRootBone_SetBit(void* Obj)
	{
		((UVRIKSkeletalMeshTranslator*)Obj)->bHasRootBone = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bHasRootBone = { "bHasRootBone", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKSkeletalMeshTranslator), &Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bHasRootBone_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bHasRootBone_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bHasRootBone_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bTrial_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
	};
#endif
	void Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bTrial_SetBit(void* Obj)
	{
		((UVRIKSkeletalMeshTranslator*)Obj)->bTrial = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bTrial = { "bTrial", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKSkeletalMeshTranslator), &Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bTrial_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bTrial_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bTrial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bIsCalibrated_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
	};
#endif
	void Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bIsCalibrated_SetBit(void* Obj)
	{
		((UVRIKSkeletalMeshTranslator*)Obj)->bIsCalibrated = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bIsCalibrated = { "bIsCalibrated", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKSkeletalMeshTranslator), &Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bIsCalibrated_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bIsCalibrated_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bIsCalibrated_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bIsInitialized_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Are BodySetup and references initialized?" },
	};
#endif
	void Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bIsInitialized_SetBit(void* Obj)
	{
		((UVRIKSkeletalMeshTranslator*)Obj)->bIsInitialized = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bIsInitialized = { "bIsInitialized", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKSkeletalMeshTranslator), &Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bIsInitialized_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bIsInitialized_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bIsInitialized_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bDisableFlexibleTorso_MetaData[] = {
		{ "Category", "VR IK Skeletal Mesh Translator|Setup" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Disable torso bending (for debug purposes)" },
	};
#endif
	void Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bDisableFlexibleTorso_SetBit(void* Obj)
	{
		((UVRIKSkeletalMeshTranslator*)Obj)->bDisableFlexibleTorso = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bDisableFlexibleTorso = { "bDisableFlexibleTorso", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKSkeletalMeshTranslator), &Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bDisableFlexibleTorso_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bDisableFlexibleTorso_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bDisableFlexibleTorso_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_HandTwistLimit_MetaData[] = {
		{ "Category", "VR IK Skeletal Mesh Translator|Setup" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Maximum possible twist value" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_HandTwistLimit = { "HandTwistLimit", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, HandTwistLimit), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_HandTwistLimit_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_HandTwistLimit_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_MeshHeadHalfHeight_MetaData[] = {
		{ "Category", "VR IK Skeletal Mesh Translator|Setup" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Half height of skeletal mesh head (to calculate full height)" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_MeshHeadHalfHeight = { "MeshHeadHalfHeight", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, MeshHeadHalfHeight), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_MeshHeadHalfHeight_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_MeshHeadHalfHeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_SpineVerticalOffset_MetaData[] = {
		{ "Category", "VR IK Skeletal Mesh Translator|Setup" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "This parameter allow to shift torso up and down to proper fit custom skeletal mesh. This setting\nis not affected by MeshScale.Z" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_SpineVerticalOffset = { "SpineVerticalOffset", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, SpineVerticalOffset), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_SpineVerticalOffset_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_SpineVerticalOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bHandsIKLateUpdate_MetaData[] = {
		{ "Category", "VR IK Skeletal Mesh Translator|Setup" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Component makes additional Two-Bone IK calcualtion for hands when updating PoseSnapshot\nto make sure skeletal mesh sizes are taken into account correctly and hands follow\nmotion cotrollers precisely. Disable to save CPU time." },
	};
#endif
	void Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bHandsIKLateUpdate_SetBit(void* Obj)
	{
		((UVRIKSkeletalMeshTranslator*)Obj)->bHandsIKLateUpdate = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bHandsIKLateUpdate = { "bHandsIKLateUpdate", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKSkeletalMeshTranslator), &Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bHandsIKLateUpdate_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bHandsIKLateUpdate_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bHandsIKLateUpdate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bInvertElbows_MetaData[] = {
		{ "Category", "VR IK Skeletal Mesh Translator|Setup" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Turn on if elbows are bended at the wrong side" },
	};
#endif
	void Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bInvertElbows_SetBit(void* Obj)
	{
		((UVRIKSkeletalMeshTranslator*)Obj)->bInvertElbows = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bInvertElbows = { "bInvertElbows", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKSkeletalMeshTranslator), &Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bInvertElbows_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bInvertElbows_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bInvertElbows_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bLockRootBoneRotation_MetaData[] = {
		{ "Category", "VR IK Skeletal Mesh Translator|Setup" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "If true, root bone always keep zero rotation" },
	};
#endif
	void Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bLockRootBoneRotation_SetBit(void* Obj)
	{
		((UVRIKSkeletalMeshTranslator*)Obj)->bLockRootBoneRotation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bLockRootBoneRotation = { "bLockRootBoneRotation", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKSkeletalMeshTranslator), &Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bLockRootBoneRotation_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bLockRootBoneRotation_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bLockRootBoneRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bRootMotion_MetaData[] = {
		{ "Category", "VR IK Skeletal Mesh Translator|Setup" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "If true, automatically moves skeletal mesh component when you call UpdateSkeleton()\nOtherwise, keeps default mesh position and only moves bones.\nIgnored if Pelvis is root bone." },
	};
#endif
	void Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bRootMotion_SetBit(void* Obj)
	{
		((UVRIKSkeletalMeshTranslator*)Obj)->bRootMotion = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bRootMotion = { "bRootMotion", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKSkeletalMeshTranslator), &Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bRootMotion_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bRootMotion_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bRootMotion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ScaleMultiplierHorizontal_MetaData[] = {
		{ "Category", "VR IK Skeletal Mesh Translator|Setup" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Mesh scale coefficient (only if RescaleMesh is true)" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ScaleMultiplierHorizontal = { "ScaleMultiplierHorizontal", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, ScaleMultiplierHorizontal), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ScaleMultiplierHorizontal_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ScaleMultiplierHorizontal_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ScaleMultiplierVertical_MetaData[] = {
		{ "Category", "VR IK Skeletal Mesh Translator|Setup" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Mesh scale coefficient (only if RescaleMesh is true)" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ScaleMultiplierVertical = { "ScaleMultiplierVertical", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, ScaleMultiplierVertical), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ScaleMultiplierVertical_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ScaleMultiplierVertical_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bRescaleMesh_MetaData[] = {
		{ "Category", "VR IK Skeletal Mesh Translator|Setup" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Apply player's height to Mesh Relative Scale?" },
	};
#endif
	void Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bRescaleMesh_SetBit(void* Obj)
	{
		((UVRIKSkeletalMeshTranslator*)Obj)->bRescaleMesh = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bRescaleMesh = { "bRescaleMesh", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKSkeletalMeshTranslator), &Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bRescaleMesh_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bRescaleMesh_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bRescaleMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmTwistsLeft_MetaData[] = {
		{ "Category", "VR IK Skeletal Mesh Translator|Skeleton" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Twist bones and multipliers for left forearm" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmTwistsLeft = { "LowerarmTwistsLeft", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, LowerarmTwistsLeft), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmTwistsLeft_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmTwistsLeft_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmTwistsLeft_Key_KeyProp = { "LowerarmTwistsLeft_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmTwistsLeft_ValueProp = { "LowerarmTwistsLeft", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmTwistsRight_MetaData[] = {
		{ "Category", "VR IK Skeletal Mesh Translator|Skeleton" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Twist bones and multipliers for right forearm" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmTwistsRight = { "LowerarmTwistsRight", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, LowerarmTwistsRight), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmTwistsRight_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmTwistsRight_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmTwistsRight_Key_KeyProp = { "LowerarmTwistsRight_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmTwistsRight_ValueProp = { "LowerarmTwistsRight", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_FootLeft_MetaData[] = {
		{ "Category", "VR IK Skeletal Mesh Translator|Skeleton" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Name of Left Foot Bone at the controlled Skeletal Mesh" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_FootLeft = { "FootLeft", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, FootLeft), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_FootLeft_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_FootLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CalfLeft_MetaData[] = {
		{ "Category", "VR IK Skeletal Mesh Translator|Skeleton" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Name of Left Calf Bone at the controlled Skeletal Mesh" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CalfLeft = { "CalfLeft", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, CalfLeft), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CalfLeft_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CalfLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ThighLeft_MetaData[] = {
		{ "Category", "VR IK Skeletal Mesh Translator|Skeleton" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Name of Left Thigh Bone at the controlled Skeletal Mesh" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ThighLeft = { "ThighLeft", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, ThighLeft), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ThighLeft_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ThighLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_FootRight_MetaData[] = {
		{ "Category", "VR IK Skeletal Mesh Translator|Skeleton" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Name of Right Foot Bone at the controlled Skeletal Mesh" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_FootRight = { "FootRight", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, FootRight), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_FootRight_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_FootRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CalfRight_MetaData[] = {
		{ "Category", "VR IK Skeletal Mesh Translator|Skeleton" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Name of Right Calf Bone at the controlled Skeletal Mesh" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CalfRight = { "CalfRight", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, CalfRight), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CalfRight_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CalfRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ThighRight_MetaData[] = {
		{ "Category", "VR IK Skeletal Mesh Translator|Skeleton" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Name of Right Thigh Bone at the controlled Skeletal Mesh" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ThighRight = { "ThighRight", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, ThighRight), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ThighRight_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ThighRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_PalmLeft_MetaData[] = {
		{ "Category", "VR IK Skeletal Mesh Translator|Skeleton" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Name of Left Hand Bone at the controlled Skeletal Mesh" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_PalmLeft = { "PalmLeft", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, PalmLeft), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_PalmLeft_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_PalmLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmLeft_MetaData[] = {
		{ "Category", "VR IK Skeletal Mesh Translator|Skeleton" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Name of Left Lowerarm Bone at the controlled Skeletal Mesh" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmLeft = { "LowerarmLeft", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, LowerarmLeft), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmLeft_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_UpperarmLeft_MetaData[] = {
		{ "Category", "VR IK Skeletal Mesh Translator|Skeleton" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Name of Left Upperarm Bone at the controlled Skeletal Mesh" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_UpperarmLeft = { "UpperarmLeft", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, UpperarmLeft), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_UpperarmLeft_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_UpperarmLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ShoulderLeft_MetaData[] = {
		{ "Category", "VR IK Skeletal Mesh Translator|Skeleton" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Name of Left Collarbone at the controlled Skeletal Mesh" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ShoulderLeft = { "ShoulderLeft", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, ShoulderLeft), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ShoulderLeft_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ShoulderLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_PalmRight_MetaData[] = {
		{ "Category", "VR IK Skeletal Mesh Translator|Skeleton" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Name of Right Hand Bone at the controlled Skeletal Mesh" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_PalmRight = { "PalmRight", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, PalmRight), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_PalmRight_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_PalmRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmRight_MetaData[] = {
		{ "Category", "VR IK Skeletal Mesh Translator|Skeleton" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Name of Right Lowerarm Bone at the controlled Skeletal Mesh" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmRight = { "LowerarmRight", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, LowerarmRight), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmRight_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_UpperarmRight_MetaData[] = {
		{ "Category", "VR IK Skeletal Mesh Translator|Skeleton" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Name of Right Upperarm Bone at the controlled Skeletal Mesh" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_UpperarmRight = { "UpperarmRight", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, UpperarmRight), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_UpperarmRight_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_UpperarmRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ShoulderRight_MetaData[] = {
		{ "Category", "VR IK Skeletal Mesh Translator|Skeleton" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Name of Right Collarbone at the controlled Skeletal Mesh" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ShoulderRight = { "ShoulderRight", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, ShoulderRight), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ShoulderRight_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ShoulderRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_Head_MetaData[] = {
		{ "Category", "VR IK Skeletal Mesh Translator|Skeleton" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Name of Head Bone at the controlled Skeletal Mesh" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_Head = { "Head", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, Head), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_Head_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_Head_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_Ribcage_MetaData[] = {
		{ "Category", "VR IK Skeletal Mesh Translator|Skeleton" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Name of Ribcage Bone at the controlled Skeletal Mesh" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_Ribcage = { "Ribcage", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, Ribcage), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_Ribcage_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_Ribcage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_Pelvis_MetaData[] = {
		{ "Category", "VR IK Skeletal Mesh Translator|Skeleton" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Name of Pelvis Bone at the controlled Skeletal Mesh" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_Pelvis = { "Pelvis", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, Pelvis), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_Pelvis_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_Pelvis_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_RootBone_MetaData[] = {
		{ "Category", "VR IK Skeletal Mesh Translator|Skeleton" },
		{ "ModuleRelativePath", "Public/VRIKSkeletalMeshTranslator.h" },
		{ "ToolTip", "Name of Root Bone at the controlled Skeletal Mesh\nKeep it as None if your skeleton doesn't have a separate root bone" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_RootBone = { "RootBone", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, RootBone), METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_RootBone_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_RootBone_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bComponentWasMovedInTick,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmTwistLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmTwistRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CapturedBody,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CapturedBody_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CapturedBody_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LastRotationOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CapturedSkeletonL,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CapturedSkeletonL_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CapturedSkeletonR,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CapturedSkeletonR_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CapturedBodyRaw,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_BodySetup,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ComponentWorldTransform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_BodyMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_VRIKSolver,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_TrialYear,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_TrialMonth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bHasRootBone,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bTrial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bIsCalibrated,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bIsInitialized,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bDisableFlexibleTorso,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_HandTwistLimit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_MeshHeadHalfHeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_SpineVerticalOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bHandsIKLateUpdate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bInvertElbows,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bLockRootBoneRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bRootMotion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ScaleMultiplierHorizontal,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ScaleMultiplierVertical,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_bRescaleMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmTwistsLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmTwistsLeft_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmTwistsLeft_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmTwistsRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmTwistsRight_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmTwistsRight_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_FootLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CalfLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ThighLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_FootRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_CalfRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ThighRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_PalmLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_UpperarmLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ShoulderLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_PalmRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_LowerarmRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_UpperarmRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_ShoulderRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_Head,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_Ribcage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_Pelvis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::NewProp_RootBone,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVRIKSkeletalMeshTranslator>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::ClassParams = {
		&UVRIKSkeletalMeshTranslator::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVRIKSkeletalMeshTranslator()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVRIKSkeletalMeshTranslator, 2735881562);
	template<> VRIKBODYRUNTIME_API UClass* StaticClass<UVRIKSkeletalMeshTranslator>()
	{
		return UVRIKSkeletalMeshTranslator::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVRIKSkeletalMeshTranslator(Z_Construct_UClass_UVRIKSkeletalMeshTranslator, &UVRIKSkeletalMeshTranslator::StaticClass, TEXT("/Script/VRIKBodyRuntime"), TEXT("UVRIKSkeletalMeshTranslator"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVRIKSkeletalMeshTranslator);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
