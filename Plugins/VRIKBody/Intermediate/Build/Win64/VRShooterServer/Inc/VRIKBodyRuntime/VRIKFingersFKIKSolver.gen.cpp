// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VRIKBodyRuntime/Public/VRIKFingersFKIKSolver.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVRIKFingersFKIKSolver() {}
// Cross Module References
	VRIKBODYRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo();
	UPackage* Z_Construct_UPackage__Script_VRIKBodyRuntime();
	VRIKBODYRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FVRIK_FingerRotation();
	VRIKBODYRUNTIME_API UClass* Z_Construct_UClass_UVRIKFingersFKIKSolver_NoRegister();
	VRIKBODYRUNTIME_API UClass* Z_Construct_UClass_UVRIKFingersFKIKSolver();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInput();
	VRIKBODYRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FVRIK_FingersPosePreset();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInputDetailed();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationFromDegrees();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationToDegrees();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_CreateFingersFKIKSolver();
	ENGINE_API UClass* Z_Construct_UClass_USkeletalMeshComponent_NoRegister();
	VRIKBODYRUNTIME_API UClass* Z_Construct_UClass_UVRIKFingersSolverSetup_NoRegister();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerDescription();
	VRIKBODYRUNTIME_API UEnum* Z_Construct_UEnum_VRIKBodyRuntime_EVRIKFingerName();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerKnuckles();
	VRIKBODYRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FVRIK_Knuckle();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_GrabObject();
	ENGINE_API UClass* Z_Construct_UClass_UPrimitiveComponent_NoRegister();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_Initialize();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsFingerEnabled();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsInitialized();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_ReleaseObject();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingerEnabled();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersPose();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersTraceReferencePose();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetVRInputReferencePose();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_Trace();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_UpdateFingers();
	VRIKBODYRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FVRIK_FingerSolver();
	ENGINE_API UEnum* Z_Construct_UEnum_Engine_ECollisionChannel();
	VRIKBODYRUNTIME_API UEnum* Z_Construct_UEnum_VRIKBodyRuntime_EVRIK_VRHand();
// End Cross Module References
class UScriptStruct* FVRIK_FingersDetailedInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern VRIKBODYRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo, Z_Construct_UPackage__Script_VRIKBodyRuntime(), TEXT("VRIK_FingersDetailedInfo"), sizeof(FVRIK_FingersDetailedInfo), Get_Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Hash());
	}
	return Singleton;
}
template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<FVRIK_FingersDetailedInfo>()
{
	return FVRIK_FingersDetailedInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FVRIK_FingersDetailedInfo(FVRIK_FingersDetailedInfo::StaticStruct, TEXT("/Script/VRIKBodyRuntime"), TEXT("VRIK_FingersDetailedInfo"), false, nullptr, nullptr);
static struct FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFVRIK_FingersDetailedInfo
{
	FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFVRIK_FingersDetailedInfo()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("VRIK_FingersDetailedInfo")),new UScriptStruct::TCppStructOps<FVRIK_FingersDetailedInfo>);
	}
} ScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFVRIK_FingersDetailedInfo;
	struct Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PinkyBones_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PinkyBones;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PinkyBones_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RingBones_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_RingBones;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RingBones_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MiddleBones_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_MiddleBones;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MiddleBones_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IndexBones_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_IndexBones;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_IndexBones_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ThumbBones_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ThumbBones;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ThumbBones_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Local rotations of all bones of human hand. Applied to VR Input Reference Pose by ApplyDetailedVRInput function." },
	};
#endif
	void* Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FVRIK_FingersDetailedInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_PinkyBones_MetaData[] = {
		{ "Category", "Fingers Detailed Info" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Local rotations of pinky finger bones in degrees (relative to VR Input Reference Pose or previous bone)" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_PinkyBones = { "PinkyBones", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_FingersDetailedInfo, PinkyBones), METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_PinkyBones_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_PinkyBones_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_PinkyBones_Inner = { "PinkyBones", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVRIK_FingerRotation, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_RingBones_MetaData[] = {
		{ "Category", "Fingers Detailed Info" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Local rotations of ring finger bones in degrees (relative to VR Input Reference Pose or previous bone)" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_RingBones = { "RingBones", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_FingersDetailedInfo, RingBones), METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_RingBones_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_RingBones_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_RingBones_Inner = { "RingBones", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVRIK_FingerRotation, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_MiddleBones_MetaData[] = {
		{ "Category", "Fingers Detailed Info" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Local rotations of middle finger bones in degrees (relative to VR Input Reference Pose or previous bone)" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_MiddleBones = { "MiddleBones", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_FingersDetailedInfo, MiddleBones), METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_MiddleBones_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_MiddleBones_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_MiddleBones_Inner = { "MiddleBones", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVRIK_FingerRotation, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_IndexBones_MetaData[] = {
		{ "Category", "Fingers Detailed Info" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Local rotations of index bones in degrees (relative to VR Input Reference Pose or previous bone)" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_IndexBones = { "IndexBones", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_FingersDetailedInfo, IndexBones), METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_IndexBones_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_IndexBones_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_IndexBones_Inner = { "IndexBones", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVRIK_FingerRotation, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_ThumbBones_MetaData[] = {
		{ "Category", "Fingers Detailed Info" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Local rotations of thumb bones in degrees (relative to VR Input Reference Pose or previous bone)" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_ThumbBones = { "ThumbBones", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_FingersDetailedInfo, ThumbBones), METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_ThumbBones_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_ThumbBones_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_ThumbBones_Inner = { "ThumbBones", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVRIK_FingerRotation, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_PinkyBones,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_PinkyBones_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_RingBones,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_RingBones_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_MiddleBones,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_MiddleBones_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_IndexBones,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_IndexBones_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_ThumbBones,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::NewProp_ThumbBones_Inner,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_VRIKBodyRuntime,
		nullptr,
		&NewStructOps,
		"VRIK_FingersDetailedInfo",
		sizeof(FVRIK_FingersDetailedInfo),
		alignof(FVRIK_FingersDetailedInfo),
		Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_VRIKBodyRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("VRIK_FingersDetailedInfo"), sizeof(FVRIK_FingersDetailedInfo), Get_Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Hash() { return 1616658110U; }
	void UVRIKFingersFKIKSolver::StaticRegisterNativesUVRIKFingersFKIKSolver()
	{
		UClass* Class = UVRIKFingersFKIKSolver::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ApplyVRInput", &UVRIKFingersFKIKSolver::execApplyVRInput },
			{ "ApplyVRInputDetailed", &UVRIKFingersFKIKSolver::execApplyVRInputDetailed },
			{ "ConvertFingerRotationFromDegrees", &UVRIKFingersFKIKSolver::execConvertFingerRotationFromDegrees },
			{ "ConvertFingerRotationToDegrees", &UVRIKFingersFKIKSolver::execConvertFingerRotationToDegrees },
			{ "CreateFingersFKIKSolver", &UVRIKFingersFKIKSolver::execCreateFingersFKIKSolver },
			{ "GetFingerDescription", &UVRIKFingersFKIKSolver::execGetFingerDescription },
			{ "GetFingerKnuckles", &UVRIKFingersFKIKSolver::execGetFingerKnuckles },
			{ "GrabObject", &UVRIKFingersFKIKSolver::execGrabObject },
			{ "Initialize", &UVRIKFingersFKIKSolver::execInitialize },
			{ "IsFingerEnabled", &UVRIKFingersFKIKSolver::execIsFingerEnabled },
			{ "IsInitialized", &UVRIKFingersFKIKSolver::execIsInitialized },
			{ "ReleaseObject", &UVRIKFingersFKIKSolver::execReleaseObject },
			{ "SetFingerEnabled", &UVRIKFingersFKIKSolver::execSetFingerEnabled },
			{ "SetFingersPose", &UVRIKFingersFKIKSolver::execSetFingersPose },
			{ "SetFingersTraceReferencePose", &UVRIKFingersFKIKSolver::execSetFingersTraceReferencePose },
			{ "SetVRInputReferencePose", &UVRIKFingersFKIKSolver::execSetVRInputReferencePose },
			{ "Trace", &UVRIKFingersFKIKSolver::execTrace },
			{ "UpdateFingers", &UVRIKFingersFKIKSolver::execUpdateFingers },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInput_Statics
	{
		struct VRIKFingersFKIKSolver_eventApplyVRInput_Parms
		{
			FVRIK_FingersPosePreset NewFingersRotation;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewFingersRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NewFingersRotation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInput_Statics::NewProp_NewFingersRotation_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInput_Statics::NewProp_NewFingersRotation = { "NewFingersRotation", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKFingersFKIKSolver_eventApplyVRInput_Parms, NewFingersRotation), Z_Construct_UScriptStruct_FVRIK_FingersPosePreset, METADATA_PARAMS(Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInput_Statics::NewProp_NewFingersRotation_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInput_Statics::NewProp_NewFingersRotation_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInput_Statics::NewProp_NewFingersRotation,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInput_Statics::Function_MetaDataParams[] = {
		{ "Category", "Fingers Solver" },
		{ "DisplayName", "Apply VR Input" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Apply fingers curl values from Valve Knuckles or Oculus Touch controllers. This function should be called every Tick.\n@param        ThumbCurl               Curl value returned by curresponding input axis for thumb finger\n@param        IndexCurl               Curl value returned by curresponding input axis for index finger\n@param        MiddleCurl              Curl value returned by curresponding input axis for middle finger\n@param        AnnularCurl             Curl value returned by curresponding input axis for ring finger\n@param        MercurialCurl   Curl value returned by curresponding input axis for pinky finger" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKFingersFKIKSolver, nullptr, "ApplyVRInput", sizeof(VRIKFingersFKIKSolver_eventApplyVRInput_Parms), Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInput_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInput_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInput_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInputDetailed_Statics
	{
		struct VRIKFingersFKIKSolver_eventApplyVRInputDetailed_Parms
		{
			FVRIK_FingersDetailedInfo NewFingersRotation;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewFingersRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NewFingersRotation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInputDetailed_Statics::NewProp_NewFingersRotation_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInputDetailed_Statics::NewProp_NewFingersRotation = { "NewFingersRotation", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKFingersFKIKSolver_eventApplyVRInputDetailed_Parms, NewFingersRotation), Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo, METADATA_PARAMS(Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInputDetailed_Statics::NewProp_NewFingersRotation_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInputDetailed_Statics::NewProp_NewFingersRotation_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInputDetailed_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInputDetailed_Statics::NewProp_NewFingersRotation,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInputDetailed_Statics::Function_MetaDataParams[] = {
		{ "Category", "Fingers Solver" },
		{ "DisplayName", "Apply VR Input Detailed" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Input values in degrees for all bones of all fingers" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInputDetailed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKFingersFKIKSolver, nullptr, "ApplyVRInputDetailed", sizeof(VRIKFingersFKIKSolver_eventApplyVRInputDetailed_Parms), Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInputDetailed_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInputDetailed_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInputDetailed_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInputDetailed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInputDetailed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInputDetailed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationFromDegrees_Statics
	{
		struct VRIKFingersFKIKSolver_eventConvertFingerRotationFromDegrees_Parms
		{
			FVRIK_FingerRotation InFingerRot;
			FVRIK_FingerRotation ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InFingerRot_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InFingerRot;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationFromDegrees_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKFingersFKIKSolver_eventConvertFingerRotationFromDegrees_Parms, ReturnValue), Z_Construct_UScriptStruct_FVRIK_FingerRotation, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationFromDegrees_Statics::NewProp_InFingerRot_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationFromDegrees_Statics::NewProp_InFingerRot = { "InFingerRot", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKFingersFKIKSolver_eventConvertFingerRotationFromDegrees_Parms, InFingerRot), Z_Construct_UScriptStruct_FVRIK_FingerRotation, METADATA_PARAMS(Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationFromDegrees_Statics::NewProp_InFingerRot_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationFromDegrees_Statics::NewProp_InFingerRot_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationFromDegrees_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationFromDegrees_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationFromDegrees_Statics::NewProp_InFingerRot,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationFromDegrees_Statics::Function_MetaDataParams[] = {
		{ "Category", "Fingers Solver" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Convert finger orientation in degrees (used in detailed VR input) to multiplier-based (used in poses, VR input)\n@param        InFingerRot             Finger orientation in degree\n@return                                       Finger orientation with values from 0 to 1" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationFromDegrees_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKFingersFKIKSolver, nullptr, "ConvertFingerRotationFromDegrees", sizeof(VRIKFingersFKIKSolver_eventConvertFingerRotationFromDegrees_Parms), Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationFromDegrees_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationFromDegrees_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationFromDegrees_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationFromDegrees_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationFromDegrees()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationFromDegrees_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationToDegrees_Statics
	{
		struct VRIKFingersFKIKSolver_eventConvertFingerRotationToDegrees_Parms
		{
			FVRIK_FingerRotation InFingerRot;
			FVRIK_FingerRotation ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InFingerRot_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InFingerRot;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationToDegrees_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKFingersFKIKSolver_eventConvertFingerRotationToDegrees_Parms, ReturnValue), Z_Construct_UScriptStruct_FVRIK_FingerRotation, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationToDegrees_Statics::NewProp_InFingerRot_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationToDegrees_Statics::NewProp_InFingerRot = { "InFingerRot", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKFingersFKIKSolver_eventConvertFingerRotationToDegrees_Parms, InFingerRot), Z_Construct_UScriptStruct_FVRIK_FingerRotation, METADATA_PARAMS(Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationToDegrees_Statics::NewProp_InFingerRot_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationToDegrees_Statics::NewProp_InFingerRot_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationToDegrees_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationToDegrees_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationToDegrees_Statics::NewProp_InFingerRot,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationToDegrees_Statics::Function_MetaDataParams[] = {
		{ "Category", "Fingers Solver" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Convert multiplier-based finger orientation (used in poses, VR input) to degrees (used in detailed VR input)\n@param        InFingerRot             Finger orientation with values from 0 to 1\n@return                                       Finger orientation in degrees" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationToDegrees_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKFingersFKIKSolver, nullptr, "ConvertFingerRotationToDegrees", sizeof(VRIKFingersFKIKSolver_eventConvertFingerRotationToDegrees_Parms), Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationToDegrees_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationToDegrees_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationToDegrees_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationToDegrees_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationToDegrees()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationToDegrees_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKFingersFKIKSolver_CreateFingersFKIKSolver_Statics
	{
		struct VRIKFingersFKIKSolver_eventCreateFingersFKIKSolver_Parms
		{
			UVRIKFingersSolverSetup* SolverSetup;
			USkeletalMeshComponent* SkeletalMeshComp;
			UVRIKFingersFKIKSolver* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SkeletalMeshComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SkeletalMeshComp;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SolverSetup;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_CreateFingersFKIKSolver_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKFingersFKIKSolver_eventCreateFingersFKIKSolver_Parms, ReturnValue), Z_Construct_UClass_UVRIKFingersFKIKSolver_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKFingersFKIKSolver_CreateFingersFKIKSolver_Statics::NewProp_SkeletalMeshComp_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_CreateFingersFKIKSolver_Statics::NewProp_SkeletalMeshComp = { "SkeletalMeshComp", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKFingersFKIKSolver_eventCreateFingersFKIKSolver_Parms, SkeletalMeshComp), Z_Construct_UClass_USkeletalMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UVRIKFingersFKIKSolver_CreateFingersFKIKSolver_Statics::NewProp_SkeletalMeshComp_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_CreateFingersFKIKSolver_Statics::NewProp_SkeletalMeshComp_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_CreateFingersFKIKSolver_Statics::NewProp_SolverSetup = { "SolverSetup", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKFingersFKIKSolver_eventCreateFingersFKIKSolver_Parms, SolverSetup), Z_Construct_UClass_UVRIKFingersSolverSetup_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKFingersFKIKSolver_CreateFingersFKIKSolver_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_CreateFingersFKIKSolver_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_CreateFingersFKIKSolver_Statics::NewProp_SkeletalMeshComp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_CreateFingersFKIKSolver_Statics::NewProp_SolverSetup,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKFingersFKIKSolver_CreateFingersFKIKSolver_Statics::Function_MetaDataParams[] = {
		{ "Category", "Fingers Solver" },
		{ "DisplayName", "Create Fingers FK/IK Solver" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Create and initialize new FingersFKIKSolver object\n@param        SolverSetup                     Reference to FingersSolverSetup object wtih information about fingers for this hand.\n@param        SkeletalMeshComp        Reference to controlled skeletal mesh component" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_CreateFingersFKIKSolver_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKFingersFKIKSolver, nullptr, "CreateFingersFKIKSolver", sizeof(VRIKFingersFKIKSolver_eventCreateFingersFKIKSolver_Parms), Z_Construct_UFunction_UVRIKFingersFKIKSolver_CreateFingersFKIKSolver_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_CreateFingersFKIKSolver_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKFingersFKIKSolver_CreateFingersFKIKSolver_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_CreateFingersFKIKSolver_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_CreateFingersFKIKSolver()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKFingersFKIKSolver_CreateFingersFKIKSolver_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerDescription_Statics
	{
		struct VRIKFingersFKIKSolver_eventGetFingerDescription_Parms
		{
			EVRIKFingerName FingerName;
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_FingerName;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_FingerName_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerDescription_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKFingersFKIKSolver_eventGetFingerDescription_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerDescription_Statics::NewProp_FingerName = { "FingerName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKFingersFKIKSolver_eventGetFingerDescription_Parms, FingerName), Z_Construct_UEnum_VRIKBodyRuntime_EVRIKFingerName, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerDescription_Statics::NewProp_FingerName_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerDescription_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerDescription_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerDescription_Statics::NewProp_FingerName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerDescription_Statics::NewProp_FingerName_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerDescription_Statics::Function_MetaDataParams[] = {
		{ "Category", "Fingers Solver" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Debug function, returns relative transforms of finger knuckles in String\n@param        FingerName              Name of finger to check\n@return                                       String formatted as A [<Alpha>] <KnuckleBone1> [loc=<RelaiveLocation> rol=<RelativeRotation>] <KnuckleBone2> [loc=<RelaiveLocation> rol=<RelativeRotation>] ..." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerDescription_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKFingersFKIKSolver, nullptr, "GetFingerDescription", sizeof(VRIKFingersFKIKSolver_eventGetFingerDescription_Parms), Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerDescription_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerDescription_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerDescription_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerDescription_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerDescription()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerDescription_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerKnuckles_Statics
	{
		struct VRIKFingersFKIKSolver_eventGetFingerKnuckles_Parms
		{
			EVRIKFingerName FingerName;
			TArray<FVRIK_Knuckle> OutKnuckles;
		};
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OutKnuckles;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutKnuckles_Inner;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_FingerName;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_FingerName_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerKnuckles_Statics::NewProp_OutKnuckles = { "OutKnuckles", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKFingersFKIKSolver_eventGetFingerKnuckles_Parms, OutKnuckles), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerKnuckles_Statics::NewProp_OutKnuckles_Inner = { "OutKnuckles", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVRIK_Knuckle, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerKnuckles_Statics::NewProp_FingerName = { "FingerName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKFingersFKIKSolver_eventGetFingerKnuckles_Parms, FingerName), Z_Construct_UEnum_VRIKBodyRuntime_EVRIKFingerName, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerKnuckles_Statics::NewProp_FingerName_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerKnuckles_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerKnuckles_Statics::NewProp_OutKnuckles,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerKnuckles_Statics::NewProp_OutKnuckles_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerKnuckles_Statics::NewProp_FingerName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerKnuckles_Statics::NewProp_FingerName_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerKnuckles_Statics::Function_MetaDataParams[] = {
		{ "Category", "Fingers Solver" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Get information about knuckles for specified finger\n@param        FingerName              Name of finger to check\n@return                                       Array of knuckles with transforms and bone names" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerKnuckles_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKFingersFKIKSolver, nullptr, "GetFingerKnuckles", sizeof(VRIKFingersFKIKSolver_eventGetFingerKnuckles_Parms), Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerKnuckles_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerKnuckles_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerKnuckles_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerKnuckles_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerKnuckles()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerKnuckles_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKFingersFKIKSolver_GrabObject_Statics
	{
		struct VRIKFingersFKIKSolver_eventGrabObject_Parms
		{
			UPrimitiveComponent* Object;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Object_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Object;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKFingersFKIKSolver_GrabObject_Statics::NewProp_Object_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_GrabObject_Statics::NewProp_Object = { "Object", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKFingersFKIKSolver_eventGrabObject_Parms, Object), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UVRIKFingersFKIKSolver_GrabObject_Statics::NewProp_Object_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_GrabObject_Statics::NewProp_Object_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKFingersFKIKSolver_GrabObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_GrabObject_Statics::NewProp_Object,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKFingersFKIKSolver_GrabObject_Statics::Function_MetaDataParams[] = {
		{ "Category", "VR Hands" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Grab gameplay object by hand. The function doesn't attach object to hand mesh and only apply rotation to fingers.\n@param GrabReferencePose      Name (key in FingerPoses map) of tracing reference pose. This pose will be adjusted by tracing\n@param Object                         Component to grab" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_GrabObject_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKFingersFKIKSolver, nullptr, "GrabObject", sizeof(VRIKFingersFKIKSolver_eventGrabObject_Parms), Z_Construct_UFunction_UVRIKFingersFKIKSolver_GrabObject_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_GrabObject_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKFingersFKIKSolver_GrabObject_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_GrabObject_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_GrabObject()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKFingersFKIKSolver_GrabObject_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKFingersFKIKSolver_Initialize_Statics
	{
		struct VRIKFingersFKIKSolver_eventInitialize_Parms
		{
			UVRIKFingersSolverSetup* SolverSetup;
			USkeletalMeshComponent* SkeletalMeshComp;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SkeletalMeshComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SkeletalMeshComp;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SolverSetup;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVRIKFingersFKIKSolver_Initialize_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VRIKFingersFKIKSolver_eventInitialize_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_Initialize_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VRIKFingersFKIKSolver_eventInitialize_Parms), &Z_Construct_UFunction_UVRIKFingersFKIKSolver_Initialize_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKFingersFKIKSolver_Initialize_Statics::NewProp_SkeletalMeshComp_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_Initialize_Statics::NewProp_SkeletalMeshComp = { "SkeletalMeshComp", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKFingersFKIKSolver_eventInitialize_Parms, SkeletalMeshComp), Z_Construct_UClass_USkeletalMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UVRIKFingersFKIKSolver_Initialize_Statics::NewProp_SkeletalMeshComp_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_Initialize_Statics::NewProp_SkeletalMeshComp_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_Initialize_Statics::NewProp_SolverSetup = { "SolverSetup", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKFingersFKIKSolver_eventInitialize_Parms, SolverSetup), Z_Construct_UClass_UVRIKFingersSolverSetup_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKFingersFKIKSolver_Initialize_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_Initialize_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_Initialize_Statics::NewProp_SkeletalMeshComp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_Initialize_Statics::NewProp_SolverSetup,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKFingersFKIKSolver_Initialize_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Initialize object. Should be called before using.\n@param        SolverSetup                     Reference to FingersSolverSetup object wtih information about fingers for this hand.\n@param        SkeletalMeshComp        Reference to controlled skeletal mesh component" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_Initialize_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKFingersFKIKSolver, nullptr, "Initialize", sizeof(VRIKFingersFKIKSolver_eventInitialize_Parms), Z_Construct_UFunction_UVRIKFingersFKIKSolver_Initialize_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_Initialize_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKFingersFKIKSolver_Initialize_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_Initialize_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_Initialize()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKFingersFKIKSolver_Initialize_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsFingerEnabled_Statics
	{
		struct VRIKFingersFKIKSolver_eventIsFingerEnabled_Parms
		{
			EVRIKFingerName FingerName;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_FingerName;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_FingerName_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsFingerEnabled_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VRIKFingersFKIKSolver_eventIsFingerEnabled_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsFingerEnabled_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VRIKFingersFKIKSolver_eventIsFingerEnabled_Parms), &Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsFingerEnabled_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsFingerEnabled_Statics::NewProp_FingerName = { "FingerName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKFingersFKIKSolver_eventIsFingerEnabled_Parms, FingerName), Z_Construct_UEnum_VRIKBodyRuntime_EVRIKFingerName, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsFingerEnabled_Statics::NewProp_FingerName_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsFingerEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsFingerEnabled_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsFingerEnabled_Statics::NewProp_FingerName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsFingerEnabled_Statics::NewProp_FingerName_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsFingerEnabled_Statics::Function_MetaDataParams[] = {
		{ "Category", "Fingers Solver" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Check if finger enabled\n@param        FingerName              Name of finger to check\n@return                                       True if finger was enabled in the solver" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsFingerEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKFingersFKIKSolver, nullptr, "IsFingerEnabled", sizeof(VRIKFingersFKIKSolver_eventIsFingerEnabled_Parms), Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsFingerEnabled_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsFingerEnabled_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsFingerEnabled_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsFingerEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsFingerEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsFingerEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsInitialized_Statics
	{
		struct VRIKFingersFKIKSolver_eventIsInitialized_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsInitialized_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VRIKFingersFKIKSolver_eventIsInitialized_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsInitialized_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VRIKFingersFKIKSolver_eventIsInitialized_Parms), &Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsInitialized_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsInitialized_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsInitialized_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsInitialized_Statics::Function_MetaDataParams[] = {
		{ "Category", "Fingers Solver" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Was object initialized successfully?" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsInitialized_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKFingersFKIKSolver, nullptr, "IsInitialized", sizeof(VRIKFingersFKIKSolver_eventIsInitialized_Parms), Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsInitialized_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsInitialized_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsInitialized_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsInitialized_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsInitialized()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsInitialized_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKFingersFKIKSolver_ReleaseObject_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKFingersFKIKSolver_ReleaseObject_Statics::Function_MetaDataParams[] = {
		{ "Category", "VR Hands" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Return fingers rotation from traced to current pose." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_ReleaseObject_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKFingersFKIKSolver, nullptr, "ReleaseObject", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKFingersFKIKSolver_ReleaseObject_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_ReleaseObject_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_ReleaseObject()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKFingersFKIKSolver_ReleaseObject_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingerEnabled_Statics
	{
		struct VRIKFingersFKIKSolver_eventSetFingerEnabled_Parms
		{
			EVRIKFingerName FingerName;
			bool bNewEnabled;
		};
		static void NewProp_bNewEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bNewEnabled;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_FingerName;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_FingerName_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingerEnabled_Statics::NewProp_bNewEnabled_SetBit(void* Obj)
	{
		((VRIKFingersFKIKSolver_eventSetFingerEnabled_Parms*)Obj)->bNewEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingerEnabled_Statics::NewProp_bNewEnabled = { "bNewEnabled", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VRIKFingersFKIKSolver_eventSetFingerEnabled_Parms), &Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingerEnabled_Statics::NewProp_bNewEnabled_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingerEnabled_Statics::NewProp_FingerName = { "FingerName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKFingersFKIKSolver_eventSetFingerEnabled_Parms, FingerName), Z_Construct_UEnum_VRIKBodyRuntime_EVRIKFingerName, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingerEnabled_Statics::NewProp_FingerName_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingerEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingerEnabled_Statics::NewProp_bNewEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingerEnabled_Statics::NewProp_FingerName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingerEnabled_Statics::NewProp_FingerName_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingerEnabled_Statics::Function_MetaDataParams[] = {
		{ "Category", "Fingers Solver" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Enable or disable finger solving\n@param        FingerName              Name of finger to enable or disable\n@param        bNewEnabled             Value to set" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingerEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKFingersFKIKSolver, nullptr, "SetFingerEnabled", sizeof(VRIKFingersFKIKSolver_eventSetFingerEnabled_Parms), Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingerEnabled_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingerEnabled_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingerEnabled_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingerEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingerEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingerEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersPose_Statics
	{
		struct VRIKFingersFKIKSolver_eventSetFingersPose_Parms
		{
			FVRIK_FingersPosePreset NewPose;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewPose_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NewPose;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersPose_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VRIKFingersFKIKSolver_eventSetFingersPose_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersPose_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VRIKFingersFKIKSolver_eventSetFingersPose_Parms), &Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersPose_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersPose_Statics::NewProp_NewPose_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersPose_Statics::NewProp_NewPose = { "NewPose", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKFingersFKIKSolver_eventSetFingersPose_Parms, NewPose), Z_Construct_UScriptStruct_FVRIK_FingersPosePreset, METADATA_PARAMS(Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersPose_Statics::NewProp_NewPose_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersPose_Statics::NewProp_NewPose_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersPose_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersPose_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersPose_Statics::NewProp_NewPose,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersPose_Statics::Function_MetaDataParams[] = {
		{ "Category", "VR Hands" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Set fingers pose from FingerPoses map by name.\n@param PoseName Key in FingerPoses map" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersPose_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKFingersFKIKSolver, nullptr, "SetFingersPose", sizeof(VRIKFingersFKIKSolver_eventSetFingersPose_Parms), Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersPose_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersPose_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersPose_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersPose_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersPose()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersPose_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersTraceReferencePose_Statics
	{
		struct VRIKFingersFKIKSolver_eventSetFingersTraceReferencePose_Parms
		{
			FVRIK_FingersPosePreset NewRefPose;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewRefPose_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NewRefPose;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersTraceReferencePose_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VRIKFingersFKIKSolver_eventSetFingersTraceReferencePose_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersTraceReferencePose_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VRIKFingersFKIKSolver_eventSetFingersTraceReferencePose_Parms), &Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersTraceReferencePose_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersTraceReferencePose_Statics::NewProp_NewRefPose_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersTraceReferencePose_Statics::NewProp_NewRefPose = { "NewRefPose", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKFingersFKIKSolver_eventSetFingersTraceReferencePose_Parms, NewRefPose), Z_Construct_UScriptStruct_FVRIK_FingersPosePreset, METADATA_PARAMS(Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersTraceReferencePose_Statics::NewProp_NewRefPose_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersTraceReferencePose_Statics::NewProp_NewRefPose_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersTraceReferencePose_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersTraceReferencePose_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersTraceReferencePose_Statics::NewProp_NewRefPose,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersTraceReferencePose_Statics::Function_MetaDataParams[] = {
		{ "Category", "VR Hands" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Set reference pose for fingers tracing. It should be 'grabbing' pose which need\nto be adjusted to attached object by tracing.\n@param PoseName Key in FingerPoses map" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersTraceReferencePose_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKFingersFKIKSolver, nullptr, "SetFingersTraceReferencePose", sizeof(VRIKFingersFKIKSolver_eventSetFingersTraceReferencePose_Parms), Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersTraceReferencePose_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersTraceReferencePose_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersTraceReferencePose_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersTraceReferencePose_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersTraceReferencePose()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersTraceReferencePose_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetVRInputReferencePose_Statics
	{
		struct VRIKFingersFKIKSolver_eventSetVRInputReferencePose_Parms
		{
			FVRIK_FingersPosePreset NewRefPose;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewRefPose_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NewRefPose;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetVRInputReferencePose_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VRIKFingersFKIKSolver_eventSetVRInputReferencePose_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetVRInputReferencePose_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VRIKFingersFKIKSolver_eventSetVRInputReferencePose_Parms), &Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetVRInputReferencePose_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetVRInputReferencePose_Statics::NewProp_NewRefPose_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetVRInputReferencePose_Statics::NewProp_NewRefPose = { "NewRefPose", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKFingersFKIKSolver_eventSetVRInputReferencePose_Parms, NewRefPose), Z_Construct_UScriptStruct_FVRIK_FingersPosePreset, METADATA_PARAMS(Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetVRInputReferencePose_Statics::NewProp_NewRefPose_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetVRInputReferencePose_Statics::NewProp_NewRefPose_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetVRInputReferencePose_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetVRInputReferencePose_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetVRInputReferencePose_Statics::NewProp_NewRefPose,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetVRInputReferencePose_Statics::Function_MetaDataParams[] = {
		{ "Category", "VR Hands" },
		{ "DisplayName", "Set VR Input Reference Pose" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Set reference pose for VR input from Valve Knuckles or Oculus Touch. It should be pose of fully open hand.\n@param PoseName Key in FingerPoses map" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetVRInputReferencePose_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKFingersFKIKSolver, nullptr, "SetVRInputReferencePose", sizeof(VRIKFingersFKIKSolver_eventSetVRInputReferencePose_Parms), Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetVRInputReferencePose_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetVRInputReferencePose_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetVRInputReferencePose_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetVRInputReferencePose_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetVRInputReferencePose()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetVRInputReferencePose_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKFingersFKIKSolver_Trace_Statics
	{
		struct VRIKFingersFKIKSolver_eventTrace_Parms
		{
			bool bTracingInTick;
		};
		static void NewProp_bTracingInTick_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bTracingInTick;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVRIKFingersFKIKSolver_Trace_Statics::NewProp_bTracingInTick_SetBit(void* Obj)
	{
		((VRIKFingersFKIKSolver_eventTrace_Parms*)Obj)->bTracingInTick = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_Trace_Statics::NewProp_bTracingInTick = { "bTracingInTick", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VRIKFingersFKIKSolver_eventTrace_Parms), &Z_Construct_UFunction_UVRIKFingersFKIKSolver_Trace_Statics::NewProp_bTracingInTick_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKFingersFKIKSolver_Trace_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_Trace_Statics::NewProp_bTracingInTick,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKFingersFKIKSolver_Trace_Statics::Function_MetaDataParams[] = {
		{ "Category", "Fingers Solver" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Trace scene to find object to grab and update fingers positions. Don't call this function directly.\n@param        bTracingInTick          Notifies if function called every tick. If true, interpolation would be used to soften fingers movement. Otherwise, position would be updated instantly." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_Trace_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKFingersFKIKSolver, nullptr, "Trace", sizeof(VRIKFingersFKIKSolver_eventTrace_Parms), Z_Construct_UFunction_UVRIKFingersFKIKSolver_Trace_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_Trace_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKFingersFKIKSolver_Trace_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_Trace_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_Trace()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKFingersFKIKSolver_Trace_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKFingersFKIKSolver_UpdateFingers_Statics
	{
		struct VRIKFingersFKIKSolver_eventUpdateFingers_Parms
		{
			bool bTrace;
			bool bUpdateFingersPose;
		};
		static void NewProp_bUpdateFingersPose_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUpdateFingersPose;
		static void NewProp_bTrace_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bTrace;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVRIKFingersFKIKSolver_UpdateFingers_Statics::NewProp_bUpdateFingersPose_SetBit(void* Obj)
	{
		((VRIKFingersFKIKSolver_eventUpdateFingers_Parms*)Obj)->bUpdateFingersPose = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_UpdateFingers_Statics::NewProp_bUpdateFingersPose = { "bUpdateFingersPose", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VRIKFingersFKIKSolver_eventUpdateFingers_Parms), &Z_Construct_UFunction_UVRIKFingersFKIKSolver_UpdateFingers_Statics::NewProp_bUpdateFingersPose_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UVRIKFingersFKIKSolver_UpdateFingers_Statics::NewProp_bTrace_SetBit(void* Obj)
	{
		((VRIKFingersFKIKSolver_eventUpdateFingers_Parms*)Obj)->bTrace = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_UpdateFingers_Statics::NewProp_bTrace = { "bTrace", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VRIKFingersFKIKSolver_eventUpdateFingers_Parms), &Z_Construct_UFunction_UVRIKFingersFKIKSolver_UpdateFingers_Statics::NewProp_bTrace_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKFingersFKIKSolver_UpdateFingers_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_UpdateFingers_Statics::NewProp_bUpdateFingersPose,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKFingersFKIKSolver_UpdateFingers_Statics::NewProp_bTrace,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKFingersFKIKSolver_UpdateFingers_Statics::Function_MetaDataParams[] = {
		{ "Category", "Fingers Solver" },
		{ "CPP_Default_bTrace", "false" },
		{ "CPP_Default_bUpdateFingersPose", "true" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Update current fingers transform. Call this function at the end of the Update() event in animation blueprint.\n@param        bTrace                          Should do tracing to detect object in hand (reference pose should be initiaized)?\n@param        bUpdateFingersPose      Should apply fingers pose initialized by SetFingersPose(...)?" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKFingersFKIKSolver_UpdateFingers_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKFingersFKIKSolver, nullptr, "UpdateFingers", sizeof(VRIKFingersFKIKSolver_eventUpdateFingers_Parms), Z_Construct_UFunction_UVRIKFingersFKIKSolver_UpdateFingers_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_UpdateFingers_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKFingersFKIKSolver_UpdateFingers_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKFingersFKIKSolver_UpdateFingers_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKFingersFKIKSolver_UpdateFingers()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKFingersFKIKSolver_UpdateFingers_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UVRIKFingersFKIKSolver_NoRegister()
	{
		return UVRIKFingersFKIKSolver::StaticClass();
	}
	struct Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HandSideMultiplier_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HandSideMultiplier;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentFingersPose_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CurrentFingersPose;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VRStatus_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_VRStatus;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_VRStatus_Key_KeyProp;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_VRStatus_Key_KeyProp_Underlying;
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_VRStatus_ValueProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TraceStartTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TraceStartTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseRuntimeFingersPose_MetaData[];
#endif
		static void NewProp_bUseRuntimeFingersPose_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseRuntimeFingersPose;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bHasDetailedVRInputInFrame_MetaData[];
#endif
		static void NewProp_bHasDetailedVRInputInFrame_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bHasDetailedVRInputInFrame;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bHasVRInputInFrame_MetaData[];
#endif
		static void NewProp_bHasVRInputInFrame_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bHasVRInputInFrame;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VRInputDetailed_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_VRInputDetailed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VRInput_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_VRInput;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TracingStatus_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_TracingStatus;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TracingStatus_Key_KeyProp;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TracingStatus_Key_KeyProp_Underlying;
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_TracingStatus_ValueProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FingersSolverSetup_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FingersSolverSetup;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Mesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Mesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsInitialized_MetaData[];
#endif
		static void NewProp_bIsInitialized_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsInitialized;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDrawDebugGeometry_MetaData[];
#endif
		static void NewProp_bDrawDebugGeometry_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDrawDebugGeometry;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PosesInterpolationSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PosesInterpolationSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputMaxRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InputMaxRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputMinRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InputMinRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilterObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FilterObject;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Fingers_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Fingers;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Fingers_Key_KeyProp;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Fingers_Key_KeyProp_Underlying;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Fingers_ValueProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bTraceByObjectType_MetaData[];
#endif
		static void NewProp_bTraceByObjectType_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bTraceByObjectType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TraceChannel_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TraceChannel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TraceHalfDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TraceHalfDistance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Hand_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Hand;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Hand_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_VRIKBodyRuntime,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInput, "ApplyVRInput" }, // 3580530878
		{ &Z_Construct_UFunction_UVRIKFingersFKIKSolver_ApplyVRInputDetailed, "ApplyVRInputDetailed" }, // 1008124747
		{ &Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationFromDegrees, "ConvertFingerRotationFromDegrees" }, // 2028268379
		{ &Z_Construct_UFunction_UVRIKFingersFKIKSolver_ConvertFingerRotationToDegrees, "ConvertFingerRotationToDegrees" }, // 1605038733
		{ &Z_Construct_UFunction_UVRIKFingersFKIKSolver_CreateFingersFKIKSolver, "CreateFingersFKIKSolver" }, // 4274225309
		{ &Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerDescription, "GetFingerDescription" }, // 4256221631
		{ &Z_Construct_UFunction_UVRIKFingersFKIKSolver_GetFingerKnuckles, "GetFingerKnuckles" }, // 321062047
		{ &Z_Construct_UFunction_UVRIKFingersFKIKSolver_GrabObject, "GrabObject" }, // 59514316
		{ &Z_Construct_UFunction_UVRIKFingersFKIKSolver_Initialize, "Initialize" }, // 2882861800
		{ &Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsFingerEnabled, "IsFingerEnabled" }, // 3961532305
		{ &Z_Construct_UFunction_UVRIKFingersFKIKSolver_IsInitialized, "IsInitialized" }, // 1844221299
		{ &Z_Construct_UFunction_UVRIKFingersFKIKSolver_ReleaseObject, "ReleaseObject" }, // 129858902
		{ &Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingerEnabled, "SetFingerEnabled" }, // 1534237671
		{ &Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersPose, "SetFingersPose" }, // 2107351579
		{ &Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetFingersTraceReferencePose, "SetFingersTraceReferencePose" }, // 728874379
		{ &Z_Construct_UFunction_UVRIKFingersFKIKSolver_SetVRInputReferencePose, "SetVRInputReferencePose" }, // 2918830174
		{ &Z_Construct_UFunction_UVRIKFingersFKIKSolver_Trace, "Trace" }, // 2182902707
		{ &Z_Construct_UFunction_UVRIKFingersFKIKSolver_UpdateFingers, "UpdateFingers" }, // 351124583
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "VRIKFingersFKIKSolver.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Trace and calculate fingers transforms\nResult saved in FVRIK_Knuckle::RelativeTransform" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_HandSideMultiplier_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_HandSideMultiplier = { "HandSideMultiplier", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKFingersFKIKSolver, HandSideMultiplier), METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_HandSideMultiplier_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_HandSideMultiplier_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_CurrentFingersPose_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_CurrentFingersPose = { "CurrentFingersPose", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKFingersFKIKSolver, CurrentFingersPose), Z_Construct_UScriptStruct_FVRIK_FingersPosePreset, METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_CurrentFingersPose_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_CurrentFingersPose_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_VRStatus_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_VRStatus = { "VRStatus", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKFingersFKIKSolver, VRStatus), METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_VRStatus_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_VRStatus_MetaData)) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_VRStatus_Key_KeyProp = { "VRStatus_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UEnum_VRIKBodyRuntime_EVRIKFingerName, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_VRStatus_Key_KeyProp_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_VRStatus_ValueProp = { "VRStatus", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_TraceStartTime_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Trace start time when grabbing object" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_TraceStartTime = { "TraceStartTime", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKFingersFKIKSolver, TraceStartTime), METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_TraceStartTime_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_TraceStartTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bUseRuntimeFingersPose_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Has traced pose for fingers?" },
	};
#endif
	void Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bUseRuntimeFingersPose_SetBit(void* Obj)
	{
		((UVRIKFingersFKIKSolver*)Obj)->bUseRuntimeFingersPose = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bUseRuntimeFingersPose = { "bUseRuntimeFingersPose", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKFingersFKIKSolver), &Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bUseRuntimeFingersPose_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bUseRuntimeFingersPose_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bUseRuntimeFingersPose_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bHasDetailedVRInputInFrame_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
	};
#endif
	void Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bHasDetailedVRInputInFrame_SetBit(void* Obj)
	{
		((UVRIKFingersFKIKSolver*)Obj)->bHasDetailedVRInputInFrame = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bHasDetailedVRInputInFrame = { "bHasDetailedVRInputInFrame", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKFingersFKIKSolver), &Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bHasDetailedVRInputInFrame_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bHasDetailedVRInputInFrame_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bHasDetailedVRInputInFrame_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bHasVRInputInFrame_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
	};
#endif
	void Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bHasVRInputInFrame_SetBit(void* Obj)
	{
		((UVRIKFingersFKIKSolver*)Obj)->bHasVRInputInFrame = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bHasVRInputInFrame = { "bHasVRInputInFrame", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKFingersFKIKSolver), &Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bHasVRInputInFrame_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bHasVRInputInFrame_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bHasVRInputInFrame_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_VRInputDetailed_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_VRInputDetailed = { "VRInputDetailed", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKFingersFKIKSolver, VRInputDetailed), Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo, METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_VRInputDetailed_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_VRInputDetailed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_VRInput_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_VRInput = { "VRInput", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKFingersFKIKSolver, VRInput), Z_Construct_UScriptStruct_FVRIK_FingersPosePreset, METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_VRInput_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_VRInput_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_TracingStatus_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_TracingStatus = { "TracingStatus", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKFingersFKIKSolver, TracingStatus), METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_TracingStatus_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_TracingStatus_MetaData)) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_TracingStatus_Key_KeyProp = { "TracingStatus_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UEnum_VRIKBodyRuntime_EVRIKFingerName, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_TracingStatus_Key_KeyProp_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_TracingStatus_ValueProp = { "TracingStatus", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_FingersSolverSetup_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_FingersSolverSetup = { "FingersSolverSetup", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKFingersFKIKSolver, FingersSolverSetup), Z_Construct_UClass_UVRIKFingersSolverSetup_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_FingersSolverSetup_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_FingersSolverSetup_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_Mesh_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_Mesh = { "Mesh", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKFingersFKIKSolver, Mesh), Z_Construct_UClass_USkeletalMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_Mesh_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_Mesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bIsInitialized_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
	};
#endif
	void Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bIsInitialized_SetBit(void* Obj)
	{
		((UVRIKFingersFKIKSolver*)Obj)->bIsInitialized = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bIsInitialized = { "bIsInitialized", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKFingersFKIKSolver), &Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bIsInitialized_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bIsInitialized_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bIsInitialized_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bDrawDebugGeometry_MetaData[] = {
		{ "Category", "Debug" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Should draw debug lines for tracing?" },
	};
#endif
	void Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bDrawDebugGeometry_SetBit(void* Obj)
	{
		((UVRIKFingersFKIKSolver*)Obj)->bDrawDebugGeometry = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bDrawDebugGeometry = { "bDrawDebugGeometry", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKFingersFKIKSolver), &Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bDrawDebugGeometry_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bDrawDebugGeometry_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bDrawDebugGeometry_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_PosesInterpolationSpeed_MetaData[] = {
		{ "Category", "Setup" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Interpolatoin speed for processing poses applied by SetFingersPose function" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_PosesInterpolationSpeed = { "PosesInterpolationSpeed", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKFingersFKIKSolver, PosesInterpolationSpeed), METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_PosesInterpolationSpeed_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_PosesInterpolationSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_InputMaxRotation_MetaData[] = {
		{ "Category", "Setup" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Upper border for VR input (in degrees). VR input values (0..1) are interpolated to (InputMinRotation, InputMaxRotation)" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_InputMaxRotation = { "InputMaxRotation", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKFingersFKIKSolver, InputMaxRotation), METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_InputMaxRotation_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_InputMaxRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_InputMinRotation_MetaData[] = {
		{ "Category", "Setup" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Lower border for VR input (in degrees). VR input values (0..1) are interpolated to (InputMinRotation, InputMaxRotation)" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_InputMinRotation = { "InputMinRotation", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKFingersFKIKSolver, InputMinRotation), METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_InputMinRotation_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_InputMinRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_FilterObject_MetaData[] = {
		{ "Category", "Setup" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "If valid, Trace() function would ignore all objects but this" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_FilterObject = { "FilterObject", nullptr, (EPropertyFlags)0x001000000008000c, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKFingersFKIKSolver, FilterObject), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_FilterObject_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_FilterObject_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_Fingers_MetaData[] = {
		{ "Category", "Setup" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Out fingers data. Initialized from UVRIKFingersSolverSetup object." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_Fingers = { "Fingers", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKFingersFKIKSolver, Fingers), METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_Fingers_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_Fingers_MetaData)) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_Fingers_Key_KeyProp = { "Fingers_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UEnum_VRIKBodyRuntime_EVRIKFingerName, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_Fingers_Key_KeyProp_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_Fingers_ValueProp = { "Fingers", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FVRIK_FingerSolver, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bTraceByObjectType_MetaData[] = {
		{ "Category", "VR Hand" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Should trace by trace channel (false) or object type (true)" },
	};
#endif
	void Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bTraceByObjectType_SetBit(void* Obj)
	{
		((UVRIKFingersFKIKSolver*)Obj)->bTraceByObjectType = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bTraceByObjectType = { "bTraceByObjectType", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKFingersFKIKSolver), &Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bTraceByObjectType_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bTraceByObjectType_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bTraceByObjectType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_TraceChannel_MetaData[] = {
		{ "Category", "Setup" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Trace channel to probe: Visible, Camera etc" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_TraceChannel = { "TraceChannel", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKFingersFKIKSolver, TraceChannel), Z_Construct_UEnum_Engine_ECollisionChannel, METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_TraceChannel_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_TraceChannel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_TraceHalfDistance_MetaData[] = {
		{ "Category", "Setup" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Trace distance from knuckle to inside and outside" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_TraceHalfDistance = { "TraceHalfDistance", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKFingersFKIKSolver, TraceHalfDistance), METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_TraceHalfDistance_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_TraceHalfDistance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_Hand_MetaData[] = {
		{ "Category", "Setup" },
		{ "ModuleRelativePath", "Public/VRIKFingersFKIKSolver.h" },
		{ "ToolTip", "Hand side associated with this object" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_Hand = { "Hand", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKFingersFKIKSolver, Hand), Z_Construct_UEnum_VRIKBodyRuntime_EVRIK_VRHand, METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_Hand_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_Hand_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_Hand_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_HandSideMultiplier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_CurrentFingersPose,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_VRStatus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_VRStatus_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_VRStatus_Key_KeyProp_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_VRStatus_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_TraceStartTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bUseRuntimeFingersPose,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bHasDetailedVRInputInFrame,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bHasVRInputInFrame,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_VRInputDetailed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_VRInput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_TracingStatus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_TracingStatus_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_TracingStatus_Key_KeyProp_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_TracingStatus_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_FingersSolverSetup,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_Mesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bIsInitialized,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bDrawDebugGeometry,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_PosesInterpolationSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_InputMaxRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_InputMinRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_FilterObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_Fingers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_Fingers_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_Fingers_Key_KeyProp_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_Fingers_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_bTraceByObjectType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_TraceChannel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_TraceHalfDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_Hand,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::NewProp_Hand_Underlying,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVRIKFingersFKIKSolver>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::ClassParams = {
		&UVRIKFingersFKIKSolver::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVRIKFingersFKIKSolver()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVRIKFingersFKIKSolver, 3482890379);
	template<> VRIKBODYRUNTIME_API UClass* StaticClass<UVRIKFingersFKIKSolver>()
	{
		return UVRIKFingersFKIKSolver::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVRIKFingersFKIKSolver(Z_Construct_UClass_UVRIKFingersFKIKSolver, &UVRIKFingersFKIKSolver::StaticClass, TEXT("/Script/VRIKBodyRuntime"), TEXT("UVRIKFingersFKIKSolver"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVRIKFingersFKIKSolver);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
