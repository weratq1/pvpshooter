// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VRIKBodyRuntime/Public/VRIKBody.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVRIKBody() {}
// Cross Module References
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UDelegateFunction_VRIKBodyRuntime_IKBodySimple__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_VRIKBodyRuntime();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UDelegateFunction_VRIKBodyRuntime_IKBodyRepeatable__DelegateSignature();
	VRIKBODYRUNTIME_API UEnum* Z_Construct_UEnum_VRIKBodyRuntime_EBodyOrientation();
	VRIKBODYRUNTIME_API UEnum* Z_Construct_UEnum_VRIKBodyRuntime_EVRInputSetup();
	VRIKBODYRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FIKBodyData();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	VRIKBODYRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FVRInputData();
	VRIKBODYRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FNetworkIKBodyData();
	VRIKBODYRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FNetworkTransform();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FQuat();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FVector_NetQuantize100();
	VRIKBODYRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FCalibratedBody();
	VRIKBODYRUNTIME_API UClass* Z_Construct_UClass_UVRIKBody_NoRegister();
	VRIKBODYRUNTIME_API UClass* Z_Construct_UClass_UVRIKBody();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_ActivateInput();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_AttachHandToComponent();
	ENGINE_API UClass* Z_Construct_UClass_UPrimitiveComponent_NoRegister();
	INPUTCORE_API UEnum* Z_Construct_UEnum_InputCore_EControllerHand();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_AutoCalibrateBodyAtPose();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_CalcFeetIKTransforms2();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_CalcFeetInstantaneousTransforms();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_CalcHandIKTransforms();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_CalcShouldersWithOffset();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtIPose();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtTPose();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_CalibrateSkeleton();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_ClampElbowRotation();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_ClientResetCalibrationStatus();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_ClientRestoreCalibratedBody();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_ComputeFrame();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_ConvertDataToSkeletonFriendly();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_DeactivateInput();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_DetachHandFromComponent();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_DoResetCalibrationStatus();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_DoRestoreCalibratedBody();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_GetCalibratedBody();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_GetCharacterHeight();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_GetCharacterLegsLength();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_GetClearArmSpan();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_GetClearCharacterHeight();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_GetCorrelationKoef();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_GetElbowJointTarget();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_GetKneeJointTarget();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_GetLastFrameData();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_Initialize();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_IsBodyCalibrated();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_IsInitialized();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_IsValidCalibrationData();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_OnRep_InputBodyState();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_OnRep_InputHandLeft();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_OnRep_InputHandRight();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_OnRep_InputHMD();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_PackDataForReplication();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_ResetCalibrationStatus();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_ResetFootsTimerL_Tick();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_ResetFootsTimerR_Tick();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_ResetTorso();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_RestoreCalibratedBody();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_RibcageYawTimer_Tick();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_ServerResetCalibrationStatus();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_ServerRestoreCalibratedBody();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_ServerUpdateBodyState();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_ServerUpdateInputs();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_SetManualBodyOrientation();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_TraceFloor();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_UpdateInputTransforms();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_UseAutoBodyOrientation();
	VRIKBODYRUNTIME_API UFunction* Z_Construct_UFunction_UVRIKBody_VRInputTimer_Tick();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FTimerHandle();
	ENGINE_API UClass* Z_Construct_UClass_APawn_NoRegister();
	ENGINE_API UEnum* Z_Construct_UEnum_Engine_ECollisionChannel();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_VRIKBodyRuntime_IKBodySimple__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_VRIKBodyRuntime_IKBodySimple__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_VRIKBodyRuntime_IKBodySimple__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_VRIKBodyRuntime, nullptr, "IKBodySimple__DelegateSignature", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_VRIKBodyRuntime_IKBodySimple__DelegateSignature_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UDelegateFunction_VRIKBodyRuntime_IKBodySimple__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_VRIKBodyRuntime_IKBodySimple__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_VRIKBodyRuntime_IKBodySimple__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_VRIKBodyRuntime_IKBodyRepeatable__DelegateSignature_Statics
	{
		struct _Script_VRIKBodyRuntime_eventIKBodyRepeatable_Parms
		{
			int32 Iteration;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Iteration;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UDelegateFunction_VRIKBodyRuntime_IKBodyRepeatable__DelegateSignature_Statics::NewProp_Iteration = { "Iteration", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_VRIKBodyRuntime_eventIKBodyRepeatable_Parms, Iteration), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_VRIKBodyRuntime_IKBodyRepeatable__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_VRIKBodyRuntime_IKBodyRepeatable__DelegateSignature_Statics::NewProp_Iteration,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_VRIKBodyRuntime_IKBodyRepeatable__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_VRIKBodyRuntime_IKBodyRepeatable__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_VRIKBodyRuntime, nullptr, "IKBodyRepeatable__DelegateSignature", sizeof(_Script_VRIKBodyRuntime_eventIKBodyRepeatable_Parms), Z_Construct_UDelegateFunction_VRIKBodyRuntime_IKBodyRepeatable__DelegateSignature_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UDelegateFunction_VRIKBodyRuntime_IKBodyRepeatable__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_VRIKBodyRuntime_IKBodyRepeatable__DelegateSignature_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UDelegateFunction_VRIKBodyRuntime_IKBodyRepeatable__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_VRIKBodyRuntime_IKBodyRepeatable__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_VRIKBodyRuntime_IKBodyRepeatable__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	static UEnum* EBodyOrientation_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_VRIKBodyRuntime_EBodyOrientation, Z_Construct_UPackage__Script_VRIKBodyRuntime(), TEXT("EBodyOrientation"));
		}
		return Singleton;
	}
	template<> VRIKBODYRUNTIME_API UEnum* StaticEnum<EBodyOrientation>()
	{
		return EBodyOrientation_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EBodyOrientation(EBodyOrientation_StaticEnum, TEXT("/Script/VRIKBodyRuntime"), TEXT("EBodyOrientation"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_VRIKBodyRuntime_EBodyOrientation_Hash() { return 3278599083U; }
	UEnum* Z_Construct_UEnum_VRIKBodyRuntime_EBodyOrientation()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_VRIKBodyRuntime();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EBodyOrientation"), 0, Get_Z_Construct_UEnum_VRIKBodyRuntime_EBodyOrientation_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EBodyOrientation::Stand", (int64)EBodyOrientation::Stand },
				{ "EBodyOrientation::Sit", (int64)EBodyOrientation::Sit },
				{ "EBodyOrientation::Crawl", (int64)EBodyOrientation::Crawl },
				{ "EBodyOrientation::LieDown_FaceUp", (int64)EBodyOrientation::LieDown_FaceUp },
				{ "EBodyOrientation::LieDown_FaceDown", (int64)EBodyOrientation::LieDown_FaceDown },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Crawl.DisplayName", "Crawl" },
				{ "IsBlueprintBase", "true" },
				{ "LieDown_FaceDown.DisplayName", "Lie Down with Face Down" },
				{ "LieDown_FaceUp.DisplayName", "Lie Down with Face Up" },
				{ "ModuleRelativePath", "Public/VRIKBody.h" },
				{ "Sit.DisplayName", "Sit" },
				{ "Stand.DisplayName", "Stand" },
				{ "ToolTip", "Current body orientation state. Body calculation depends on this state." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_VRIKBodyRuntime,
				nullptr,
				"EBodyOrientation",
				"EBodyOrientation",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EVRInputSetup_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_VRIKBodyRuntime_EVRInputSetup, Z_Construct_UPackage__Script_VRIKBodyRuntime(), TEXT("EVRInputSetup"));
		}
		return Singleton;
	}
	template<> VRIKBODYRUNTIME_API UEnum* StaticEnum<EVRInputSetup>()
	{
		return EVRInputSetup_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EVRInputSetup(EVRInputSetup_StaticEnum, TEXT("/Script/VRIKBodyRuntime"), TEXT("EVRInputSetup"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_VRIKBodyRuntime_EVRInputSetup_Hash() { return 923283097U; }
	UEnum* Z_Construct_UEnum_VRIKBodyRuntime_EVRInputSetup()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_VRIKBodyRuntime();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EVRInputSetup"), 0, Get_Z_Construct_UEnum_VRIKBodyRuntime_EVRInputSetup_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EVRInputSetup::DirectVRInput", (int64)EVRInputSetup::DirectVRInput },
				{ "EVRInputSetup::InputFromComponents", (int64)EVRInputSetup::InputFromComponents },
				{ "EVRInputSetup::InputFromVariables", (int64)EVRInputSetup::InputFromVariables },
				{ "EVRInputSetup::EmulateInput", (int64)EVRInputSetup::EmulateInput },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "DirectVRInput.DisplayName", "Direct VR Input" },
				{ "EmulateInput.DisplayName", "Emulate (Debug)" },
				{ "InputFromComponents.DisplayName", "Input From Components" },
				{ "InputFromVariables.DisplayName", "Input From Variables" },
				{ "IsBlueprintBase", "true" },
				{ "ModuleRelativePath", "Public/VRIKBody.h" },
				{ "ToolTip", "VR headset and controllers input mode:\nDirectVRInput - input from UHeadMountedDisplayFunctionLibrary and I Motioncontroller interface\nInputFromComponents - input from scene components (recommended)\nInputFromVariables - input from variables manually updated in blueprint" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_VRIKBodyRuntime,
				nullptr,
				"EVRInputSetup",
				"EVRInputSetup",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FIKBodyData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern VRIKBODYRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FIKBodyData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FIKBodyData, Z_Construct_UPackage__Script_VRIKBodyRuntime(), TEXT("IKBodyData"), sizeof(FIKBodyData), Get_Z_Construct_UScriptStruct_FIKBodyData_Hash());
	}
	return Singleton;
}
template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<FIKBodyData>()
{
	return FIKBodyData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FIKBodyData(FIKBodyData::StaticStruct, TEXT("/Script/VRIKBodyRuntime"), TEXT("IKBodyData"), false, nullptr, nullptr);
static struct FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFIKBodyData
{
	FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFIKBodyData()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("IKBodyData")),new UScriptStruct::TCppStructOps<FIKBodyData>);
	}
} ScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFIKBodyData;
	struct Z_Construct_UScriptStruct_FIKBodyData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LowerarmTwistLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LowerarmTwistLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LowerarmTwistRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LowerarmTwistRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GroundLevelLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_GroundLevelLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GroundLevelRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_GroundLevelRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GroundLevel_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_GroundLevel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Velocity_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Velocity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IsSitting_MetaData[];
#endif
		static void NewProp_IsSitting_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsSitting;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BodyOrientation_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_BodyOrientation;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_BodyOrientation_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IsJumping_MetaData[];
#endif
		static void NewProp_IsJumping_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsJumping;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FootLeftCurrent_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FootLeftCurrent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FootRightCurrent_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FootRightCurrent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CalfLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CalfLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ThighLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ThighLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CalfRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CalfRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ThighRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ThighRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FootLeftTarget_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FootLeftTarget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FootRightTarget_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FootRightTarget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ElbowJointTargetLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ElbowJointTargetLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ElbowJointTargetRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ElbowJointTargetRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HandLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HandLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ForearmLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ForearmLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UpperarmLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UpperarmLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CollarboneLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CollarboneLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HandRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HandRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ForearmRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ForearmRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UpperarmRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UpperarmRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CollarboneRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CollarboneRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Head_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Head;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Neck_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Neck;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Ribcage_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Ribcage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Pelvis_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Pelvis;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Calculated instantaneous body state" },
	};
#endif
	void* Z_Construct_UScriptStruct_FIKBodyData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FIKBodyData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_LowerarmTwistLeft_MetaData[] = {
		{ "Category", "IKBody State" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Twist value between elbow and wrist" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_LowerarmTwistLeft = { "LowerarmTwistLeft", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIKBodyData, LowerarmTwistLeft), METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_LowerarmTwistLeft_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_LowerarmTwistLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_LowerarmTwistRight_MetaData[] = {
		{ "Category", "IKBody State" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Twist value between elbow and wrist" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_LowerarmTwistRight = { "LowerarmTwistRight", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIKBodyData, LowerarmTwistRight), METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_LowerarmTwistRight_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_LowerarmTwistRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_GroundLevelLeft_MetaData[] = {
		{ "Category", "IKBody State" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Current ground Z coordinate for left foot" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_GroundLevelLeft = { "GroundLevelLeft", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIKBodyData, GroundLevelLeft), METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_GroundLevelLeft_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_GroundLevelLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_GroundLevelRight_MetaData[] = {
		{ "Category", "IKBody State" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Current ground Z coordinate for right foot" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_GroundLevelRight = { "GroundLevelRight", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIKBodyData, GroundLevelRight), METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_GroundLevelRight_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_GroundLevelRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_GroundLevel_MetaData[] = {
		{ "Category", "IKBody State" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Current Ground Z Coordinate" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_GroundLevel = { "GroundLevel", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIKBodyData, GroundLevel), METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_GroundLevel_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_GroundLevel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_Velocity_MetaData[] = {
		{ "Category", "IKBody State" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Current Player Vector Velocity" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_Velocity = { "Velocity", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIKBodyData, Velocity), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_Velocity_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_Velocity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_IsSitting_MetaData[] = {
		{ "Category", "IKBody State" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Flag indicates if character is sitting" },
	};
#endif
	void Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_IsSitting_SetBit(void* Obj)
	{
		((FIKBodyData*)Obj)->IsSitting = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_IsSitting = { "IsSitting", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FIKBodyData), &Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_IsSitting_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_IsSitting_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_IsSitting_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_BodyOrientation_MetaData[] = {
		{ "Category", "IKBody State" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "is player crawling or laying down?" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_BodyOrientation = { "BodyOrientation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIKBodyData, BodyOrientation), Z_Construct_UEnum_VRIKBodyRuntime_EBodyOrientation, METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_BodyOrientation_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_BodyOrientation_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_BodyOrientation_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_IsJumping_MetaData[] = {
		{ "Category", "IKBody State" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Flag indicates if character is jumping" },
	};
#endif
	void Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_IsJumping_SetBit(void* Obj)
	{
		((FIKBodyData*)Obj)->IsJumping = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_IsJumping = { "IsJumping", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FIKBodyData), &Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_IsJumping_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_IsJumping_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_IsJumping_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_FootLeftCurrent_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Left Feet IK instantaneous Transform (only if ComputeFeetIKTargets is set)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_FootLeftCurrent = { "FootLeftCurrent", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIKBodyData, FootLeftCurrent), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_FootLeftCurrent_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_FootLeftCurrent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_FootRightCurrent_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Right Feet IK instantaneous Transform (only if ComputeFeetIKTargets is set)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_FootRightCurrent = { "FootRightCurrent", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIKBodyData, FootRightCurrent), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_FootRightCurrent_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_FootRightCurrent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_CalfLeft_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Left Calf Transform (only if ComputeLegsIK is set)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_CalfLeft = { "CalfLeft", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIKBodyData, CalfLeft), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_CalfLeft_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_CalfLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_ThighLeft_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Left Thigh Transform (only if ComputeLegsIK is set)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_ThighLeft = { "ThighLeft", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIKBodyData, ThighLeft), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_ThighLeft_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_ThighLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_CalfRight_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Right Calf Transform (only if ComputeLegsIK is set)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_CalfRight = { "CalfRight", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIKBodyData, CalfRight), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_CalfRight_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_CalfRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_ThighRight_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Right Thigh Transform (only if ComputeLegsIK is set)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_ThighRight = { "ThighRight", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIKBodyData, ThighRight), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_ThighRight_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_ThighRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_FootLeftTarget_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Left Feet IK Transform (don't use this value)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_FootLeftTarget = { "FootLeftTarget", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIKBodyData, FootLeftTarget), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_FootLeftTarget_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_FootLeftTarget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_FootRightTarget_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Right Feet IK Transform (don't use this value)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_FootRightTarget = { "FootRightTarget", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIKBodyData, FootRightTarget), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_FootRightTarget_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_FootRightTarget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_ElbowJointTargetLeft_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "IK Joint Target for left hand in world space (if ComputeHandsIK is true)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_ElbowJointTargetLeft = { "ElbowJointTargetLeft", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIKBodyData, ElbowJointTargetLeft), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_ElbowJointTargetLeft_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_ElbowJointTargetLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_ElbowJointTargetRight_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "IK Joint Target for right hand in world space (if ComputeHandsIK is true)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_ElbowJointTargetRight = { "ElbowJointTargetRight", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIKBodyData, ElbowJointTargetRight), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_ElbowJointTargetRight_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_ElbowJointTargetRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_HandLeft_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Left Palm Transform" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_HandLeft = { "HandLeft", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIKBodyData, HandLeft), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_HandLeft_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_HandLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_ForearmLeft_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Left Forearm Transform (only if ComputeHandsIK is on)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_ForearmLeft = { "ForearmLeft", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIKBodyData, ForearmLeft), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_ForearmLeft_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_ForearmLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_UpperarmLeft_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Left Forearm Transform (only if ComputeHandsIK is on)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_UpperarmLeft = { "UpperarmLeft", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIKBodyData, UpperarmLeft), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_UpperarmLeft_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_UpperarmLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_CollarboneLeft_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Left collarbone relative (yaw, pitch) rotation" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_CollarboneLeft = { "CollarboneLeft", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIKBodyData, CollarboneLeft), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_CollarboneLeft_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_CollarboneLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_HandRight_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Right Palm Transform" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_HandRight = { "HandRight", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIKBodyData, HandRight), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_HandRight_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_HandRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_ForearmRight_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Right Forearm Transform (only if ComputeHandsIK is on)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_ForearmRight = { "ForearmRight", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIKBodyData, ForearmRight), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_ForearmRight_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_ForearmRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_UpperarmRight_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Right Upperarm Transform (only if ComputeHandsIK is on)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_UpperarmRight = { "UpperarmRight", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIKBodyData, UpperarmRight), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_UpperarmRight_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_UpperarmRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_CollarboneRight_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Right collarbone relative (yaw, pitch) rotation" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_CollarboneRight = { "CollarboneRight", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIKBodyData, CollarboneRight), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_CollarboneRight_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_CollarboneRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_Head_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Head Transform" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_Head = { "Head", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIKBodyData, Head), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_Head_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_Head_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_Neck_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Neck Transform" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_Neck = { "Neck", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIKBodyData, Neck), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_Neck_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_Neck_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_Ribcage_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Ribcage/Spine Transform in world space" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_Ribcage = { "Ribcage", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIKBodyData, Ribcage), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_Ribcage_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_Ribcage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_Pelvis_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Pelvis Transform (always in world space)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_Pelvis = { "Pelvis", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIKBodyData, Pelvis), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_Pelvis_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_Pelvis_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FIKBodyData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_LowerarmTwistLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_LowerarmTwistRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_GroundLevelLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_GroundLevelRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_GroundLevel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_Velocity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_IsSitting,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_BodyOrientation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_BodyOrientation_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_IsJumping,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_FootLeftCurrent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_FootRightCurrent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_CalfLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_ThighLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_CalfRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_ThighRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_FootLeftTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_FootRightTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_ElbowJointTargetLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_ElbowJointTargetRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_HandLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_ForearmLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_UpperarmLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_CollarboneLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_HandRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_ForearmRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_UpperarmRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_CollarboneRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_Head,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_Neck,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_Ribcage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIKBodyData_Statics::NewProp_Pelvis,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FIKBodyData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_VRIKBodyRuntime,
		nullptr,
		&NewStructOps,
		"IKBodyData",
		sizeof(FIKBodyData),
		alignof(FIKBodyData),
		Z_Construct_UScriptStruct_FIKBodyData_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FIKBodyData_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FIKBodyData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FIKBodyData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FIKBodyData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_VRIKBodyRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("IKBodyData"), sizeof(FIKBodyData), Get_Z_Construct_UScriptStruct_FIKBodyData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FIKBodyData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FIKBodyData_Hash() { return 660102254U; }
class UScriptStruct* FVRInputData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern VRIKBODYRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FVRInputData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FVRInputData, Z_Construct_UPackage__Script_VRIKBodyRuntime(), TEXT("VRInputData"), sizeof(FVRInputData), Get_Z_Construct_UScriptStruct_FVRInputData_Hash());
	}
	return Singleton;
}
template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<FVRInputData>()
{
	return FVRInputData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FVRInputData(FVRInputData::StaticStruct, TEXT("/Script/VRIKBodyRuntime"), TEXT("VRInputData"), false, nullptr, nullptr);
static struct FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFVRInputData
{
	FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFVRInputData()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("VRInputData")),new UScriptStruct::TCppStructOps<FVRInputData>);
	}
} ScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFVRInputData;
	struct Z_Construct_UScriptStruct_FVRInputData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RightHandRot_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RightHandRot;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LeftHandRot_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LeftHandRot;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HeadRot_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HeadRot;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RightHandLoc_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RightHandLoc;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LeftHandLoc_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LeftHandLoc;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HeadLoc_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HeadLoc;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRInputData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Internal struct to store VR Input history" },
	};
#endif
	void* Z_Construct_UScriptStruct_FVRInputData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FVRInputData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRInputData_Statics::NewProp_RightHandRot_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Right Motion Controller Rotation" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVRInputData_Statics::NewProp_RightHandRot = { "RightHandRot", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRInputData, RightHandRot), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UScriptStruct_FVRInputData_Statics::NewProp_RightHandRot_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRInputData_Statics::NewProp_RightHandRot_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRInputData_Statics::NewProp_LeftHandRot_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Left Motion Controller Rotation" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVRInputData_Statics::NewProp_LeftHandRot = { "LeftHandRot", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRInputData, LeftHandRot), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UScriptStruct_FVRInputData_Statics::NewProp_LeftHandRot_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRInputData_Statics::NewProp_LeftHandRot_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRInputData_Statics::NewProp_HeadRot_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "VR Headset Rotation" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVRInputData_Statics::NewProp_HeadRot = { "HeadRot", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRInputData, HeadRot), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UScriptStruct_FVRInputData_Statics::NewProp_HeadRot_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRInputData_Statics::NewProp_HeadRot_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRInputData_Statics::NewProp_RightHandLoc_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Right Motion Controller Location" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVRInputData_Statics::NewProp_RightHandLoc = { "RightHandLoc", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRInputData, RightHandLoc), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FVRInputData_Statics::NewProp_RightHandLoc_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRInputData_Statics::NewProp_RightHandLoc_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRInputData_Statics::NewProp_LeftHandLoc_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Left Motion Controller Location" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVRInputData_Statics::NewProp_LeftHandLoc = { "LeftHandLoc", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRInputData, LeftHandLoc), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FVRInputData_Statics::NewProp_LeftHandLoc_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRInputData_Statics::NewProp_LeftHandLoc_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRInputData_Statics::NewProp_HeadLoc_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "VR Headset Location" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVRInputData_Statics::NewProp_HeadLoc = { "HeadLoc", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRInputData, HeadLoc), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FVRInputData_Statics::NewProp_HeadLoc_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRInputData_Statics::NewProp_HeadLoc_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FVRInputData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRInputData_Statics::NewProp_RightHandRot,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRInputData_Statics::NewProp_LeftHandRot,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRInputData_Statics::NewProp_HeadRot,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRInputData_Statics::NewProp_RightHandLoc,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRInputData_Statics::NewProp_LeftHandLoc,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRInputData_Statics::NewProp_HeadLoc,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FVRInputData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_VRIKBodyRuntime,
		nullptr,
		&NewStructOps,
		"VRInputData",
		sizeof(FVRInputData),
		alignof(FVRInputData),
		Z_Construct_UScriptStruct_FVRInputData_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FVRInputData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FVRInputData_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRInputData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FVRInputData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FVRInputData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_VRIKBodyRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("VRInputData"), sizeof(FVRInputData), Get_Z_Construct_UScriptStruct_FVRInputData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FVRInputData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FVRInputData_Hash() { return 2192775788U; }
class UScriptStruct* FNetworkIKBodyData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern VRIKBODYRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FNetworkIKBodyData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNetworkIKBodyData, Z_Construct_UPackage__Script_VRIKBodyRuntime(), TEXT("NetworkIKBodyData"), sizeof(FNetworkIKBodyData), Get_Z_Construct_UScriptStruct_FNetworkIKBodyData_Hash());
	}
	return Singleton;
}
template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<FNetworkIKBodyData>()
{
	return FNetworkIKBodyData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNetworkIKBodyData(FNetworkIKBodyData::StaticStruct, TEXT("/Script/VRIKBodyRuntime"), TEXT("NetworkIKBodyData"), false, nullptr, nullptr);
static struct FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFNetworkIKBodyData
{
	FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFNetworkIKBodyData()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("NetworkIKBodyData")),new UScriptStruct::TCppStructOps<FNetworkIKBodyData>);
	}
} ScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFNetworkIKBodyData;
	struct Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LowerarmTwistLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LowerarmTwistLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LowerarmTwistRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LowerarmTwistRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GroundLevelLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_GroundLevelLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GroundLevelRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_GroundLevelRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GroundLevel_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_GroundLevel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Velocity_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Velocity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BodyOrientation_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_BodyOrientation;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_BodyOrientation_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IsJumping_MetaData[];
#endif
		static void NewProp_IsJumping_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsJumping;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FootLeftCurrent_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FootLeftCurrent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FootRightCurrent_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FootRightCurrent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ElbowJointTargetLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ElbowJointTargetLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ElbowJointTargetRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ElbowJointTargetRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HandLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HandLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CollarboneLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CollarboneLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HandRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HandRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CollarboneRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CollarboneRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Head_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Head;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Neck_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Neck;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Ribcage_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Ribcage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Pelvis_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Pelvis;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Simplified Body State struct for networking" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNetworkIKBodyData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_LowerarmTwistLeft_MetaData[] = {
		{ "Category", "IKBody State" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Twist value between elbow and wrist" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_LowerarmTwistLeft = { "LowerarmTwistLeft", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNetworkIKBodyData, LowerarmTwistLeft), METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_LowerarmTwistLeft_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_LowerarmTwistLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_LowerarmTwistRight_MetaData[] = {
		{ "Category", "IKBody State" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Twist value between elbow and wrist" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_LowerarmTwistRight = { "LowerarmTwistRight", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNetworkIKBodyData, LowerarmTwistRight), METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_LowerarmTwistRight_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_LowerarmTwistRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_GroundLevelLeft_MetaData[] = {
		{ "Category", "IKBody State" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Current ground Z coordinate for the left foot" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_GroundLevelLeft = { "GroundLevelLeft", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNetworkIKBodyData, GroundLevelLeft), METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_GroundLevelLeft_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_GroundLevelLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_GroundLevelRight_MetaData[] = {
		{ "Category", "IKBody State" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Current ground Z coordinate for the right foot" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_GroundLevelRight = { "GroundLevelRight", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNetworkIKBodyData, GroundLevelRight), METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_GroundLevelRight_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_GroundLevelRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_GroundLevel_MetaData[] = {
		{ "Category", "IKBody State" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Current ground Z coordinate" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_GroundLevel = { "GroundLevel", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNetworkIKBodyData, GroundLevel), METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_GroundLevel_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_GroundLevel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_Velocity_MetaData[] = {
		{ "Category", "IKBody State" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Current player velocity vector" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_Velocity = { "Velocity", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNetworkIKBodyData, Velocity), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_Velocity_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_Velocity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_BodyOrientation_MetaData[] = {
		{ "Category", "IKBody State" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "is player crawling or laying down?" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_BodyOrientation = { "BodyOrientation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNetworkIKBodyData, BodyOrientation), Z_Construct_UEnum_VRIKBodyRuntime_EBodyOrientation, METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_BodyOrientation_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_BodyOrientation_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_BodyOrientation_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_IsJumping_MetaData[] = {
		{ "Category", "IKBody State" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Is player jumbing or not" },
	};
#endif
	void Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_IsJumping_SetBit(void* Obj)
	{
		((FNetworkIKBodyData*)Obj)->IsJumping = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_IsJumping = { "IsJumping", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FNetworkIKBodyData), &Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_IsJumping_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_IsJumping_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_IsJumping_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_FootLeftCurrent_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Left foot transform" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_FootLeftCurrent = { "FootLeftCurrent", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNetworkIKBodyData, FootLeftCurrent), Z_Construct_UScriptStruct_FNetworkTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_FootLeftCurrent_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_FootLeftCurrent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_FootRightCurrent_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Right foot transform" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_FootRightCurrent = { "FootRightCurrent", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNetworkIKBodyData, FootRightCurrent), Z_Construct_UScriptStruct_FNetworkTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_FootRightCurrent_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_FootRightCurrent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_ElbowJointTargetLeft_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Left elbow IK target" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_ElbowJointTargetLeft = { "ElbowJointTargetLeft", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNetworkIKBodyData, ElbowJointTargetLeft), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_ElbowJointTargetLeft_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_ElbowJointTargetLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_ElbowJointTargetRight_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Right elbow IK target" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_ElbowJointTargetRight = { "ElbowJointTargetRight", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNetworkIKBodyData, ElbowJointTargetRight), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_ElbowJointTargetRight_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_ElbowJointTargetRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_HandLeft_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Left hand palm transform" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_HandLeft = { "HandLeft", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNetworkIKBodyData, HandLeft), Z_Construct_UScriptStruct_FNetworkTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_HandLeft_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_HandLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_CollarboneLeft_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Left collarbone rotation" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_CollarboneLeft = { "CollarboneLeft", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNetworkIKBodyData, CollarboneLeft), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_CollarboneLeft_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_CollarboneLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_HandRight_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Right hand palm transform" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_HandRight = { "HandRight", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNetworkIKBodyData, HandRight), Z_Construct_UScriptStruct_FNetworkTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_HandRight_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_HandRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_CollarboneRight_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Right collarbone rotation" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_CollarboneRight = { "CollarboneRight", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNetworkIKBodyData, CollarboneRight), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_CollarboneRight_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_CollarboneRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_Head_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Head transform" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_Head = { "Head", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNetworkIKBodyData, Head), Z_Construct_UScriptStruct_FNetworkTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_Head_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_Head_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_Neck_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Neck Transform" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_Neck = { "Neck", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNetworkIKBodyData, Neck), Z_Construct_UScriptStruct_FNetworkTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_Neck_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_Neck_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_Ribcage_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Ribcage transform" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_Ribcage = { "Ribcage", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNetworkIKBodyData, Ribcage), Z_Construct_UScriptStruct_FNetworkTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_Ribcage_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_Ribcage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_Pelvis_MetaData[] = {
		{ "Category", "IKBody Bone Transform" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Pelvis transform" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_Pelvis = { "Pelvis", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNetworkIKBodyData, Pelvis), Z_Construct_UScriptStruct_FNetworkTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_Pelvis_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_Pelvis_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_LowerarmTwistLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_LowerarmTwistRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_GroundLevelLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_GroundLevelRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_GroundLevel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_Velocity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_BodyOrientation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_BodyOrientation_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_IsJumping,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_FootLeftCurrent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_FootRightCurrent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_ElbowJointTargetLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_ElbowJointTargetRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_HandLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_CollarboneLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_HandRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_CollarboneRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_Head,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_Neck,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_Ribcage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::NewProp_Pelvis,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_VRIKBodyRuntime,
		nullptr,
		&NewStructOps,
		"NetworkIKBodyData",
		sizeof(FNetworkIKBodyData),
		alignof(FNetworkIKBodyData),
		Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNetworkIKBodyData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNetworkIKBodyData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_VRIKBodyRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NetworkIKBodyData"), sizeof(FNetworkIKBodyData), Get_Z_Construct_UScriptStruct_FNetworkIKBodyData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNetworkIKBodyData_Hash() { return 1772360596U; }
class UScriptStruct* FNetworkTransform::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern VRIKBODYRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FNetworkTransform_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNetworkTransform, Z_Construct_UPackage__Script_VRIKBodyRuntime(), TEXT("NetworkTransform"), sizeof(FNetworkTransform), Get_Z_Construct_UScriptStruct_FNetworkTransform_Hash());
	}
	return Singleton;
}
template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<FNetworkTransform>()
{
	return FNetworkTransform::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNetworkTransform(FNetworkTransform::StaticStruct, TEXT("/Script/VRIKBodyRuntime"), TEXT("NetworkTransform"), false, nullptr, nullptr);
static struct FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFNetworkTransform
{
	FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFNetworkTransform()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("NetworkTransform")),new UScriptStruct::TCppStructOps<FNetworkTransform>);
	}
} ScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFNetworkTransform;
	struct Z_Construct_UScriptStruct_FNetworkTransform_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Rotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Rotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Location_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Location;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkTransform_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Simplified Transform Data for networking" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNetworkTransform_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNetworkTransform>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkTransform_Statics::NewProp_Rotation_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Rotation in actor space" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNetworkTransform_Statics::NewProp_Rotation = { "Rotation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNetworkTransform, Rotation), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkTransform_Statics::NewProp_Rotation_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkTransform_Statics::NewProp_Rotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetworkTransform_Statics::NewProp_Location_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Location in actor space" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNetworkTransform_Statics::NewProp_Location = { "Location", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNetworkTransform, Location), Z_Construct_UScriptStruct_FVector_NetQuantize100, METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkTransform_Statics::NewProp_Location_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkTransform_Statics::NewProp_Location_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNetworkTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkTransform_Statics::NewProp_Rotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetworkTransform_Statics::NewProp_Location,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNetworkTransform_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_VRIKBodyRuntime,
		nullptr,
		&NewStructOps,
		"NetworkTransform",
		sizeof(FNetworkTransform),
		alignof(FNetworkTransform),
		Z_Construct_UScriptStruct_FNetworkTransform_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkTransform_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNetworkTransform_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FNetworkTransform_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNetworkTransform()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNetworkTransform_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_VRIKBodyRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NetworkTransform"), sizeof(FNetworkTransform), Get_Z_Construct_UScriptStruct_FNetworkTransform_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNetworkTransform_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNetworkTransform_Hash() { return 1514594587U; }
class UScriptStruct* FCalibratedBody::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern VRIKBODYRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FCalibratedBody_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCalibratedBody, Z_Construct_UPackage__Script_VRIKBodyRuntime(), TEXT("CalibratedBody"), sizeof(FCalibratedBody), Get_Z_Construct_UScriptStruct_FCalibratedBody_Hash());
	}
	return Singleton;
}
template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<FCalibratedBody>()
{
	return FCalibratedBody::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCalibratedBody(FCalibratedBody::StaticStruct, TEXT("/Script/VRIKBodyRuntime"), TEXT("CalibratedBody"), false, nullptr, nullptr);
static struct FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFCalibratedBody
{
	FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFCalibratedBody()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("CalibratedBody")),new UScriptStruct::TCppStructOps<FCalibratedBody>);
	}
} ScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFCalibratedBody;
	struct Z_Construct_UScriptStruct_FCalibratedBody_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_fHandLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_fHandLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_fSpineLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_fSpineLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_vNeckToHeadsetOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_vNeckToHeadsetOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LegsLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LegsLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ArmSpanClear_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ArmSpanClear;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CharacterHeightClear_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CharacterHeightClear;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CharacterHeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CharacterHeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_fBodyWidth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_fBodyWidth;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCalibratedBody_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Data set describing calibrated body state. Required to save and load body params on player respawn" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCalibratedBody>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_fHandLength_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Player's hand length (only for calubrated body)" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_fHandLength = { "fHandLength", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCalibratedBody, fHandLength), METADATA_PARAMS(Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_fHandLength_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_fHandLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_fSpineLength_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Approximate pelvis-to-ricage length" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_fSpineLength = { "fSpineLength", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCalibratedBody, fSpineLength), METADATA_PARAMS(Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_fSpineLength_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_fSpineLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_vNeckToHeadsetOffset_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Location of a neck relative to VR headset" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_vNeckToHeadsetOffset = { "vNeckToHeadsetOffset", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCalibratedBody, vNeckToHeadsetOffset), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_vNeckToHeadsetOffset_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_vNeckToHeadsetOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_LegsLength_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Feet-pelvis approximate vertical distance" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_LegsLength = { "LegsLength", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCalibratedBody, LegsLength), METADATA_PARAMS(Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_LegsLength_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_LegsLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_ArmSpanClear_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Clear horizontal distance between two motion controllers at T pose" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_ArmSpanClear = { "ArmSpanClear", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCalibratedBody, ArmSpanClear), METADATA_PARAMS(Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_ArmSpanClear_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_ArmSpanClear_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_CharacterHeightClear_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Clear vertical distance from ground to camera without any adjustments" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_CharacterHeightClear = { "CharacterHeightClear", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCalibratedBody, CharacterHeightClear), METADATA_PARAMS(Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_CharacterHeightClear_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_CharacterHeightClear_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_CharacterHeight_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Player height from ground to eyes" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_CharacterHeight = { "CharacterHeight", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCalibratedBody, CharacterHeight), METADATA_PARAMS(Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_CharacterHeight_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_CharacterHeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_fBodyWidth_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Player body width (only for calubrated body)" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_fBodyWidth = { "fBodyWidth", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCalibratedBody, fBodyWidth), METADATA_PARAMS(Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_fBodyWidth_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_fBodyWidth_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCalibratedBody_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_fHandLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_fSpineLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_vNeckToHeadsetOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_LegsLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_ArmSpanClear,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_CharacterHeightClear,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_CharacterHeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCalibratedBody_Statics::NewProp_fBodyWidth,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCalibratedBody_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_VRIKBodyRuntime,
		nullptr,
		&NewStructOps,
		"CalibratedBody",
		sizeof(FCalibratedBody),
		alignof(FCalibratedBody),
		Z_Construct_UScriptStruct_FCalibratedBody_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FCalibratedBody_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCalibratedBody_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FCalibratedBody_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCalibratedBody()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCalibratedBody_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_VRIKBodyRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CalibratedBody"), sizeof(FCalibratedBody), Get_Z_Construct_UScriptStruct_FCalibratedBody_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCalibratedBody_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCalibratedBody_Hash() { return 1679913452U; }
	static FName NAME_UVRIKBody_ClientResetCalibrationStatus = FName(TEXT("ClientResetCalibrationStatus"));
	void UVRIKBody::ClientResetCalibrationStatus()
	{
		ProcessEvent(FindFunctionChecked(NAME_UVRIKBody_ClientResetCalibrationStatus),NULL);
	}
	static FName NAME_UVRIKBody_ClientRestoreCalibratedBody = FName(TEXT("ClientRestoreCalibratedBody"));
	void UVRIKBody::ClientRestoreCalibratedBody(FCalibratedBody const& BodyParams)
	{
		VRIKBody_eventClientRestoreCalibratedBody_Parms Parms;
		Parms.BodyParams=BodyParams;
		ProcessEvent(FindFunctionChecked(NAME_UVRIKBody_ClientRestoreCalibratedBody),&Parms);
	}
	static FName NAME_UVRIKBody_ServerResetCalibrationStatus = FName(TEXT("ServerResetCalibrationStatus"));
	void UVRIKBody::ServerResetCalibrationStatus()
	{
		ProcessEvent(FindFunctionChecked(NAME_UVRIKBody_ServerResetCalibrationStatus),NULL);
	}
	static FName NAME_UVRIKBody_ServerRestoreCalibratedBody = FName(TEXT("ServerRestoreCalibratedBody"));
	void UVRIKBody::ServerRestoreCalibratedBody(FCalibratedBody const& BodyParams)
	{
		VRIKBody_eventServerRestoreCalibratedBody_Parms Parms;
		Parms.BodyParams=BodyParams;
		ProcessEvent(FindFunctionChecked(NAME_UVRIKBody_ServerRestoreCalibratedBody),&Parms);
	}
	static FName NAME_UVRIKBody_ServerUpdateBodyState = FName(TEXT("ServerUpdateBodyState"));
	void UVRIKBody::ServerUpdateBodyState(FNetworkIKBodyData const& State)
	{
		VRIKBody_eventServerUpdateBodyState_Parms Parms;
		Parms.State=State;
		ProcessEvent(FindFunctionChecked(NAME_UVRIKBody_ServerUpdateBodyState),&Parms);
	}
	static FName NAME_UVRIKBody_ServerUpdateInputs = FName(TEXT("ServerUpdateInputs"));
	void UVRIKBody::ServerUpdateInputs(FNetworkTransform const& Head, FNetworkTransform const& HandRight, FNetworkTransform const& HandLeft)
	{
		VRIKBody_eventServerUpdateInputs_Parms Parms;
		Parms.Head=Head;
		Parms.HandRight=HandRight;
		Parms.HandLeft=HandLeft;
		ProcessEvent(FindFunctionChecked(NAME_UVRIKBody_ServerUpdateInputs),&Parms);
	}
	void UVRIKBody::StaticRegisterNativesUVRIKBody()
	{
		UClass* Class = UVRIKBody::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ActivateInput", &UVRIKBody::execActivateInput },
			{ "AttachHandToComponent", &UVRIKBody::execAttachHandToComponent },
			{ "AutoCalibrateBodyAtPose", &UVRIKBody::execAutoCalibrateBodyAtPose },
			{ "CalcFeetIKTransforms2", &UVRIKBody::execCalcFeetIKTransforms2 },
			{ "CalcFeetInstantaneousTransforms", &UVRIKBody::execCalcFeetInstantaneousTransforms },
			{ "CalcHandIKTransforms", &UVRIKBody::execCalcHandIKTransforms },
			{ "CalcShouldersWithOffset", &UVRIKBody::execCalcShouldersWithOffset },
			{ "CalculateTwoBoneIK", &UVRIKBody::execCalculateTwoBoneIK },
			{ "CalibrateBodyAtIPose", &UVRIKBody::execCalibrateBodyAtIPose },
			{ "CalibrateBodyAtTPose", &UVRIKBody::execCalibrateBodyAtTPose },
			{ "CalibrateSkeleton", &UVRIKBody::execCalibrateSkeleton },
			{ "ClampElbowRotation", &UVRIKBody::execClampElbowRotation },
			{ "ClientResetCalibrationStatus", &UVRIKBody::execClientResetCalibrationStatus },
			{ "ClientRestoreCalibratedBody", &UVRIKBody::execClientRestoreCalibratedBody },
			{ "ComputeFrame", &UVRIKBody::execComputeFrame },
			{ "ConvertDataToSkeletonFriendly", &UVRIKBody::execConvertDataToSkeletonFriendly },
			{ "DeactivateInput", &UVRIKBody::execDeactivateInput },
			{ "DetachHandFromComponent", &UVRIKBody::execDetachHandFromComponent },
			{ "DoResetCalibrationStatus", &UVRIKBody::execDoResetCalibrationStatus },
			{ "DoRestoreCalibratedBody", &UVRIKBody::execDoRestoreCalibratedBody },
			{ "GetCalibratedBody", &UVRIKBody::execGetCalibratedBody },
			{ "GetCharacterHeight", &UVRIKBody::execGetCharacterHeight },
			{ "GetCharacterLegsLength", &UVRIKBody::execGetCharacterLegsLength },
			{ "GetClearArmSpan", &UVRIKBody::execGetClearArmSpan },
			{ "GetClearCharacterHeight", &UVRIKBody::execGetClearCharacterHeight },
			{ "GetCorrelationKoef", &UVRIKBody::execGetCorrelationKoef },
			{ "GetElbowJointTarget", &UVRIKBody::execGetElbowJointTarget },
			{ "GetKneeJointTarget", &UVRIKBody::execGetKneeJointTarget },
			{ "GetLastFrameData", &UVRIKBody::execGetLastFrameData },
			{ "Initialize", &UVRIKBody::execInitialize },
			{ "IsBodyCalibrated", &UVRIKBody::execIsBodyCalibrated },
			{ "IsInitialized", &UVRIKBody::execIsInitialized },
			{ "IsValidCalibrationData", &UVRIKBody::execIsValidCalibrationData },
			{ "OnRep_InputBodyState", &UVRIKBody::execOnRep_InputBodyState },
			{ "OnRep_InputHandLeft", &UVRIKBody::execOnRep_InputHandLeft },
			{ "OnRep_InputHandRight", &UVRIKBody::execOnRep_InputHandRight },
			{ "OnRep_InputHMD", &UVRIKBody::execOnRep_InputHMD },
			{ "PackDataForReplication", &UVRIKBody::execPackDataForReplication },
			{ "ResetCalibrationStatus", &UVRIKBody::execResetCalibrationStatus },
			{ "ResetFootsTimerL_Tick", &UVRIKBody::execResetFootsTimerL_Tick },
			{ "ResetFootsTimerR_Tick", &UVRIKBody::execResetFootsTimerR_Tick },
			{ "ResetTorso", &UVRIKBody::execResetTorso },
			{ "RestoreCalibratedBody", &UVRIKBody::execRestoreCalibratedBody },
			{ "RibcageYawTimer_Tick", &UVRIKBody::execRibcageYawTimer_Tick },
			{ "ServerResetCalibrationStatus", &UVRIKBody::execServerResetCalibrationStatus },
			{ "ServerRestoreCalibratedBody", &UVRIKBody::execServerRestoreCalibratedBody },
			{ "ServerUpdateBodyState", &UVRIKBody::execServerUpdateBodyState },
			{ "ServerUpdateInputs", &UVRIKBody::execServerUpdateInputs },
			{ "SetManualBodyOrientation", &UVRIKBody::execSetManualBodyOrientation },
			{ "TraceFloor", &UVRIKBody::execTraceFloor },
			{ "UpdateInputTransforms", &UVRIKBody::execUpdateInputTransforms },
			{ "UseAutoBodyOrientation", &UVRIKBody::execUseAutoBodyOrientation },
			{ "VRInputTimer_Tick", &UVRIKBody::execVRInputTimer_Tick },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UVRIKBody_ActivateInput_Statics
	{
		struct VRIKBody_eventActivateInput_Parms
		{
			USceneComponent* RootComp;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RootComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RootComp;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVRIKBody_ActivateInput_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VRIKBody_eventActivateInput_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVRIKBody_ActivateInput_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VRIKBody_eventActivateInput_Parms), &Z_Construct_UFunction_UVRIKBody_ActivateInput_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_ActivateInput_Statics::NewProp_RootComp_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVRIKBody_ActivateInput_Statics::NewProp_RootComp = { "RootComp", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventActivateInput_Parms, RootComp), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_ActivateInput_Statics::NewProp_RootComp_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ActivateInput_Statics::NewProp_RootComp_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_ActivateInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_ActivateInput_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_ActivateInput_Statics::NewProp_RootComp,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_ActivateInput_Statics::Function_MetaDataParams[] = {
		{ "Category", "IKBody" },
		{ "CPP_Default_RootComp", "None" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "This function activates recording of HMD and motion controllers locations and orientation. If use replication or Input from Components, use Initialize(...) instead.\n      @param RootComp in a Parent Component for VR Camera and controllers, equal to player area centre at the floor.\n      @return true if successful." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_ActivateInput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "ActivateInput", sizeof(VRIKBody_eventActivateInput_Parms), Z_Construct_UFunction_UVRIKBody_ActivateInput_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ActivateInput_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_ActivateInput_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ActivateInput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_ActivateInput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_ActivateInput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics
	{
		struct VRIKBody_eventAttachHandToComponent_Parms
		{
			EControllerHand Hand;
			UPrimitiveComponent* Component;
			FName SocketName;
			FTransform RelativeTransform;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RelativeTransform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RelativeTransform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SocketName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_SocketName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Component_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Component;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Hand;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Hand_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VRIKBody_eventAttachHandToComponent_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VRIKBody_eventAttachHandToComponent_Parms), &Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics::NewProp_RelativeTransform_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics::NewProp_RelativeTransform = { "RelativeTransform", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventAttachHandToComponent_Parms, RelativeTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics::NewProp_RelativeTransform_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics::NewProp_RelativeTransform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics::NewProp_SocketName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics::NewProp_SocketName = { "SocketName", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventAttachHandToComponent_Parms, SocketName), METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics::NewProp_SocketName_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics::NewProp_SocketName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics::NewProp_Component_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics::NewProp_Component = { "Component", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventAttachHandToComponent_Parms, Component), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics::NewProp_Component_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics::NewProp_Component_MetaData)) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics::NewProp_Hand = { "Hand", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventAttachHandToComponent_Parms, Hand), Z_Construct_UEnum_InputCore_EControllerHand, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics::NewProp_Hand_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics::NewProp_RelativeTransform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics::NewProp_SocketName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics::NewProp_Component,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics::NewProp_Hand,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics::NewProp_Hand_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics::Function_MetaDataParams[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Detaches Palm from Motion Controller and attaches to primitive component (for example, two-handed rifle)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "AttachHandToComponent", sizeof(VRIKBody_eventAttachHandToComponent_Parms), Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_AttachHandToComponent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_AttachHandToComponent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_AutoCalibrateBodyAtPose_Statics
	{
		struct VRIKBody_eventAutoCalibrateBodyAtPose_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVRIKBody_AutoCalibrateBodyAtPose_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VRIKBody_eventAutoCalibrateBodyAtPose_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVRIKBody_AutoCalibrateBodyAtPose_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VRIKBody_eventAutoCalibrateBodyAtPose_Parms), &Z_Construct_UFunction_UVRIKBody_AutoCalibrateBodyAtPose_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_AutoCalibrateBodyAtPose_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_AutoCalibrateBodyAtPose_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_AutoCalibrateBodyAtPose_Statics::Function_MetaDataParams[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Calibrate Body Params at T or I pose (detected automatically)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_AutoCalibrateBodyAtPose_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "AutoCalibrateBodyAtPose", sizeof(VRIKBody_eventAutoCalibrateBodyAtPose_Parms), Z_Construct_UFunction_UVRIKBody_AutoCalibrateBodyAtPose_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_AutoCalibrateBodyAtPose_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_AutoCalibrateBodyAtPose_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_AutoCalibrateBodyAtPose_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_AutoCalibrateBodyAtPose()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_AutoCalibrateBodyAtPose_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_CalcFeetIKTransforms2_Statics
	{
		struct VRIKBody_eventCalcFeetIKTransforms2_Parms
		{
			float DeltaTime;
			float FloorZ;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FloorZ;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DeltaTime;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UVRIKBody_CalcFeetIKTransforms2_Statics::NewProp_FloorZ = { "FloorZ", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventCalcFeetIKTransforms2_Parms, FloorZ), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UVRIKBody_CalcFeetIKTransforms2_Statics::NewProp_DeltaTime = { "DeltaTime", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventCalcFeetIKTransforms2_Parms, DeltaTime), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_CalcFeetIKTransforms2_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_CalcFeetIKTransforms2_Statics::NewProp_FloorZ,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_CalcFeetIKTransforms2_Statics::NewProp_DeltaTime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_CalcFeetIKTransforms2_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_CalcFeetIKTransforms2_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "CalcFeetIKTransforms2", sizeof(VRIKBody_eventCalcFeetIKTransforms2_Parms), Z_Construct_UFunction_UVRIKBody_CalcFeetIKTransforms2_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_CalcFeetIKTransforms2_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_CalcFeetIKTransforms2_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_CalcFeetIKTransforms2_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_CalcFeetIKTransforms2()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_CalcFeetIKTransforms2_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_CalcFeetInstantaneousTransforms_Statics
	{
		struct VRIKBody_eventCalcFeetInstantaneousTransforms_Parms
		{
			float DeltaTime;
			float FloorZ;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FloorZ;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DeltaTime;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UVRIKBody_CalcFeetInstantaneousTransforms_Statics::NewProp_FloorZ = { "FloorZ", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventCalcFeetInstantaneousTransforms_Parms, FloorZ), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UVRIKBody_CalcFeetInstantaneousTransforms_Statics::NewProp_DeltaTime = { "DeltaTime", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventCalcFeetInstantaneousTransforms_Parms, DeltaTime), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_CalcFeetInstantaneousTransforms_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_CalcFeetInstantaneousTransforms_Statics::NewProp_FloorZ,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_CalcFeetInstantaneousTransforms_Statics::NewProp_DeltaTime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_CalcFeetInstantaneousTransforms_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_CalcFeetInstantaneousTransforms_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "CalcFeetInstantaneousTransforms", sizeof(VRIKBody_eventCalcFeetInstantaneousTransforms_Parms), Z_Construct_UFunction_UVRIKBody_CalcFeetInstantaneousTransforms_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_CalcFeetInstantaneousTransforms_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_CalcFeetInstantaneousTransforms_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_CalcFeetInstantaneousTransforms_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_CalcFeetInstantaneousTransforms()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_CalcFeetInstantaneousTransforms_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_CalcHandIKTransforms_Statics
	{
		struct VRIKBody_eventCalcHandIKTransforms_Parms
		{
			EControllerHand Hand;
		};
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Hand;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Hand_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UVRIKBody_CalcHandIKTransforms_Statics::NewProp_Hand = { "Hand", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventCalcHandIKTransforms_Parms, Hand), Z_Construct_UEnum_InputCore_EControllerHand, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UVRIKBody_CalcHandIKTransforms_Statics::NewProp_Hand_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_CalcHandIKTransforms_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_CalcHandIKTransforms_Statics::NewProp_Hand,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_CalcHandIKTransforms_Statics::NewProp_Hand_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_CalcHandIKTransforms_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_CalcHandIKTransforms_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "CalcHandIKTransforms", sizeof(VRIKBody_eventCalcHandIKTransforms_Parms), Z_Construct_UFunction_UVRIKBody_CalcHandIKTransforms_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_CalcHandIKTransforms_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_CalcHandIKTransforms_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_CalcHandIKTransforms_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_CalcHandIKTransforms()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_CalcHandIKTransforms_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_CalcShouldersWithOffset_Statics
	{
		struct VRIKBody_eventCalcShouldersWithOffset_Parms
		{
			EControllerHand Hand;
		};
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Hand;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Hand_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UVRIKBody_CalcShouldersWithOffset_Statics::NewProp_Hand = { "Hand", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventCalcShouldersWithOffset_Parms, Hand), Z_Construct_UEnum_InputCore_EControllerHand, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UVRIKBody_CalcShouldersWithOffset_Statics::NewProp_Hand_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_CalcShouldersWithOffset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_CalcShouldersWithOffset_Statics::NewProp_Hand,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_CalcShouldersWithOffset_Statics::NewProp_Hand_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_CalcShouldersWithOffset_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_CalcShouldersWithOffset_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "CalcShouldersWithOffset", sizeof(VRIKBody_eventCalcShouldersWithOffset_Parms), Z_Construct_UFunction_UVRIKBody_CalcShouldersWithOffset_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_CalcShouldersWithOffset_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_CalcShouldersWithOffset_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_CalcShouldersWithOffset_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_CalcShouldersWithOffset()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_CalcShouldersWithOffset_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics
	{
		struct VRIKBody_eventCalculateTwoBoneIK_Parms
		{
			FVector ChainStart;
			FVector ChainEnd;
			FVector JointTarget;
			float UpperBoneSize;
			float LowerBoneSize;
			FTransform UpperBone;
			FTransform LowerBone;
			float BendSide;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BendSide;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LowerBone;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UpperBone;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LowerBoneSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LowerBoneSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UpperBoneSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_UpperBoneSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_JointTarget_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_JointTarget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChainEnd_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ChainEnd;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChainStart_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ChainStart;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::NewProp_BendSide = { "BendSide", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventCalculateTwoBoneIK_Parms, BendSide), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::NewProp_LowerBone = { "LowerBone", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventCalculateTwoBoneIK_Parms, LowerBone), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::NewProp_UpperBone = { "UpperBone", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventCalculateTwoBoneIK_Parms, UpperBone), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::NewProp_LowerBoneSize_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::NewProp_LowerBoneSize = { "LowerBoneSize", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventCalculateTwoBoneIK_Parms, LowerBoneSize), METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::NewProp_LowerBoneSize_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::NewProp_LowerBoneSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::NewProp_UpperBoneSize_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::NewProp_UpperBoneSize = { "UpperBoneSize", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventCalculateTwoBoneIK_Parms, UpperBoneSize), METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::NewProp_UpperBoneSize_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::NewProp_UpperBoneSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::NewProp_JointTarget_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::NewProp_JointTarget = { "JointTarget", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventCalculateTwoBoneIK_Parms, JointTarget), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::NewProp_JointTarget_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::NewProp_JointTarget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::NewProp_ChainEnd_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::NewProp_ChainEnd = { "ChainEnd", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventCalculateTwoBoneIK_Parms, ChainEnd), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::NewProp_ChainEnd_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::NewProp_ChainEnd_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::NewProp_ChainStart_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::NewProp_ChainStart = { "ChainStart", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventCalculateTwoBoneIK_Parms, ChainStart), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::NewProp_ChainStart_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::NewProp_ChainStart_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::NewProp_BendSide,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::NewProp_LowerBone,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::NewProp_UpperBone,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::NewProp_LowerBoneSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::NewProp_UpperBoneSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::NewProp_JointTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::NewProp_ChainEnd,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::NewProp_ChainStart,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "simple two-bone IK" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "CalculateTwoBoneIK", sizeof(VRIKBody_eventCalculateTwoBoneIK_Parms), Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00C40401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtIPose_Statics
	{
		struct VRIKBody_eventCalibrateBodyAtIPose_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtIPose_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VRIKBody_eventCalibrateBodyAtIPose_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtIPose_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VRIKBody_eventCalibrateBodyAtIPose_Parms), &Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtIPose_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtIPose_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtIPose_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtIPose_Statics::Function_MetaDataParams[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Calibrate Body Params at I-Pose (hand down)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtIPose_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "CalibrateBodyAtIPose", sizeof(VRIKBody_eventCalibrateBodyAtIPose_Parms), Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtIPose_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtIPose_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtIPose_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtIPose_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtIPose()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtIPose_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtTPose_Statics
	{
		struct VRIKBody_eventCalibrateBodyAtTPose_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtTPose_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VRIKBody_eventCalibrateBodyAtTPose_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtTPose_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VRIKBody_eventCalibrateBodyAtTPose_Parms), &Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtTPose_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtTPose_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtTPose_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtTPose_Statics::Function_MetaDataParams[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Calibrate Body Params at T-Pose (hand to the left and right)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtTPose_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "CalibrateBodyAtTPose", sizeof(VRIKBody_eventCalibrateBodyAtTPose_Parms), Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtTPose_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtTPose_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtTPose_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtTPose_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtTPose()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtTPose_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_CalibrateSkeleton_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_CalibrateSkeleton_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "perform calibration" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_CalibrateSkeleton_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "CalibrateSkeleton", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_CalibrateSkeleton_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_CalibrateSkeleton_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_CalibrateSkeleton()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_CalibrateSkeleton_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_ClampElbowRotation_Statics
	{
		struct VRIKBody_eventClampElbowRotation_Parms
		{
			FVector InternalSide;
			FVector UpSide;
			FVector HandUpVector;
			FVector CurrentAngle;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CurrentAngle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HandUpVector_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HandUpVector;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UpSide_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UpSide;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InternalSide_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InternalSide;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_ClampElbowRotation_Statics::NewProp_CurrentAngle = { "CurrentAngle", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventClampElbowRotation_Parms, CurrentAngle), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_ClampElbowRotation_Statics::NewProp_HandUpVector_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_ClampElbowRotation_Statics::NewProp_HandUpVector = { "HandUpVector", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventClampElbowRotation_Parms, HandUpVector), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_ClampElbowRotation_Statics::NewProp_HandUpVector_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ClampElbowRotation_Statics::NewProp_HandUpVector_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_ClampElbowRotation_Statics::NewProp_UpSide_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_ClampElbowRotation_Statics::NewProp_UpSide = { "UpSide", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventClampElbowRotation_Parms, UpSide), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_ClampElbowRotation_Statics::NewProp_UpSide_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ClampElbowRotation_Statics::NewProp_UpSide_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_ClampElbowRotation_Statics::NewProp_InternalSide_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_ClampElbowRotation_Statics::NewProp_InternalSide = { "InternalSide", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventClampElbowRotation_Parms, InternalSide), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_ClampElbowRotation_Statics::NewProp_InternalSide_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ClampElbowRotation_Statics::NewProp_InternalSide_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_ClampElbowRotation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_ClampElbowRotation_Statics::NewProp_CurrentAngle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_ClampElbowRotation_Statics::NewProp_HandUpVector,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_ClampElbowRotation_Statics::NewProp_UpSide,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_ClampElbowRotation_Statics::NewProp_InternalSide,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_ClampElbowRotation_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "helper function to calculate elbow IK targets" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_ClampElbowRotation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "ClampElbowRotation", sizeof(VRIKBody_eventClampElbowRotation_Parms), Z_Construct_UFunction_UVRIKBody_ClampElbowRotation_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ClampElbowRotation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00C40401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_ClampElbowRotation_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ClampElbowRotation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_ClampElbowRotation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_ClampElbowRotation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_ClientResetCalibrationStatus_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_ClientResetCalibrationStatus_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Reset body calibration state on clients (replicated version)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_ClientResetCalibrationStatus_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "ClientResetCalibrationStatus", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00084CC0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_ClientResetCalibrationStatus_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ClientResetCalibrationStatus_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_ClientResetCalibrationStatus()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_ClientResetCalibrationStatus_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_ClientRestoreCalibratedBody_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BodyParams_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BodyParams;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_ClientRestoreCalibratedBody_Statics::NewProp_BodyParams_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_ClientRestoreCalibratedBody_Statics::NewProp_BodyParams = { "BodyParams", nullptr, (EPropertyFlags)0x0010000008000082, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventClientRestoreCalibratedBody_Parms, BodyParams), Z_Construct_UScriptStruct_FCalibratedBody, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_ClientRestoreCalibratedBody_Statics::NewProp_BodyParams_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ClientRestoreCalibratedBody_Statics::NewProp_BodyParams_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_ClientRestoreCalibratedBody_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_ClientRestoreCalibratedBody_Statics::NewProp_BodyParams,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_ClientRestoreCalibratedBody_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Restore body calibration params on clients (replicated version)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_ClientRestoreCalibratedBody_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "ClientRestoreCalibratedBody", sizeof(VRIKBody_eventClientRestoreCalibratedBody_Parms), Z_Construct_UFunction_UVRIKBody_ClientRestoreCalibratedBody_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ClientRestoreCalibratedBody_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00084CC0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_ClientRestoreCalibratedBody_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ClientRestoreCalibratedBody_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_ClientRestoreCalibratedBody()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_ClientRestoreCalibratedBody_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_ComputeFrame_Statics
	{
		struct VRIKBody_eventComputeFrame_Parms
		{
			float DeltaTime;
			FIKBodyData ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DeltaTime;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_ComputeFrame_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventComputeFrame_Parms, ReturnValue), Z_Construct_UScriptStruct_FIKBodyData, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UVRIKBody_ComputeFrame_Statics::NewProp_DeltaTime = { "DeltaTime", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventComputeFrame_Parms, DeltaTime), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_ComputeFrame_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_ComputeFrame_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_ComputeFrame_Statics::NewProp_DeltaTime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_ComputeFrame_Statics::Function_MetaDataParams[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Main function to calculate Body State.\n      @param DeltaTime - world tick delta time\n      @return a struct describing current body state" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_ComputeFrame_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "ComputeFrame", sizeof(VRIKBody_eventComputeFrame_Parms), Z_Construct_UFunction_UVRIKBody_ComputeFrame_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ComputeFrame_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_ComputeFrame_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ComputeFrame_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_ComputeFrame()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_ComputeFrame_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_ConvertDataToSkeletonFriendly_Statics
	{
		struct VRIKBody_eventConvertDataToSkeletonFriendly_Parms
		{
			FIKBodyData WorldSpaceIKBody;
			FIKBodyData ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldSpaceIKBody_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WorldSpaceIKBody;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_ConvertDataToSkeletonFriendly_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventConvertDataToSkeletonFriendly_Parms, ReturnValue), Z_Construct_UScriptStruct_FIKBodyData, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_ConvertDataToSkeletonFriendly_Statics::NewProp_WorldSpaceIKBody_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_ConvertDataToSkeletonFriendly_Statics::NewProp_WorldSpaceIKBody = { "WorldSpaceIKBody", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventConvertDataToSkeletonFriendly_Parms, WorldSpaceIKBody), Z_Construct_UScriptStruct_FIKBodyData, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_ConvertDataToSkeletonFriendly_Statics::NewProp_WorldSpaceIKBody_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ConvertDataToSkeletonFriendly_Statics::NewProp_WorldSpaceIKBody_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_ConvertDataToSkeletonFriendly_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_ConvertDataToSkeletonFriendly_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_ConvertDataToSkeletonFriendly_Statics::NewProp_WorldSpaceIKBody,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_ConvertDataToSkeletonFriendly_Statics::Function_MetaDataParams[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Legacy function! Only works for default UE4 Skeleton.\n      Converts calculated body parts orientation to skeleton bones\n      orientation. It's useful to directly set bone transforms in Anim Blueprint\n      using 'Modify (Transform) Bone' node. Keep in mind that transforms in the\n      returned struct are still in world space." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_ConvertDataToSkeletonFriendly_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "ConvertDataToSkeletonFriendly", sizeof(VRIKBody_eventConvertDataToSkeletonFriendly_Parms), Z_Construct_UFunction_UVRIKBody_ConvertDataToSkeletonFriendly_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ConvertDataToSkeletonFriendly_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_ConvertDataToSkeletonFriendly_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ConvertDataToSkeletonFriendly_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_ConvertDataToSkeletonFriendly()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_ConvertDataToSkeletonFriendly_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_DeactivateInput_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_DeactivateInput_Statics::Function_MetaDataParams[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "This function stops recording of HMD and motion controllers locations and orientation" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_DeactivateInput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "DeactivateInput", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_DeactivateInput_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_DeactivateInput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_DeactivateInput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_DeactivateInput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_DetachHandFromComponent_Statics
	{
		struct VRIKBody_eventDetachHandFromComponent_Parms
		{
			EControllerHand Hand;
		};
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Hand;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Hand_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UVRIKBody_DetachHandFromComponent_Statics::NewProp_Hand = { "Hand", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventDetachHandFromComponent_Parms, Hand), Z_Construct_UEnum_InputCore_EControllerHand, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UVRIKBody_DetachHandFromComponent_Statics::NewProp_Hand_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_DetachHandFromComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_DetachHandFromComponent_Statics::NewProp_Hand,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_DetachHandFromComponent_Statics::NewProp_Hand_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_DetachHandFromComponent_Statics::Function_MetaDataParams[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Reattach hand palm to Motion Controller Component" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_DetachHandFromComponent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "DetachHandFromComponent", sizeof(VRIKBody_eventDetachHandFromComponent_Parms), Z_Construct_UFunction_UVRIKBody_DetachHandFromComponent_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_DetachHandFromComponent_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_DetachHandFromComponent_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_DetachHandFromComponent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_DetachHandFromComponent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_DetachHandFromComponent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_DoResetCalibrationStatus_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_DoResetCalibrationStatus_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Reset body calibration state" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_DoResetCalibrationStatus_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "DoResetCalibrationStatus", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_DoResetCalibrationStatus_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_DoResetCalibrationStatus_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_DoResetCalibrationStatus()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_DoResetCalibrationStatus_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_DoRestoreCalibratedBody_Statics
	{
		struct VRIKBody_eventDoRestoreCalibratedBody_Parms
		{
			FCalibratedBody BodyParams;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BodyParams_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BodyParams;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_DoRestoreCalibratedBody_Statics::NewProp_BodyParams_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_DoRestoreCalibratedBody_Statics::NewProp_BodyParams = { "BodyParams", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventDoRestoreCalibratedBody_Parms, BodyParams), Z_Construct_UScriptStruct_FCalibratedBody, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_DoRestoreCalibratedBody_Statics::NewProp_BodyParams_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_DoRestoreCalibratedBody_Statics::NewProp_BodyParams_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_DoRestoreCalibratedBody_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_DoRestoreCalibratedBody_Statics::NewProp_BodyParams,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_DoRestoreCalibratedBody_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Restoring body calibration from calbirated body struct variable" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_DoRestoreCalibratedBody_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "DoRestoreCalibratedBody", sizeof(VRIKBody_eventDoRestoreCalibratedBody_Parms), Z_Construct_UFunction_UVRIKBody_DoRestoreCalibratedBody_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_DoRestoreCalibratedBody_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00480401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_DoRestoreCalibratedBody_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_DoRestoreCalibratedBody_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_DoRestoreCalibratedBody()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_DoRestoreCalibratedBody_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_GetCalibratedBody_Statics
	{
		struct VRIKBody_eventGetCalibratedBody_Parms
		{
			FCalibratedBody ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_GetCalibratedBody_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventGetCalibratedBody_Parms, ReturnValue), Z_Construct_UScriptStruct_FCalibratedBody, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_GetCalibratedBody_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_GetCalibratedBody_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_GetCalibratedBody_Statics::Function_MetaDataParams[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "@return The object describing body calibration results. Use it to save and restore body params if you need to respawn player." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_GetCalibratedBody_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "GetCalibratedBody", sizeof(VRIKBody_eventGetCalibratedBody_Parms), Z_Construct_UFunction_UVRIKBody_GetCalibratedBody_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_GetCalibratedBody_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_GetCalibratedBody_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_GetCalibratedBody_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_GetCalibratedBody()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_GetCalibratedBody_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_GetCharacterHeight_Statics
	{
		struct VRIKBody_eventGetCharacterHeight_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UVRIKBody_GetCharacterHeight_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventGetCharacterHeight_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_GetCharacterHeight_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_GetCharacterHeight_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_GetCharacterHeight_Statics::Function_MetaDataParams[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Returns calculated or calibrated character height" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_GetCharacterHeight_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "GetCharacterHeight", sizeof(VRIKBody_eventGetCharacterHeight_Parms), Z_Construct_UFunction_UVRIKBody_GetCharacterHeight_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_GetCharacterHeight_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_GetCharacterHeight_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_GetCharacterHeight_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_GetCharacterHeight()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_GetCharacterHeight_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_GetCharacterLegsLength_Statics
	{
		struct VRIKBody_eventGetCharacterLegsLength_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UVRIKBody_GetCharacterLegsLength_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventGetCharacterLegsLength_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_GetCharacterLegsLength_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_GetCharacterLegsLength_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_GetCharacterLegsLength_Statics::Function_MetaDataParams[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Returns calculated or calibrated legs length" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_GetCharacterLegsLength_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "GetCharacterLegsLength", sizeof(VRIKBody_eventGetCharacterLegsLength_Parms), Z_Construct_UFunction_UVRIKBody_GetCharacterLegsLength_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_GetCharacterLegsLength_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_GetCharacterLegsLength_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_GetCharacterLegsLength_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_GetCharacterLegsLength()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_GetCharacterLegsLength_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_GetClearArmSpan_Statics
	{
		struct VRIKBody_eventGetClearArmSpan_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UVRIKBody_GetClearArmSpan_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventGetClearArmSpan_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_GetClearArmSpan_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_GetClearArmSpan_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_GetClearArmSpan_Statics::Function_MetaDataParams[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Clear horizontal distance between palms without any adjustments" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_GetClearArmSpan_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "GetClearArmSpan", sizeof(VRIKBody_eventGetClearArmSpan_Parms), Z_Construct_UFunction_UVRIKBody_GetClearArmSpan_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_GetClearArmSpan_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_GetClearArmSpan_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_GetClearArmSpan_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_GetClearArmSpan()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_GetClearArmSpan_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_GetClearCharacterHeight_Statics
	{
		struct VRIKBody_eventGetClearCharacterHeight_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UVRIKBody_GetClearCharacterHeight_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventGetClearCharacterHeight_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_GetClearCharacterHeight_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_GetClearCharacterHeight_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_GetClearCharacterHeight_Statics::Function_MetaDataParams[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Clear vertical distance from ground to camera without any adjustments" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_GetClearCharacterHeight_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "GetClearCharacterHeight", sizeof(VRIKBody_eventGetClearCharacterHeight_Parms), Z_Construct_UFunction_UVRIKBody_GetClearCharacterHeight_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_GetClearCharacterHeight_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_GetClearCharacterHeight_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_GetClearCharacterHeight_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_GetClearCharacterHeight()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_GetClearCharacterHeight_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_GetCorrelationKoef_Statics
	{
		struct VRIKBody_eventGetCorrelationKoef_Parms
		{
			int32 StartIndex;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_StartIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UVRIKBody_GetCorrelationKoef_Statics::NewProp_StartIndex = { "StartIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventGetCorrelationKoef_Parms, StartIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_GetCorrelationKoef_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_GetCorrelationKoef_Statics::NewProp_StartIndex,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_GetCorrelationKoef_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_GetCorrelationKoef_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "GetCorrelationKoef", sizeof(VRIKBody_eventGetCorrelationKoef_Parms), Z_Construct_UFunction_UVRIKBody_GetCorrelationKoef_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_GetCorrelationKoef_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_GetCorrelationKoef_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_GetCorrelationKoef_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_GetCorrelationKoef()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_GetCorrelationKoef_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_GetElbowJointTarget_Statics
	{
		struct VRIKBody_eventGetElbowJointTarget_Parms
		{
			FVector RightElbowTarget;
			FVector LeftElbowTarget;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LeftElbowTarget;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RightElbowTarget;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_GetElbowJointTarget_Statics::NewProp_LeftElbowTarget = { "LeftElbowTarget", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventGetElbowJointTarget_Parms, LeftElbowTarget), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_GetElbowJointTarget_Statics::NewProp_RightElbowTarget = { "RightElbowTarget", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventGetElbowJointTarget_Parms, RightElbowTarget), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_GetElbowJointTarget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_GetElbowJointTarget_Statics::NewProp_LeftElbowTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_GetElbowJointTarget_Statics::NewProp_RightElbowTarget,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_GetElbowJointTarget_Statics::Function_MetaDataParams[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "@return Joint Targets for elbows to use in TwoBodyIK animation node (in world space)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_GetElbowJointTarget_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "GetElbowJointTarget", sizeof(VRIKBody_eventGetElbowJointTarget_Parms), Z_Construct_UFunction_UVRIKBody_GetElbowJointTarget_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_GetElbowJointTarget_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_GetElbowJointTarget_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_GetElbowJointTarget_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_GetElbowJointTarget()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_GetElbowJointTarget_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_GetKneeJointTarget_Statics
	{
		struct VRIKBody_eventGetKneeJointTarget_Parms
		{
			FVector RightKneeTarget;
			FVector LeftKneeTarget;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LeftKneeTarget;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RightKneeTarget;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_GetKneeJointTarget_Statics::NewProp_LeftKneeTarget = { "LeftKneeTarget", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventGetKneeJointTarget_Parms, LeftKneeTarget), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_GetKneeJointTarget_Statics::NewProp_RightKneeTarget = { "RightKneeTarget", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventGetKneeJointTarget_Parms, RightKneeTarget), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_GetKneeJointTarget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_GetKneeJointTarget_Statics::NewProp_LeftKneeTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_GetKneeJointTarget_Statics::NewProp_RightKneeTarget,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_GetKneeJointTarget_Statics::Function_MetaDataParams[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "@return Joint Targets for knees to use in TwoBodyIK animation node (in world space)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_GetKneeJointTarget_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "GetKneeJointTarget", sizeof(VRIKBody_eventGetKneeJointTarget_Parms), Z_Construct_UFunction_UVRIKBody_GetKneeJointTarget_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_GetKneeJointTarget_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_GetKneeJointTarget_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_GetKneeJointTarget_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_GetKneeJointTarget()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_GetKneeJointTarget_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_GetLastFrameData_Statics
	{
		struct VRIKBody_eventGetLastFrameData_Parms
		{
			FIKBodyData ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_GetLastFrameData_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventGetLastFrameData_Parms, ReturnValue), Z_Construct_UScriptStruct_FIKBodyData, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_GetLastFrameData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_GetLastFrameData_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_GetLastFrameData_Statics::Function_MetaDataParams[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Returns a last body state struct calculated by ComputeFrame(...) function" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_GetLastFrameData_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "GetLastFrameData", sizeof(VRIKBody_eventGetLastFrameData_Parms), Z_Construct_UFunction_UVRIKBody_GetLastFrameData_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_GetLastFrameData_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_GetLastFrameData_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_GetLastFrameData_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_GetLastFrameData()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_GetLastFrameData_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_Initialize_Statics
	{
		struct VRIKBody_eventInitialize_Parms
		{
			USceneComponent* Camera;
			UPrimitiveComponent* RightController;
			UPrimitiveComponent* LeftController;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LeftController_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LeftController;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RightController_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RightController;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Camera_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Camera;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_Initialize_Statics::NewProp_LeftController_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVRIKBody_Initialize_Statics::NewProp_LeftController = { "LeftController", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventInitialize_Parms, LeftController), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_Initialize_Statics::NewProp_LeftController_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_Initialize_Statics::NewProp_LeftController_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_Initialize_Statics::NewProp_RightController_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVRIKBody_Initialize_Statics::NewProp_RightController = { "RightController", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventInitialize_Parms, RightController), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_Initialize_Statics::NewProp_RightController_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_Initialize_Statics::NewProp_RightController_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_Initialize_Statics::NewProp_Camera_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVRIKBody_Initialize_Statics::NewProp_Camera = { "Camera", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventInitialize_Parms, Camera), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_Initialize_Statics::NewProp_Camera_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_Initialize_Statics::NewProp_Camera_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_Initialize_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_Initialize_Statics::NewProp_LeftController,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_Initialize_Statics::NewProp_RightController,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_Initialize_Statics::NewProp_Camera,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_Initialize_Statics::Function_MetaDataParams[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Call this function on BeginPlay to setup component references if VRInputOption is equal to 'Input From Components' and in a multiplayer games.\n      For networking use only Motion Controller components as hand controllers or make sure this components are properly initialized\n      (tracking must be enabled on local PCs and disabled on remotes) manually." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_Initialize_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "Initialize", sizeof(VRIKBody_eventInitialize_Parms), Z_Construct_UFunction_UVRIKBody_Initialize_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_Initialize_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_Initialize_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_Initialize_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_Initialize()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_Initialize_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_IsBodyCalibrated_Statics
	{
		struct VRIKBody_eventIsBodyCalibrated_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVRIKBody_IsBodyCalibrated_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VRIKBody_eventIsBodyCalibrated_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVRIKBody_IsBodyCalibrated_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VRIKBody_eventIsBodyCalibrated_Parms), &Z_Construct_UFunction_UVRIKBody_IsBodyCalibrated_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_IsBodyCalibrated_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_IsBodyCalibrated_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_IsBodyCalibrated_Statics::Function_MetaDataParams[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Return true if calibration was complete or calibration data is loaded by RestoreCalibratedBody" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_IsBodyCalibrated_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "IsBodyCalibrated", sizeof(VRIKBody_eventIsBodyCalibrated_Parms), Z_Construct_UFunction_UVRIKBody_IsBodyCalibrated_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_IsBodyCalibrated_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_IsBodyCalibrated_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_IsBodyCalibrated_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_IsBodyCalibrated()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_IsBodyCalibrated_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_IsInitialized_Statics
	{
		struct VRIKBody_eventIsInitialized_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVRIKBody_IsInitialized_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VRIKBody_eventIsInitialized_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVRIKBody_IsInitialized_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VRIKBody_eventIsInitialized_Parms), &Z_Construct_UFunction_UVRIKBody_IsInitialized_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_IsInitialized_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_IsInitialized_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_IsInitialized_Statics::Function_MetaDataParams[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "@return true if component was initialized by ActivateInput(...) or Initialize(...) function" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_IsInitialized_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "IsInitialized", sizeof(VRIKBody_eventIsInitialized_Parms), Z_Construct_UFunction_UVRIKBody_IsInitialized_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_IsInitialized_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_IsInitialized_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_IsInitialized_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_IsInitialized()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_IsInitialized_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_IsValidCalibrationData_Statics
	{
		struct VRIKBody_eventIsValidCalibrationData_Parms
		{
			FCalibratedBody BodyParams;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BodyParams_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BodyParams;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVRIKBody_IsValidCalibrationData_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VRIKBody_eventIsValidCalibrationData_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVRIKBody_IsValidCalibrationData_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VRIKBody_eventIsValidCalibrationData_Parms), &Z_Construct_UFunction_UVRIKBody_IsValidCalibrationData_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_IsValidCalibrationData_Statics::NewProp_BodyParams_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_IsValidCalibrationData_Statics::NewProp_BodyParams = { "BodyParams", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventIsValidCalibrationData_Parms, BodyParams), Z_Construct_UScriptStruct_FCalibratedBody, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_IsValidCalibrationData_Statics::NewProp_BodyParams_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_IsValidCalibrationData_Statics::NewProp_BodyParams_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_IsValidCalibrationData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_IsValidCalibrationData_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_IsValidCalibrationData_Statics::NewProp_BodyParams,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_IsValidCalibrationData_Statics::Function_MetaDataParams[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Check body calibration params" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_IsValidCalibrationData_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "IsValidCalibrationData", sizeof(VRIKBody_eventIsValidCalibrationData_Parms), Z_Construct_UFunction_UVRIKBody_IsValidCalibrationData_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_IsValidCalibrationData_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_IsValidCalibrationData_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_IsValidCalibrationData_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_IsValidCalibrationData()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_IsValidCalibrationData_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_OnRep_InputBodyState_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_OnRep_InputBodyState_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Replication notifies" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_OnRep_InputBodyState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "OnRep_InputBodyState", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_OnRep_InputBodyState_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_OnRep_InputBodyState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_OnRep_InputBodyState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_OnRep_InputBodyState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_OnRep_InputHandLeft_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_OnRep_InputHandLeft_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_OnRep_InputHandLeft_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "OnRep_InputHandLeft", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_OnRep_InputHandLeft_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_OnRep_InputHandLeft_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_OnRep_InputHandLeft()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_OnRep_InputHandLeft_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_OnRep_InputHandRight_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_OnRep_InputHandRight_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_OnRep_InputHandRight_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "OnRep_InputHandRight", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_OnRep_InputHandRight_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_OnRep_InputHandRight_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_OnRep_InputHandRight()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_OnRep_InputHandRight_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_OnRep_InputHMD_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_OnRep_InputHMD_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_OnRep_InputHMD_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "OnRep_InputHMD", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_OnRep_InputHMD_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_OnRep_InputHMD_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_OnRep_InputHMD()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_OnRep_InputHMD_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_PackDataForReplication_Statics
	{
		struct VRIKBody_eventPackDataForReplication_Parms
		{
			FTransform Head;
			FTransform HandRight;
			FTransform HandLeft;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HandLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HandLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HandRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HandRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Head_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Head;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_PackDataForReplication_Statics::NewProp_HandLeft_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_PackDataForReplication_Statics::NewProp_HandLeft = { "HandLeft", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventPackDataForReplication_Parms, HandLeft), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_PackDataForReplication_Statics::NewProp_HandLeft_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_PackDataForReplication_Statics::NewProp_HandLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_PackDataForReplication_Statics::NewProp_HandRight_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_PackDataForReplication_Statics::NewProp_HandRight = { "HandRight", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventPackDataForReplication_Parms, HandRight), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_PackDataForReplication_Statics::NewProp_HandRight_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_PackDataForReplication_Statics::NewProp_HandRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_PackDataForReplication_Statics::NewProp_Head_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_PackDataForReplication_Statics::NewProp_Head = { "Head", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventPackDataForReplication_Parms, Head), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_PackDataForReplication_Statics::NewProp_Head_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_PackDataForReplication_Statics::NewProp_Head_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_PackDataForReplication_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_PackDataForReplication_Statics::NewProp_HandLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_PackDataForReplication_Statics::NewProp_HandRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_PackDataForReplication_Statics::NewProp_Head,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_PackDataForReplication_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "pack calc/input to networking structs" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_PackDataForReplication_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "PackDataForReplication", sizeof(VRIKBody_eventPackDataForReplication_Parms), Z_Construct_UFunction_UVRIKBody_PackDataForReplication_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_PackDataForReplication_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00C40401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_PackDataForReplication_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_PackDataForReplication_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_PackDataForReplication()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_PackDataForReplication_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_ResetCalibrationStatus_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_ResetCalibrationStatus_Statics::Function_MetaDataParams[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Mark body as non-calibrated. Function (replicated) keeps existing body params, but allow to recalibrate body if necessary." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_ResetCalibrationStatus_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "ResetCalibrationStatus", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_ResetCalibrationStatus_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ResetCalibrationStatus_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_ResetCalibrationStatus()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_ResetCalibrationStatus_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_ResetFootsTimerL_Tick_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_ResetFootsTimerL_Tick_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_ResetFootsTimerL_Tick_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "ResetFootsTimerL_Tick", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_ResetFootsTimerL_Tick_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ResetFootsTimerL_Tick_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_ResetFootsTimerL_Tick()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_ResetFootsTimerL_Tick_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_ResetFootsTimerR_Tick_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_ResetFootsTimerR_Tick_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_ResetFootsTimerR_Tick_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "ResetFootsTimerR_Tick", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_ResetFootsTimerR_Tick_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ResetFootsTimerR_Tick_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_ResetFootsTimerR_Tick()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_ResetFootsTimerR_Tick_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_ResetTorso_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_ResetTorso_Statics::Function_MetaDataParams[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Call this function to set Pitch and Roll of Pelvis to zero and Yaw equal to Head Yaw" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_ResetTorso_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "ResetTorso", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_ResetTorso_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ResetTorso_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_ResetTorso()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_ResetTorso_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_RestoreCalibratedBody_Statics
	{
		struct VRIKBody_eventRestoreCalibratedBody_Parms
		{
			FCalibratedBody BodyParams;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BodyParams_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BodyParams;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_RestoreCalibratedBody_Statics::NewProp_BodyParams_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_RestoreCalibratedBody_Statics::NewProp_BodyParams = { "BodyParams", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventRestoreCalibratedBody_Parms, BodyParams), Z_Construct_UScriptStruct_FCalibratedBody, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_RestoreCalibratedBody_Statics::NewProp_BodyParams_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_RestoreCalibratedBody_Statics::NewProp_BodyParams_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_RestoreCalibratedBody_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_RestoreCalibratedBody_Statics::NewProp_BodyParams,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_RestoreCalibratedBody_Statics::Function_MetaDataParams[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Load body calibration params" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_RestoreCalibratedBody_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "RestoreCalibratedBody", sizeof(VRIKBody_eventRestoreCalibratedBody_Parms), Z_Construct_UFunction_UVRIKBody_RestoreCalibratedBody_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_RestoreCalibratedBody_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_RestoreCalibratedBody_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_RestoreCalibratedBody_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_RestoreCalibratedBody()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_RestoreCalibratedBody_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_RibcageYawTimer_Tick_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_RibcageYawTimer_Tick_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_RibcageYawTimer_Tick_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "RibcageYawTimer_Tick", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_RibcageYawTimer_Tick_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_RibcageYawTimer_Tick_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_RibcageYawTimer_Tick()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_RibcageYawTimer_Tick_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_ServerResetCalibrationStatus_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_ServerResetCalibrationStatus_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Reset body calibration state (replicated version)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_ServerResetCalibrationStatus_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "ServerResetCalibrationStatus", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x80280CC0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_ServerResetCalibrationStatus_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ServerResetCalibrationStatus_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_ServerResetCalibrationStatus()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_ServerResetCalibrationStatus_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_ServerRestoreCalibratedBody_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BodyParams_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BodyParams;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_ServerRestoreCalibratedBody_Statics::NewProp_BodyParams_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_ServerRestoreCalibratedBody_Statics::NewProp_BodyParams = { "BodyParams", nullptr, (EPropertyFlags)0x0010000008000082, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventServerRestoreCalibratedBody_Parms, BodyParams), Z_Construct_UScriptStruct_FCalibratedBody, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_ServerRestoreCalibratedBody_Statics::NewProp_BodyParams_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ServerRestoreCalibratedBody_Statics::NewProp_BodyParams_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_ServerRestoreCalibratedBody_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_ServerRestoreCalibratedBody_Statics::NewProp_BodyParams,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_ServerRestoreCalibratedBody_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Load body calibration params (replicated version)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_ServerRestoreCalibratedBody_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "ServerRestoreCalibratedBody", sizeof(VRIKBody_eventServerRestoreCalibratedBody_Parms), Z_Construct_UFunction_UVRIKBody_ServerRestoreCalibratedBody_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ServerRestoreCalibratedBody_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x80280CC0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_ServerRestoreCalibratedBody_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ServerRestoreCalibratedBody_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_ServerRestoreCalibratedBody()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_ServerRestoreCalibratedBody_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_ServerUpdateBodyState_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_State_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_State;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_ServerUpdateBodyState_Statics::NewProp_State_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_ServerUpdateBodyState_Statics::NewProp_State = { "State", nullptr, (EPropertyFlags)0x0010000008000082, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventServerUpdateBodyState_Parms, State), Z_Construct_UScriptStruct_FNetworkIKBodyData, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_ServerUpdateBodyState_Statics::NewProp_State_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ServerUpdateBodyState_Statics::NewProp_State_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_ServerUpdateBodyState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_ServerUpdateBodyState_Statics::NewProp_State,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_ServerUpdateBodyState_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "send body state to server" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_ServerUpdateBodyState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "ServerUpdateBodyState", sizeof(VRIKBody_eventServerUpdateBodyState_Parms), Z_Construct_UFunction_UVRIKBody_ServerUpdateBodyState_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ServerUpdateBodyState_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x80240C41, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_ServerUpdateBodyState_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ServerUpdateBodyState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_ServerUpdateBodyState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_ServerUpdateBodyState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_ServerUpdateInputs_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HandLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HandLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HandRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HandRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Head_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Head;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_ServerUpdateInputs_Statics::NewProp_HandLeft_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_ServerUpdateInputs_Statics::NewProp_HandLeft = { "HandLeft", nullptr, (EPropertyFlags)0x0010000008000082, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventServerUpdateInputs_Parms, HandLeft), Z_Construct_UScriptStruct_FNetworkTransform, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_ServerUpdateInputs_Statics::NewProp_HandLeft_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ServerUpdateInputs_Statics::NewProp_HandLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_ServerUpdateInputs_Statics::NewProp_HandRight_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_ServerUpdateInputs_Statics::NewProp_HandRight = { "HandRight", nullptr, (EPropertyFlags)0x0010000008000082, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventServerUpdateInputs_Parms, HandRight), Z_Construct_UScriptStruct_FNetworkTransform, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_ServerUpdateInputs_Statics::NewProp_HandRight_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ServerUpdateInputs_Statics::NewProp_HandRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_ServerUpdateInputs_Statics::NewProp_Head_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_ServerUpdateInputs_Statics::NewProp_Head = { "Head", nullptr, (EPropertyFlags)0x0010000008000082, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventServerUpdateInputs_Parms, Head), Z_Construct_UScriptStruct_FNetworkTransform, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_ServerUpdateInputs_Statics::NewProp_Head_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ServerUpdateInputs_Statics::NewProp_Head_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_ServerUpdateInputs_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_ServerUpdateInputs_Statics::NewProp_HandLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_ServerUpdateInputs_Statics::NewProp_HandRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_ServerUpdateInputs_Statics::NewProp_Head,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_ServerUpdateInputs_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "send VR input to server" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_ServerUpdateInputs_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "ServerUpdateInputs", sizeof(VRIKBody_eventServerUpdateInputs_Parms), Z_Construct_UFunction_UVRIKBody_ServerUpdateInputs_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ServerUpdateInputs_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x80240C41, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_ServerUpdateInputs_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_ServerUpdateInputs_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_ServerUpdateInputs()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_ServerUpdateInputs_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_SetManualBodyOrientation_Statics
	{
		struct VRIKBody_eventSetManualBodyOrientation_Parms
		{
			EBodyOrientation NewOrientation;
		};
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_NewOrientation;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_NewOrientation_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UVRIKBody_SetManualBodyOrientation_Statics::NewProp_NewOrientation = { "NewOrientation", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventSetManualBodyOrientation_Parms, NewOrientation), Z_Construct_UEnum_VRIKBodyRuntime_EBodyOrientation, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UVRIKBody_SetManualBodyOrientation_Statics::NewProp_NewOrientation_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_SetManualBodyOrientation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_SetManualBodyOrientation_Statics::NewProp_NewOrientation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_SetManualBodyOrientation_Statics::NewProp_NewOrientation_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_SetManualBodyOrientation_Statics::Function_MetaDataParams[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Manually set body orientation (for example, lying at the ground)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_SetManualBodyOrientation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "SetManualBodyOrientation", sizeof(VRIKBody_eventSetManualBodyOrientation_Parms), Z_Construct_UFunction_UVRIKBody_SetManualBodyOrientation_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_SetManualBodyOrientation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_SetManualBodyOrientation_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_SetManualBodyOrientation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_SetManualBodyOrientation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_SetManualBodyOrientation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_TraceFloor_Statics
	{
		struct VRIKBody_eventTraceFloor_Parms
		{
			FVector HeadLocation;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HeadLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HeadLocation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_TraceFloor_Statics::NewProp_HeadLocation_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_TraceFloor_Statics::NewProp_HeadLocation = { "HeadLocation", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventTraceFloor_Parms, HeadLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_TraceFloor_Statics::NewProp_HeadLocation_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_TraceFloor_Statics::NewProp_HeadLocation_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_TraceFloor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_TraceFloor_Statics::NewProp_HeadLocation,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_TraceFloor_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "single line trace to floor" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_TraceFloor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "TraceFloor", sizeof(VRIKBody_eventTraceFloor_Parms), Z_Construct_UFunction_UVRIKBody_TraceFloor_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_TraceFloor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00C40401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_TraceFloor_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_TraceFloor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_TraceFloor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_TraceFloor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_UpdateInputTransforms_Statics
	{
		struct VRIKBody_eventUpdateInputTransforms_Parms
		{
			FTransform HeadTransform;
			FTransform RightHandTransform;
			FTransform LeftHandTransform;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LeftHandTransform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LeftHandTransform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RightHandTransform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RightHandTransform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HeadTransform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HeadTransform;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_UpdateInputTransforms_Statics::NewProp_LeftHandTransform_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_UpdateInputTransforms_Statics::NewProp_LeftHandTransform = { "LeftHandTransform", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventUpdateInputTransforms_Parms, LeftHandTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_UpdateInputTransforms_Statics::NewProp_LeftHandTransform_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_UpdateInputTransforms_Statics::NewProp_LeftHandTransform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_UpdateInputTransforms_Statics::NewProp_RightHandTransform_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_UpdateInputTransforms_Statics::NewProp_RightHandTransform = { "RightHandTransform", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventUpdateInputTransforms_Parms, RightHandTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_UpdateInputTransforms_Statics::NewProp_RightHandTransform_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_UpdateInputTransforms_Statics::NewProp_RightHandTransform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_UpdateInputTransforms_Statics::NewProp_HeadTransform_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVRIKBody_UpdateInputTransforms_Statics::NewProp_HeadTransform = { "HeadTransform", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VRIKBody_eventUpdateInputTransforms_Parms, HeadTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_UpdateInputTransforms_Statics::NewProp_HeadTransform_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_UpdateInputTransforms_Statics::NewProp_HeadTransform_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVRIKBody_UpdateInputTransforms_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_UpdateInputTransforms_Statics::NewProp_LeftHandTransform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_UpdateInputTransforms_Statics::NewProp_RightHandTransform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVRIKBody_UpdateInputTransforms_Statics::NewProp_HeadTransform,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_UpdateInputTransforms_Statics::Function_MetaDataParams[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Head/hands Input if VRInputOption is 'Input from Variables'" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_UpdateInputTransforms_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "UpdateInputTransforms", sizeof(VRIKBody_eventUpdateInputTransforms_Parms), Z_Construct_UFunction_UVRIKBody_UpdateInputTransforms_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_UpdateInputTransforms_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_UpdateInputTransforms_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_UpdateInputTransforms_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_UpdateInputTransforms()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_UpdateInputTransforms_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_UseAutoBodyOrientation_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_UseAutoBodyOrientation_Statics::Function_MetaDataParams[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Restore automatic body calibration" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_UseAutoBodyOrientation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "UseAutoBodyOrientation", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_UseAutoBodyOrientation_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_UseAutoBodyOrientation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_UseAutoBodyOrientation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_UseAutoBodyOrientation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVRIKBody_VRInputTimer_Tick_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVRIKBody_VRInputTimer_Tick_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVRIKBody_VRInputTimer_Tick_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVRIKBody, nullptr, "VRInputTimer_Tick", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVRIKBody_VRInputTimer_Tick_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UVRIKBody_VRInputTimer_Tick_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVRIKBody_VRInputTimer_Tick()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVRIKBody_VRInputTimer_Tick_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UVRIKBody_NoRegister()
	{
		return UVRIKBody::StaticClass();
	}
	struct Z_Construct_UClass_UVRIKBody_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviousElbowRollLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PreviousElbowRollLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviousElbowRollRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PreviousElbowRollRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentVRInputIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_CurrentVRInputIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrevFrameActorRot_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PrevFrameActorRot;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShoulderPitchOffsetLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ShoulderPitchOffsetLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShoulderPitchOffsetRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ShoulderPitchOffsetRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShoulderYawOffsetLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ShoulderYawOffsetLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShoulderYawOffsetRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ShoulderYawOffsetRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RecalcCharacteHeightTimeJumping_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RecalcCharacteHeightTimeJumping;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RecalcCharacteHeightTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RecalcCharacteHeightTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HeadLocationBeforeSitting_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HeadLocationBeforeSitting;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HeadPitchBeforeSitting_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HeadPitchBeforeSitting;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TracedFloorLevelL_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TracedFloorLevelL;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TracedFloorLevelR_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TracedFloorLevelR;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TracedFloorLevel_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TracedFloorLevel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCalibratedI_MetaData[];
#endif
		static void NewProp_bCalibratedI_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCalibratedI;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCalibratedT_MetaData[];
#endif
		static void NewProp_bCalibratedT_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCalibratedT;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CalI_ControllerR_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CalI_ControllerR;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CalI_ControllerL_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CalI_ControllerL;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CalI_Head_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CalI_Head;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CalT_ControllerR_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CalT_ControllerR;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CalT_ControllerL_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CalT_ControllerL;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CalT_Head_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CalT_Head;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ThighLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ThighLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UpperarmLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_UpperarmLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StartRotationYaw_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_StartRotationYaw;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_nIsRotating_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_nIsRotating;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LegsLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LegsLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ArmSpanClear_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ArmSpanClear;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CharacterHeightClear_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CharacterHeightClear;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CharacterHeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CharacterHeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_JumpingOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_JumpingOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsWalking_MetaData[];
#endif
		static void NewProp_bIsWalking_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsWalking;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsStanding_MetaData[];
#endif
		static void NewProp_bIsStanding_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsStanding;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsSitting_MetaData[];
#endif
		static void NewProp_bIsSitting_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsSitting;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bTorsoRollRotation_MetaData[];
#endif
		static void NewProp_bTorsoRollRotation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bTorsoRollRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bTorsoPitchRotation_MetaData[];
#endif
		static void NewProp_bTorsoPitchRotation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bTorsoPitchRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bTorsoYawRotation_MetaData[];
#endif
		static void NewProp_bTorsoYawRotation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bTorsoYawRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModifyHeightStartTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ModifyHeightStartTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_nModifyHeightState_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_nModifyHeightState;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResetFootLocationR_MetaData[];
#endif
		static void NewProp_ResetFootLocationR_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ResetFootLocationR;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResetFootLocationL_MetaData[];
#endif
		static void NewProp_ResetFootLocationL_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ResetFootLocationL;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SavedTorsoDirection_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SavedTorsoDirection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bResetTorso_MetaData[];
#endif
		static void NewProp_bResetTorso_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bResetTorso;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bYawInterpToHead_MetaData[];
#endif
		static void NewProp_bYawInterpToHead_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bYawInterpToHead;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_nYawControlCounter_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_nYawControlCounter;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bResetFeet_MetaData[];
#endif
		static void NewProp_bResetFeet_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bResetFeet;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_fDeltaTimeR_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_fDeltaTimeR;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_fDeltaTimeL_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_fDeltaTimeL;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FeetMovingStartedTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FeetMovingStartedTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FeetCyclePhase_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FeetCyclePhase;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FootLastTransformR_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FootLastTransformR;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FootLastTransformL_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FootLastTransformL;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FootTickTransformR_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FootTickTransformR;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FootTickTransformL_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FootTickTransformL;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FootTargetTransformR_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FootTargetTransformR;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FootTargetTransformL_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FootTargetTransformL;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_hTorsoYawTimer_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_hTorsoYawTimer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_hResetFootRTimer_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_hResetFootRTimer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_hResetFootLTimer_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_hResetFootLTimer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_hVRInputTimer_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_hVRInputTimer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HandAttachSocketLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_HandAttachSocketLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HandAttachSocketRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_HandAttachSocketRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HandAttachTransformLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HandAttachTransformLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HandAttachTransformRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HandAttachTransformRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HandParentLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_HandParentLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HandParentRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_HandParentRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HandAttachedLeft_MetaData[];
#endif
		static void NewProp_HandAttachedLeft_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_HandAttachedLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HandAttachedRight_MetaData[];
#endif
		static void NewProp_HandAttachedRight_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_HandAttachedRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_rPrevCamRotator_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_rPrevCamRotator;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_vPrevCamLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_vPrevCamLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputHandLeft_Target_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InputHandLeft_Target;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputHandRight_Target_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InputHandRight_Target;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputHMD_Target_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InputHMD_Target;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SkeletonTransformData_Target_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SkeletonTransformData_Target;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputHandLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InputHandLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputHandRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InputHandRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputHMD_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InputHMD;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SkeletonTransformDataRelative_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SkeletonTransformDataRelative;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SkeletonTransformData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SkeletonTransformData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NT_InputHandLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NT_InputHandLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NT_InputHandRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NT_InputHandRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NT_InputHMD_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NT_InputHMD;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NT_SkeletonTransformData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NT_SkeletonTransformData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bManualBodyOrientation_MetaData[];
#endif
		static void NewProp_bManualBodyOrientation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bManualBodyOrientation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDebugOutput_MetaData[];
#endif
		static void NewProp_bDebugOutput_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDebugOutput;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsInitialized_MetaData[];
#endif
		static void NewProp_bIsInitialized_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsInitialized;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FloorBaseComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FloorBaseComp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OwningPawn_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OwningPawn;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LeftHandController_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LeftHandController;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RightHandController_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RightHandController;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CameraComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnHeadNod_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnHeadNod;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnHeadShake_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnHeadShake;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnStandUp_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnStandUp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnSitDown_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnSitDown;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnGrounded_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnGrounded;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnJumpStarted_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnJumpStarted;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnCalibrationComplete_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnCalibrationComplete;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SmoothingInterpolationSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SmoothingInterpolationSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReplicateInWorldSpace_MetaData[];
#endif
		static void NewProp_ReplicateInWorldSpace_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReplicateInWorldSpace;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReplicateComponentsMovement_MetaData[];
#endif
		static void NewProp_ReplicateComponentsMovement_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReplicateComponentsMovement;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReplicateFullBodyState_MetaData[];
#endif
		static void NewProp_ReplicateFullBodyState_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReplicateFullBodyState;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SoftHeadRotationLimit_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SoftHeadRotationLimit;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxHeadRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxHeadRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RibcageToNeckOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RibcageToNeckOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NeckToHeadsetOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NeckToHeadsetOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FootOffsetToGround_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FootOffsetToGround;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HandLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HandLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpineLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SpineLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HeadHeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HeadHeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HeadHalfWidth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HeadHalfWidth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BodyThickness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BodyThickness;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BodyWidth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BodyWidth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDoManualBodyBending_MetaData[];
#endif
		static void NewProp_bDoManualBodyBending_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDoManualBodyBending;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TorsoResetToHeadInterval_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TorsoResetToHeadInterval;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDoPawnOrientationAdjustment_MetaData[];
#endif
		static void NewProp_bDoPawnOrientationAdjustment_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDoPawnOrientationAdjustment;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimerInterval_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TimerInterval;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RightPalmOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RightPalmOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LeftPalmOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LeftPalmOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFollowHeadRotationWhenSitting_MetaData[];
#endif
		static void NewProp_bFollowHeadRotationWhenSitting_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFollowHeadRotationWhenSitting;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bLockTorsoToHead_MetaData[];
#endif
		static void NewProp_bLockTorsoToHead_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bLockTorsoToHead;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TorsoRotationSensitivity_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TorsoRotationSensitivity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnableTorsoTwist_MetaData[];
#endif
		static void NewProp_EnableTorsoTwist_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_EnableTorsoTwist;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DetectContinuousHeadYawRotation_MetaData[];
#endif
		static void NewProp_DetectContinuousHeadYawRotation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_DetectContinuousHeadYawRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FollowPawnTransform_MetaData[];
#endif
		static void NewProp_FollowPawnTransform_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_FollowPawnTransform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LockShouldersRotation_MetaData[];
#endif
		static void NewProp_LockShouldersRotation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_LockShouldersRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VRInputOption_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_VRInputOption;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_VRInputOption_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FloorCollisionObjectType_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_FloorCollisionObjectType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TraceFloorByObjectType_MetaData[];
#endif
		static void NewProp_TraceFloorByObjectType_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_TraceFloorByObjectType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UseActorLocationAsFloor_MetaData[];
#endif
		static void NewProp_UseActorLocationAsFloor_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_UseActorLocationAsFloor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ComputeHandsIK_MetaData[];
#endif
		static void NewProp_ComputeHandsIK_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ComputeHandsIK;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ComputeLegsIK_MetaData[];
#endif
		static void NewProp_ComputeLegsIK_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ComputeLegsIK;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ComputeFeetIKTargets_MetaData[];
#endif
		static void NewProp_ComputeFeetIKTargets_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ComputeFeetIKTargets;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVRIKBody_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_VRIKBodyRuntime,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UVRIKBody_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UVRIKBody_ActivateInput, "ActivateInput" }, // 109936970
		{ &Z_Construct_UFunction_UVRIKBody_AttachHandToComponent, "AttachHandToComponent" }, // 1873221220
		{ &Z_Construct_UFunction_UVRIKBody_AutoCalibrateBodyAtPose, "AutoCalibrateBodyAtPose" }, // 3219380048
		{ &Z_Construct_UFunction_UVRIKBody_CalcFeetIKTransforms2, "CalcFeetIKTransforms2" }, // 4053084597
		{ &Z_Construct_UFunction_UVRIKBody_CalcFeetInstantaneousTransforms, "CalcFeetInstantaneousTransforms" }, // 3017635514
		{ &Z_Construct_UFunction_UVRIKBody_CalcHandIKTransforms, "CalcHandIKTransforms" }, // 531719788
		{ &Z_Construct_UFunction_UVRIKBody_CalcShouldersWithOffset, "CalcShouldersWithOffset" }, // 3951210745
		{ &Z_Construct_UFunction_UVRIKBody_CalculateTwoBoneIK, "CalculateTwoBoneIK" }, // 3960218828
		{ &Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtIPose, "CalibrateBodyAtIPose" }, // 2280435744
		{ &Z_Construct_UFunction_UVRIKBody_CalibrateBodyAtTPose, "CalibrateBodyAtTPose" }, // 1749980396
		{ &Z_Construct_UFunction_UVRIKBody_CalibrateSkeleton, "CalibrateSkeleton" }, // 3715131928
		{ &Z_Construct_UFunction_UVRIKBody_ClampElbowRotation, "ClampElbowRotation" }, // 3940613589
		{ &Z_Construct_UFunction_UVRIKBody_ClientResetCalibrationStatus, "ClientResetCalibrationStatus" }, // 990341822
		{ &Z_Construct_UFunction_UVRIKBody_ClientRestoreCalibratedBody, "ClientRestoreCalibratedBody" }, // 2477348758
		{ &Z_Construct_UFunction_UVRIKBody_ComputeFrame, "ComputeFrame" }, // 2355873371
		{ &Z_Construct_UFunction_UVRIKBody_ConvertDataToSkeletonFriendly, "ConvertDataToSkeletonFriendly" }, // 2245894798
		{ &Z_Construct_UFunction_UVRIKBody_DeactivateInput, "DeactivateInput" }, // 2102776204
		{ &Z_Construct_UFunction_UVRIKBody_DetachHandFromComponent, "DetachHandFromComponent" }, // 3664808072
		{ &Z_Construct_UFunction_UVRIKBody_DoResetCalibrationStatus, "DoResetCalibrationStatus" }, // 3526388261
		{ &Z_Construct_UFunction_UVRIKBody_DoRestoreCalibratedBody, "DoRestoreCalibratedBody" }, // 3626071435
		{ &Z_Construct_UFunction_UVRIKBody_GetCalibratedBody, "GetCalibratedBody" }, // 2753444784
		{ &Z_Construct_UFunction_UVRIKBody_GetCharacterHeight, "GetCharacterHeight" }, // 849983668
		{ &Z_Construct_UFunction_UVRIKBody_GetCharacterLegsLength, "GetCharacterLegsLength" }, // 3568101661
		{ &Z_Construct_UFunction_UVRIKBody_GetClearArmSpan, "GetClearArmSpan" }, // 726337169
		{ &Z_Construct_UFunction_UVRIKBody_GetClearCharacterHeight, "GetClearCharacterHeight" }, // 1912188224
		{ &Z_Construct_UFunction_UVRIKBody_GetCorrelationKoef, "GetCorrelationKoef" }, // 1185149083
		{ &Z_Construct_UFunction_UVRIKBody_GetElbowJointTarget, "GetElbowJointTarget" }, // 3599190640
		{ &Z_Construct_UFunction_UVRIKBody_GetKneeJointTarget, "GetKneeJointTarget" }, // 1158343109
		{ &Z_Construct_UFunction_UVRIKBody_GetLastFrameData, "GetLastFrameData" }, // 2726127541
		{ &Z_Construct_UFunction_UVRIKBody_Initialize, "Initialize" }, // 1866515114
		{ &Z_Construct_UFunction_UVRIKBody_IsBodyCalibrated, "IsBodyCalibrated" }, // 2507376283
		{ &Z_Construct_UFunction_UVRIKBody_IsInitialized, "IsInitialized" }, // 2998610835
		{ &Z_Construct_UFunction_UVRIKBody_IsValidCalibrationData, "IsValidCalibrationData" }, // 1751241689
		{ &Z_Construct_UFunction_UVRIKBody_OnRep_InputBodyState, "OnRep_InputBodyState" }, // 1113171214
		{ &Z_Construct_UFunction_UVRIKBody_OnRep_InputHandLeft, "OnRep_InputHandLeft" }, // 406028912
		{ &Z_Construct_UFunction_UVRIKBody_OnRep_InputHandRight, "OnRep_InputHandRight" }, // 844367517
		{ &Z_Construct_UFunction_UVRIKBody_OnRep_InputHMD, "OnRep_InputHMD" }, // 3125150479
		{ &Z_Construct_UFunction_UVRIKBody_PackDataForReplication, "PackDataForReplication" }, // 453172525
		{ &Z_Construct_UFunction_UVRIKBody_ResetCalibrationStatus, "ResetCalibrationStatus" }, // 2517007620
		{ &Z_Construct_UFunction_UVRIKBody_ResetFootsTimerL_Tick, "ResetFootsTimerL_Tick" }, // 1491927736
		{ &Z_Construct_UFunction_UVRIKBody_ResetFootsTimerR_Tick, "ResetFootsTimerR_Tick" }, // 1842858295
		{ &Z_Construct_UFunction_UVRIKBody_ResetTorso, "ResetTorso" }, // 3846726541
		{ &Z_Construct_UFunction_UVRIKBody_RestoreCalibratedBody, "RestoreCalibratedBody" }, // 452469591
		{ &Z_Construct_UFunction_UVRIKBody_RibcageYawTimer_Tick, "RibcageYawTimer_Tick" }, // 1266415098
		{ &Z_Construct_UFunction_UVRIKBody_ServerResetCalibrationStatus, "ServerResetCalibrationStatus" }, // 735950382
		{ &Z_Construct_UFunction_UVRIKBody_ServerRestoreCalibratedBody, "ServerRestoreCalibratedBody" }, // 3600106857
		{ &Z_Construct_UFunction_UVRIKBody_ServerUpdateBodyState, "ServerUpdateBodyState" }, // 3212874072
		{ &Z_Construct_UFunction_UVRIKBody_ServerUpdateInputs, "ServerUpdateInputs" }, // 327960665
		{ &Z_Construct_UFunction_UVRIKBody_SetManualBodyOrientation, "SetManualBodyOrientation" }, // 4197976653
		{ &Z_Construct_UFunction_UVRIKBody_TraceFloor, "TraceFloor" }, // 1000949797
		{ &Z_Construct_UFunction_UVRIKBody_UpdateInputTransforms, "UpdateInputTransforms" }, // 1803331746
		{ &Z_Construct_UFunction_UVRIKBody_UseAutoBodyOrientation, "UseAutoBodyOrientation" }, // 531519356
		{ &Z_Construct_UFunction_UVRIKBody_VRInputTimer_Tick, "VRInputTimer_Tick" }, // 1582994480
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "IKBody" },
		{ "IncludePath", "VRIKBody.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "This component uses input received from VR head set and controllers to calculate approximate player's body state" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_PreviousElbowRollLeft_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_PreviousElbowRollLeft = { "PreviousElbowRollLeft", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, PreviousElbowRollLeft), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_PreviousElbowRollLeft_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_PreviousElbowRollLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_PreviousElbowRollRight_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_PreviousElbowRollRight = { "PreviousElbowRollRight", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, PreviousElbowRollRight), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_PreviousElbowRollRight_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_PreviousElbowRollRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_CurrentVRInputIndex_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Saved VR Input Data" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_CurrentVRInputIndex = { "CurrentVRInputIndex", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, CurrentVRInputIndex), nullptr, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_CurrentVRInputIndex_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_CurrentVRInputIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_PrevFrameActorRot_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_PrevFrameActorRot = { "PrevFrameActorRot", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, PrevFrameActorRot), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_PrevFrameActorRot_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_PrevFrameActorRot_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_ShoulderPitchOffsetLeft_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_ShoulderPitchOffsetLeft = { "ShoulderPitchOffsetLeft", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, ShoulderPitchOffsetLeft), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_ShoulderPitchOffsetLeft_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_ShoulderPitchOffsetLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_ShoulderPitchOffsetRight_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_ShoulderPitchOffsetRight = { "ShoulderPitchOffsetRight", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, ShoulderPitchOffsetRight), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_ShoulderPitchOffsetRight_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_ShoulderPitchOffsetRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_ShoulderYawOffsetLeft_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_ShoulderYawOffsetLeft = { "ShoulderYawOffsetLeft", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, ShoulderYawOffsetLeft), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_ShoulderYawOffsetLeft_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_ShoulderYawOffsetLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_ShoulderYawOffsetRight_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_ShoulderYawOffsetRight = { "ShoulderYawOffsetRight", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, ShoulderYawOffsetRight), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_ShoulderYawOffsetRight_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_ShoulderYawOffsetRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_RecalcCharacteHeightTimeJumping_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_RecalcCharacteHeightTimeJumping = { "RecalcCharacteHeightTimeJumping", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, RecalcCharacteHeightTimeJumping), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_RecalcCharacteHeightTimeJumping_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_RecalcCharacteHeightTimeJumping_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_RecalcCharacteHeightTime_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_RecalcCharacteHeightTime = { "RecalcCharacteHeightTime", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, RecalcCharacteHeightTime), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_RecalcCharacteHeightTime_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_RecalcCharacteHeightTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_HeadLocationBeforeSitting_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_HeadLocationBeforeSitting = { "HeadLocationBeforeSitting", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, HeadLocationBeforeSitting), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_HeadLocationBeforeSitting_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_HeadLocationBeforeSitting_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_HeadPitchBeforeSitting_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Variables to calculate body state" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_HeadPitchBeforeSitting = { "HeadPitchBeforeSitting", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, HeadPitchBeforeSitting), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_HeadPitchBeforeSitting_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_HeadPitchBeforeSitting_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_TracedFloorLevelL_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_TracedFloorLevelL = { "TracedFloorLevelL", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, TracedFloorLevelL), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_TracedFloorLevelL_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_TracedFloorLevelL_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_TracedFloorLevelR_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_TracedFloorLevelR = { "TracedFloorLevelR", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, TracedFloorLevelR), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_TracedFloorLevelR_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_TracedFloorLevelR_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_TracedFloorLevel_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_TracedFloorLevel = { "TracedFloorLevel", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, TracedFloorLevel), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_TracedFloorLevel_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_TracedFloorLevel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_bCalibratedI_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_bCalibratedI_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->bCalibratedI = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_bCalibratedI = { "bCalibratedI", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_bCalibratedI_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bCalibratedI_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bCalibratedI_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_bCalibratedT_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_bCalibratedT_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->bCalibratedT = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_bCalibratedT = { "bCalibratedT", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_bCalibratedT_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bCalibratedT_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bCalibratedT_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_CalI_ControllerR_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_CalI_ControllerR = { "CalI_ControllerR", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, CalI_ControllerR), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_CalI_ControllerR_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_CalI_ControllerR_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_CalI_ControllerL_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_CalI_ControllerL = { "CalI_ControllerL", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, CalI_ControllerL), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_CalI_ControllerL_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_CalI_ControllerL_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_CalI_Head_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_CalI_Head = { "CalI_Head", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, CalI_Head), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_CalI_Head_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_CalI_Head_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_CalT_ControllerR_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_CalT_ControllerR = { "CalT_ControllerR", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, CalT_ControllerR), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_CalT_ControllerR_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_CalT_ControllerR_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_CalT_ControllerL_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_CalT_ControllerL = { "CalT_ControllerL", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, CalT_ControllerL), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_CalT_ControllerL_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_CalT_ControllerL_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_CalT_Head_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Calibration Setup" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_CalT_Head = { "CalT_Head", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, CalT_Head), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_CalT_Head_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_CalT_Head_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_ThighLength_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_ThighLength = { "ThighLength", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, ThighLength), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_ThighLength_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_ThighLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_UpperarmLength_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_UpperarmLength = { "UpperarmLength", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, UpperarmLength), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_UpperarmLength_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_UpperarmLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_StartRotationYaw_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_StartRotationYaw = { "StartRotationYaw", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, StartRotationYaw), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_StartRotationYaw_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_StartRotationYaw_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_nIsRotating_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_nIsRotating = { "nIsRotating", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, nIsRotating), nullptr, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_nIsRotating_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_nIsRotating_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_LegsLength_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_LegsLength = { "LegsLength", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, LegsLength), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_LegsLength_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_LegsLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_ArmSpanClear_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_ArmSpanClear = { "ArmSpanClear", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, ArmSpanClear), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_ArmSpanClear_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_ArmSpanClear_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_CharacterHeightClear_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_CharacterHeightClear = { "CharacterHeightClear", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, CharacterHeightClear), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_CharacterHeightClear_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_CharacterHeightClear_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_CharacterHeight_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_CharacterHeight = { "CharacterHeight", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, CharacterHeight), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_CharacterHeight_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_CharacterHeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_JumpingOffset_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_JumpingOffset = { "JumpingOffset", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, JumpingOffset), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_JumpingOffset_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_JumpingOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_bIsWalking_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_bIsWalking_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->bIsWalking = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_bIsWalking = { "bIsWalking", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_bIsWalking_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bIsWalking_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bIsWalking_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_bIsStanding_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_bIsStanding_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->bIsStanding = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_bIsStanding = { "bIsStanding", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_bIsStanding_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bIsStanding_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bIsStanding_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_bIsSitting_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_bIsSitting_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->bIsSitting = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_bIsSitting = { "bIsSitting", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_bIsSitting_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bIsSitting_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bIsSitting_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_bTorsoRollRotation_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_bTorsoRollRotation_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->bTorsoRollRotation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_bTorsoRollRotation = { "bTorsoRollRotation", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_bTorsoRollRotation_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bTorsoRollRotation_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bTorsoRollRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_bTorsoPitchRotation_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_bTorsoPitchRotation_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->bTorsoPitchRotation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_bTorsoPitchRotation = { "bTorsoPitchRotation", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_bTorsoPitchRotation_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bTorsoPitchRotation_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bTorsoPitchRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_bTorsoYawRotation_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Internal current body state" },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_bTorsoYawRotation_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->bTorsoYawRotation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_bTorsoYawRotation = { "bTorsoYawRotation", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_bTorsoYawRotation_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bTorsoYawRotation_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bTorsoYawRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_ModifyHeightStartTime_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "0 - waiting; 1 - modify on; 2 - don't modify" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_ModifyHeightStartTime = { "ModifyHeightStartTime", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, ModifyHeightStartTime), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_ModifyHeightStartTime_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_ModifyHeightStartTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_nModifyHeightState_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_nModifyHeightState = { "nModifyHeightState", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, nModifyHeightState), nullptr, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_nModifyHeightState_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_nModifyHeightState_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_ResetFootLocationR_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_ResetFootLocationR_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->ResetFootLocationR = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_ResetFootLocationR = { "ResetFootLocationR", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_ResetFootLocationR_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_ResetFootLocationR_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_ResetFootLocationR_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_ResetFootLocationL_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Initial auto calibration setup" },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_ResetFootLocationL_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->ResetFootLocationL = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_ResetFootLocationL = { "ResetFootLocationL", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_ResetFootLocationL_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_ResetFootLocationL_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_ResetFootLocationL_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_SavedTorsoDirection_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_SavedTorsoDirection = { "SavedTorsoDirection", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, SavedTorsoDirection), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_SavedTorsoDirection_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_SavedTorsoDirection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_bResetTorso_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_bResetTorso_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->bResetTorso = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_bResetTorso = { "bResetTorso", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_bResetTorso_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bResetTorso_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bResetTorso_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_bYawInterpToHead_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_bYawInterpToHead_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->bYawInterpToHead = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_bYawInterpToHead = { "bYawInterpToHead", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_bYawInterpToHead_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bYawInterpToHead_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bYawInterpToHead_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_nYawControlCounter_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Reset ribcage yaw rotation after timeout" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_nYawControlCounter = { "nYawControlCounter", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, nYawControlCounter), nullptr, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_nYawControlCounter_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_nYawControlCounter_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_bResetFeet_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_bResetFeet_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->bResetFeet = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_bResetFeet = { "bResetFeet", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_bResetFeet_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bResetFeet_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bResetFeet_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_fDeltaTimeR_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_fDeltaTimeR = { "fDeltaTimeR", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, fDeltaTimeR), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_fDeltaTimeR_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_fDeltaTimeR_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_fDeltaTimeL_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_fDeltaTimeL = { "fDeltaTimeL", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, fDeltaTimeL), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_fDeltaTimeL_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_fDeltaTimeL_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_FeetMovingStartedTime_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_FeetMovingStartedTime = { "FeetMovingStartedTime", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, FeetMovingStartedTime), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_FeetMovingStartedTime_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_FeetMovingStartedTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_FeetCyclePhase_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_FeetCyclePhase = { "FeetCyclePhase", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, FeetCyclePhase), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_FeetCyclePhase_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_FeetCyclePhase_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootLastTransformR_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootLastTransformR = { "FootLastTransformR", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, FootLastTransformR), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootLastTransformR_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootLastTransformR_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootLastTransformL_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootLastTransformL = { "FootLastTransformL", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, FootLastTransformL), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootLastTransformL_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootLastTransformL_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootTickTransformR_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootTickTransformR = { "FootTickTransformR", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, FootTickTransformR), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootTickTransformR_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootTickTransformR_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootTickTransformL_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootTickTransformL = { "FootTickTransformL", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, FootTickTransformL), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootTickTransformL_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootTickTransformL_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootTargetTransformR_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootTargetTransformR = { "FootTargetTransformR", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, FootTargetTransformR), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootTargetTransformR_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootTargetTransformR_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootTargetTransformL_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Feet animation" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootTargetTransformL = { "FootTargetTransformL", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, FootTargetTransformL), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootTargetTransformL_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootTargetTransformL_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_hTorsoYawTimer_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_hTorsoYawTimer = { "hTorsoYawTimer", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, hTorsoYawTimer), Z_Construct_UScriptStruct_FTimerHandle, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_hTorsoYawTimer_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_hTorsoYawTimer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_hResetFootRTimer_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_hResetFootRTimer = { "hResetFootRTimer", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, hResetFootRTimer), Z_Construct_UScriptStruct_FTimerHandle, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_hResetFootRTimer_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_hResetFootRTimer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_hResetFootLTimer_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_hResetFootLTimer = { "hResetFootLTimer", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, hResetFootLTimer), Z_Construct_UScriptStruct_FTimerHandle, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_hResetFootLTimer_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_hResetFootLTimer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_hVRInputTimer_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Timers" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_hVRInputTimer = { "hVRInputTimer", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, hVRInputTimer), Z_Construct_UScriptStruct_FTimerHandle, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_hVRInputTimer_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_hVRInputTimer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachSocketLeft_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachSocketLeft = { "HandAttachSocketLeft", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, HandAttachSocketLeft), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachSocketLeft_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachSocketLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachSocketRight_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachSocketRight = { "HandAttachSocketRight", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, HandAttachSocketRight), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachSocketRight_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachSocketRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachTransformLeft_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachTransformLeft = { "HandAttachTransformLeft", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, HandAttachTransformLeft), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachTransformLeft_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachTransformLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachTransformRight_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachTransformRight = { "HandAttachTransformRight", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, HandAttachTransformRight), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachTransformRight_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachTransformRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandParentLeft_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandParentLeft = { "HandParentLeft", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, HandParentLeft), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandParentLeft_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandParentLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandParentRight_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandParentRight = { "HandParentRight", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, HandParentRight), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandParentRight_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandParentRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachedLeft_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachedLeft_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->HandAttachedLeft = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachedLeft = { "HandAttachedLeft", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachedLeft_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachedLeft_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachedLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachedRight_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Hands Attachment\nIn some interactions hands need to be attached to scene components instead of motion controllers" },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachedRight_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->HandAttachedRight = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachedRight = { "HandAttachedRight", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachedRight_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachedRight_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachedRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_rPrevCamRotator_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_rPrevCamRotator = { "rPrevCamRotator", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, rPrevCamRotator), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_rPrevCamRotator_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_rPrevCamRotator_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_vPrevCamLocation_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Previous headset transform" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_vPrevCamLocation = { "vPrevCamLocation", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, vPrevCamLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_vPrevCamLocation_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_vPrevCamLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_InputHandLeft_Target_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_InputHandLeft_Target = { "InputHandLeft_Target", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, InputHandLeft_Target), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_InputHandLeft_Target_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_InputHandLeft_Target_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_InputHandRight_Target_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_InputHandRight_Target = { "InputHandRight_Target", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, InputHandRight_Target), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_InputHandRight_Target_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_InputHandRight_Target_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_InputHMD_Target_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_InputHMD_Target = { "InputHMD_Target", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, InputHMD_Target), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_InputHMD_Target_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_InputHMD_Target_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_SkeletonTransformData_Target_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Target (not current) replicated transforms. Current body state is interpolated to target." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_SkeletonTransformData_Target = { "SkeletonTransformData_Target", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, SkeletonTransformData_Target), Z_Construct_UScriptStruct_FIKBodyData, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_SkeletonTransformData_Target_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_SkeletonTransformData_Target_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_InputHandLeft_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_InputHandLeft = { "InputHandLeft", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, InputHandLeft), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_InputHandLeft_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_InputHandLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_InputHandRight_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_InputHandRight = { "InputHandRight", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, InputHandRight), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_InputHandRight_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_InputHandRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_InputHMD_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Current Input for manual input from variables" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_InputHMD = { "InputHMD", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, InputHMD), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_InputHMD_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_InputHMD_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_SkeletonTransformDataRelative_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_SkeletonTransformDataRelative = { "SkeletonTransformDataRelative", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, SkeletonTransformDataRelative), Z_Construct_UScriptStruct_FIKBodyData, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_SkeletonTransformDataRelative_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_SkeletonTransformDataRelative_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_SkeletonTransformData_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Calculation result" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_SkeletonTransformData = { "SkeletonTransformData", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, SkeletonTransformData), Z_Construct_UScriptStruct_FIKBodyData, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_SkeletonTransformData_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_SkeletonTransformData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_NT_InputHandLeft_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_NT_InputHandLeft = { "NT_InputHandLeft", "OnRep_InputHandLeft", (EPropertyFlags)0x0040000100000020, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, NT_InputHandLeft), Z_Construct_UScriptStruct_FNetworkTransform, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_NT_InputHandLeft_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_NT_InputHandLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_NT_InputHandRight_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_NT_InputHandRight = { "NT_InputHandRight", "OnRep_InputHandRight", (EPropertyFlags)0x0040000100000020, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, NT_InputHandRight), Z_Construct_UScriptStruct_FNetworkTransform, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_NT_InputHandRight_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_NT_InputHandRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_NT_InputHMD_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_NT_InputHMD = { "NT_InputHMD", "OnRep_InputHMD", (EPropertyFlags)0x0040000100000020, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, NT_InputHMD), Z_Construct_UScriptStruct_FNetworkTransform, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_NT_InputHMD_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_NT_InputHMD_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_NT_SkeletonTransformData_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Replicated body state and VR Input" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_NT_SkeletonTransformData = { "NT_SkeletonTransformData", "OnRep_InputBodyState", (EPropertyFlags)0x0040000100000020, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, NT_SkeletonTransformData), Z_Construct_UScriptStruct_FNetworkIKBodyData, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_NT_SkeletonTransformData_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_NT_SkeletonTransformData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_bManualBodyOrientation_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_bManualBodyOrientation_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->bManualBodyOrientation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_bManualBodyOrientation = { "bManualBodyOrientation", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_bManualBodyOrientation_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bManualBodyOrientation_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bManualBodyOrientation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_bDebugOutput_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_bDebugOutput_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->bDebugOutput = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_bDebugOutput = { "bDebugOutput", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_bDebugOutput_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bDebugOutput_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bDebugOutput_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_bIsInitialized_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Global State" },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_bIsInitialized_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->bIsInitialized = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_bIsInitialized = { "bIsInitialized", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_bIsInitialized_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bIsInitialized_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bIsInitialized_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_FloorBaseComp_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_FloorBaseComp = { "FloorBaseComp", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, FloorBaseComp), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_FloorBaseComp_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_FloorBaseComp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_OwningPawn_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Pointer to player's pawn" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_OwningPawn = { "OwningPawn", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, OwningPawn), Z_Construct_UClass_APawn_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_OwningPawn_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_OwningPawn_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_LeftHandController_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Motion Controller component pointer for left hand" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_LeftHandController = { "LeftHandController", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, LeftHandController), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_LeftHandController_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_LeftHandController_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_RightHandController_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Motion Controller component pointer for right hand" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_RightHandController = { "RightHandController", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, RightHandController), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_RightHandController_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_RightHandController_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_CameraComponent_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "HMD camera component pointer" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_CameraComponent = { "CameraComponent", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, CameraComponent), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_CameraComponent_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_CameraComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnHeadNod_MetaData[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Not in use!" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnHeadNod = { "OnHeadNod", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::MulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, OnHeadNod), Z_Construct_UDelegateFunction_VRIKBodyRuntime_IKBodyRepeatable__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnHeadNod_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnHeadNod_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnHeadShake_MetaData[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Not in use!" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnHeadShake = { "OnHeadShake", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::MulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, OnHeadShake), Z_Construct_UDelegateFunction_VRIKBodyRuntime_IKBodyRepeatable__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnHeadShake_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnHeadShake_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnStandUp_MetaData[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Event called when player stands up, NOT IN USE" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnStandUp = { "OnStandUp", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::MulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, OnStandUp), Z_Construct_UDelegateFunction_VRIKBodyRuntime_IKBodySimple__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnStandUp_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnStandUp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnSitDown_MetaData[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Event called when player finishing squatting, NOT IN USE" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnSitDown = { "OnSitDown", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::MulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, OnSitDown), Z_Construct_UDelegateFunction_VRIKBodyRuntime_IKBodySimple__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnSitDown_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnSitDown_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnGrounded_MetaData[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Event called when player ends a jump, NOT IN USE" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnGrounded = { "OnGrounded", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::MulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, OnGrounded), Z_Construct_UDelegateFunction_VRIKBodyRuntime_IKBodySimple__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnGrounded_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnGrounded_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnJumpStarted_MetaData[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Event called when player starts a jump, NOT IN USE" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnJumpStarted = { "OnJumpStarted", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::MulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, OnJumpStarted), Z_Construct_UDelegateFunction_VRIKBodyRuntime_IKBodySimple__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnJumpStarted_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnJumpStarted_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnCalibrationComplete_MetaData[] = {
		{ "Category", "IKBody" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Body Calibration Complete successfully" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnCalibrationComplete = { "OnCalibrationComplete", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::MulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, OnCalibrationComplete), Z_Construct_UDelegateFunction_VRIKBodyRuntime_IKBodySimple__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnCalibrationComplete_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnCalibrationComplete_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_SmoothingInterpolationSpeed_MetaData[] = {
		{ "Category", "IKBody | Networking" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Interpolation speed for bones transforms received via replication." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_SmoothingInterpolationSpeed = { "SmoothingInterpolationSpeed", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, SmoothingInterpolationSpeed), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_SmoothingInterpolationSpeed_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_SmoothingInterpolationSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_ReplicateInWorldSpace_MetaData[] = {
		{ "Category", "IKBody | Networking" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "If ReplicateInWorldSpace is set to false (by default), all body data would be converted to actor space\n      before replication and reconverted to world space on remote machines. This operation adds a lot of transforms\n      calculations, but allow to use sliding (Onward-style) locomotion which implies that pawn locations\n      on different PCs are slightly asynchronous at the same moment. Set ReplicateInWorldSpace to true if\n      you don't use sliding player locomotion to optimize CPU usage." },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_ReplicateInWorldSpace_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->ReplicateInWorldSpace = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_ReplicateInWorldSpace = { "ReplicateInWorldSpace", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_ReplicateInWorldSpace_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_ReplicateInWorldSpace_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_ReplicateInWorldSpace_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_ReplicateComponentsMovement_MetaData[] = {
		{ "Category", "IKBody | Networking" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Only available if VR Input Option is 'Input from components' or components are initialized by Initialize(...)\n      function. This flag orders component to move remote Motion Controllers and Head component with a body." },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_ReplicateComponentsMovement_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->ReplicateComponentsMovement = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_ReplicateComponentsMovement = { "ReplicateComponentsMovement", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_ReplicateComponentsMovement_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_ReplicateComponentsMovement_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_ReplicateComponentsMovement_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_ReplicateFullBodyState_MetaData[] = {
		{ "Category", "IKBody | Networking" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "If true, the component calculates body state on local PC and send it to server for other connected users.\n      If false, component sends Head and Hands transforms to server, and every client perform body calculation\n      for each player locally. Choose first option (true) if CPU performance is a priority in your project. Choose remote\n      calculations (false) to optimize a project for network bandwidth." },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_ReplicateFullBodyState_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->ReplicateFullBodyState = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_ReplicateFullBodyState = { "ReplicateFullBodyState", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_ReplicateFullBodyState_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_ReplicateFullBodyState_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_ReplicateFullBodyState_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_SoftHeadRotationLimit_MetaData[] = {
		{ "Category", "IKBody | Body Params" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Angle between head and ribcage Yaw rotations to  trigger ribcage rotation (torso twist, EnableTorsoTwist must be enabled); 0 to disable" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_SoftHeadRotationLimit = { "SoftHeadRotationLimit", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, SoftHeadRotationLimit), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_SoftHeadRotationLimit_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_SoftHeadRotationLimit_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_MaxHeadRotation_MetaData[] = {
		{ "Category", "IKBody | Body Params" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Max possible angle between head and ribcage Yaw rotations (used to correct pelvis rotation)" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_MaxHeadRotation = { "MaxHeadRotation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, MaxHeadRotation), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_MaxHeadRotation_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_MaxHeadRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_RibcageToNeckOffset_MetaData[] = {
		{ "Category", "IKBody | Body Params" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Component Space Offset from Ribcage to Neck (X is forward, Z is up)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_RibcageToNeckOffset = { "RibcageToNeckOffset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, RibcageToNeckOffset), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_RibcageToNeckOffset_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_RibcageToNeckOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_NeckToHeadsetOffset_MetaData[] = {
		{ "Category", "IKBody | Body Params" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Don't modify this param if using calibration (X is forward, Z is up)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_NeckToHeadsetOffset = { "NeckToHeadsetOffset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, NeckToHeadsetOffset), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_NeckToHeadsetOffset_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_NeckToHeadsetOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootOffsetToGround_MetaData[] = {
		{ "Category", "IKBody | Body Params" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Distance from feet to ground, useful to correct skeletal mesh feet bones Z-offset" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootOffsetToGround = { "FootOffsetToGround", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, FootOffsetToGround), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootOffsetToGround_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootOffsetToGround_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandLength_MetaData[] = {
		{ "Category", "IKBody | Body Params" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Approximate distance from collarbone to palm" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandLength = { "HandLength", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, HandLength), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandLength_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_SpineLength_MetaData[] = {
		{ "Category", "IKBody | Body Params" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Approximate distance from pelvis to neck" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_SpineLength = { "SpineLength", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, SpineLength), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_SpineLength_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_SpineLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_HeadHeight_MetaData[] = {
		{ "Category", "IKBody | Body Params" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Approximate head height" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_HeadHeight = { "HeadHeight", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, HeadHeight), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_HeadHeight_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_HeadHeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_HeadHalfWidth_MetaData[] = {
		{ "Category", "IKBody | Body Params" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Approximate head radius" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_HeadHalfWidth = { "HeadHalfWidth", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, HeadHalfWidth), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_HeadHalfWidth_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_HeadHalfWidth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_BodyThickness_MetaData[] = {
		{ "Category", "IKBody | Body Params" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Relative X-axis body size" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_BodyThickness = { "BodyThickness", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, BodyThickness), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_BodyThickness_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_BodyThickness_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_BodyWidth_MetaData[] = {
		{ "Category", "IKBody | Body Params" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Relative Y-axis body size" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_BodyWidth = { "BodyWidth", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, BodyWidth), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_BodyWidth_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_BodyWidth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_bDoManualBodyBending_MetaData[] = {
		{ "Category", "IKBody | Setup" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Force component to blend player's spine when sitting/bending" },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_bDoManualBodyBending_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->bDoManualBodyBending = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_bDoManualBodyBending = { "bDoManualBodyBending", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_bDoManualBodyBending_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bDoManualBodyBending_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bDoManualBodyBending_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_TorsoResetToHeadInterval_MetaData[] = {
		{ "Category", "IKBody | Setup" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Timer interval to reset torso rotation to head" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_TorsoResetToHeadInterval = { "TorsoResetToHeadInterval", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, TorsoResetToHeadInterval), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_TorsoResetToHeadInterval_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_TorsoResetToHeadInterval_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_bDoPawnOrientationAdjustment_MetaData[] = {
		{ "Category", "IKBody | Setup" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Enable this flag if your pawn can has pitch and roll rotaion" },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_bDoPawnOrientationAdjustment_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->bDoPawnOrientationAdjustment = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_bDoPawnOrientationAdjustment = { "bDoPawnOrientationAdjustment", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_bDoPawnOrientationAdjustment_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bDoPawnOrientationAdjustment_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bDoPawnOrientationAdjustment_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_TimerInterval_MetaData[] = {
		{ "Category", "IKBody | Setup" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Tick Interval of the timer, calculating correlation betwen hands and head" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_TimerInterval = { "TimerInterval", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, TimerInterval), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_TimerInterval_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_TimerInterval_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_RightPalmOffset_MetaData[] = {
		{ "Category", "IKBody | Setup" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Right palm location relative to Motion Controller Component transform. Default value works fine if you hold HTC Vive controller\n      as a pistol. For other cases reset this transform to zero." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_RightPalmOffset = { "RightPalmOffset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, RightPalmOffset), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_RightPalmOffset_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_RightPalmOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_LeftPalmOffset_MetaData[] = {
		{ "Category", "IKBody | Setup" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Left palm location relative to Motion Controller Component transform. Default value works fine if you hold HTC Vive controller\n      as a pistol. For other cases reset this transform to zero." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_LeftPalmOffset = { "LeftPalmOffset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, LeftPalmOffset), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_LeftPalmOffset_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_LeftPalmOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_bFollowHeadRotationWhenSitting_MetaData[] = {
		{ "Category", "IKBody | Setup" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Always rotate torso with head when actor is sitting" },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_bFollowHeadRotationWhenSitting_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->bFollowHeadRotationWhenSitting = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_bFollowHeadRotationWhenSitting = { "bFollowHeadRotationWhenSitting", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_bFollowHeadRotationWhenSitting_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bFollowHeadRotationWhenSitting_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bFollowHeadRotationWhenSitting_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_bLockTorsoToHead_MetaData[] = {
		{ "Category", "IKBody | Setup" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Check for hard attachment of torso rotation to head rotation" },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_bLockTorsoToHead_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->bLockTorsoToHead = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_bLockTorsoToHead = { "bLockTorsoToHead", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_bLockTorsoToHead_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bLockTorsoToHead_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_bLockTorsoToHead_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_TorsoRotationSensitivity_MetaData[] = {
		{ "Category", "IKBody | Setup" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "How strongly should torso follow head rotation?" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_TorsoRotationSensitivity = { "TorsoRotationSensitivity", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, TorsoRotationSensitivity), METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_TorsoRotationSensitivity_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_TorsoRotationSensitivity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_EnableTorsoTwist_MetaData[] = {
		{ "Category", "IKBody | Setup" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Enable twisting to decrease errors in torso orientation by rotiting ribcage. False value locks ribcage and pelvis rotations." },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_EnableTorsoTwist_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->EnableTorsoTwist = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_EnableTorsoTwist = { "EnableTorsoTwist", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_EnableTorsoTwist_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_EnableTorsoTwist_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_EnableTorsoTwist_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_DetectContinuousHeadYawRotation_MetaData[] = {
		{ "Category", "IKBody | Setup" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "If true, VRIKBody will try to detect continuous head rotations. Otherwise hip is aligned to Head Yaw by timeout" },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_DetectContinuousHeadYawRotation_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->DetectContinuousHeadYawRotation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_DetectContinuousHeadYawRotation = { "DetectContinuousHeadYawRotation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_DetectContinuousHeadYawRotation_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_DetectContinuousHeadYawRotation_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_DetectContinuousHeadYawRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_FollowPawnTransform_MetaData[] = {
		{ "Category", "IKBody | Setup" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "This flag only works if VRInputOption is DirectVRInput. If true, component uses Pawn Actor Transform to locate body\n      in world space. Otherwise it retuns body relative to Pawn Origin. World space calculation is required for unnatural locomotion." },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_FollowPawnTransform_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->FollowPawnTransform = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_FollowPawnTransform = { "FollowPawnTransform", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_FollowPawnTransform_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_FollowPawnTransform_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_FollowPawnTransform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_LockShouldersRotation_MetaData[] = {
		{ "Category", "IKBody | Setup" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "If true, shoulders don't rotate to follow hands" },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_LockShouldersRotation_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->LockShouldersRotation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_LockShouldersRotation = { "LockShouldersRotation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_LockShouldersRotation_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_LockShouldersRotation_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_LockShouldersRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_VRInputOption_MetaData[] = {
		{ "Category", "IKBody | Setup" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "If VR Input Option is equal to 'Input from Components', call Initialize(...) function to setup Camera and Hand components.\n      Don't use 'Input from Variables' if component is replicated." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_VRInputOption = { "VRInputOption", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, VRInputOption), Z_Construct_UEnum_VRIKBodyRuntime_EVRInputSetup, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_VRInputOption_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_VRInputOption_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_VRInputOption_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_FloorCollisionObjectType_MetaData[] = {
		{ "Category", "IKBody | Setup" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Collision channel (Object Type) of floor if UseActorLocationAsFloor is false" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_FloorCollisionObjectType = { "FloorCollisionObjectType", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKBody, FloorCollisionObjectType), Z_Construct_UEnum_Engine_ECollisionChannel, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_FloorCollisionObjectType_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_FloorCollisionObjectType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_TraceFloorByObjectType_MetaData[] = {
		{ "Category", "IKBody | Setup" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "If true, component is tracing groun by Object Type, if false - by channel" },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_TraceFloorByObjectType_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->TraceFloorByObjectType = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_TraceFloorByObjectType = { "TraceFloorByObjectType", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_TraceFloorByObjectType_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_TraceFloorByObjectType_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_TraceFloorByObjectType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_UseActorLocationAsFloor_MetaData[] = {
		{ "Category", "IKBody | Setup" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "If true, Owning Pawn location Z will be used as floor coordinate; doesn't work properly on slopes and staircases" },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_UseActorLocationAsFloor_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->UseActorLocationAsFloor = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_UseActorLocationAsFloor = { "UseActorLocationAsFloor", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_UseActorLocationAsFloor_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_UseActorLocationAsFloor_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_UseActorLocationAsFloor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_ComputeHandsIK_MetaData[] = {
		{ "Category", "IKBody | Setup" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Set this flag to True if you need upperarm and forearm transforms" },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_ComputeHandsIK_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->ComputeHandsIK = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_ComputeHandsIK = { "ComputeHandsIK", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_ComputeHandsIK_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_ComputeHandsIK_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_ComputeHandsIK_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_ComputeLegsIK_MetaData[] = {
		{ "Category", "IKBody | Setup" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Set this flag to True if you need thigh and calf transforms. Only works if ComputeFeetIKTargets is true." },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_ComputeLegsIK_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->ComputeLegsIK = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_ComputeLegsIK = { "ComputeLegsIK", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_ComputeLegsIK_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_ComputeLegsIK_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_ComputeLegsIK_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKBody_Statics::NewProp_ComputeFeetIKTargets_MetaData[] = {
		{ "Category", "IKBody | Setup" },
		{ "ModuleRelativePath", "Public/VRIKBody.h" },
		{ "ToolTip", "Set this flag to True if you need Feet Transform Predictions" },
	};
#endif
	void Z_Construct_UClass_UVRIKBody_Statics::NewProp_ComputeFeetIKTargets_SetBit(void* Obj)
	{
		((UVRIKBody*)Obj)->ComputeFeetIKTargets = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKBody_Statics::NewProp_ComputeFeetIKTargets = { "ComputeFeetIKTargets", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKBody), &Z_Construct_UClass_UVRIKBody_Statics::NewProp_ComputeFeetIKTargets_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::NewProp_ComputeFeetIKTargets_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::NewProp_ComputeFeetIKTargets_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UVRIKBody_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_PreviousElbowRollLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_PreviousElbowRollRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_CurrentVRInputIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_PrevFrameActorRot,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_ShoulderPitchOffsetLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_ShoulderPitchOffsetRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_ShoulderYawOffsetLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_ShoulderYawOffsetRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_RecalcCharacteHeightTimeJumping,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_RecalcCharacteHeightTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_HeadLocationBeforeSitting,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_HeadPitchBeforeSitting,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_TracedFloorLevelL,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_TracedFloorLevelR,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_TracedFloorLevel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_bCalibratedI,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_bCalibratedT,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_CalI_ControllerR,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_CalI_ControllerL,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_CalI_Head,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_CalT_ControllerR,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_CalT_ControllerL,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_CalT_Head,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_ThighLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_UpperarmLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_StartRotationYaw,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_nIsRotating,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_LegsLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_ArmSpanClear,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_CharacterHeightClear,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_CharacterHeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_JumpingOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_bIsWalking,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_bIsStanding,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_bIsSitting,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_bTorsoRollRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_bTorsoPitchRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_bTorsoYawRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_ModifyHeightStartTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_nModifyHeightState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_ResetFootLocationR,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_ResetFootLocationL,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_SavedTorsoDirection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_bResetTorso,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_bYawInterpToHead,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_nYawControlCounter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_bResetFeet,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_fDeltaTimeR,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_fDeltaTimeL,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_FeetMovingStartedTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_FeetCyclePhase,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootLastTransformR,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootLastTransformL,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootTickTransformR,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootTickTransformL,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootTargetTransformR,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootTargetTransformL,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_hTorsoYawTimer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_hResetFootRTimer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_hResetFootLTimer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_hVRInputTimer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachSocketLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachSocketRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachTransformLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachTransformRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandParentLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandParentRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachedLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandAttachedRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_rPrevCamRotator,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_vPrevCamLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_InputHandLeft_Target,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_InputHandRight_Target,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_InputHMD_Target,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_SkeletonTransformData_Target,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_InputHandLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_InputHandRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_InputHMD,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_SkeletonTransformDataRelative,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_SkeletonTransformData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_NT_InputHandLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_NT_InputHandRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_NT_InputHMD,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_NT_SkeletonTransformData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_bManualBodyOrientation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_bDebugOutput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_bIsInitialized,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_FloorBaseComp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_OwningPawn,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_LeftHandController,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_RightHandController,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_CameraComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnHeadNod,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnHeadShake,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnStandUp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnSitDown,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnGrounded,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnJumpStarted,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_OnCalibrationComplete,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_SmoothingInterpolationSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_ReplicateInWorldSpace,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_ReplicateComponentsMovement,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_ReplicateFullBodyState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_SoftHeadRotationLimit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_MaxHeadRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_RibcageToNeckOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_NeckToHeadsetOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_FootOffsetToGround,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_HandLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_SpineLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_HeadHeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_HeadHalfWidth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_BodyThickness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_BodyWidth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_bDoManualBodyBending,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_TorsoResetToHeadInterval,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_bDoPawnOrientationAdjustment,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_TimerInterval,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_RightPalmOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_LeftPalmOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_bFollowHeadRotationWhenSitting,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_bLockTorsoToHead,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_TorsoRotationSensitivity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_EnableTorsoTwist,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_DetectContinuousHeadYawRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_FollowPawnTransform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_LockShouldersRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_VRInputOption,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_VRInputOption_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_FloorCollisionObjectType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_TraceFloorByObjectType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_UseActorLocationAsFloor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_ComputeHandsIK,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_ComputeLegsIK,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKBody_Statics::NewProp_ComputeFeetIKTargets,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVRIKBody_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVRIKBody>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVRIKBody_Statics::ClassParams = {
		&UVRIKBody::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UVRIKBody_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UVRIKBody_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UVRIKBody_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVRIKBody()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVRIKBody_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVRIKBody, 896534723);
	template<> VRIKBODYRUNTIME_API UClass* StaticClass<UVRIKBody>()
	{
		return UVRIKBody::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVRIKBody(Z_Construct_UClass_UVRIKBody, &UVRIKBody::StaticClass, TEXT("/Script/VRIKBodyRuntime"), TEXT("UVRIKBody"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVRIKBody);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
