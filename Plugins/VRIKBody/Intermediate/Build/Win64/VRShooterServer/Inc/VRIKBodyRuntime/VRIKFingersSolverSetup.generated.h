// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VRIKBODYRUNTIME_VRIKFingersSolverSetup_generated_h
#error "VRIKFingersSolverSetup.generated.h already included, missing '#pragma once' in VRIKFingersSolverSetup.h"
#endif
#define VRIKBODYRUNTIME_VRIKFingersSolverSetup_generated_h

#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersSolverSetup_h_431_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics; \
	static class UScriptStruct* StaticStruct();


template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<struct FVRIK_FingersPosePreset>();

#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersSolverSetup_h_417_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVRIK_TwistData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<struct FVRIK_TwistData>();

#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersSolverSetup_h_402_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVRIK_FingerKnucklePointer_Statics; \
	static class UScriptStruct* StaticStruct();


template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<struct FVRIK_FingerKnucklePointer>();

#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersSolverSetup_h_350_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics; \
	static class UScriptStruct* StaticStruct();


template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<struct FVRIK_FingerSolver>();

#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersSolverSetup_h_305_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics; \
	static class UScriptStruct* StaticStruct();


template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<struct FVRIK_Knuckle>();

#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersSolverSetup_h_70_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVRIK_FingerRotation_Statics; \
	static class UScriptStruct* StaticStruct();


template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<struct FVRIK_FingerRotation>();

#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersSolverSetup_h_460_RPC_WRAPPERS
#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersSolverSetup_h_460_RPC_WRAPPERS_NO_PURE_DECLS
#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersSolverSetup_h_460_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVRIKFingersSolverSetup(); \
	friend struct Z_Construct_UClass_UVRIKFingersSolverSetup_Statics; \
public: \
	DECLARE_CLASS(UVRIKFingersSolverSetup, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VRIKBodyRuntime"), NO_API) \
	DECLARE_SERIALIZER(UVRIKFingersSolverSetup)


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersSolverSetup_h_460_INCLASS \
private: \
	static void StaticRegisterNativesUVRIKFingersSolverSetup(); \
	friend struct Z_Construct_UClass_UVRIKFingersSolverSetup_Statics; \
public: \
	DECLARE_CLASS(UVRIKFingersSolverSetup, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VRIKBodyRuntime"), NO_API) \
	DECLARE_SERIALIZER(UVRIKFingersSolverSetup)


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersSolverSetup_h_460_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVRIKFingersSolverSetup(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVRIKFingersSolverSetup) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVRIKFingersSolverSetup); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVRIKFingersSolverSetup); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVRIKFingersSolverSetup(UVRIKFingersSolverSetup&&); \
	NO_API UVRIKFingersSolverSetup(const UVRIKFingersSolverSetup&); \
public:


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersSolverSetup_h_460_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVRIKFingersSolverSetup(UVRIKFingersSolverSetup&&); \
	NO_API UVRIKFingersSolverSetup(const UVRIKFingersSolverSetup&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVRIKFingersSolverSetup); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVRIKFingersSolverSetup); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVRIKFingersSolverSetup)


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersSolverSetup_h_460_PRIVATE_PROPERTY_OFFSET
#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersSolverSetup_h_457_PROLOG
#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersSolverSetup_h_460_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersSolverSetup_h_460_PRIVATE_PROPERTY_OFFSET \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersSolverSetup_h_460_RPC_WRAPPERS \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersSolverSetup_h_460_INCLASS \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersSolverSetup_h_460_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersSolverSetup_h_460_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersSolverSetup_h_460_PRIVATE_PROPERTY_OFFSET \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersSolverSetup_h_460_RPC_WRAPPERS_NO_PURE_DECLS \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersSolverSetup_h_460_INCLASS_NO_PURE_DECLS \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersSolverSetup_h_460_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VRIKBODYRUNTIME_API UClass* StaticClass<class UVRIKFingersSolverSetup>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersSolverSetup_h


#define FOREACH_ENUM_EVRIKFINGERNAME(op) \
	op(EVRIKFingerName::FN_Thumb) \
	op(EVRIKFingerName::FN_Index) \
	op(EVRIKFingerName::FN_Middle) \
	op(EVRIKFingerName::FN_Ring) \
	op(EVRIKFingerName::FN_Pinky) \
	op(EVRIKFingerName::EFingerName_MAX) 

enum class EVRIKFingerName : uint8;
template<> VRIKBODYRUNTIME_API UEnum* StaticEnum<EVRIKFingerName>();

#define FOREACH_ENUM_EVRIK_BONEORIENTATIONAXIS(op) \
	op(EVRIK_BoneOrientationAxis::X) \
	op(EVRIK_BoneOrientationAxis::Y) \
	op(EVRIK_BoneOrientationAxis::Z) \
	op(EVRIK_BoneOrientationAxis::X_Neg) \
	op(EVRIK_BoneOrientationAxis::Y_Neg) \
	op(EVRIK_BoneOrientationAxis::Z_Neg) \
	op(EVRIK_BoneOrientationAxis::BOA_MAX) 

enum class EVRIK_BoneOrientationAxis : uint8;
template<> VRIKBODYRUNTIME_API UEnum* StaticEnum<EVRIK_BoneOrientationAxis>();

#define FOREACH_ENUM_EVRIK_HANDSTYLE(op) \
	op(EVRIK_HandStyle::HS_FullArm) \
	op(EVRIK_HandStyle::HS_Lowerarm) \
	op(EVRIK_HandStyle::HS_HandOnly) 

enum class EVRIK_HandStyle : uint8;
template<> VRIKBODYRUNTIME_API UEnum* StaticEnum<EVRIK_HandStyle>();

#define FOREACH_ENUM_EVRIK_VRHAND(op) \
	op(EVRIK_VRHand::VRH_Left) \
	op(EVRIK_VRHand::VRH_Right) 

enum class EVRIK_VRHand : uint8;
template<> VRIKBODYRUNTIME_API UEnum* StaticEnum<EVRIK_VRHand>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
