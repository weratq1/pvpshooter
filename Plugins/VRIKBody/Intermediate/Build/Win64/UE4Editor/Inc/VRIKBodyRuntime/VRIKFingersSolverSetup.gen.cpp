// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VRIKBodyRuntime/Public/VRIKFingersSolverSetup.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVRIKFingersSolverSetup() {}
// Cross Module References
	VRIKBODYRUNTIME_API UEnum* Z_Construct_UEnum_VRIKBodyRuntime_EVRIKFingerName();
	UPackage* Z_Construct_UPackage__Script_VRIKBodyRuntime();
	VRIKBODYRUNTIME_API UEnum* Z_Construct_UEnum_VRIKBodyRuntime_EVRIK_BoneOrientationAxis();
	VRIKBODYRUNTIME_API UEnum* Z_Construct_UEnum_VRIKBodyRuntime_EVRIK_HandStyle();
	VRIKBODYRUNTIME_API UEnum* Z_Construct_UEnum_VRIKBodyRuntime_EVRIK_VRHand();
	VRIKBODYRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FVRIK_FingersPosePreset();
	VRIKBODYRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FVRIK_FingerRotation();
	VRIKBODYRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FVRIK_TwistData();
	VRIKBODYRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FVRIK_FingerKnucklePointer();
	VRIKBODYRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FVRIK_FingerSolver();
	VRIKBODYRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FVRIK_Knuckle();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	VRIKBODYRUNTIME_API UClass* Z_Construct_UClass_UVRIKFingersSolverSetup_NoRegister();
	VRIKBODYRUNTIME_API UClass* Z_Construct_UClass_UVRIKFingersSolverSetup();
	ENGINE_API UClass* Z_Construct_UClass_UDataAsset();
	ENGINE_API UEnum* Z_Construct_UEnum_Engine_ECollisionChannel();
// End Cross Module References
	static UEnum* EVRIKFingerName_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_VRIKBodyRuntime_EVRIKFingerName, Z_Construct_UPackage__Script_VRIKBodyRuntime(), TEXT("EVRIKFingerName"));
		}
		return Singleton;
	}
	template<> VRIKBODYRUNTIME_API UEnum* StaticEnum<EVRIKFingerName>()
	{
		return EVRIKFingerName_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EVRIKFingerName(EVRIKFingerName_StaticEnum, TEXT("/Script/VRIKBodyRuntime"), TEXT("EVRIKFingerName"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_VRIKBodyRuntime_EVRIKFingerName_Hash() { return 3871839842U; }
	UEnum* Z_Construct_UEnum_VRIKBodyRuntime_EVRIKFingerName()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_VRIKBodyRuntime();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EVRIKFingerName"), 0, Get_Z_Construct_UEnum_VRIKBodyRuntime_EVRIKFingerName_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EVRIKFingerName::FN_Thumb", (int64)EVRIKFingerName::FN_Thumb },
				{ "EVRIKFingerName::FN_Index", (int64)EVRIKFingerName::FN_Index },
				{ "EVRIKFingerName::FN_Middle", (int64)EVRIKFingerName::FN_Middle },
				{ "EVRIKFingerName::FN_Ring", (int64)EVRIKFingerName::FN_Ring },
				{ "EVRIKFingerName::FN_Pinky", (int64)EVRIKFingerName::FN_Pinky },
				{ "EVRIKFingerName::EFingerName_MAX", (int64)EVRIKFingerName::EFingerName_MAX },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "EFingerName_MAX.Hidden", "" },
				{ "FN_Index.DisplayName", "Index Finger" },
				{ "FN_Middle.DisplayName", "Middle Finger" },
				{ "FN_Pinky.DisplayName", "Pinky Finger" },
				{ "FN_Ring.DisplayName", "Ring Finger" },
				{ "FN_Thumb.DisplayName", "Thumb Finger" },
				{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
				{ "ToolTip", "Humanoid fingers" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_VRIKBodyRuntime,
				nullptr,
				"EVRIKFingerName",
				"EVRIKFingerName",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EVRIK_BoneOrientationAxis_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_VRIKBodyRuntime_EVRIK_BoneOrientationAxis, Z_Construct_UPackage__Script_VRIKBodyRuntime(), TEXT("EVRIK_BoneOrientationAxis"));
		}
		return Singleton;
	}
	template<> VRIKBODYRUNTIME_API UEnum* StaticEnum<EVRIK_BoneOrientationAxis>()
	{
		return EVRIK_BoneOrientationAxis_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EVRIK_BoneOrientationAxis(EVRIK_BoneOrientationAxis_StaticEnum, TEXT("/Script/VRIKBodyRuntime"), TEXT("EVRIK_BoneOrientationAxis"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_VRIKBodyRuntime_EVRIK_BoneOrientationAxis_Hash() { return 2571759281U; }
	UEnum* Z_Construct_UEnum_VRIKBodyRuntime_EVRIK_BoneOrientationAxis()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_VRIKBodyRuntime();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EVRIK_BoneOrientationAxis"), 0, Get_Z_Construct_UEnum_VRIKBodyRuntime_EVRIK_BoneOrientationAxis_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EVRIK_BoneOrientationAxis::X", (int64)EVRIK_BoneOrientationAxis::X },
				{ "EVRIK_BoneOrientationAxis::Y", (int64)EVRIK_BoneOrientationAxis::Y },
				{ "EVRIK_BoneOrientationAxis::Z", (int64)EVRIK_BoneOrientationAxis::Z },
				{ "EVRIK_BoneOrientationAxis::X_Neg", (int64)EVRIK_BoneOrientationAxis::X_Neg },
				{ "EVRIK_BoneOrientationAxis::Y_Neg", (int64)EVRIK_BoneOrientationAxis::Y_Neg },
				{ "EVRIK_BoneOrientationAxis::Z_Neg", (int64)EVRIK_BoneOrientationAxis::Z_Neg },
				{ "EVRIK_BoneOrientationAxis::BOA_MAX", (int64)EVRIK_BoneOrientationAxis::BOA_MAX },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "BOA_MAX.Hidden", "" },
				{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
				{ "ToolTip", "Bone axis value" },
				{ "X.DisplayName", "X+" },
				{ "X_Neg.DisplayName", "X-" },
				{ "Y.DisplayName", "Y+" },
				{ "Y_Neg.DisplayName", "Y-" },
				{ "Z.DisplayName", "Z+" },
				{ "Z_Neg.DisplayName", "Z-" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_VRIKBodyRuntime,
				nullptr,
				"EVRIK_BoneOrientationAxis",
				"EVRIK_BoneOrientationAxis",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EVRIK_HandStyle_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_VRIKBodyRuntime_EVRIK_HandStyle, Z_Construct_UPackage__Script_VRIKBodyRuntime(), TEXT("EVRIK_HandStyle"));
		}
		return Singleton;
	}
	template<> VRIKBODYRUNTIME_API UEnum* StaticEnum<EVRIK_HandStyle>()
	{
		return EVRIK_HandStyle_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EVRIK_HandStyle(EVRIK_HandStyle_StaticEnum, TEXT("/Script/VRIKBodyRuntime"), TEXT("EVRIK_HandStyle"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_VRIKBodyRuntime_EVRIK_HandStyle_Hash() { return 3201598275U; }
	UEnum* Z_Construct_UEnum_VRIKBodyRuntime_EVRIK_HandStyle()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_VRIKBodyRuntime();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EVRIK_HandStyle"), 0, Get_Z_Construct_UEnum_VRIKBodyRuntime_EVRIK_HandStyle_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EVRIK_HandStyle::HS_FullArm", (int64)EVRIK_HandStyle::HS_FullArm },
				{ "EVRIK_HandStyle::HS_Lowerarm", (int64)EVRIK_HandStyle::HS_Lowerarm },
				{ "EVRIK_HandStyle::HS_HandOnly", (int64)EVRIK_HandStyle::HS_HandOnly },
				{ "EVRIK_HandStyle::HS_MAX", (int64)EVRIK_HandStyle::HS_MAX },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "HS_FullArm.DisplayName", "Full Arm" },
				{ "HS_HandOnly.DisplayName", "Hand Only" },
				{ "HS_Lowerarm.DisplayName", "Lowerarm" },
				{ "HS_MAX.Hidden", "" },
				{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
				{ "ToolTip", "Arm visibility" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_VRIKBodyRuntime,
				nullptr,
				"EVRIK_HandStyle",
				"EVRIK_HandStyle",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EVRIK_VRHand_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_VRIKBodyRuntime_EVRIK_VRHand, Z_Construct_UPackage__Script_VRIKBodyRuntime(), TEXT("EVRIK_VRHand"));
		}
		return Singleton;
	}
	template<> VRIKBODYRUNTIME_API UEnum* StaticEnum<EVRIK_VRHand>()
	{
		return EVRIK_VRHand_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EVRIK_VRHand(EVRIK_VRHand_StaticEnum, TEXT("/Script/VRIKBodyRuntime"), TEXT("EVRIK_VRHand"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_VRIKBodyRuntime_EVRIK_VRHand_Hash() { return 3233863695U; }
	UEnum* Z_Construct_UEnum_VRIKBodyRuntime_EVRIK_VRHand()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_VRIKBodyRuntime();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EVRIK_VRHand"), 0, Get_Z_Construct_UEnum_VRIKBodyRuntime_EVRIK_VRHand_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EVRIK_VRHand::VRH_Left", (int64)EVRIK_VRHand::VRH_Left },
				{ "EVRIK_VRHand::VRH_Right", (int64)EVRIK_VRHand::VRH_Right },
				{ "EVRIK_VRHand::VRH_MAX", (int64)EVRIK_VRHand::VRH_MAX },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
				{ "ToolTip", "Humanoid hand side, left or right" },
				{ "VRH_Left.DisplayName", "Left" },
				{ "VRH_MAX.Hidden", "" },
				{ "VRH_Right.DisplayName", "Right" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_VRIKBodyRuntime,
				nullptr,
				"EVRIK_VRHand",
				"EVRIK_VRHand",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FVRIK_FingersPosePreset::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern VRIKBODYRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FVRIK_FingersPosePreset, Z_Construct_UPackage__Script_VRIKBodyRuntime(), TEXT("VRIK_FingersPosePreset"), sizeof(FVRIK_FingersPosePreset), Get_Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Hash());
	}
	return Singleton;
}
template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<FVRIK_FingersPosePreset>()
{
	return FVRIK_FingersPosePreset::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FVRIK_FingersPosePreset(FVRIK_FingersPosePreset::StaticStruct, TEXT("/Script/VRIKBodyRuntime"), TEXT("VRIK_FingersPosePreset"), false, nullptr, nullptr);
static struct FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFVRIK_FingersPosePreset
{
	FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFVRIK_FingersPosePreset()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("VRIK_FingersPosePreset")),new UScriptStruct::TCppStructOps<FVRIK_FingersPosePreset>);
	}
} ScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFVRIK_FingersPosePreset;
	struct Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PinkyRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PinkyRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RingRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RingRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MiddleRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MiddleRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IndexRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_IndexRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ThumbRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ThumbRotation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Fingers pose description: rotation of all fingers" },
	};
#endif
	void* Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FVRIK_FingersPosePreset>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::NewProp_PinkyRotation_MetaData[] = {
		{ "Category", "Finger Solver" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Pinky finger rotation. Set curl value and others if necessary." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::NewProp_PinkyRotation = { "PinkyRotation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_FingersPosePreset, PinkyRotation), Z_Construct_UScriptStruct_FVRIK_FingerRotation, METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::NewProp_PinkyRotation_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::NewProp_PinkyRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::NewProp_RingRotation_MetaData[] = {
		{ "Category", "Finger Solver" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Ring finger rotation. Set curl value and others if necessary." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::NewProp_RingRotation = { "RingRotation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_FingersPosePreset, RingRotation), Z_Construct_UScriptStruct_FVRIK_FingerRotation, METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::NewProp_RingRotation_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::NewProp_RingRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::NewProp_MiddleRotation_MetaData[] = {
		{ "Category", "Finger Solver" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Middle finger rotation. Set curl value and others if necessary." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::NewProp_MiddleRotation = { "MiddleRotation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_FingersPosePreset, MiddleRotation), Z_Construct_UScriptStruct_FVRIK_FingerRotation, METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::NewProp_MiddleRotation_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::NewProp_MiddleRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::NewProp_IndexRotation_MetaData[] = {
		{ "Category", "Finger Solver" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Index finger rotation. Set curl value and others if necessary." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::NewProp_IndexRotation = { "IndexRotation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_FingersPosePreset, IndexRotation), Z_Construct_UScriptStruct_FVRIK_FingerRotation, METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::NewProp_IndexRotation_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::NewProp_IndexRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::NewProp_ThumbRotation_MetaData[] = {
		{ "Category", "Finger Solver" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Thumb rotation. All axes usually require adjustment." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::NewProp_ThumbRotation = { "ThumbRotation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_FingersPosePreset, ThumbRotation), Z_Construct_UScriptStruct_FVRIK_FingerRotation, METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::NewProp_ThumbRotation_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::NewProp_ThumbRotation_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::NewProp_PinkyRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::NewProp_RingRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::NewProp_MiddleRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::NewProp_IndexRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::NewProp_ThumbRotation,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_VRIKBodyRuntime,
		nullptr,
		&NewStructOps,
		"VRIK_FingersPosePreset",
		sizeof(FVRIK_FingersPosePreset),
		alignof(FVRIK_FingersPosePreset),
		Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FVRIK_FingersPosePreset()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_VRIKBodyRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("VRIK_FingersPosePreset"), sizeof(FVRIK_FingersPosePreset), Get_Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FVRIK_FingersPosePreset_Hash() { return 4202382395U; }
class UScriptStruct* FVRIK_TwistData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern VRIKBODYRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FVRIK_TwistData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FVRIK_TwistData, Z_Construct_UPackage__Script_VRIKBodyRuntime(), TEXT("VRIK_TwistData"), sizeof(FVRIK_TwistData), Get_Z_Construct_UScriptStruct_FVRIK_TwistData_Hash());
	}
	return Singleton;
}
template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<FVRIK_TwistData>()
{
	return FVRIK_TwistData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FVRIK_TwistData(FVRIK_TwistData::StaticStruct, TEXT("/Script/VRIKBodyRuntime"), TEXT("VRIK_TwistData"), false, nullptr, nullptr);
static struct FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFVRIK_TwistData
{
	FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFVRIK_TwistData()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("VRIK_TwistData")),new UScriptStruct::TCppStructOps<FVRIK_TwistData>);
	}
} ScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFVRIK_TwistData;
	struct Z_Construct_UScriptStruct_FVRIK_TwistData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Mulitplier_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Mulitplier;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_TwistData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Struct to get twist rotation by bone index" },
	};
#endif
	void* Z_Construct_UScriptStruct_FVRIK_TwistData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FVRIK_TwistData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_TwistData_Statics::NewProp_Mulitplier_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Twist multiplier from UHandSkeletalMeshComponent::UpperarmTwists or UHandSkeletalMeshComponent::LowerarmTwists" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FVRIK_TwistData_Statics::NewProp_Mulitplier = { "Mulitplier", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_TwistData, Mulitplier), METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_TwistData_Statics::NewProp_Mulitplier_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_TwistData_Statics::NewProp_Mulitplier_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FVRIK_TwistData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_TwistData_Statics::NewProp_Mulitplier,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FVRIK_TwistData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_VRIKBodyRuntime,
		nullptr,
		&NewStructOps,
		"VRIK_TwistData",
		sizeof(FVRIK_TwistData),
		alignof(FVRIK_TwistData),
		Z_Construct_UScriptStruct_FVRIK_TwistData_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_TwistData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_TwistData_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_TwistData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FVRIK_TwistData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FVRIK_TwistData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_VRIKBodyRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("VRIK_TwistData"), sizeof(FVRIK_TwistData), Get_Z_Construct_UScriptStruct_FVRIK_TwistData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FVRIK_TwistData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FVRIK_TwistData_Hash() { return 3748153401U; }
class UScriptStruct* FVRIK_FingerKnucklePointer::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern VRIKBODYRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FVRIK_FingerKnucklePointer_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FVRIK_FingerKnucklePointer, Z_Construct_UPackage__Script_VRIKBodyRuntime(), TEXT("VRIK_FingerKnucklePointer"), sizeof(FVRIK_FingerKnucklePointer), Get_Z_Construct_UScriptStruct_FVRIK_FingerKnucklePointer_Hash());
	}
	return Singleton;
}
template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<FVRIK_FingerKnucklePointer>()
{
	return FVRIK_FingerKnucklePointer::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FVRIK_FingerKnucklePointer(FVRIK_FingerKnucklePointer::StaticStruct, TEXT("/Script/VRIKBodyRuntime"), TEXT("VRIK_FingerKnucklePointer"), false, nullptr, nullptr);
static struct FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFVRIK_FingerKnucklePointer
{
	FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFVRIK_FingerKnucklePointer()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("VRIK_FingerKnucklePointer")),new UScriptStruct::TCppStructOps<FVRIK_FingerKnucklePointer>);
	}
} ScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFVRIK_FingerKnucklePointer;
	struct Z_Construct_UScriptStruct_FVRIK_FingerKnucklePointer_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_KnuckleId_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_KnuckleId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FingerId_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_FingerId;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_FingerId_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_FingerKnucklePointer_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Struct to get knuckle transform by bone index" },
	};
#endif
	void* Z_Construct_UScriptStruct_FVRIK_FingerKnucklePointer_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FVRIK_FingerKnucklePointer>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_FingerKnucklePointer_Statics::NewProp_KnuckleId_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Index in FVRIK_FingerSolver::Knuckles array" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FVRIK_FingerKnucklePointer_Statics::NewProp_KnuckleId = { "KnuckleId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_FingerKnucklePointer, KnuckleId), METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_FingerKnucklePointer_Statics::NewProp_KnuckleId_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingerKnucklePointer_Statics::NewProp_KnuckleId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_FingerKnucklePointer_Statics::NewProp_FingerId_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Name of finger in UHandSkeletalMeshComponent::Fingers map" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FVRIK_FingerKnucklePointer_Statics::NewProp_FingerId = { "FingerId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_FingerKnucklePointer, FingerId), Z_Construct_UEnum_VRIKBodyRuntime_EVRIKFingerName, METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_FingerKnucklePointer_Statics::NewProp_FingerId_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingerKnucklePointer_Statics::NewProp_FingerId_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FVRIK_FingerKnucklePointer_Statics::NewProp_FingerId_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FVRIK_FingerKnucklePointer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingerKnucklePointer_Statics::NewProp_KnuckleId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingerKnucklePointer_Statics::NewProp_FingerId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingerKnucklePointer_Statics::NewProp_FingerId_Underlying,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FVRIK_FingerKnucklePointer_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_VRIKBodyRuntime,
		nullptr,
		&NewStructOps,
		"VRIK_FingerKnucklePointer",
		sizeof(FVRIK_FingerKnucklePointer),
		alignof(FVRIK_FingerKnucklePointer),
		Z_Construct_UScriptStruct_FVRIK_FingerKnucklePointer_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingerKnucklePointer_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_FingerKnucklePointer_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingerKnucklePointer_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FVRIK_FingerKnucklePointer()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FVRIK_FingerKnucklePointer_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_VRIKBodyRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("VRIK_FingerKnucklePointer"), sizeof(FVRIK_FingerKnucklePointer), Get_Z_Construct_UScriptStruct_FVRIK_FingerKnucklePointer_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FVRIK_FingerKnucklePointer_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FVRIK_FingerKnucklePointer_Hash() { return 2753658346U; }
class UScriptStruct* FVRIK_FingerSolver::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern VRIKBODYRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FVRIK_FingerSolver_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FVRIK_FingerSolver, Z_Construct_UPackage__Script_VRIKBodyRuntime(), TEXT("VRIK_FingerSolver"), sizeof(FVRIK_FingerSolver), Get_Z_Construct_UScriptStruct_FVRIK_FingerSolver_Hash());
	}
	return Singleton;
}
template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<FVRIK_FingerSolver>()
{
	return FVRIK_FingerSolver::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FVRIK_FingerSolver(FVRIK_FingerSolver::StaticStruct, TEXT("/Script/VRIKBodyRuntime"), TEXT("VRIK_FingerSolver"), false, nullptr, nullptr);
static struct FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFVRIK_FingerSolver
{
	FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFVRIK_FingerSolver()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("VRIK_FingerSolver")),new UScriptStruct::TCppStructOps<FVRIK_FingerSolver>);
	}
} ScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFVRIK_FingerSolver;
	struct Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RootBoneName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_RootBoneName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Knuckles_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Knuckles;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Knuckles_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Alpha_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Alpha;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnabled_MetaData[];
#endif
		static void NewProp_bEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutwardAxis_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_OutwardAxis;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_OutwardAxis_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RootRadius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RootRadius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TipRadius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TipRadius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_KnucklesNum_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_KnucklesNum;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TipBoneName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_TipBoneName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Figer description. Used both for input and output" },
	};
#endif
	void* Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FVRIK_FingerSolver>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_RootBoneName_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Calculated: hand bone name" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_RootBoneName = { "RootBoneName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_FingerSolver, RootBoneName), METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_RootBoneName_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_RootBoneName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_Knuckles_MetaData[] = {
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Calculated: array of knuckles" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_Knuckles = { "Knuckles", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_FingerSolver, Knuckles), METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_Knuckles_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_Knuckles_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_Knuckles_Inner = { "Knuckles", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVRIK_Knuckle, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_Alpha_MetaData[] = {
		{ "Category", "Finger Solver" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Current alpha to blend between input finger pose and calculated finger pose" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_Alpha = { "Alpha", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_FingerSolver, Alpha), METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_Alpha_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_Alpha_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_bEnabled_MetaData[] = {
		{ "Category", "Finger Solver" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Should solver process this finger?" },
	};
#endif
	void Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_bEnabled_SetBit(void* Obj)
	{
		((FVRIK_FingerSolver*)Obj)->bEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_bEnabled = { "bEnabled", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FVRIK_FingerSolver), &Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_bEnabled_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_bEnabled_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_bEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_OutwardAxis_MetaData[] = {
		{ "Category", "Finger Solver" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Outward local axis of the finger (normal to fingers plane)" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_OutwardAxis = { "OutwardAxis", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_FingerSolver, OutwardAxis), Z_Construct_UEnum_VRIKBodyRuntime_EVRIK_BoneOrientationAxis, METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_OutwardAxis_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_OutwardAxis_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_OutwardAxis_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_RootRadius_MetaData[] = {
		{ "Category", "Finger Solver" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Radius of first bone" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_RootRadius = { "RootRadius", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_FingerSolver, RootRadius), METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_RootRadius_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_RootRadius_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_TipRadius_MetaData[] = {
		{ "Category", "Finger Solver" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Radius of tip bone (smallest)" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_TipRadius = { "TipRadius", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_FingerSolver, TipRadius), METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_TipRadius_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_TipRadius_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_KnucklesNum_MetaData[] = {
		{ "Category", "Finger Solver" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Number of knuckles" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_KnucklesNum = { "KnucklesNum", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_FingerSolver, KnucklesNum), METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_KnucklesNum_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_KnucklesNum_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_TipBoneName_MetaData[] = {
		{ "Category", "Finger Solver" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Name of last knuckle bone" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_TipBoneName = { "TipBoneName", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_FingerSolver, TipBoneName), METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_TipBoneName_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_TipBoneName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_RootBoneName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_Knuckles,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_Knuckles_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_Alpha,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_bEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_OutwardAxis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_OutwardAxis_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_RootRadius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_TipRadius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_KnucklesNum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::NewProp_TipBoneName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_VRIKBodyRuntime,
		nullptr,
		&NewStructOps,
		"VRIK_FingerSolver",
		sizeof(FVRIK_FingerSolver),
		alignof(FVRIK_FingerSolver),
		Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FVRIK_FingerSolver()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FVRIK_FingerSolver_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_VRIKBodyRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("VRIK_FingerSolver"), sizeof(FVRIK_FingerSolver), Get_Z_Construct_UScriptStruct_FVRIK_FingerSolver_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FVRIK_FingerSolver_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FVRIK_FingerSolver_Hash() { return 3711423539U; }
class UScriptStruct* FVRIK_Knuckle::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern VRIKBODYRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FVRIK_Knuckle_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FVRIK_Knuckle, Z_Construct_UPackage__Script_VRIKBodyRuntime(), TEXT("VRIK_Knuckle"), sizeof(FVRIK_Knuckle), Get_Z_Construct_UScriptStruct_FVRIK_Knuckle_Hash());
	}
	return Singleton;
}
template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<FVRIK_Knuckle>()
{
	return FVRIK_Knuckle::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FVRIK_Knuckle(FVRIK_Knuckle::StaticStruct, TEXT("/Script/VRIKBodyRuntime"), TEXT("VRIK_Knuckle"), false, nullptr, nullptr);
static struct FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFVRIK_Knuckle
{
	FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFVRIK_Knuckle()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("VRIK_Knuckle")),new UScriptStruct::TCppStructOps<FVRIK_Knuckle>);
	}
} ScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFVRIK_Knuckle;
	struct Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputRefPoseRelativeTransform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InputRefPoseRelativeTransform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TraceRefPoseRelativeTransform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TraceRefPoseRelativeTransform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RefPoseRelativeTransform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RefPoseRelativeTransform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RelativeTransform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RelativeTransform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldTransform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WorldTransform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Length_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Length;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Radius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Radius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoneIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_BoneIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoneName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_BoneName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Description of finger knuckle. Initialized in UFingerFKIKSolver::Initialize." },
	};
#endif
	void* Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FVRIK_Knuckle>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_InputRefPoseRelativeTransform_MetaData[] = {
		{ "Category", "Knuckle" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Relative transform for vr input" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_InputRefPoseRelativeTransform = { "InputRefPoseRelativeTransform", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_Knuckle, InputRefPoseRelativeTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_InputRefPoseRelativeTransform_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_InputRefPoseRelativeTransform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_TraceRefPoseRelativeTransform_MetaData[] = {
		{ "Category", "Knuckle" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Relative transform from input animation pose" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_TraceRefPoseRelativeTransform = { "TraceRefPoseRelativeTransform", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_Knuckle, TraceRefPoseRelativeTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_TraceRefPoseRelativeTransform_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_TraceRefPoseRelativeTransform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_RefPoseRelativeTransform_MetaData[] = {
		{ "Category", "Knuckle" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Relative transform from input animation skeleton" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_RefPoseRelativeTransform = { "RefPoseRelativeTransform", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_Knuckle, RefPoseRelativeTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_RefPoseRelativeTransform_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_RefPoseRelativeTransform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_RelativeTransform_MetaData[] = {
		{ "Category", "Knuckle" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Current relative transform" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_RelativeTransform = { "RelativeTransform", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_Knuckle, RelativeTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_RelativeTransform_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_RelativeTransform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_WorldTransform_MetaData[] = {
		{ "Category", "Knuckle" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Instantaneous transform in world space" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_WorldTransform = { "WorldTransform", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_Knuckle, WorldTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_WorldTransform_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_WorldTransform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_Length_MetaData[] = {
		{ "Category", "Knuckle" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Distance to the next knuckle (or finger end)" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_Length = { "Length", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_Knuckle, Length), METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_Length_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_Length_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_Radius_MetaData[] = {
		{ "Category", "Knuckle" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Knuckle radius" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_Radius = { "Radius", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_Knuckle, Radius), METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_Radius_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_Radius_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_BoneIndex_MetaData[] = {
		{ "Category", "Knuckle" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Index of bone in skeleton" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_BoneIndex = { "BoneIndex", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_Knuckle, BoneIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_BoneIndex_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_BoneIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_BoneName_MetaData[] = {
		{ "Category", "Knuckle" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Name of skeleton bone" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_BoneName = { "BoneName", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_Knuckle, BoneName), METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_BoneName_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_BoneName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_InputRefPoseRelativeTransform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_TraceRefPoseRelativeTransform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_RefPoseRelativeTransform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_RelativeTransform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_WorldTransform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_Length,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_Radius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_BoneIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::NewProp_BoneName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_VRIKBodyRuntime,
		nullptr,
		&NewStructOps,
		"VRIK_Knuckle",
		sizeof(FVRIK_Knuckle),
		alignof(FVRIK_Knuckle),
		Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FVRIK_Knuckle()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FVRIK_Knuckle_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_VRIKBodyRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("VRIK_Knuckle"), sizeof(FVRIK_Knuckle), Get_Z_Construct_UScriptStruct_FVRIK_Knuckle_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FVRIK_Knuckle_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FVRIK_Knuckle_Hash() { return 2971979829U; }
class UScriptStruct* FVRIK_FingerRotation::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern VRIKBODYRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FVRIK_FingerRotation_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FVRIK_FingerRotation, Z_Construct_UPackage__Script_VRIKBodyRuntime(), TEXT("VRIK_FingerRotation"), sizeof(FVRIK_FingerRotation), Get_Z_Construct_UScriptStruct_FVRIK_FingerRotation_Hash());
	}
	return Singleton;
}
template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<FVRIK_FingerRotation>()
{
	return FVRIK_FingerRotation::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FVRIK_FingerRotation(FVRIK_FingerRotation::StaticStruct, TEXT("/Script/VRIKBodyRuntime"), TEXT("VRIK_FingerRotation"), false, nullptr, nullptr);
static struct FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFVRIK_FingerRotation
{
	FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFVRIK_FingerRotation()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("VRIK_FingerRotation")),new UScriptStruct::TCppStructOps<FVRIK_FingerRotation>);
	}
} ScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFVRIK_FingerRotation;
	struct Z_Construct_UScriptStruct_FVRIK_FingerRotation_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RollValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RollValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpreadValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SpreadValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurlValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CurlValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_FingerRotation_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Finger 3DOF rotation" },
	};
#endif
	void* Z_Construct_UScriptStruct_FVRIK_FingerRotation_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FVRIK_FingerRotation>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_FingerRotation_Statics::NewProp_RollValue_MetaData[] = {
		{ "Category", "Finger Rotation" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "One unit is 20 degrees" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FVRIK_FingerRotation_Statics::NewProp_RollValue = { "RollValue", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_FingerRotation, RollValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_FingerRotation_Statics::NewProp_RollValue_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingerRotation_Statics::NewProp_RollValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_FingerRotation_Statics::NewProp_SpreadValue_MetaData[] = {
		{ "Category", "Finger Rotation" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "One unit is 20 degrees" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FVRIK_FingerRotation_Statics::NewProp_SpreadValue = { "SpreadValue", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_FingerRotation, SpreadValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_FingerRotation_Statics::NewProp_SpreadValue_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingerRotation_Statics::NewProp_SpreadValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FVRIK_FingerRotation_Statics::NewProp_CurlValue_MetaData[] = {
		{ "Category", "Finger Rotation" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "One unit is 90 degrees" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FVRIK_FingerRotation_Statics::NewProp_CurlValue = { "CurlValue", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FVRIK_FingerRotation, CurlValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_FingerRotation_Statics::NewProp_CurlValue_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingerRotation_Statics::NewProp_CurlValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FVRIK_FingerRotation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingerRotation_Statics::NewProp_RollValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingerRotation_Statics::NewProp_SpreadValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FVRIK_FingerRotation_Statics::NewProp_CurlValue,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FVRIK_FingerRotation_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_VRIKBodyRuntime,
		nullptr,
		&NewStructOps,
		"VRIK_FingerRotation",
		sizeof(FVRIK_FingerRotation),
		alignof(FVRIK_FingerRotation),
		Z_Construct_UScriptStruct_FVRIK_FingerRotation_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingerRotation_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FVRIK_FingerRotation_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FVRIK_FingerRotation_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FVRIK_FingerRotation()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FVRIK_FingerRotation_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_VRIKBodyRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("VRIK_FingerRotation"), sizeof(FVRIK_FingerRotation), Get_Z_Construct_UScriptStruct_FVRIK_FingerRotation_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FVRIK_FingerRotation_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FVRIK_FingerRotation_Hash() { return 3487053135U; }
	void UVRIKFingersSolverSetup::StaticRegisterNativesUVRIKFingersSolverSetup()
	{
	}
	UClass* Z_Construct_UClass_UVRIKFingersSolverSetup_NoRegister()
	{
		return UVRIKFingersSolverSetup::StaticClass();
	}
	struct Z_Construct_UClass_UVRIKFingersSolverSetup_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Fingers_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Fingers;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Fingers_Key_KeyProp;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Fingers_Key_KeyProp_Underlying;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Fingers_ValueProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PosesInterpolationSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PosesInterpolationSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputMaxRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InputMaxRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputMinRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InputMinRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bTraceByObjectType_MetaData[];
#endif
		static void NewProp_bTraceByObjectType_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bTraceByObjectType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TraceChannel_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TraceChannel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TraceHalfDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TraceHalfDistance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Hand_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Hand;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Hand_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataAsset,
		(UObject* (*)())Z_Construct_UPackage__Script_VRIKBodyRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "VRIKFingersSolverSetup.h" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Global fingers settings. Create separate objects for all hands." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_Fingers_MetaData[] = {
		{ "Category", "Setup" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Fingers settings." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_Fingers = { "Fingers", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKFingersSolverSetup, Fingers), METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_Fingers_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_Fingers_MetaData)) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_Fingers_Key_KeyProp = { "Fingers_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UEnum_VRIKBodyRuntime_EVRIKFingerName, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_Fingers_Key_KeyProp_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_Fingers_ValueProp = { "Fingers", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FVRIK_FingerSolver, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_PosesInterpolationSpeed_MetaData[] = {
		{ "Category", "Setup" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Interpolatoin speed for processing poses applied by SetFingersPose function" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_PosesInterpolationSpeed = { "PosesInterpolationSpeed", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKFingersSolverSetup, PosesInterpolationSpeed), METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_PosesInterpolationSpeed_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_PosesInterpolationSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_InputMaxRotation_MetaData[] = {
		{ "Category", "Setup" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Upper border for VR input (in degrees). VR input values (0..1) are interpolated to (InputMinRotation, InputMaxRotation)" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_InputMaxRotation = { "InputMaxRotation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKFingersSolverSetup, InputMaxRotation), METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_InputMaxRotation_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_InputMaxRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_InputMinRotation_MetaData[] = {
		{ "Category", "Setup" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Lower border for VR input (in degrees). VR input values (0..1) are interpolated to (InputMinRotation, InputMaxRotation)" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_InputMinRotation = { "InputMinRotation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKFingersSolverSetup, InputMinRotation), METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_InputMinRotation_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_InputMinRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_bTraceByObjectType_MetaData[] = {
		{ "Category", "Setup" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Should trace by trace channel (false) or object type (true)" },
	};
#endif
	void Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_bTraceByObjectType_SetBit(void* Obj)
	{
		((UVRIKFingersSolverSetup*)Obj)->bTraceByObjectType = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_bTraceByObjectType = { "bTraceByObjectType", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVRIKFingersSolverSetup), &Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_bTraceByObjectType_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_bTraceByObjectType_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_bTraceByObjectType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_TraceChannel_MetaData[] = {
		{ "Category", "Setup" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Trace channel to probe: Visible, Camera etc" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_TraceChannel = { "TraceChannel", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKFingersSolverSetup, TraceChannel), Z_Construct_UEnum_Engine_ECollisionChannel, METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_TraceChannel_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_TraceChannel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_TraceHalfDistance_MetaData[] = {
		{ "Category", "Setup" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Trace distance from knuckle to inside and outside" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_TraceHalfDistance = { "TraceHalfDistance", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKFingersSolverSetup, TraceHalfDistance), METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_TraceHalfDistance_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_TraceHalfDistance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_Hand_MetaData[] = {
		{ "Category", "Setup" },
		{ "ModuleRelativePath", "Public/VRIKFingersSolverSetup.h" },
		{ "ToolTip", "Hand side associated with this asset" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_Hand = { "Hand", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVRIKFingersSolverSetup, Hand), Z_Construct_UEnum_VRIKBodyRuntime_EVRIK_VRHand, METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_Hand_MetaData, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_Hand_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_Hand_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_Fingers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_Fingers_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_Fingers_Key_KeyProp_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_Fingers_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_PosesInterpolationSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_InputMaxRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_InputMinRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_bTraceByObjectType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_TraceChannel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_TraceHalfDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_Hand,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::NewProp_Hand_Underlying,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVRIKFingersSolverSetup>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::ClassParams = {
		&UVRIKFingersSolverSetup::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVRIKFingersSolverSetup()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVRIKFingersSolverSetup_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVRIKFingersSolverSetup, 2003180487);
	template<> VRIKBODYRUNTIME_API UClass* StaticClass<UVRIKFingersSolverSetup>()
	{
		return UVRIKFingersSolverSetup::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVRIKFingersSolverSetup(Z_Construct_UClass_UVRIKFingersSolverSetup, &UVRIKFingersSolverSetup::StaticClass, TEXT("/Script/VRIKBodyRuntime"), TEXT("UVRIKFingersSolverSetup"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVRIKFingersSolverSetup);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
