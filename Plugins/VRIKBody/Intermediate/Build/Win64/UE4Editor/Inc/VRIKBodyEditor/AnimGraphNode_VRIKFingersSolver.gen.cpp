// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VRIKBodyEditor/Public/AnimGraphNode_VRIKFingersSolver.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAnimGraphNode_VRIKFingersSolver() {}
// Cross Module References
	VRIKBODYEDITOR_API UClass* Z_Construct_UClass_UAnimGraphNode_VRIKFingersSolver_NoRegister();
	VRIKBODYEDITOR_API UClass* Z_Construct_UClass_UAnimGraphNode_VRIKFingersSolver();
	ANIMGRAPH_API UClass* Z_Construct_UClass_UAnimGraphNode_Base();
	UPackage* Z_Construct_UPackage__Script_VRIKBodyEditor();
	VRIKBODYRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver();
// End Cross Module References
	void UAnimGraphNode_VRIKFingersSolver::StaticRegisterNativesUAnimGraphNode_VRIKFingersSolver()
	{
	}
	UClass* Z_Construct_UClass_UAnimGraphNode_VRIKFingersSolver_NoRegister()
	{
		return UAnimGraphNode_VRIKFingersSolver::StaticClass();
	}
	struct Z_Construct_UClass_UAnimGraphNode_VRIKFingersSolver_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Node_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Node;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAnimGraphNode_VRIKFingersSolver_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAnimGraphNode_Base,
		(UObject* (*)())Z_Construct_UPackage__Script_VRIKBodyEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAnimGraphNode_VRIKFingersSolver_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AnimGraphNode_VRIKFingersSolver.h" },
		{ "ModuleRelativePath", "Public/AnimGraphNode_VRIKFingersSolver.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAnimGraphNode_VRIKFingersSolver_Statics::NewProp_Node_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/AnimGraphNode_VRIKFingersSolver.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UAnimGraphNode_VRIKFingersSolver_Statics::NewProp_Node = { "Node", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAnimGraphNode_VRIKFingersSolver, Node), Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver, METADATA_PARAMS(Z_Construct_UClass_UAnimGraphNode_VRIKFingersSolver_Statics::NewProp_Node_MetaData, ARRAY_COUNT(Z_Construct_UClass_UAnimGraphNode_VRIKFingersSolver_Statics::NewProp_Node_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAnimGraphNode_VRIKFingersSolver_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAnimGraphNode_VRIKFingersSolver_Statics::NewProp_Node,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAnimGraphNode_VRIKFingersSolver_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAnimGraphNode_VRIKFingersSolver>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAnimGraphNode_VRIKFingersSolver_Statics::ClassParams = {
		&UAnimGraphNode_VRIKFingersSolver::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAnimGraphNode_VRIKFingersSolver_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_UAnimGraphNode_VRIKFingersSolver_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAnimGraphNode_VRIKFingersSolver_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UAnimGraphNode_VRIKFingersSolver_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAnimGraphNode_VRIKFingersSolver()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAnimGraphNode_VRIKFingersSolver_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAnimGraphNode_VRIKFingersSolver, 2983172070);
	template<> VRIKBODYEDITOR_API UClass* StaticClass<UAnimGraphNode_VRIKFingersSolver>()
	{
		return UAnimGraphNode_VRIKFingersSolver::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAnimGraphNode_VRIKFingersSolver(Z_Construct_UClass_UAnimGraphNode_VRIKFingersSolver, &UAnimGraphNode_VRIKFingersSolver::StaticClass, TEXT("/Script/VRIKBodyEditor"), TEXT("UAnimGraphNode_VRIKFingersSolver"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAnimGraphNode_VRIKFingersSolver);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
