// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVRIK_FingerRotation;
enum class EVRIKFingerName : uint8;
class UPrimitiveComponent;
struct FVRIK_FingersPosePreset;
struct FVRIK_Knuckle;
struct FVRIK_FingersDetailedInfo;
class UVRIKFingersSolverSetup;
class USkeletalMeshComponent;
class UVRIKFingersFKIKSolver;
#ifdef VRIKBODYRUNTIME_VRIKFingersFKIKSolver_generated_h
#error "VRIKFingersFKIKSolver.generated.h already included, missing '#pragma once' in VRIKFingersFKIKSolver.h"
#endif
#define VRIKBODYRUNTIME_VRIKFingersFKIKSolver_generated_h

#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersFKIKSolver_h_17_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVRIK_FingersDetailedInfo_Statics; \
	static class UScriptStruct* StaticStruct();


template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<struct FVRIK_FingersDetailedInfo>();

#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersFKIKSolver_h_47_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execConvertFingerRotationFromDegrees) \
	{ \
		P_GET_STRUCT_REF(FVRIK_FingerRotation,Z_Param_Out_InFingerRot); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FVRIK_FingerRotation*)Z_Param__Result=UVRIKFingersFKIKSolver::ConvertFingerRotationFromDegrees(Z_Param_Out_InFingerRot); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execConvertFingerRotationToDegrees) \
	{ \
		P_GET_STRUCT_REF(FVRIK_FingerRotation,Z_Param_Out_InFingerRot); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FVRIK_FingerRotation*)Z_Param__Result=UVRIKFingersFKIKSolver::ConvertFingerRotationToDegrees(Z_Param_Out_InFingerRot); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetFingerEnabled) \
	{ \
		P_GET_ENUM(EVRIKFingerName,Z_Param_FingerName); \
		P_GET_UBOOL(Z_Param_bNewEnabled); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetFingerEnabled(EVRIKFingerName(Z_Param_FingerName),Z_Param_bNewEnabled); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execReleaseObject) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ReleaseObject(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGrabObject) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_Object); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GrabObject(Z_Param_Object); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetVRInputReferencePose) \
	{ \
		P_GET_STRUCT_REF(FVRIK_FingersPosePreset,Z_Param_Out_NewRefPose); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->SetVRInputReferencePose(Z_Param_Out_NewRefPose); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetFingersTraceReferencePose) \
	{ \
		P_GET_STRUCT_REF(FVRIK_FingersPosePreset,Z_Param_Out_NewRefPose); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->SetFingersTraceReferencePose(Z_Param_Out_NewRefPose); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetFingersPose) \
	{ \
		P_GET_STRUCT_REF(FVRIK_FingersPosePreset,Z_Param_Out_NewPose); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->SetFingersPose(Z_Param_Out_NewPose); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsFingerEnabled) \
	{ \
		P_GET_ENUM(EVRIKFingerName,Z_Param_FingerName); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->IsFingerEnabled(EVRIKFingerName(Z_Param_FingerName)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetFingerKnuckles) \
	{ \
		P_GET_ENUM(EVRIKFingerName,Z_Param_FingerName); \
		P_GET_TARRAY_REF(FVRIK_Knuckle,Z_Param_Out_OutKnuckles); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GetFingerKnuckles(EVRIKFingerName(Z_Param_FingerName),Z_Param_Out_OutKnuckles); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetFingerDescription) \
	{ \
		P_GET_ENUM(EVRIKFingerName,Z_Param_FingerName); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->GetFingerDescription(EVRIKFingerName(Z_Param_FingerName)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsInitialized) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->IsInitialized(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execApplyVRInputDetailed) \
	{ \
		P_GET_STRUCT_REF(FVRIK_FingersDetailedInfo,Z_Param_Out_NewFingersRotation); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ApplyVRInputDetailed(Z_Param_Out_NewFingersRotation); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execApplyVRInput) \
	{ \
		P_GET_STRUCT_REF(FVRIK_FingersPosePreset,Z_Param_Out_NewFingersRotation); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ApplyVRInput(Z_Param_Out_NewFingersRotation); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTrace) \
	{ \
		P_GET_UBOOL(Z_Param_bTracingInTick); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Trace(Z_Param_bTracingInTick); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUpdateFingers) \
	{ \
		P_GET_UBOOL(Z_Param_bTrace); \
		P_GET_UBOOL(Z_Param_bUpdateFingersPose); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UpdateFingers(Z_Param_bTrace,Z_Param_bUpdateFingersPose); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCreateFingersFKIKSolver) \
	{ \
		P_GET_OBJECT(UVRIKFingersSolverSetup,Z_Param_SolverSetup); \
		P_GET_OBJECT(USkeletalMeshComponent,Z_Param_SkeletalMeshComp); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(UVRIKFingersFKIKSolver**)Z_Param__Result=UVRIKFingersFKIKSolver::CreateFingersFKIKSolver(Z_Param_SolverSetup,Z_Param_SkeletalMeshComp); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execInitialize) \
	{ \
		P_GET_OBJECT(UVRIKFingersSolverSetup,Z_Param_SolverSetup); \
		P_GET_OBJECT(USkeletalMeshComponent,Z_Param_SkeletalMeshComp); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->Initialize(Z_Param_SolverSetup,Z_Param_SkeletalMeshComp); \
		P_NATIVE_END; \
	}


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersFKIKSolver_h_47_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execConvertFingerRotationFromDegrees) \
	{ \
		P_GET_STRUCT_REF(FVRIK_FingerRotation,Z_Param_Out_InFingerRot); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FVRIK_FingerRotation*)Z_Param__Result=UVRIKFingersFKIKSolver::ConvertFingerRotationFromDegrees(Z_Param_Out_InFingerRot); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execConvertFingerRotationToDegrees) \
	{ \
		P_GET_STRUCT_REF(FVRIK_FingerRotation,Z_Param_Out_InFingerRot); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FVRIK_FingerRotation*)Z_Param__Result=UVRIKFingersFKIKSolver::ConvertFingerRotationToDegrees(Z_Param_Out_InFingerRot); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetFingerEnabled) \
	{ \
		P_GET_ENUM(EVRIKFingerName,Z_Param_FingerName); \
		P_GET_UBOOL(Z_Param_bNewEnabled); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetFingerEnabled(EVRIKFingerName(Z_Param_FingerName),Z_Param_bNewEnabled); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execReleaseObject) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ReleaseObject(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGrabObject) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_Object); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GrabObject(Z_Param_Object); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetVRInputReferencePose) \
	{ \
		P_GET_STRUCT_REF(FVRIK_FingersPosePreset,Z_Param_Out_NewRefPose); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->SetVRInputReferencePose(Z_Param_Out_NewRefPose); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetFingersTraceReferencePose) \
	{ \
		P_GET_STRUCT_REF(FVRIK_FingersPosePreset,Z_Param_Out_NewRefPose); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->SetFingersTraceReferencePose(Z_Param_Out_NewRefPose); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetFingersPose) \
	{ \
		P_GET_STRUCT_REF(FVRIK_FingersPosePreset,Z_Param_Out_NewPose); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->SetFingersPose(Z_Param_Out_NewPose); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsFingerEnabled) \
	{ \
		P_GET_ENUM(EVRIKFingerName,Z_Param_FingerName); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->IsFingerEnabled(EVRIKFingerName(Z_Param_FingerName)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetFingerKnuckles) \
	{ \
		P_GET_ENUM(EVRIKFingerName,Z_Param_FingerName); \
		P_GET_TARRAY_REF(FVRIK_Knuckle,Z_Param_Out_OutKnuckles); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GetFingerKnuckles(EVRIKFingerName(Z_Param_FingerName),Z_Param_Out_OutKnuckles); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetFingerDescription) \
	{ \
		P_GET_ENUM(EVRIKFingerName,Z_Param_FingerName); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->GetFingerDescription(EVRIKFingerName(Z_Param_FingerName)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsInitialized) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->IsInitialized(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execApplyVRInputDetailed) \
	{ \
		P_GET_STRUCT_REF(FVRIK_FingersDetailedInfo,Z_Param_Out_NewFingersRotation); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ApplyVRInputDetailed(Z_Param_Out_NewFingersRotation); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execApplyVRInput) \
	{ \
		P_GET_STRUCT_REF(FVRIK_FingersPosePreset,Z_Param_Out_NewFingersRotation); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ApplyVRInput(Z_Param_Out_NewFingersRotation); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTrace) \
	{ \
		P_GET_UBOOL(Z_Param_bTracingInTick); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Trace(Z_Param_bTracingInTick); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUpdateFingers) \
	{ \
		P_GET_UBOOL(Z_Param_bTrace); \
		P_GET_UBOOL(Z_Param_bUpdateFingersPose); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UpdateFingers(Z_Param_bTrace,Z_Param_bUpdateFingersPose); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCreateFingersFKIKSolver) \
	{ \
		P_GET_OBJECT(UVRIKFingersSolverSetup,Z_Param_SolverSetup); \
		P_GET_OBJECT(USkeletalMeshComponent,Z_Param_SkeletalMeshComp); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(UVRIKFingersFKIKSolver**)Z_Param__Result=UVRIKFingersFKIKSolver::CreateFingersFKIKSolver(Z_Param_SolverSetup,Z_Param_SkeletalMeshComp); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execInitialize) \
	{ \
		P_GET_OBJECT(UVRIKFingersSolverSetup,Z_Param_SolverSetup); \
		P_GET_OBJECT(USkeletalMeshComponent,Z_Param_SkeletalMeshComp); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->Initialize(Z_Param_SolverSetup,Z_Param_SkeletalMeshComp); \
		P_NATIVE_END; \
	}


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersFKIKSolver_h_47_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVRIKFingersFKIKSolver(); \
	friend struct Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics; \
public: \
	DECLARE_CLASS(UVRIKFingersFKIKSolver, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VRIKBodyRuntime"), NO_API) \
	DECLARE_SERIALIZER(UVRIKFingersFKIKSolver)


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersFKIKSolver_h_47_INCLASS \
private: \
	static void StaticRegisterNativesUVRIKFingersFKIKSolver(); \
	friend struct Z_Construct_UClass_UVRIKFingersFKIKSolver_Statics; \
public: \
	DECLARE_CLASS(UVRIKFingersFKIKSolver, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VRIKBodyRuntime"), NO_API) \
	DECLARE_SERIALIZER(UVRIKFingersFKIKSolver)


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersFKIKSolver_h_47_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVRIKFingersFKIKSolver(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVRIKFingersFKIKSolver) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVRIKFingersFKIKSolver); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVRIKFingersFKIKSolver); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVRIKFingersFKIKSolver(UVRIKFingersFKIKSolver&&); \
	NO_API UVRIKFingersFKIKSolver(const UVRIKFingersFKIKSolver&); \
public:


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersFKIKSolver_h_47_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVRIKFingersFKIKSolver(UVRIKFingersFKIKSolver&&); \
	NO_API UVRIKFingersFKIKSolver(const UVRIKFingersFKIKSolver&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVRIKFingersFKIKSolver); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVRIKFingersFKIKSolver); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UVRIKFingersFKIKSolver)


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersFKIKSolver_h_47_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bIsInitialized() { return STRUCT_OFFSET(UVRIKFingersFKIKSolver, bIsInitialized); } \
	FORCEINLINE static uint32 __PPO__Mesh() { return STRUCT_OFFSET(UVRIKFingersFKIKSolver, Mesh); } \
	FORCEINLINE static uint32 __PPO__FingersSolverSetup() { return STRUCT_OFFSET(UVRIKFingersFKIKSolver, FingersSolverSetup); } \
	FORCEINLINE static uint32 __PPO__TracingStatus() { return STRUCT_OFFSET(UVRIKFingersFKIKSolver, TracingStatus); } \
	FORCEINLINE static uint32 __PPO__VRInput() { return STRUCT_OFFSET(UVRIKFingersFKIKSolver, VRInput); } \
	FORCEINLINE static uint32 __PPO__VRInputDetailed() { return STRUCT_OFFSET(UVRIKFingersFKIKSolver, VRInputDetailed); } \
	FORCEINLINE static uint32 __PPO__bHasVRInputInFrame() { return STRUCT_OFFSET(UVRIKFingersFKIKSolver, bHasVRInputInFrame); } \
	FORCEINLINE static uint32 __PPO__bHasDetailedVRInputInFrame() { return STRUCT_OFFSET(UVRIKFingersFKIKSolver, bHasDetailedVRInputInFrame); } \
	FORCEINLINE static uint32 __PPO__bUseRuntimeFingersPose() { return STRUCT_OFFSET(UVRIKFingersFKIKSolver, bUseRuntimeFingersPose); } \
	FORCEINLINE static uint32 __PPO__TraceStartTime() { return STRUCT_OFFSET(UVRIKFingersFKIKSolver, TraceStartTime); } \
	FORCEINLINE static uint32 __PPO__VRStatus() { return STRUCT_OFFSET(UVRIKFingersFKIKSolver, VRStatus); } \
	FORCEINLINE static uint32 __PPO__CurrentFingersPose() { return STRUCT_OFFSET(UVRIKFingersFKIKSolver, CurrentFingersPose); } \
	FORCEINLINE static uint32 __PPO__HandSideMultiplier() { return STRUCT_OFFSET(UVRIKFingersFKIKSolver, HandSideMultiplier); }


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersFKIKSolver_h_44_PROLOG
#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersFKIKSolver_h_47_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersFKIKSolver_h_47_PRIVATE_PROPERTY_OFFSET \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersFKIKSolver_h_47_RPC_WRAPPERS \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersFKIKSolver_h_47_INCLASS \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersFKIKSolver_h_47_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersFKIKSolver_h_47_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersFKIKSolver_h_47_PRIVATE_PROPERTY_OFFSET \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersFKIKSolver_h_47_RPC_WRAPPERS_NO_PURE_DECLS \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersFKIKSolver_h_47_INCLASS_NO_PURE_DECLS \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersFKIKSolver_h_47_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VRIKBODYRUNTIME_API UClass* StaticClass<class UVRIKFingersFKIKSolver>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKFingersFKIKSolver_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
