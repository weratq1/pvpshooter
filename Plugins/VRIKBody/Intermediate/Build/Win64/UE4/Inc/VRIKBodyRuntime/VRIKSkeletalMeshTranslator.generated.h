// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector;
struct FTransform;
struct FBoneRotatorSetup;
struct FRotator;
class UWorld;
 struct FTransform;
struct FPoseSnapshot;
struct FSkeletalMeshSetup;
class USkeletalMeshComponent;
class UVRIKBody;
#ifdef VRIKBODYRUNTIME_VRIKSkeletalMeshTranslator_generated_h
#error "VRIKSkeletalMeshTranslator.generated.h already included, missing '#pragma once' in VRIKSkeletalMeshTranslator.h"
#endif
#define VRIKBODYRUNTIME_VRIKSkeletalMeshTranslator_generated_h

#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKSkeletalMeshTranslator_h_41_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSkeletalMeshSetup_Statics; \
	static class UScriptStruct* StaticStruct();


template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<struct FSkeletalMeshSetup>();

#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKSkeletalMeshTranslator_h_20_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FBoneRotatorSetup_Statics; \
	static class UScriptStruct* StaticStruct();


template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<struct FBoneRotatorSetup>();

#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKSkeletalMeshTranslator_h_171_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execCalculateTwoBoneIK) \
	{ \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_ChainStart); \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_ChainEnd); \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_JointTarget); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_UpperBoneSize); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_LowerBoneSize); \
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_UpperBone); \
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_LowerBone); \
		P_GET_STRUCT_REF(FBoneRotatorSetup,Z_Param_Out_LimbSpace); \
		P_GET_UBOOL(Z_Param_bInvertBending); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->CalculateTwoBoneIK(Z_Param_Out_ChainStart,Z_Param_Out_ChainEnd,Z_Param_Out_JointTarget,Z_Param_UpperBoneSize,Z_Param_LowerBoneSize,Z_Param_Out_UpperBone,Z_Param_Out_LowerBone,Z_Param_Out_LimbSpace,Z_Param_bInvertBending); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execVRIKBody_OnCalibrationComplete) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->VRIKBody_OnCalibrationComplete(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMakeRotByTwoAxes) \
	{ \
		P_GET_PROPERTY(UByteProperty,Z_Param_MainAxis); \
		P_GET_STRUCT(FVector,Z_Param_MainAxisDirection); \
		P_GET_PROPERTY(UByteProperty,Z_Param_SecondaryAxis); \
		P_GET_STRUCT(FVector,Z_Param_SecondaryAxisDirection); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FRotator*)Z_Param__Result=P_THIS->MakeRotByTwoAxes(EAxis::Type(Z_Param_MainAxis),Z_Param_MainAxisDirection,EAxis::Type(Z_Param_SecondaryAxis),Z_Param_SecondaryAxisDirection); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDrawAxes) \
	{ \
		P_GET_OBJECT(UWorld,Z_Param_World); \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_Location); \
		P_GET_STRUCT(FBoneRotatorSetup,Z_Param_RotSetup); \
		P_GET_STRUCT(FRotator,Z_Param_Rot); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DrawAxes(Z_Param_World,Z_Param_Out_Location,Z_Param_RotSetup,Z_Param_Rot); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCalculateSpine) \
	{ \
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_Start); \
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_End); \
		P_GET_TARRAY_REF(FName,Z_Param_Out_RelativeBoneNames); \
		P_GET_TMAP_REF(FName,FTransform,Z_Param_Out_RelativeTransforms); \
		P_GET_TARRAY_REF(FTransform,Z_Param_Out_ResultTransforms); \
		P_GET_UBOOL(Z_Param_bDrawDebugGeometry); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->CalculateSpine(Z_Param_Out_Start,Z_Param_Out_End,Z_Param_Out_RelativeBoneNames,Z_Param_Out_RelativeTransforms,Z_Param_Out_ResultTransforms,Z_Param_bDrawDebugGeometry); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUpdateSkeletonBonesSetup) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->UpdateSkeletonBonesSetup(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCalcTorsoIK) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->CalcTorsoIK(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDebugShowBonesOrientation) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DebugShowBonesOrientation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDebugGetYawRotation) \
	{ \
		P_GET_STRUCT(FVector,Z_Param_ForwardVector); \
		P_GET_STRUCT(FVector,Z_Param_UpVector); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->DebugGetYawRotation(Z_Param_ForwardVector,Z_Param_UpVector); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execForceSetComponentAsCalibrated) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ForceSetComponentAsCalibrated(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintBoneInfo) \
	{ \
		P_GET_STRUCT_REF(FBoneRotatorSetup,Z_Param_Out_BoneSetup); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->PrintBoneInfo(Z_Param_Out_BoneSetup); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetMeshWorldTransform) \
	{ \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_Location); \
		P_GET_STRUCT_REF(FRotator,Z_Param_Out_Rotation); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GetMeshWorldTransform(Z_Param_Out_Location,Z_Param_Out_Rotation); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetLastPose) \
	{ \
		P_GET_STRUCT_REF(FPoseSnapshot,Z_Param_Out_ReturnPoseValue); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->GetLastPose(Z_Param_Out_ReturnPoseValue); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execRefreshComponentPosition) \
	{ \
		P_GET_UBOOL(Z_Param_bUpdateCapturedBones); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->RefreshComponentPosition(Z_Param_bUpdateCapturedBones); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUpdateSkeleton) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UpdateSkeleton(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execRestoreSkeletalMeshSetup) \
	{ \
		P_GET_STRUCT_REF(FSkeletalMeshSetup,Z_Param_Out_SkeletalMeshSetup); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->RestoreSkeletalMeshSetup(Z_Param_Out_SkeletalMeshSetup); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetSkeletalMeshSetup) \
	{ \
		P_GET_STRUCT_REF(FSkeletalMeshSetup,Z_Param_Out_ReturnValue); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GetSkeletalMeshSetup(Z_Param_Out_ReturnValue); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsInitialized) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->IsInitialized(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execInitialize) \
	{ \
		P_GET_OBJECT(USkeletalMeshComponent,Z_Param_ControlledSkeletalMesh); \
		P_GET_OBJECT(UVRIKBody,Z_Param_VRIKBodyComponent); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->Initialize(Z_Param_ControlledSkeletalMesh,Z_Param_VRIKBodyComponent); \
		P_NATIVE_END; \
	}


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKSkeletalMeshTranslator_h_171_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execCalculateTwoBoneIK) \
	{ \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_ChainStart); \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_ChainEnd); \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_JointTarget); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_UpperBoneSize); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_LowerBoneSize); \
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_UpperBone); \
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_LowerBone); \
		P_GET_STRUCT_REF(FBoneRotatorSetup,Z_Param_Out_LimbSpace); \
		P_GET_UBOOL(Z_Param_bInvertBending); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->CalculateTwoBoneIK(Z_Param_Out_ChainStart,Z_Param_Out_ChainEnd,Z_Param_Out_JointTarget,Z_Param_UpperBoneSize,Z_Param_LowerBoneSize,Z_Param_Out_UpperBone,Z_Param_Out_LowerBone,Z_Param_Out_LimbSpace,Z_Param_bInvertBending); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execVRIKBody_OnCalibrationComplete) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->VRIKBody_OnCalibrationComplete(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMakeRotByTwoAxes) \
	{ \
		P_GET_PROPERTY(UByteProperty,Z_Param_MainAxis); \
		P_GET_STRUCT(FVector,Z_Param_MainAxisDirection); \
		P_GET_PROPERTY(UByteProperty,Z_Param_SecondaryAxis); \
		P_GET_STRUCT(FVector,Z_Param_SecondaryAxisDirection); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FRotator*)Z_Param__Result=P_THIS->MakeRotByTwoAxes(EAxis::Type(Z_Param_MainAxis),Z_Param_MainAxisDirection,EAxis::Type(Z_Param_SecondaryAxis),Z_Param_SecondaryAxisDirection); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDrawAxes) \
	{ \
		P_GET_OBJECT(UWorld,Z_Param_World); \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_Location); \
		P_GET_STRUCT(FBoneRotatorSetup,Z_Param_RotSetup); \
		P_GET_STRUCT(FRotator,Z_Param_Rot); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DrawAxes(Z_Param_World,Z_Param_Out_Location,Z_Param_RotSetup,Z_Param_Rot); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCalculateSpine) \
	{ \
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_Start); \
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_End); \
		P_GET_TARRAY_REF(FName,Z_Param_Out_RelativeBoneNames); \
		P_GET_TMAP_REF(FName,FTransform,Z_Param_Out_RelativeTransforms); \
		P_GET_TARRAY_REF(FTransform,Z_Param_Out_ResultTransforms); \
		P_GET_UBOOL(Z_Param_bDrawDebugGeometry); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->CalculateSpine(Z_Param_Out_Start,Z_Param_Out_End,Z_Param_Out_RelativeBoneNames,Z_Param_Out_RelativeTransforms,Z_Param_Out_ResultTransforms,Z_Param_bDrawDebugGeometry); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUpdateSkeletonBonesSetup) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->UpdateSkeletonBonesSetup(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCalcTorsoIK) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->CalcTorsoIK(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDebugShowBonesOrientation) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DebugShowBonesOrientation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDebugGetYawRotation) \
	{ \
		P_GET_STRUCT(FVector,Z_Param_ForwardVector); \
		P_GET_STRUCT(FVector,Z_Param_UpVector); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->DebugGetYawRotation(Z_Param_ForwardVector,Z_Param_UpVector); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execForceSetComponentAsCalibrated) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ForceSetComponentAsCalibrated(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintBoneInfo) \
	{ \
		P_GET_STRUCT_REF(FBoneRotatorSetup,Z_Param_Out_BoneSetup); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->PrintBoneInfo(Z_Param_Out_BoneSetup); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetMeshWorldTransform) \
	{ \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_Location); \
		P_GET_STRUCT_REF(FRotator,Z_Param_Out_Rotation); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GetMeshWorldTransform(Z_Param_Out_Location,Z_Param_Out_Rotation); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetLastPose) \
	{ \
		P_GET_STRUCT_REF(FPoseSnapshot,Z_Param_Out_ReturnPoseValue); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->GetLastPose(Z_Param_Out_ReturnPoseValue); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execRefreshComponentPosition) \
	{ \
		P_GET_UBOOL(Z_Param_bUpdateCapturedBones); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->RefreshComponentPosition(Z_Param_bUpdateCapturedBones); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUpdateSkeleton) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UpdateSkeleton(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execRestoreSkeletalMeshSetup) \
	{ \
		P_GET_STRUCT_REF(FSkeletalMeshSetup,Z_Param_Out_SkeletalMeshSetup); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->RestoreSkeletalMeshSetup(Z_Param_Out_SkeletalMeshSetup); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetSkeletalMeshSetup) \
	{ \
		P_GET_STRUCT_REF(FSkeletalMeshSetup,Z_Param_Out_ReturnValue); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GetSkeletalMeshSetup(Z_Param_Out_ReturnValue); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsInitialized) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->IsInitialized(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execInitialize) \
	{ \
		P_GET_OBJECT(USkeletalMeshComponent,Z_Param_ControlledSkeletalMesh); \
		P_GET_OBJECT(UVRIKBody,Z_Param_VRIKBodyComponent); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->Initialize(Z_Param_ControlledSkeletalMesh,Z_Param_VRIKBodyComponent); \
		P_NATIVE_END; \
	}


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKSkeletalMeshTranslator_h_171_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVRIKSkeletalMeshTranslator(); \
	friend struct Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics; \
public: \
	DECLARE_CLASS(UVRIKSkeletalMeshTranslator, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/VRIKBodyRuntime"), NO_API) \
	DECLARE_SERIALIZER(UVRIKSkeletalMeshTranslator)


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKSkeletalMeshTranslator_h_171_INCLASS \
private: \
	static void StaticRegisterNativesUVRIKSkeletalMeshTranslator(); \
	friend struct Z_Construct_UClass_UVRIKSkeletalMeshTranslator_Statics; \
public: \
	DECLARE_CLASS(UVRIKSkeletalMeshTranslator, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/VRIKBodyRuntime"), NO_API) \
	DECLARE_SERIALIZER(UVRIKSkeletalMeshTranslator)


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKSkeletalMeshTranslator_h_171_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVRIKSkeletalMeshTranslator(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVRIKSkeletalMeshTranslator) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVRIKSkeletalMeshTranslator); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVRIKSkeletalMeshTranslator); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVRIKSkeletalMeshTranslator(UVRIKSkeletalMeshTranslator&&); \
	NO_API UVRIKSkeletalMeshTranslator(const UVRIKSkeletalMeshTranslator&); \
public:


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKSkeletalMeshTranslator_h_171_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVRIKSkeletalMeshTranslator(UVRIKSkeletalMeshTranslator&&); \
	NO_API UVRIKSkeletalMeshTranslator(const UVRIKSkeletalMeshTranslator&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVRIKSkeletalMeshTranslator); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVRIKSkeletalMeshTranslator); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UVRIKSkeletalMeshTranslator)


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKSkeletalMeshTranslator_h_171_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bIsInitialized() { return STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, bIsInitialized); } \
	FORCEINLINE static uint32 __PPO__bIsCalibrated() { return STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, bIsCalibrated); } \
	FORCEINLINE static uint32 __PPO__bTrial() { return STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, bTrial); } \
	FORCEINLINE static uint32 __PPO__bHasRootBone() { return STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, bHasRootBone); } \
	FORCEINLINE static uint32 __PPO__TrialMonth() { return STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, TrialMonth); } \
	FORCEINLINE static uint32 __PPO__TrialYear() { return STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, TrialYear); } \
	FORCEINLINE static uint32 __PPO__VRIKSolver() { return STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, VRIKSolver); } \
	FORCEINLINE static uint32 __PPO__BodyMesh() { return STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, BodyMesh); } \
	FORCEINLINE static uint32 __PPO__ComponentWorldTransform() { return STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, ComponentWorldTransform); } \
	FORCEINLINE static uint32 __PPO__BodySetup() { return STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, BodySetup); } \
	FORCEINLINE static uint32 __PPO__CapturedBodyRaw() { return STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, CapturedBodyRaw); } \
	FORCEINLINE static uint32 __PPO__CapturedSkeletonR() { return STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, CapturedSkeletonR); } \
	FORCEINLINE static uint32 __PPO__CapturedSkeletonL() { return STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, CapturedSkeletonL); } \
	FORCEINLINE static uint32 __PPO__LastRotationOffset() { return STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, LastRotationOffset); } \
	FORCEINLINE static uint32 __PPO__CapturedBody() { return STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, CapturedBody); } \
	FORCEINLINE static uint32 __PPO__LowerarmTwistRight() { return STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, LowerarmTwistRight); } \
	FORCEINLINE static uint32 __PPO__LowerarmTwistLeft() { return STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, LowerarmTwistLeft); } \
	FORCEINLINE static uint32 __PPO__bComponentWasMovedInTick() { return STRUCT_OFFSET(UVRIKSkeletalMeshTranslator, bComponentWasMovedInTick); }


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKSkeletalMeshTranslator_h_168_PROLOG
#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKSkeletalMeshTranslator_h_171_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKSkeletalMeshTranslator_h_171_PRIVATE_PROPERTY_OFFSET \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKSkeletalMeshTranslator_h_171_RPC_WRAPPERS \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKSkeletalMeshTranslator_h_171_INCLASS \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKSkeletalMeshTranslator_h_171_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKSkeletalMeshTranslator_h_171_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKSkeletalMeshTranslator_h_171_PRIVATE_PROPERTY_OFFSET \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKSkeletalMeshTranslator_h_171_RPC_WRAPPERS_NO_PURE_DECLS \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKSkeletalMeshTranslator_h_171_INCLASS_NO_PURE_DECLS \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKSkeletalMeshTranslator_h_171_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VRIKBODYRUNTIME_API UClass* StaticClass<class UVRIKSkeletalMeshTranslator>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKSkeletalMeshTranslator_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
