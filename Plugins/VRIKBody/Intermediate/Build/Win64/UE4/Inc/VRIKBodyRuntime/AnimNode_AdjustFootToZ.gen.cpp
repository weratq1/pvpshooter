// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VRIKBodyRuntime/Public/AnimNode_AdjustFootToZ.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAnimNode_AdjustFootToZ() {}
// Cross Module References
	VRIKBODYRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ();
	UPackage* Z_Construct_UPackage__Script_VRIKBodyRuntime();
	ANIMGRAPHRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FAnimNode_SkeletalControlBase();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FBoneReference();
// End Cross Module References
class UScriptStruct* FAnimNode_AdjustFootToZ::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern VRIKBODYRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ, Z_Construct_UPackage__Script_VRIKBodyRuntime(), TEXT("AnimNode_AdjustFootToZ"), sizeof(FAnimNode_AdjustFootToZ), Get_Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Hash());
	}
	return Singleton;
}
template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<FAnimNode_AdjustFootToZ>()
{
	return FAnimNode_AdjustFootToZ::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FAnimNode_AdjustFootToZ(FAnimNode_AdjustFootToZ::StaticStruct, TEXT("/Script/VRIKBodyRuntime"), TEXT("AnimNode_AdjustFootToZ"), false, nullptr, nullptr);
static struct FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFAnimNode_AdjustFootToZ
{
	FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFAnimNode_AdjustFootToZ()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("AnimNode_AdjustFootToZ")),new UScriptStruct::TCppStructOps<FAnimNode_AdjustFootToZ>);
	}
} ScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFAnimNode_AdjustFootToZ;
	struct Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PelvisRotationAroundUpAxisInCS_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PelvisRotationAroundUpAxisInCS;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GroundLevelZ_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_GroundLevelZ;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IgnoreGroundBelowFoot_MetaData[];
#endif
		static void NewProp_IgnoreGroundBelowFoot_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IgnoreGroundBelowFoot;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IKBone_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_IKBone;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/AnimNode_AdjustFootToZ.h" },
		{ "ToolTip", "The Two Bone IK control applies an inverse kinematic (IK) solver to a leg 3-joint chain to adjust foot z to desired ground level" },
	};
#endif
	void* Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAnimNode_AdjustFootToZ>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics::NewProp_PelvisRotationAroundUpAxisInCS_MetaData[] = {
		{ "Category", "EndEffector" },
		{ "ModuleRelativePath", "Public/AnimNode_AdjustFootToZ.h" },
		{ "ToolTip", "Rotation of pelvis around up axis in component space, typically 0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics::NewProp_PelvisRotationAroundUpAxisInCS = { "PelvisRotationAroundUpAxisInCS", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAnimNode_AdjustFootToZ, PelvisRotationAroundUpAxisInCS), METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics::NewProp_PelvisRotationAroundUpAxisInCS_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics::NewProp_PelvisRotationAroundUpAxisInCS_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics::NewProp_GroundLevelZ_MetaData[] = {
		{ "Category", "EndEffector" },
		{ "ModuleRelativePath", "Public/AnimNode_AdjustFootToZ.h" },
		{ "PinShownByDefault", "" },
		{ "ToolTip", "Effector Location. Target Location to reach." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics::NewProp_GroundLevelZ = { "GroundLevelZ", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAnimNode_AdjustFootToZ, GroundLevelZ), METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics::NewProp_GroundLevelZ_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics::NewProp_GroundLevelZ_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics::NewProp_IgnoreGroundBelowFoot_MetaData[] = {
		{ "Category", "EndEffector" },
		{ "ModuleRelativePath", "Public/AnimNode_AdjustFootToZ.h" },
		{ "PinShownByDefault", "" },
		{ "ToolTip", "Set to true to disable IK adjustment for ground Z coordinate below current foot Z coordinate **" },
	};
#endif
	void Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics::NewProp_IgnoreGroundBelowFoot_SetBit(void* Obj)
	{
		((FAnimNode_AdjustFootToZ*)Obj)->IgnoreGroundBelowFoot = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics::NewProp_IgnoreGroundBelowFoot = { "IgnoreGroundBelowFoot", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FAnimNode_AdjustFootToZ), &Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics::NewProp_IgnoreGroundBelowFoot_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics::NewProp_IgnoreGroundBelowFoot_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics::NewProp_IgnoreGroundBelowFoot_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics::NewProp_IKBone_MetaData[] = {
		{ "Category", "IK" },
		{ "ModuleRelativePath", "Public/AnimNode_AdjustFootToZ.h" },
		{ "ToolTip", "Name of bone to control. This is the main bone chain to modify from. *" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics::NewProp_IKBone = { "IKBone", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAnimNode_AdjustFootToZ, IKBone), Z_Construct_UScriptStruct_FBoneReference, METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics::NewProp_IKBone_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics::NewProp_IKBone_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics::NewProp_PelvisRotationAroundUpAxisInCS,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics::NewProp_GroundLevelZ,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics::NewProp_IgnoreGroundBelowFoot,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics::NewProp_IKBone,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_VRIKBodyRuntime,
		Z_Construct_UScriptStruct_FAnimNode_SkeletalControlBase,
		&NewStructOps,
		"AnimNode_AdjustFootToZ",
		sizeof(FAnimNode_AdjustFootToZ),
		alignof(FAnimNode_AdjustFootToZ),
		Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_VRIKBodyRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("AnimNode_AdjustFootToZ"), sizeof(FAnimNode_AdjustFootToZ), Get_Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Hash() { return 507010478U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
