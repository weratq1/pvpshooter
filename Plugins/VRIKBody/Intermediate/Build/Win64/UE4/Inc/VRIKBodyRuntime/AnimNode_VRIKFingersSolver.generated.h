// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VRIKBODYRUNTIME_AnimNode_VRIKFingersSolver_generated_h
#error "AnimNode_VRIKFingersSolver.generated.h already included, missing '#pragma once' in AnimNode_VRIKFingersSolver.h"
#endif
#define VRIKBODYRUNTIME_AnimNode_VRIKFingersSolver_generated_h

#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_AnimNode_VRIKFingersSolver_h_23_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__bBoneIndicesCached() { return STRUCT_OFFSET(FAnimNode_VRIKFingersSolver, bBoneIndicesCached); } \
	typedef FAnimNode_Base Super;


template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<struct FAnimNode_VRIKFingersSolver>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_AnimNode_VRIKFingersSolver_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
