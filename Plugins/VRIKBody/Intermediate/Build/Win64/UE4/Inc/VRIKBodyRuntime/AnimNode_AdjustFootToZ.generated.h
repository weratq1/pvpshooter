// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VRIKBODYRUNTIME_AnimNode_AdjustFootToZ_generated_h
#error "AnimNode_AdjustFootToZ.generated.h already included, missing '#pragma once' in AnimNode_AdjustFootToZ.h"
#endif
#define VRIKBODYRUNTIME_AnimNode_AdjustFootToZ_generated_h

#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_AnimNode_AdjustFootToZ_h_13_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAnimNode_AdjustFootToZ_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FAnimNode_SkeletalControlBase Super;


template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<struct FAnimNode_AdjustFootToZ>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_AnimNode_AdjustFootToZ_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
