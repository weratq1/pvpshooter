// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FTransform;
struct FNetworkTransform;
struct FNetworkIKBodyData;
struct FVector;
enum class EControllerHand : uint8;
struct FCalibratedBody;
enum class EBodyOrientation : uint8;
class UPrimitiveComponent;
struct FIKBodyData;
class USceneComponent;
#ifdef VRIKBODYRUNTIME_VRIKBody_generated_h
#error "VRIKBody.generated.h already included, missing '#pragma once' in VRIKBody.h"
#endif
#define VRIKBODYRUNTIME_VRIKBody_generated_h

#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKBody_h_193_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FIKBodyData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<struct FIKBodyData>();

#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKBody_h_167_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVRInputData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<struct FVRInputData>();

#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKBody_h_99_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNetworkIKBodyData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<struct FNetworkIKBodyData>();

#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKBody_h_85_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNetworkTransform_Statics; \
	static class UScriptStruct* StaticStruct();


template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<struct FNetworkTransform>();

#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKBody_h_53_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCalibratedBody_Statics; \
	static class UScriptStruct* StaticStruct();


template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<struct FCalibratedBody>();

#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKBody_h_21_DELEGATE \
static inline void FIKBodySimple_DelegateWrapper(const FMulticastScriptDelegate& IKBodySimple) \
{ \
	IKBodySimple.ProcessMulticastDelegate<UObject>(NULL); \
}


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKBody_h_20_DELEGATE \
struct _Script_VRIKBodyRuntime_eventIKBodyRepeatable_Parms \
{ \
	int32 Iteration; \
}; \
static inline void FIKBodyRepeatable_DelegateWrapper(const FMulticastScriptDelegate& IKBodyRepeatable, int32 Iteration) \
{ \
	_Script_VRIKBodyRuntime_eventIKBodyRepeatable_Parms Parms; \
	Parms.Iteration=Iteration; \
	IKBodyRepeatable.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKBody_h_300_RPC_WRAPPERS \
	virtual bool ServerUpdateInputs_Validate(FNetworkTransform const& , FNetworkTransform const& , FNetworkTransform const& ); \
	virtual void ServerUpdateInputs_Implementation(FNetworkTransform const& Head, FNetworkTransform const& HandRight, FNetworkTransform const& HandLeft); \
	virtual bool ServerUpdateBodyState_Validate(FNetworkIKBodyData const& ); \
	virtual void ServerUpdateBodyState_Implementation(FNetworkIKBodyData const& State); \
	virtual void ClientResetCalibrationStatus_Implementation(); \
	virtual bool ServerResetCalibrationStatus_Validate(); \
	virtual void ServerResetCalibrationStatus_Implementation(); \
	virtual void ClientRestoreCalibratedBody_Implementation(FCalibratedBody const& BodyParams); \
	virtual bool ServerRestoreCalibratedBody_Validate(FCalibratedBody const& ); \
	virtual void ServerRestoreCalibratedBody_Implementation(FCalibratedBody const& BodyParams); \
 \
	DECLARE_FUNCTION(execPackDataForReplication) \
	{ \
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_Head); \
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_HandRight); \
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_HandLeft); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->PackDataForReplication(Z_Param_Out_Head,Z_Param_Out_HandRight,Z_Param_Out_HandLeft); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execServerUpdateInputs) \
	{ \
		P_GET_STRUCT(FNetworkTransform,Z_Param_Head); \
		P_GET_STRUCT(FNetworkTransform,Z_Param_HandRight); \
		P_GET_STRUCT(FNetworkTransform,Z_Param_HandLeft); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!P_THIS->ServerUpdateInputs_Validate(Z_Param_Head,Z_Param_HandRight,Z_Param_HandLeft)) \
		{ \
			RPC_ValidateFailed(TEXT("ServerUpdateInputs_Validate")); \
			return; \
		} \
		P_THIS->ServerUpdateInputs_Implementation(Z_Param_Head,Z_Param_HandRight,Z_Param_HandLeft); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execServerUpdateBodyState) \
	{ \
		P_GET_STRUCT(FNetworkIKBodyData,Z_Param_State); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!P_THIS->ServerUpdateBodyState_Validate(Z_Param_State)) \
		{ \
			RPC_ValidateFailed(TEXT("ServerUpdateBodyState_Validate")); \
			return; \
		} \
		P_THIS->ServerUpdateBodyState_Implementation(Z_Param_State); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execClampElbowRotation) \
	{ \
		P_GET_STRUCT(FVector,Z_Param_InternalSide); \
		P_GET_STRUCT(FVector,Z_Param_UpSide); \
		P_GET_STRUCT(FVector,Z_Param_HandUpVector); \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_CurrentAngle); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ClampElbowRotation(Z_Param_InternalSide,Z_Param_UpSide,Z_Param_HandUpVector,Z_Param_Out_CurrentAngle); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCalculateTwoBoneIK) \
	{ \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_ChainStart); \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_ChainEnd); \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_JointTarget); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_UpperBoneSize); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_LowerBoneSize); \
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_UpperBone); \
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_LowerBone); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_BendSide); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->CalculateTwoBoneIK(Z_Param_Out_ChainStart,Z_Param_Out_ChainEnd,Z_Param_Out_JointTarget,Z_Param_UpperBoneSize,Z_Param_LowerBoneSize,Z_Param_Out_UpperBone,Z_Param_Out_LowerBone,Z_Param_BendSide); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTraceFloor) \
	{ \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_HeadLocation); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->TraceFloor(Z_Param_Out_HeadLocation); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCalibrateSkeleton) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->CalibrateSkeleton(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCalcFeetIKTransforms2) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_DeltaTime); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_FloorZ); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->CalcFeetIKTransforms2(Z_Param_DeltaTime,Z_Param_FloorZ); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCalcFeetInstantaneousTransforms) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_DeltaTime); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_FloorZ); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->CalcFeetInstantaneousTransforms(Z_Param_DeltaTime,Z_Param_FloorZ); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCalcHandIKTransforms) \
	{ \
		P_GET_ENUM(EControllerHand,Z_Param_Hand); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->CalcHandIKTransforms(EControllerHand(Z_Param_Hand)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCalcShouldersWithOffset) \
	{ \
		P_GET_ENUM(EControllerHand,Z_Param_Hand); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->CalcShouldersWithOffset(EControllerHand(Z_Param_Hand)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCorrelationKoef) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_StartIndex); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GetCorrelationKoef(Z_Param_StartIndex); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execResetFootsTimerR_Tick) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ResetFootsTimerR_Tick(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execResetFootsTimerL_Tick) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ResetFootsTimerL_Tick(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execRibcageYawTimer_Tick) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->RibcageYawTimer_Tick(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execVRInputTimer_Tick) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->VRInputTimer_Tick(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnRep_InputHandLeft) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnRep_InputHandLeft(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnRep_InputHandRight) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnRep_InputHandRight(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnRep_InputHMD) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnRep_InputHMD(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnRep_InputBodyState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnRep_InputBodyState(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDoResetCalibrationStatus) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DoResetCalibrationStatus(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execClientResetCalibrationStatus) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ClientResetCalibrationStatus_Implementation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execServerResetCalibrationStatus) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!P_THIS->ServerResetCalibrationStatus_Validate()) \
		{ \
			RPC_ValidateFailed(TEXT("ServerResetCalibrationStatus_Validate")); \
			return; \
		} \
		P_THIS->ServerResetCalibrationStatus_Implementation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDoRestoreCalibratedBody) \
	{ \
		P_GET_STRUCT_REF(FCalibratedBody,Z_Param_Out_BodyParams); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DoRestoreCalibratedBody(Z_Param_Out_BodyParams); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execClientRestoreCalibratedBody) \
	{ \
		P_GET_STRUCT(FCalibratedBody,Z_Param_BodyParams); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ClientRestoreCalibratedBody_Implementation(Z_Param_BodyParams); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execServerRestoreCalibratedBody) \
	{ \
		P_GET_STRUCT(FCalibratedBody,Z_Param_BodyParams); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!P_THIS->ServerRestoreCalibratedBody_Validate(Z_Param_BodyParams)) \
		{ \
			RPC_ValidateFailed(TEXT("ServerRestoreCalibratedBody_Validate")); \
			return; \
		} \
		P_THIS->ServerRestoreCalibratedBody_Implementation(Z_Param_BodyParams); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUseAutoBodyOrientation) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UseAutoBodyOrientation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetManualBodyOrientation) \
	{ \
		P_GET_ENUM(EBodyOrientation,Z_Param_NewOrientation); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetManualBodyOrientation(EBodyOrientation(Z_Param_NewOrientation)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsInitialized) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->IsInitialized(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetKneeJointTarget) \
	{ \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_RightKneeTarget); \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_LeftKneeTarget); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GetKneeJointTarget(Z_Param_Out_RightKneeTarget,Z_Param_Out_LeftKneeTarget); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetElbowJointTarget) \
	{ \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_RightElbowTarget); \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_LeftElbowTarget); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GetElbowJointTarget(Z_Param_Out_RightElbowTarget,Z_Param_Out_LeftElbowTarget); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsBodyCalibrated) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->IsBodyCalibrated(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetClearArmSpan) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetClearArmSpan(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetClearCharacterHeight) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetClearCharacterHeight(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsValidCalibrationData) \
	{ \
		P_GET_STRUCT_REF(FCalibratedBody,Z_Param_Out_BodyParams); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->IsValidCalibrationData(Z_Param_Out_BodyParams); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execResetCalibrationStatus) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ResetCalibrationStatus(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execRestoreCalibratedBody) \
	{ \
		P_GET_STRUCT_REF(FCalibratedBody,Z_Param_Out_BodyParams); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->RestoreCalibratedBody(Z_Param_Out_BodyParams); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCalibratedBody) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FCalibratedBody*)Z_Param__Result=P_THIS->GetCalibratedBody(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUpdateInputTransforms) \
	{ \
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_HeadTransform); \
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_RightHandTransform); \
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_LeftHandTransform); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UpdateInputTransforms(Z_Param_Out_HeadTransform,Z_Param_Out_RightHandTransform,Z_Param_Out_LeftHandTransform); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDetachHandFromComponent) \
	{ \
		P_GET_ENUM(EControllerHand,Z_Param_Hand); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DetachHandFromComponent(EControllerHand(Z_Param_Hand)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAttachHandToComponent) \
	{ \
		P_GET_ENUM(EControllerHand,Z_Param_Hand); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_Component); \
		P_GET_PROPERTY_REF(UNameProperty,Z_Param_Out_SocketName); \
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_RelativeTransform); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->AttachHandToComponent(EControllerHand(Z_Param_Hand),Z_Param_Component,Z_Param_Out_SocketName,Z_Param_Out_RelativeTransform); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCharacterLegsLength) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetCharacterLegsLength(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCharacterHeight) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetCharacterHeight(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execResetTorso) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ResetTorso(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execConvertDataToSkeletonFriendly) \
	{ \
		P_GET_STRUCT_REF(FIKBodyData,Z_Param_Out_WorldSpaceIKBody); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FIKBodyData*)Z_Param__Result=P_THIS->ConvertDataToSkeletonFriendly(Z_Param_Out_WorldSpaceIKBody); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetLastFrameData) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FIKBodyData*)Z_Param__Result=P_THIS->GetLastFrameData(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execComputeFrame) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_DeltaTime); \
		P_GET_STRUCT_REF(FIKBodyData,Z_Param_Out_ReturnValue); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ComputeFrame(Z_Param_DeltaTime,Z_Param_Out_ReturnValue); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execInitialize) \
	{ \
		P_GET_OBJECT(USceneComponent,Z_Param_Camera); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_RightController); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_LeftController); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Initialize(Z_Param_Camera,Z_Param_RightController,Z_Param_LeftController); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDeactivateInput) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DeactivateInput(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execActivateInput) \
	{ \
		P_GET_OBJECT(USceneComponent,Z_Param_RootComp); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->ActivateInput(Z_Param_RootComp); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAutoCalibrateBodyAtPose) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->AutoCalibrateBodyAtPose(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCalibrateBodyAtIPose) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->CalibrateBodyAtIPose(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCalibrateBodyAtTPose) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->CalibrateBodyAtTPose(); \
		P_NATIVE_END; \
	}


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKBody_h_300_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execPackDataForReplication) \
	{ \
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_Head); \
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_HandRight); \
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_HandLeft); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->PackDataForReplication(Z_Param_Out_Head,Z_Param_Out_HandRight,Z_Param_Out_HandLeft); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execServerUpdateInputs) \
	{ \
		P_GET_STRUCT(FNetworkTransform,Z_Param_Head); \
		P_GET_STRUCT(FNetworkTransform,Z_Param_HandRight); \
		P_GET_STRUCT(FNetworkTransform,Z_Param_HandLeft); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!P_THIS->ServerUpdateInputs_Validate(Z_Param_Head,Z_Param_HandRight,Z_Param_HandLeft)) \
		{ \
			RPC_ValidateFailed(TEXT("ServerUpdateInputs_Validate")); \
			return; \
		} \
		P_THIS->ServerUpdateInputs_Implementation(Z_Param_Head,Z_Param_HandRight,Z_Param_HandLeft); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execServerUpdateBodyState) \
	{ \
		P_GET_STRUCT(FNetworkIKBodyData,Z_Param_State); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!P_THIS->ServerUpdateBodyState_Validate(Z_Param_State)) \
		{ \
			RPC_ValidateFailed(TEXT("ServerUpdateBodyState_Validate")); \
			return; \
		} \
		P_THIS->ServerUpdateBodyState_Implementation(Z_Param_State); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execClampElbowRotation) \
	{ \
		P_GET_STRUCT(FVector,Z_Param_InternalSide); \
		P_GET_STRUCT(FVector,Z_Param_UpSide); \
		P_GET_STRUCT(FVector,Z_Param_HandUpVector); \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_CurrentAngle); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ClampElbowRotation(Z_Param_InternalSide,Z_Param_UpSide,Z_Param_HandUpVector,Z_Param_Out_CurrentAngle); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCalculateTwoBoneIK) \
	{ \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_ChainStart); \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_ChainEnd); \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_JointTarget); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_UpperBoneSize); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_LowerBoneSize); \
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_UpperBone); \
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_LowerBone); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_BendSide); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->CalculateTwoBoneIK(Z_Param_Out_ChainStart,Z_Param_Out_ChainEnd,Z_Param_Out_JointTarget,Z_Param_UpperBoneSize,Z_Param_LowerBoneSize,Z_Param_Out_UpperBone,Z_Param_Out_LowerBone,Z_Param_BendSide); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTraceFloor) \
	{ \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_HeadLocation); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->TraceFloor(Z_Param_Out_HeadLocation); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCalibrateSkeleton) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->CalibrateSkeleton(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCalcFeetIKTransforms2) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_DeltaTime); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_FloorZ); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->CalcFeetIKTransforms2(Z_Param_DeltaTime,Z_Param_FloorZ); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCalcFeetInstantaneousTransforms) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_DeltaTime); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_FloorZ); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->CalcFeetInstantaneousTransforms(Z_Param_DeltaTime,Z_Param_FloorZ); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCalcHandIKTransforms) \
	{ \
		P_GET_ENUM(EControllerHand,Z_Param_Hand); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->CalcHandIKTransforms(EControllerHand(Z_Param_Hand)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCalcShouldersWithOffset) \
	{ \
		P_GET_ENUM(EControllerHand,Z_Param_Hand); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->CalcShouldersWithOffset(EControllerHand(Z_Param_Hand)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCorrelationKoef) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_StartIndex); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GetCorrelationKoef(Z_Param_StartIndex); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execResetFootsTimerR_Tick) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ResetFootsTimerR_Tick(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execResetFootsTimerL_Tick) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ResetFootsTimerL_Tick(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execRibcageYawTimer_Tick) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->RibcageYawTimer_Tick(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execVRInputTimer_Tick) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->VRInputTimer_Tick(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnRep_InputHandLeft) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnRep_InputHandLeft(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnRep_InputHandRight) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnRep_InputHandRight(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnRep_InputHMD) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnRep_InputHMD(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnRep_InputBodyState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnRep_InputBodyState(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDoResetCalibrationStatus) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DoResetCalibrationStatus(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execClientResetCalibrationStatus) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ClientResetCalibrationStatus_Implementation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execServerResetCalibrationStatus) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!P_THIS->ServerResetCalibrationStatus_Validate()) \
		{ \
			RPC_ValidateFailed(TEXT("ServerResetCalibrationStatus_Validate")); \
			return; \
		} \
		P_THIS->ServerResetCalibrationStatus_Implementation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDoRestoreCalibratedBody) \
	{ \
		P_GET_STRUCT_REF(FCalibratedBody,Z_Param_Out_BodyParams); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DoRestoreCalibratedBody(Z_Param_Out_BodyParams); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execClientRestoreCalibratedBody) \
	{ \
		P_GET_STRUCT(FCalibratedBody,Z_Param_BodyParams); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ClientRestoreCalibratedBody_Implementation(Z_Param_BodyParams); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execServerRestoreCalibratedBody) \
	{ \
		P_GET_STRUCT(FCalibratedBody,Z_Param_BodyParams); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!P_THIS->ServerRestoreCalibratedBody_Validate(Z_Param_BodyParams)) \
		{ \
			RPC_ValidateFailed(TEXT("ServerRestoreCalibratedBody_Validate")); \
			return; \
		} \
		P_THIS->ServerRestoreCalibratedBody_Implementation(Z_Param_BodyParams); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUseAutoBodyOrientation) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UseAutoBodyOrientation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetManualBodyOrientation) \
	{ \
		P_GET_ENUM(EBodyOrientation,Z_Param_NewOrientation); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetManualBodyOrientation(EBodyOrientation(Z_Param_NewOrientation)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsInitialized) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->IsInitialized(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetKneeJointTarget) \
	{ \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_RightKneeTarget); \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_LeftKneeTarget); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GetKneeJointTarget(Z_Param_Out_RightKneeTarget,Z_Param_Out_LeftKneeTarget); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetElbowJointTarget) \
	{ \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_RightElbowTarget); \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_LeftElbowTarget); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GetElbowJointTarget(Z_Param_Out_RightElbowTarget,Z_Param_Out_LeftElbowTarget); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsBodyCalibrated) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->IsBodyCalibrated(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetClearArmSpan) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetClearArmSpan(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetClearCharacterHeight) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetClearCharacterHeight(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsValidCalibrationData) \
	{ \
		P_GET_STRUCT_REF(FCalibratedBody,Z_Param_Out_BodyParams); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->IsValidCalibrationData(Z_Param_Out_BodyParams); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execResetCalibrationStatus) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ResetCalibrationStatus(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execRestoreCalibratedBody) \
	{ \
		P_GET_STRUCT_REF(FCalibratedBody,Z_Param_Out_BodyParams); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->RestoreCalibratedBody(Z_Param_Out_BodyParams); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCalibratedBody) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FCalibratedBody*)Z_Param__Result=P_THIS->GetCalibratedBody(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUpdateInputTransforms) \
	{ \
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_HeadTransform); \
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_RightHandTransform); \
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_LeftHandTransform); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UpdateInputTransforms(Z_Param_Out_HeadTransform,Z_Param_Out_RightHandTransform,Z_Param_Out_LeftHandTransform); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDetachHandFromComponent) \
	{ \
		P_GET_ENUM(EControllerHand,Z_Param_Hand); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DetachHandFromComponent(EControllerHand(Z_Param_Hand)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAttachHandToComponent) \
	{ \
		P_GET_ENUM(EControllerHand,Z_Param_Hand); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_Component); \
		P_GET_PROPERTY_REF(UNameProperty,Z_Param_Out_SocketName); \
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_RelativeTransform); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->AttachHandToComponent(EControllerHand(Z_Param_Hand),Z_Param_Component,Z_Param_Out_SocketName,Z_Param_Out_RelativeTransform); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCharacterLegsLength) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetCharacterLegsLength(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCharacterHeight) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetCharacterHeight(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execResetTorso) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ResetTorso(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execConvertDataToSkeletonFriendly) \
	{ \
		P_GET_STRUCT_REF(FIKBodyData,Z_Param_Out_WorldSpaceIKBody); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FIKBodyData*)Z_Param__Result=P_THIS->ConvertDataToSkeletonFriendly(Z_Param_Out_WorldSpaceIKBody); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetLastFrameData) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FIKBodyData*)Z_Param__Result=P_THIS->GetLastFrameData(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execComputeFrame) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_DeltaTime); \
		P_GET_STRUCT_REF(FIKBodyData,Z_Param_Out_ReturnValue); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ComputeFrame(Z_Param_DeltaTime,Z_Param_Out_ReturnValue); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execInitialize) \
	{ \
		P_GET_OBJECT(USceneComponent,Z_Param_Camera); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_RightController); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_LeftController); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Initialize(Z_Param_Camera,Z_Param_RightController,Z_Param_LeftController); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDeactivateInput) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DeactivateInput(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execActivateInput) \
	{ \
		P_GET_OBJECT(USceneComponent,Z_Param_RootComp); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->ActivateInput(Z_Param_RootComp); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAutoCalibrateBodyAtPose) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->AutoCalibrateBodyAtPose(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCalibrateBodyAtIPose) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->CalibrateBodyAtIPose(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCalibrateBodyAtTPose) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->CalibrateBodyAtTPose(); \
		P_NATIVE_END; \
	}


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKBody_h_300_EVENT_PARMS \
	struct VRIKBody_eventClientRestoreCalibratedBody_Parms \
	{ \
		FCalibratedBody BodyParams; \
	}; \
	struct VRIKBody_eventServerRestoreCalibratedBody_Parms \
	{ \
		FCalibratedBody BodyParams; \
	}; \
	struct VRIKBody_eventServerUpdateBodyState_Parms \
	{ \
		FNetworkIKBodyData State; \
	}; \
	struct VRIKBody_eventServerUpdateInputs_Parms \
	{ \
		FNetworkTransform Head; \
		FNetworkTransform HandRight; \
		FNetworkTransform HandLeft; \
	};


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKBody_h_300_CALLBACK_WRAPPERS
#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKBody_h_300_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVRIKBody(); \
	friend struct Z_Construct_UClass_UVRIKBody_Statics; \
public: \
	DECLARE_CLASS(UVRIKBody, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/VRIKBodyRuntime"), NO_API) \
	DECLARE_SERIALIZER(UVRIKBody)


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKBody_h_300_INCLASS \
private: \
	static void StaticRegisterNativesUVRIKBody(); \
	friend struct Z_Construct_UClass_UVRIKBody_Statics; \
public: \
	DECLARE_CLASS(UVRIKBody, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/VRIKBodyRuntime"), NO_API) \
	DECLARE_SERIALIZER(UVRIKBody)


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKBody_h_300_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVRIKBody(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVRIKBody) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVRIKBody); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVRIKBody); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVRIKBody(UVRIKBody&&); \
	NO_API UVRIKBody(const UVRIKBody&); \
public:


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKBody_h_300_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVRIKBody(UVRIKBody&&); \
	NO_API UVRIKBody(const UVRIKBody&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVRIKBody); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVRIKBody); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UVRIKBody)


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKBody_h_300_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraComponent() { return STRUCT_OFFSET(UVRIKBody, CameraComponent); } \
	FORCEINLINE static uint32 __PPO__RightHandController() { return STRUCT_OFFSET(UVRIKBody, RightHandController); } \
	FORCEINLINE static uint32 __PPO__LeftHandController() { return STRUCT_OFFSET(UVRIKBody, LeftHandController); } \
	FORCEINLINE static uint32 __PPO__OwningPawn() { return STRUCT_OFFSET(UVRIKBody, OwningPawn); } \
	FORCEINLINE static uint32 __PPO__FloorBaseComp() { return STRUCT_OFFSET(UVRIKBody, FloorBaseComp); } \
	FORCEINLINE static uint32 __PPO__bIsInitialized() { return STRUCT_OFFSET(UVRIKBody, bIsInitialized); } \
	FORCEINLINE static uint32 __PPO__bDebugOutput() { return STRUCT_OFFSET(UVRIKBody, bDebugOutput); } \
	FORCEINLINE static uint32 __PPO__bManualBodyOrientation() { return STRUCT_OFFSET(UVRIKBody, bManualBodyOrientation); } \
	FORCEINLINE static uint32 __PPO__NT_SkeletonTransformData() { return STRUCT_OFFSET(UVRIKBody, NT_SkeletonTransformData); } \
	FORCEINLINE static uint32 __PPO__NT_InputHMD() { return STRUCT_OFFSET(UVRIKBody, NT_InputHMD); } \
	FORCEINLINE static uint32 __PPO__NT_InputHandRight() { return STRUCT_OFFSET(UVRIKBody, NT_InputHandRight); } \
	FORCEINLINE static uint32 __PPO__NT_InputHandLeft() { return STRUCT_OFFSET(UVRIKBody, NT_InputHandLeft); } \
	FORCEINLINE static uint32 __PPO__SkeletonTransformData() { return STRUCT_OFFSET(UVRIKBody, SkeletonTransformData); } \
	FORCEINLINE static uint32 __PPO__SkeletonTransformDataRelative() { return STRUCT_OFFSET(UVRIKBody, SkeletonTransformDataRelative); } \
	FORCEINLINE static uint32 __PPO__InputHMD() { return STRUCT_OFFSET(UVRIKBody, InputHMD); } \
	FORCEINLINE static uint32 __PPO__InputHandRight() { return STRUCT_OFFSET(UVRIKBody, InputHandRight); } \
	FORCEINLINE static uint32 __PPO__InputHandLeft() { return STRUCT_OFFSET(UVRIKBody, InputHandLeft); } \
	FORCEINLINE static uint32 __PPO__SkeletonTransformData_Target() { return STRUCT_OFFSET(UVRIKBody, SkeletonTransformData_Target); } \
	FORCEINLINE static uint32 __PPO__InputHMD_Target() { return STRUCT_OFFSET(UVRIKBody, InputHMD_Target); } \
	FORCEINLINE static uint32 __PPO__InputHandRight_Target() { return STRUCT_OFFSET(UVRIKBody, InputHandRight_Target); } \
	FORCEINLINE static uint32 __PPO__InputHandLeft_Target() { return STRUCT_OFFSET(UVRIKBody, InputHandLeft_Target); } \
	FORCEINLINE static uint32 __PPO__vPrevCamLocation() { return STRUCT_OFFSET(UVRIKBody, vPrevCamLocation); } \
	FORCEINLINE static uint32 __PPO__rPrevCamRotator() { return STRUCT_OFFSET(UVRIKBody, rPrevCamRotator); } \
	FORCEINLINE static uint32 __PPO__HandAttachedRight() { return STRUCT_OFFSET(UVRIKBody, HandAttachedRight); } \
	FORCEINLINE static uint32 __PPO__HandAttachedLeft() { return STRUCT_OFFSET(UVRIKBody, HandAttachedLeft); } \
	FORCEINLINE static uint32 __PPO__HandParentRight() { return STRUCT_OFFSET(UVRIKBody, HandParentRight); } \
	FORCEINLINE static uint32 __PPO__HandParentLeft() { return STRUCT_OFFSET(UVRIKBody, HandParentLeft); } \
	FORCEINLINE static uint32 __PPO__HandAttachTransformRight() { return STRUCT_OFFSET(UVRIKBody, HandAttachTransformRight); } \
	FORCEINLINE static uint32 __PPO__HandAttachTransformLeft() { return STRUCT_OFFSET(UVRIKBody, HandAttachTransformLeft); } \
	FORCEINLINE static uint32 __PPO__HandAttachSocketRight() { return STRUCT_OFFSET(UVRIKBody, HandAttachSocketRight); } \
	FORCEINLINE static uint32 __PPO__HandAttachSocketLeft() { return STRUCT_OFFSET(UVRIKBody, HandAttachSocketLeft); } \
	FORCEINLINE static uint32 __PPO__hVRInputTimer() { return STRUCT_OFFSET(UVRIKBody, hVRInputTimer); } \
	FORCEINLINE static uint32 __PPO__hResetFootLTimer() { return STRUCT_OFFSET(UVRIKBody, hResetFootLTimer); } \
	FORCEINLINE static uint32 __PPO__hResetFootRTimer() { return STRUCT_OFFSET(UVRIKBody, hResetFootRTimer); } \
	FORCEINLINE static uint32 __PPO__hTorsoYawTimer() { return STRUCT_OFFSET(UVRIKBody, hTorsoYawTimer); } \
	FORCEINLINE static uint32 __PPO__FootTargetTransformL() { return STRUCT_OFFSET(UVRIKBody, FootTargetTransformL); } \
	FORCEINLINE static uint32 __PPO__FootTargetTransformR() { return STRUCT_OFFSET(UVRIKBody, FootTargetTransformR); } \
	FORCEINLINE static uint32 __PPO__FootTickTransformL() { return STRUCT_OFFSET(UVRIKBody, FootTickTransformL); } \
	FORCEINLINE static uint32 __PPO__FootTickTransformR() { return STRUCT_OFFSET(UVRIKBody, FootTickTransformR); } \
	FORCEINLINE static uint32 __PPO__FootLastTransformL() { return STRUCT_OFFSET(UVRIKBody, FootLastTransformL); } \
	FORCEINLINE static uint32 __PPO__FootLastTransformR() { return STRUCT_OFFSET(UVRIKBody, FootLastTransformR); } \
	FORCEINLINE static uint32 __PPO__FeetCyclePhase() { return STRUCT_OFFSET(UVRIKBody, FeetCyclePhase); } \
	FORCEINLINE static uint32 __PPO__FeetMovingStartedTime() { return STRUCT_OFFSET(UVRIKBody, FeetMovingStartedTime); } \
	FORCEINLINE static uint32 __PPO__fDeltaTimeL() { return STRUCT_OFFSET(UVRIKBody, fDeltaTimeL); } \
	FORCEINLINE static uint32 __PPO__fDeltaTimeR() { return STRUCT_OFFSET(UVRIKBody, fDeltaTimeR); } \
	FORCEINLINE static uint32 __PPO__bResetFeet() { return STRUCT_OFFSET(UVRIKBody, bResetFeet); } \
	FORCEINLINE static uint32 __PPO__nYawControlCounter() { return STRUCT_OFFSET(UVRIKBody, nYawControlCounter); } \
	FORCEINLINE static uint32 __PPO__bYawInterpToHead() { return STRUCT_OFFSET(UVRIKBody, bYawInterpToHead); } \
	FORCEINLINE static uint32 __PPO__bResetTorso() { return STRUCT_OFFSET(UVRIKBody, bResetTorso); } \
	FORCEINLINE static uint32 __PPO__SavedTorsoDirection() { return STRUCT_OFFSET(UVRIKBody, SavedTorsoDirection); } \
	FORCEINLINE static uint32 __PPO__ResetFootLocationL() { return STRUCT_OFFSET(UVRIKBody, ResetFootLocationL); } \
	FORCEINLINE static uint32 __PPO__ResetFootLocationR() { return STRUCT_OFFSET(UVRIKBody, ResetFootLocationR); } \
	FORCEINLINE static uint32 __PPO__nModifyHeightState() { return STRUCT_OFFSET(UVRIKBody, nModifyHeightState); } \
	FORCEINLINE static uint32 __PPO__ModifyHeightStartTime() { return STRUCT_OFFSET(UVRIKBody, ModifyHeightStartTime); } \
	FORCEINLINE static uint32 __PPO__bTorsoYawRotation() { return STRUCT_OFFSET(UVRIKBody, bTorsoYawRotation); } \
	FORCEINLINE static uint32 __PPO__bTorsoPitchRotation() { return STRUCT_OFFSET(UVRIKBody, bTorsoPitchRotation); } \
	FORCEINLINE static uint32 __PPO__bTorsoRollRotation() { return STRUCT_OFFSET(UVRIKBody, bTorsoRollRotation); } \
	FORCEINLINE static uint32 __PPO__bIsSitting() { return STRUCT_OFFSET(UVRIKBody, bIsSitting); } \
	FORCEINLINE static uint32 __PPO__bIsStanding() { return STRUCT_OFFSET(UVRIKBody, bIsStanding); } \
	FORCEINLINE static uint32 __PPO__bIsWalking() { return STRUCT_OFFSET(UVRIKBody, bIsWalking); } \
	FORCEINLINE static uint32 __PPO__JumpingOffset() { return STRUCT_OFFSET(UVRIKBody, JumpingOffset); } \
	FORCEINLINE static uint32 __PPO__CharacterHeight() { return STRUCT_OFFSET(UVRIKBody, CharacterHeight); } \
	FORCEINLINE static uint32 __PPO__CharacterHeightClear() { return STRUCT_OFFSET(UVRIKBody, CharacterHeightClear); } \
	FORCEINLINE static uint32 __PPO__ArmSpanClear() { return STRUCT_OFFSET(UVRIKBody, ArmSpanClear); } \
	FORCEINLINE static uint32 __PPO__LegsLength() { return STRUCT_OFFSET(UVRIKBody, LegsLength); } \
	FORCEINLINE static uint32 __PPO__nIsRotating() { return STRUCT_OFFSET(UVRIKBody, nIsRotating); } \
	FORCEINLINE static uint32 __PPO__StartRotationYaw() { return STRUCT_OFFSET(UVRIKBody, StartRotationYaw); } \
	FORCEINLINE static uint32 __PPO__UpperarmLength() { return STRUCT_OFFSET(UVRIKBody, UpperarmLength); } \
	FORCEINLINE static uint32 __PPO__ThighLength() { return STRUCT_OFFSET(UVRIKBody, ThighLength); } \
	FORCEINLINE static uint32 __PPO__CalT_Head() { return STRUCT_OFFSET(UVRIKBody, CalT_Head); } \
	FORCEINLINE static uint32 __PPO__CalT_ControllerL() { return STRUCT_OFFSET(UVRIKBody, CalT_ControllerL); } \
	FORCEINLINE static uint32 __PPO__CalT_ControllerR() { return STRUCT_OFFSET(UVRIKBody, CalT_ControllerR); } \
	FORCEINLINE static uint32 __PPO__CalI_Head() { return STRUCT_OFFSET(UVRIKBody, CalI_Head); } \
	FORCEINLINE static uint32 __PPO__CalI_ControllerL() { return STRUCT_OFFSET(UVRIKBody, CalI_ControllerL); } \
	FORCEINLINE static uint32 __PPO__CalI_ControllerR() { return STRUCT_OFFSET(UVRIKBody, CalI_ControllerR); } \
	FORCEINLINE static uint32 __PPO__bCalibratedT() { return STRUCT_OFFSET(UVRIKBody, bCalibratedT); } \
	FORCEINLINE static uint32 __PPO__bCalibratedI() { return STRUCT_OFFSET(UVRIKBody, bCalibratedI); } \
	FORCEINLINE static uint32 __PPO__TracedFloorLevel() { return STRUCT_OFFSET(UVRIKBody, TracedFloorLevel); } \
	FORCEINLINE static uint32 __PPO__TracedFloorLevelR() { return STRUCT_OFFSET(UVRIKBody, TracedFloorLevelR); } \
	FORCEINLINE static uint32 __PPO__TracedFloorLevelL() { return STRUCT_OFFSET(UVRIKBody, TracedFloorLevelL); } \
	FORCEINLINE static uint32 __PPO__HeadPitchBeforeSitting() { return STRUCT_OFFSET(UVRIKBody, HeadPitchBeforeSitting); } \
	FORCEINLINE static uint32 __PPO__HeadLocationBeforeSitting() { return STRUCT_OFFSET(UVRIKBody, HeadLocationBeforeSitting); } \
	FORCEINLINE static uint32 __PPO__RecalcCharacteHeightTime() { return STRUCT_OFFSET(UVRIKBody, RecalcCharacteHeightTime); } \
	FORCEINLINE static uint32 __PPO__RecalcCharacteHeightTimeJumping() { return STRUCT_OFFSET(UVRIKBody, RecalcCharacteHeightTimeJumping); } \
	FORCEINLINE static uint32 __PPO__ShoulderYawOffsetRight() { return STRUCT_OFFSET(UVRIKBody, ShoulderYawOffsetRight); } \
	FORCEINLINE static uint32 __PPO__ShoulderYawOffsetLeft() { return STRUCT_OFFSET(UVRIKBody, ShoulderYawOffsetLeft); } \
	FORCEINLINE static uint32 __PPO__ShoulderPitchOffsetRight() { return STRUCT_OFFSET(UVRIKBody, ShoulderPitchOffsetRight); } \
	FORCEINLINE static uint32 __PPO__ShoulderPitchOffsetLeft() { return STRUCT_OFFSET(UVRIKBody, ShoulderPitchOffsetLeft); } \
	FORCEINLINE static uint32 __PPO__PrevFrameActorRot() { return STRUCT_OFFSET(UVRIKBody, PrevFrameActorRot); } \
	FORCEINLINE static uint32 __PPO__CurrentVRInputIndex() { return STRUCT_OFFSET(UVRIKBody, CurrentVRInputIndex); } \
	FORCEINLINE static uint32 __PPO__PreviousElbowRollRight() { return STRUCT_OFFSET(UVRIKBody, PreviousElbowRollRight); } \
	FORCEINLINE static uint32 __PPO__PreviousElbowRollLeft() { return STRUCT_OFFSET(UVRIKBody, PreviousElbowRollLeft); }


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKBody_h_297_PROLOG \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKBody_h_300_EVENT_PARMS


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKBody_h_300_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKBody_h_300_PRIVATE_PROPERTY_OFFSET \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKBody_h_300_RPC_WRAPPERS \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKBody_h_300_CALLBACK_WRAPPERS \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKBody_h_300_INCLASS \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKBody_h_300_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKBody_h_300_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKBody_h_300_PRIVATE_PROPERTY_OFFSET \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKBody_h_300_RPC_WRAPPERS_NO_PURE_DECLS \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKBody_h_300_CALLBACK_WRAPPERS \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKBody_h_300_INCLASS_NO_PURE_DECLS \
	Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKBody_h_300_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VRIKBODYRUNTIME_API UClass* StaticClass<class UVRIKBody>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Git_VRShooter_Plugins_VRIKBody_Source_VRIKBodyRuntime_Public_VRIKBody_h


#define FOREACH_ENUM_EBODYORIENTATION(op) \
	op(EBodyOrientation::Stand) \
	op(EBodyOrientation::Sit) \
	op(EBodyOrientation::Crawl) \
	op(EBodyOrientation::LieDown_FaceUp) \
	op(EBodyOrientation::LieDown_FaceDown) 

enum class EBodyOrientation : uint8;
template<> VRIKBODYRUNTIME_API UEnum* StaticEnum<EBodyOrientation>();

#define FOREACH_ENUM_EVRINPUTSETUP(op) \
	op(EVRInputSetup::DirectVRInput) \
	op(EVRInputSetup::InputFromComponents) \
	op(EVRInputSetup::InputFromVariables) \
	op(EVRInputSetup::EmulateInput) 

enum class EVRInputSetup : uint8;
template<> VRIKBODYRUNTIME_API UEnum* StaticEnum<EVRInputSetup>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
