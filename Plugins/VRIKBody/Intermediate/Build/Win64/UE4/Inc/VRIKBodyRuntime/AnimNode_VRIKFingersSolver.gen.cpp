// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VRIKBodyRuntime/Public/AnimNode_VRIKFingersSolver.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAnimNode_VRIKFingersSolver() {}
// Cross Module References
	VRIKBODYRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver();
	UPackage* Z_Construct_UPackage__Script_VRIKBodyRuntime();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FAnimNode_Base();
	VRIKBODYRUNTIME_API UClass* Z_Construct_UClass_UVRIKFingersFKIKSolver_NoRegister();
	VRIKBODYRUNTIME_API UEnum* Z_Construct_UEnum_VRIKBodyRuntime_EVRIKFingerName();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FBoneReference();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FPoseLink();
// End Cross Module References
class UScriptStruct* FAnimNode_VRIKFingersSolver::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern VRIKBODYRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver, Z_Construct_UPackage__Script_VRIKBodyRuntime(), TEXT("AnimNode_VRIKFingersSolver"), sizeof(FAnimNode_VRIKFingersSolver), Get_Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Hash());
	}
	return Singleton;
}
template<> VRIKBODYRUNTIME_API UScriptStruct* StaticStruct<FAnimNode_VRIKFingersSolver>()
{
	return FAnimNode_VRIKFingersSolver::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FAnimNode_VRIKFingersSolver(FAnimNode_VRIKFingersSolver::StaticStruct, TEXT("/Script/VRIKBodyRuntime"), TEXT("AnimNode_VRIKFingersSolver"), false, nullptr, nullptr);
static struct FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFAnimNode_VRIKFingersSolver
{
	FScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFAnimNode_VRIKFingersSolver()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("AnimNode_VRIKFingersSolver")),new UScriptStruct::TCppStructOps<FAnimNode_VRIKFingersSolver>);
	}
} ScriptStruct_VRIKBodyRuntime_StaticRegisterNativesFAnimNode_VRIKFingersSolver;
	struct Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bBoneIndicesCached_MetaData[];
#endif
		static void NewProp_bBoneIndicesCached_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bBoneIndicesCached;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Alpha_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Alpha;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Solver_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Solver;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FingerTipBones_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_FingerTipBones;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_FingerTipBones_Key_KeyProp;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_FingerTipBones_Key_KeyProp_Underlying;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FingerTipBones_ValueProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputPose_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InputPose;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintInternalUseOnly", "true" },
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/AnimNode_VRIKFingersSolver.h" },
		{ "ToolTip", "Animation node: apply fingers relative transforms to animation data" },
	};
#endif
	void* Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAnimNode_VRIKFingersSolver>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_bBoneIndicesCached_MetaData[] = {
		{ "ModuleRelativePath", "Public/AnimNode_VRIKFingersSolver.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_bBoneIndicesCached_SetBit(void* Obj)
	{
		((FAnimNode_VRIKFingersSolver*)Obj)->bBoneIndicesCached = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_bBoneIndicesCached = { "bBoneIndicesCached", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FAnimNode_VRIKFingersSolver), &Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_bBoneIndicesCached_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_bBoneIndicesCached_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_bBoneIndicesCached_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_Alpha_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/AnimNode_VRIKFingersSolver.h" },
		{ "PinShownByDefault", "" },
		{ "ToolTip", "Alpha value *" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_Alpha = { "Alpha", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAnimNode_VRIKFingersSolver, Alpha), METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_Alpha_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_Alpha_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_Solver_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/AnimNode_VRIKFingersSolver.h" },
		{ "PinShownByDefault", "" },
		{ "ToolTip", "Reference to Finger FK-IK Solver object *" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_Solver = { "Solver", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAnimNode_VRIKFingersSolver, Solver), Z_Construct_UClass_UVRIKFingersFKIKSolver_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_Solver_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_Solver_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_FingerTipBones_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/AnimNode_VRIKFingersSolver.h" },
		{ "ToolTip", "Names of finger tip bones *" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_FingerTipBones = { "FingerTipBones", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAnimNode_VRIKFingersSolver, FingerTipBones), METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_FingerTipBones_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_FingerTipBones_MetaData)) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_FingerTipBones_Key_KeyProp = { "FingerTipBones_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UEnum_VRIKBodyRuntime_EVRIKFingerName, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_FingerTipBones_Key_KeyProp_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_FingerTipBones_ValueProp = { "FingerTipBones", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FBoneReference, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_InputPose_MetaData[] = {
		{ "Category", "Links" },
		{ "ModuleRelativePath", "Public/AnimNode_VRIKFingersSolver.h" },
		{ "ToolTip", "Input animation. Can contain different initial poses, but they all generally should be open (not in fist) *" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_InputPose = { "InputPose", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAnimNode_VRIKFingersSolver, InputPose), Z_Construct_UScriptStruct_FPoseLink, METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_InputPose_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_InputPose_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_bBoneIndicesCached,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_Alpha,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_Solver,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_FingerTipBones,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_FingerTipBones_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_FingerTipBones_Key_KeyProp_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_FingerTipBones_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::NewProp_InputPose,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_VRIKBodyRuntime,
		Z_Construct_UScriptStruct_FAnimNode_Base,
		&NewStructOps,
		"AnimNode_VRIKFingersSolver",
		sizeof(FAnimNode_VRIKFingersSolver),
		alignof(FAnimNode_VRIKFingersSolver),
		Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_VRIKBodyRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("AnimNode_VRIKFingersSolver"), sizeof(FAnimNode_VRIKFingersSolver), Get_Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FAnimNode_VRIKFingersSolver_Hash() { return 4148609900U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
